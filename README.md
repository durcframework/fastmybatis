# fastmybatis

![maven](https://img.shields.io/maven-central/v/io.gitee.durcframework/fastmybatis-core)

fastmybatis是一个mybatis开发框架，其宗旨为：简单、快速、有效。

- 零配置快速上手，无需依赖Spring
- 无需编写xml文件即可完成增删改查操作，支持LambdaQuery查询、支持JPA Query Method查询（findByXxx）
- 支持mysql、sqlserver、oracle、postgresql、sqlite、StarRocks（原DorisDB）
- 支持自定义sql，对于基本的增删改查不需要写SQL，对于其它特殊SQL（如统计SQL）可写在xml中
- 支持与spring-boot集成，依赖starter即可，支持Springboot3.0
- 支持插件编写
- 支持ActiveRecord模式
- 支持多租户
- 提供通用Service
- API丰富，多达40+方法，满足日常开发需求
- 轻量级，无侵入性，是官方mybatis的一种扩展


# Query查询

使用Query对象即可实现`查询/修改/删除`操作

```java
// 根据条件查询
// SELECT ... FROM t_user WHERE username = ? AND age > 30
List<TUser> list = TUser.query()
        .eq(TUser::getUsername, "张三")
        .gt(TUser:: getAge, 30)
        .list();

// 分页查询
// SELECT ... FROM t_user WHERE username = ? ORDER BY id DESC LIMIT 0, 10
PageInfo<TUser> pageInfo = this.query()
        .eq(TUser::getUsername, "张三")
        .orderByDesc(TUser::getId)
        .paginate(1, 10);
long total = pageInfo.getTotal();
List<TUser> records = pageInfo.getList();

// 条件删除, 逻辑删除
// UPDATE t_user SET deleted = 1 WHERE username = ?
int cnt = this.query()
        .eq(TUser::getUsername, "张三")
        .delete();

// 条件删除，强行删除，无视逻辑删除字段
// DELETE FROM t_user WHERE username = ?
int cnt2 = this.query()
        .eq(TUser::getUsername, "张三")
        .deleteForce();

// 更新操作
// UPDATE t_user SET username = ?, add_time=now() WHERE id = ?
int update = TUser.query()
        .set(TUser::getUsername, "李四")
        .setExpression("add_time=now()")
        .eq(TUser::getId, 6)
        .update();

```

# 快速开始（springboot）

- 新建一个springboot项目
- pom.xml添加fastmybatis-spring-boot-starter

```xml
<dependency>
    <groupId>io.gitee.durcframework</groupId>
    <artifactId>fastmybatis-spring-boot-starter</artifactId>
    <version>最新版本</version>
</dependency>
```

SpringBoot3 + Java17 依赖下面这个:

```xml
<dependency>
    <groupId>io.gitee.durcframework</groupId>
    <artifactId>fastmybatis-spring-boot3-starter</artifactId>
    <version>最新版本</version>
</dependency>
```

最新版本：![maven](https://img.shields.io/maven-central/v/io.gitee.durcframework/fastmybatis-spring-boot-starter)


- 增删改查例子

假设数据库有张`t_user`表，添加对应的实体类`TUser.java`和Mapper类`TUserMapper.java`

```java
/**
 * 增删改查例子
 */
@RestController
public class CrudController {

    @Autowired
    private UserService userService;


    /**
     * 分页查询
     * http://localhost:8080/user/page?id=10
     * http://localhost:8080/user/page?pageIndex=1&pageSize=5
     *
     * @param param
     * @return
     */
    @GetMapping("/user/page")
    public Result<PageInfo<TUser>> page(UserParam param) {
        Query query = param.toQuery();
        PageInfo<TUser> pageInfo = userService.page(query);
        return Result.ok(pageInfo);
    }

    /**
     * 新增记录，这里为了方便演示用了GET方法，实际上应该使用POST
     * http://localhost:8080/user/save?username=jim
     *
     * @param user
     * @return
     */
    @GetMapping("/user/save")
    public Result<Integer> save(TUser user) {
        userService.save(user);
        // 返回添加后的主键值
        return Result.ok(user.getId());
    }

    /**
     * 修改记录，这里为了方便演示用了GET方法，实际上应该使用POST
     * http://localhost:8080/user/update?id=10&username=jim
     *
     * @param user 表单数据
     * @return
     */
    @GetMapping("/user/update")
    public Result<?> update(TUser user) {
        userService.update(user);
        return Result.ok();
    }

    /**
     * 删除记录，这里为了方便演示用了GET方法，实际上应该使用DELETE
     * http://localhost:8080/user/delete?id=10
     *
     * @param id 主键id
     * @return
     */
    @GetMapping("/user/delete")
    public Result<?> delete(Integer id) {
        userService.deleteById(id);
        return Result.ok();
    }
}
```

- UserService.java

```java
// 实现通用接口
@Service
public class UserService implements LambdaService<TUser> {

}
```

- TUserMapper.java

```java
public interface TUserMapper extends BaseMapper<TUser> {

}
```

service和mapper不用写一行代码就能实现各种数据库操作，非常方便。

# Mapper结构图

<img src="./doc/docs/files/mappers.png" />

# Lambda查询

```java
List<TUser> list = mapper.query()
    .eq(TUser::getId, 1)
    .ge(TUser::getId, 1)
    .in(TUser::getId, Arrays.asList(1,2))
    .between(TUser::getId, 1, 2)
    .sql("id=1")
    .list();

// 查询部分字段
// SELECT id, username FROM t_user WHERE id = ?
List<TUser> tUsers1 = userLambdaService.query()
        // 只查询id, username两列
        .select(TUser::getId, TUser::getUsername)
        .eq(TUser::getId, 6)
        .list();

```

子条件查询示例：

```java
// WHERE (id = ? OR id between ? and ?) AND ( money > ? OR state = ? )
List<TUser> list = Query.query(TUser.class)
    .andLambda(q -> q.eq(TUser::getId, 3).orBetween(TUser::getId, 4, 10))
    .andLambda(q -> q.gt(TUser::getMoney, 1).orEq(TUser::getState, 1))
    .list();
```


# 条件更新

```java
int i = mapper.query()
    .set(TUser::getUsername, "王五")
    .setExpression("update_time=now()")
    .eq(TUser::getId, 6)
    .update(); // 执行跟新操作，返回影响行数
System.out.printlin("更新了" + i + "条数据");
// 对应SQL：UPDATE `t_user` SET username = ?, update_time=now()  WHERE id = ?
```


JPA查询

```java
List<Student> users = mapper.findByLastnameAndFirstname("张", "三");
Assert.assertEquals(1, users.size());
users.forEach(System.out::println);


List<Student> users = mapper.findByAgeNotIn(Arrays.asList(30, 40));
Assert.assertEquals(1, users.size());
users.forEach(System.out::println);

List<Student> users = mapper.findByAgeOrderByLastnameDesc(40);
Assert.assertEquals(2, users.size());
users.forEach(System.out::println);
```

# 综合例子

[fm-admin](https://gitee.com/durcframework/fm-admin) 

一个纯净的后台管理系统，具备基本的RBAC权限校验功能，权限校验精确到按钮级别。前端使用Vue3、Vite5、Element-Plus、TypeScript、Pinia、Tailwindcss，后端采用SpringBoot3.4 + Java17 + fastmybatis

提供代码生成工具，自动生成前后端代码，实现完整的增删改查功能。

# 工程介绍

- [doc](https://gitee.com/durcframework/fastmybatis/tree/master/doc)：开发文档
- [fastmybatis-core](https://gitee.com/durcframework/fastmybatis/tree/master/fastmybatis-core)：框架源代码
- [fastmybatis-demo](https://gitee.com/durcframework/fastmybatis/tree/master/fastmybatis-demo)：对应demo（推荐运行：fastmybatis-demo-springboot）
- [fastmybatis-spring-boot-starter](https://gitee.com/durcframework/fastmybatis/tree/master/fastmybatis-spring-boot-starter)：springboot对应的的starter

利用 [code-gen](https://gitee.com/durcframework/code-gen) 生成代码


# 相关文档

- [开发文档](https://durcframework.gitee.io/fastmybatis/)
- [fastmybatis与MyBatis generator对比](https://gitee.com/durcframework/fastmybatis/wikis/pages?sort_id=437962&doc_id=117805)
