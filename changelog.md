# 更新日志

## 3.0.17

- 修复Service继承接口查询报错问题

## 3.0.16

- 修复`orderBy(Entity::getRank)`排序关键字报错问题

## 3.0.15

- 新增getOne方法
- solon升级到3.0.7

```java
@Test
public void getOne() {
    TUser tUser = userMapper.query()
            .gt(TUser::getId, 1)
            .getOne()
            // 如果有多条记录，抛出指定异常
            .getIfThrow(new RuntimeException("查询失败"));
    System.out.println(tUser);
}
@Test
public void getOne2() {
    TUser tUser = userMapper.query()
            .gt(TUser::getId, 1)
            // 追加limit 1
            .getOne(true)
            .get();
    System.out.println(tUser);
}

@Test
public void getOne3() {
    TUser tUser = userMapper.query()
            .gt(TUser::getId, 1)
            .getOne()
            // 如果有多条数据，抛出TooManyResultsException异常
            .getOrThrow();
    System.out.println(tUser);
}

@Test
public void getOne4() {
    OneResult<TUser> result = userMapper.query()
            .eq(TUser::getId, 3)
            .getOne();
    if (result.isError()) {
        throw new RuntimeException("");
    }
    Optional<TUser> optional = result.getOptional();
    if (optional.isPresent()) {
        System.out.println(optional.get());
    }
}
```

## 3.0.14

- 新增配置项`mybatis.ignore-query-when-missing-condition=true` # 当为true时，字段不加@Condition注解不会生成查询条件

## 3.0.12

- LambdaQuery新增getCount()方法

## 3.0.11

- LambdaQuery.update()方法不支持全表更新
- LambdaService新增deleteByColumn()方法

## 3.0.10

- 修改LambdaQuery分页方法名称，避免跟Query中的page冲突

## 3.0.8

- 修复jdk.proxyN代理Mapper问题

## 3.0.7

- 修复getById传null值报错

## 3.0.6

- SearchMapper新增listUniqueValue()方法
- LambdaQuery新增listValue(), listUniqueValue(), group(), map()方法
- solon版本更新到2.8.5

## 3.0.5

- LambdaService调整save/update方式忽略null字段
- LambdaService新增getOptional()方法，getValueOptional()方法

## 3.0.4

- LambdaQuery调整ignoreLogicDelete()方法
- mybatis升级到3.5.16
- dom4j升级到2.1.4

## 3.0.3

- LambdaQuery新增ignoreLogicDelete()方法

## 3.0.2

- LambdaService新增listAll()

## 3.0.1

- IParam新增`toLambdaQuery()`方法
- PageSupport中的list初始化为空
- LambdaQuery新增get(), getOptional(), delete()方法
- DeleteMapper新增 forceDeleteByColumn(column, value) 方法
- LambdaService新增 createQuery(), createUpdateQuery() 方法

## 3.0.0

- 新增BaseMapper
- 新增LambdaService，BaseLambdaService
- LambdaQuery支持查询操作
- LambdaUpdateQuery支持更新操作

```java
// 根据条件查询
// SELECT ... FROM t_user WHERE username = ? AND age > 30
List<TUser> list = Query.query(TUser.class)
        .eq(TUser::getUsername, "张三")
        .gt(TUser:: getAge, 30)
        .list();

// 分页查询
// SELECT ... FROM t_user WHERE username = ? ORDER BY id DESC LIMIT 0, 10
PageInfo<TUser> pageInfo = Query.query(TUser.class)
        .eq(TUser::getUsername, "张三")
        .page(1, 10)
        .orderByDesc(TUser::getId)
        .page();
long total = pageInfo.getTotal();
List<TUser> records = pageInfo.getList();

// 条件删除, 逻辑删除
// UPDATE t_user SET deleted = 1 WHERE username = ?
int cnt = Query.query(TUser.class)
        .eq(TUser::getUsername, "张三")
        .delete();

// 条件删除，强行删除，无视逻辑删除字段
// DELETE FROM t_user WHERE username = ?
int cnt2 = Query.query(TUser.class)
        .eq(TUser::getUsername, "张三")
        .deleteForce();

// 更新操作
// UPDATE t_user SET username = ?, add_time=now() WHERE id = ?
int update = Query.lambdaUpdate(TUser.class)
        .set(TUser::getUsername, "李四")
        .setExpression("add_time=now()")
        .eq(TUser::getId, 6)
        .update();

```


## 2.12.2

- 条件更新支持自定义sql

```java
LambdaQuery<TUser> updateQuery2 = Query.lambdaUpdate(TUser.class);
updateQuery2.set(TUser::getUsername, "王五")
        .set(TUser::getRemark, "aaa")
        .setExpression("add_time=now()")
        .eq(TUser::getId, 6);
mapper.update(updateQuery2);
```

## 2.12.1

- 新增Query转LambdaQuery
- 新增LambdaQuery.orderByAsc, orderByDesc
- solon更新到2.8.4

## 2.12.0

【注意】：2.12.0开始groupId变了，改成了`io.gitee.durcframework`

```xml
<dependency>
  <groupId>io.gitee.durcframework</groupId>
  <artifactId>fastmybatis-spring-boot-starter</artifactId>
  <version>3.0.17</version>
</dependency>
```

- 新增Lambda条件更新

```
LambdaQuery<TUser> updateQuery = Query.lambdaUpdate(TUser.class);
updateQuery.set(TUser::getUsername, "王五");
updateQuery.eq(TUser::getId, 6);
mapper.update(updateQuery2);
// 对应SQL：UPDATE `t_user` SET username = ?  WHERE id = ?
```

- Lambda查询

```
TUser obj = mapper.getByColumn(TUser::getId, 3);
Assert.assertNotNull(obj);

List<TUser> tUsers = mapper.listByColumn(TUser::getId, 1);
Assert.assertFalse(tUsers.isEmpty());

List<TUser> tUsers1 = mapper.listByValues(TUser::getId, 1, 2, 3);
Assert.assertFalse(tUsers1.isEmpty());

List<TUser> tUsers2 = mapper.listByValues("id", 1, 2, 3);
Assert.assertFalse(tUsers2.isEmpty());

List<TUser> tUsers3 = mapper.listByCollection(TUser::getId, Arrays.asList(1, 2, 3));
Assert.assertFalse(tUsers3.isEmpty());

ColumnValue<TUser> columnValue = ColumnValue.create(TUser.class)
        .set(TUser::getRemark, "123");
int i = mapper.updateByColumn(columnValue, TUser::getId, 1);
Assert.assertEquals(1, i);

int i2 = mapper.updateById(columnValue, 1);
Assert.assertEquals(1, i2);
```

## 2.11.0

- 可以使用函数式接口指定返回字段

```java
Query query = new Query().eq("id", 6);

// SELECT id, username FROM ...
        
TUser tUser = mapper.getBySpecifiedColumns(Columns.of(TUser::getId, TUser::getUsername), query);
	
List<TUser> list = mapper.listBySpecifiedColumns(Columns.of(TUser::getId, TUser::getUsername), query);
```

- 优化字段填充功能
- 修复H2数据库下调用mapper.listMap查询异常问题

## 2.10.0

- 新增数据脱敏

fastmybatis自带了手机号、身份证号、银行卡、邮箱四种脱敏处理。以手机号脱敏为例，实现方式如下：

在实体类字段上面加`@Column`注解，并指定formatClass属性

```java
@Table(name = "t_user", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
public class TUser {

    ...
    
    // 手机号脱敏
    @Column(formatClass = MobileSensitiveFormatter.class)
    private String mobile;

    ...
}
```

- 修复listBySpecifiedColumns/listMap方法无法忽略逻辑删除字段问题

## 2.9.8

- 新增TenantContext.setDefaultBuilder方法，用来指定默认租户id
- solon升级到2.6.0

## 2.9.7

- 查询条件忽略空数组
- solon更新到2.5.4

## 2.9.6

- CommonService新增`saveBatch(Collection<E> records, int partitionSize)`，`saveBatchIgnoreNull(Collection<E> records, int partitionSize)`
- solon插件更新到2.5.3

## 2.9.5

- 修复强制使用多租户查询报错问题
- Query对象添加序列化

## 2.9.4

- 修复多数据源NPE问题

## 2.9.3

- 增强逻辑删除字段，可判断NULL字段类型

```java
    @Column(logicDelete = true, notDeleteValue = "{IS NULL}", deleteValue = "#datetime")
    private Date deleteAt;
```

- `{<SQL>}`:表示填写自定义SQL
- `#datetime`：系统内置变量，返回当前日期时间，目前值支持两种:#datetime（日期时间）,#date（仅日期，如：2023-08-10）

当deleteAt字段为NULL时，表示数据未删除

`SELECT .. FROM table WHERE delete_at IS NULL;`

删除时使用当前时间进行填充

`mapper.delete(obj)`

`UPDATE table SET delete_at = '2023-08-14 19:57:14' WHERE id = 1`


## 2.9.2

- 优化查询性能

## 2.9.1

- 修复deleteByQuery无法强制删除问题

## 2.9.0

- 新增`forceUpdateById(entity)`，`forceUpdateById(entity, boolean)`方法，可更新null值
- updateByQuery()方法可更新null值
- 优化BaseService

更新null值

```java
TUser user =  Entitys.of(TUser.class, 6);
user.setUsername("李四");
user.setRemark(null);
// UPDATE `t_user`  SET `username`='李四', `remark`=null WHERE id = 6;
int i = userMapper.forceUpdateById(user);
print("updateByQuery --> " + i);
Assert.assertEquals(1, i);
```

```java
// 根据条件批量更新，可更新null值
TUser user = Entitys.of(TUser.class);
user.setUsername("李四");
user.setRemark(null);
Query query = Query.create().eq("username", "张三");
/*
    UPDATE
        `t_user` 
    SET
        `username`='李四', `remark`=null
    WHERE
        username = '张三';
 */
int i = userMapper.updateByQuery(user, query);
```

## 2.8.1

- 新增JPA Query Method查询 [JPA Query Method](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods)

2.8.1 开始新增JPA Query Method查询，只需要在接口中定义方法就能进行查询，无须编写SQL语句。

原理：根据方法名称中的关键字自动推导出SQL语

目前实现了大部分功能，参考：[JPA Query Method](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods) 除了Distinct不支持，其它已全部支持

在Mapper中定义一个方法，以`findBy`开头

```java
/**
 * … where x.lastname = ?1 and x.firstname = ?2
 * @param lastname
 * @param firstname
 * @return
 */
List<Student> findByLastnameAndFirstname(String lastname, String firstname);
```

在Mapper中定义这个方法，就可以直接使用了，无须编写XML。

使用：

```java
    @Test
    public void findByLastnameAndFirstname() {
        List<Student> users = mapper.findByLastnameAndFirstname("张", "三");
        Assert.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }
```

更多使用示例：

<table class="tableblock frame-all grid-all fit-content">
<tbody>
<tr>
<td class="tableblock halign-left valign-top"><code>And</code></td>
<td class="tableblock halign-left valign-top"><code>findByLastnameAndFirstname</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.lastname = ?1 and x.firstname = ?2</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>Or</code></td>
<td class="tableblock halign-left valign-top"><code>findByLastnameOrFirstname</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.lastname = ?1 or x.firstname = ?2</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>Is</code>, <code>Equals</code></td>
<td class="tableblock halign-left valign-top"><code>findByFirstname</code>,<code>findByFirstnameIs</code>,<code>findByFirstnameEquals</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.firstname = ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>Between</code></td>
<td class="tableblock halign-left valign-top"><code>findByStartDateBetween</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.startDate between ?1 and ?2</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>LessThan</code></td>
<td class="tableblock halign-left valign-top"><code>findByAgeLessThan</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.age &lt; ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>LessThanEqual</code></td>
<td class="tableblock halign-left valign-top"><code>findByAgeLessThanEqual</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.age &lt;= ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>GreaterThan</code></td>
<td class="tableblock halign-left valign-top"><code>findByAgeGreaterThan</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.age &gt; ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>GreaterThanEqual</code></td>
<td class="tableblock halign-left valign-top"><code>findByAgeGreaterThanEqual</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.age &gt;= ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>After</code></td>
<td class="tableblock halign-left valign-top"><code>findByStartDateAfter</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.startDate &gt; ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>Before</code></td>
<td class="tableblock halign-left valign-top"><code>findByStartDateBefore</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.startDate &lt; ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>IsNull</code>, <code>Null</code></td>
<td class="tableblock halign-left valign-top"><code>findByAge(Is)Null</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.age is null</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>IsNotNull</code>, <code>NotNull</code></td>
<td class="tableblock halign-left valign-top"><code>findByAge(Is)NotNull</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.age not null</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>Like</code></td>
<td class="tableblock halign-left valign-top"><code>findByFirstnameLike</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.firstname like '%?1%'</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>NotLike</code></td>
<td class="tableblock halign-left valign-top"><code>findByFirstnameNotLike</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.firstname not like '%?1%'</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>StartingWith</code></td>
<td class="tableblock halign-left valign-top"><code>findByFirstnameStartingWith</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.firstname like '?1%'</code> </td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>EndingWith</code></td>
<td class="tableblock halign-left valign-top"><code>findByFirstnameEndingWith</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.firstname like '%?1'</code> </td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>Containing</code></td>
<td class="tableblock halign-left valign-top"><code>findByFirstnameContaining</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.firstname like '%?1%'</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>OrderBy</code></td>
<td class="tableblock halign-left valign-top"><code>findByAgeOrderByLastnameDesc</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.age = ?1 order by x.lastname desc</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>Not</code></td>
<td class="tableblock halign-left valign-top"><code>findByLastnameNot</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.lastname &lt;&gt; ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>In</code></td>
<td class="tableblock halign-left valign-top"><code>findByAgeIn(Collection&lt;Age&gt; ages)</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.age in ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>NotIn</code></td>
<td class="tableblock halign-left valign-top"><code>findByAgeNotIn(Collection&lt;Age&gt; ages)</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.age not in ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>True</code></td>
<td class="tableblock halign-left valign-top"><code>findByActiveTrue()</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.active = true</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>False</code></td>
<td class="tableblock halign-left valign-top"><code>findByActiveFalse()</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.active = false</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>IgnoreCase</code></td>
<td class="tableblock halign-left valign-top"><code>findByFirstnameIgnoreCase</code></td>
<td class="tableblock halign-left valign-top"><code>… where UPPER(x.firstname) = UPPER(?1)</code></td>
</tr>
</tbody>
</table>


**JPA查询使用注意事项**：

- 目前实现了大部分功能，参考：https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods 除了Distinct不支持，其它已全部支持
- 方法名称必须findBy开头
- 数据库字段名必须遵从下划线模式，如：name, add_time，驼峰命名方式不支持JPA查询
- 如果：IDEA下运行 JPA查询 mybatis报错 `Parameter 'arg0' not found`，参考这篇文章：https://www.cnblogs.com/JangoJing/p/10791831.html


## 2.7.0

- 简化标准使用方式，详情查看 `fastmybatis-demo/fastmybatis-demo-standard`
- 新增H2数据库模板
- 新增LambdaQuery


```java
Query query = Query.query(TUser.class)
    .eq(TUser::getId, 1)
    .ge(TUser::getId, 1)
    .in(TUser::getId, Arrays.asList(1,2))
    .between(TUser::getId, 1, 2)
    .sql("id=1");
List<TUser> list = mapper.list(query);
```

子表达式

```java
Query query = Query.query(TUser.class)
    .eq(TUser::getId, 6)
    // 子表达式
    .and(q -> q.eq(TUser::getUsername, "jim")
    .orEq(TUser::getState, 1)
    )
    .orLambda(q -> q.eq(TUser::getId, 1).orBetween(TUser::getId, 1, 90))
    .orEq(TUser::getUsername, "tom");
List<TUser> list = mapper.list(query);
```

## 2.6.2

- 逻辑删除时可更新其它字段

```java
    /**
     * 根据多个主键id删除，在有逻辑删除字段的情况下，做UPDATE操作
     */
    @Test
    public void deleteByIds2() {
        // 方式1，通过参数指定
        EqualColumn[] update = {
                new EqualColumn("username", "deleteUser")
                , new EqualColumn("add_time", new Date())
        };
        int i = mapper.deleteByIds(Arrays.asList(1,2,3), update);
        print("del --> " + i);
    }

    /**
     * 根据多个主键id删除，在有逻辑删除字段的情况下，做UPDATE操作
     */
    @Test
    public void deleteByIds3() {
        // 方式2，通过ThreadLocal指定,
        FastmybatisContext.setDeleteSetBlock(Arrays.asList(
                new EqualColumn("username", "deleteUser"),
                new EqualColumn("add_time", new Date())
        ));

        int i = mapper.deleteByIds(Arrays.asList(1,2,3));
        print("del --> " + i);
    }
```

- 添加checkExist方法，新增和编辑时候某列判重

```java
    @Test
    public void checkExist() {
        /*
          SELECT
              username 
          FROM
              `t_user` t 
          WHERE
              username = '王五' AND id <> 1 AND t.isdel = 0 LIMIT 0,1;
         */
        int id = 1;
        boolean checkExist = mapper.checkExist("username", "王五", id);
        System.out.println(checkExist);
    }
```

- 修复非mysql数据库delete物理删除失败问题

## 2.6.1

- 优化SQL打印插件
- 适配solon框架

## 2.6.0

感谢[@youbeiwuhuan](https://gitee.com/youbeiwuhuan)供的pr

- `@Column` 注解新增logicDeleteStrategy枚举属性 [PR](https://gitee.com/durcframework/fastmybatis/pulls/10)，可选项有
  - LogicDeleteStrategy.FIXED_VALUE：固定值策略,notDeleteValue和deleteValue生效（`UPDATE t_user SET isdel = 1 WHERE id = 1 AND isdel = 0;`）
  - LogicDeleteStrategy.ID_FILL：删除时主键填充逻辑删除字段（`UPDATE t_user SET isdel = id WHERE id = 1 AND isdel = 0;`）



- 新增配置，禁止在Mapper接口中写SQL。[PR](https://gitee.com/durcframework/fastmybatis/pulls/9)

在application.properties中指定`mybatis.disable-sql-annotation=true` （默认false）

Mapper接口中则不允许写自定义注解SQL，如:`@Select,@Update,@Delete,@Insert`

```java
    // 自定义sql
    @Update("update t_user set username = #{username} where id = #{id}")
    int updateById(@Param("id") int id, @Param("username") String username);
```

项目启动会报错

此功能开启方便团队统一代码规范，自定义SQL必须统一写在xml中

## 2.5.0

- 支持springboot3.0

## 2.4.8

- 优化SQL格式化打印插件，可自定义扩展
- 增强saveOrUpdate方法，可根据某个字段值判断插入或保存

## 2.4.7

- 新增SQL格式化打印插件

```xml
<plugins>
    <plugin interceptor="com.gitee.fastmybatis.core.support.plugin.SqlFormatterPlugin" />
</plugins>
```

- 修复单值查询LocalDateTime转换成Date错误

## 2.4.6

- 修复填充器使用LocalDateTime时间转换错误

## 2.4.5

- 修复分页查询在查不到的情况下返回pageSize不正确问题
- 修复使用高版本mysql驱动导致DateFillInsert日期转换报错问题 [#I64YO2](https://gitee.com/durcframework/fastmybatis/issues/I64YO2)

## 2.4.4

- saveBatchIgnoreNull方法取消list size判断，改为直接返回0。

如果调用者没有判断长度，如果保存空list则会报错，可能会影响到后续业务逻辑处理。

因此如果批量保存元素为空则静默处理返回0

- 新增`<R> PageEasyui<R> pageEasyui(Query query, Function<E, R> converter)`方法，更好的配合easyui datagrid组件

## 2.4.3

- 修复自定义类型字段无法生效问题

## 2.4.2

- 修复标准模式下配置不生效问题

## 2.4.1

- 优化saveBatchBySpecifiedColumns方法，指定保存哪些字段，可以是实体类属性名，也可以是数据库字段名
- 优化updateByQuery方法，指定忽略更新的字段，可以是实体类属性名，也可以是数据库字段名

## 2.4.0

- 新增saveBatchIgnoreNull方法，批量保存忽略null字段

## 2.3.10

- 修复枚举字段映射错误问题

## 2.3.9

- 修复transient关键字不生效问题

## 2.3.8

- 修复DateInsertFill失效问题

## 2.3.6

- 修复JDK高版本`cannot access class sun.net.www.protocol.file.FileURLConnection`问题

## 2.3.5

- 拼接条件默认忽略空数组或空集合
- 优化ConditionValueHandler处理

## 2.3.4

- 修复findField方法内进行name.equals()调用时出现空指针异常

## 2.3.3 

- 修复使用spring-boot-devtools导致获取不到实例

## 2.3.2

- 支持查询列表并将结果转换成树结构。测试用例：`com.myapp.TreeDataTest`

有一张菜单表，具备父子关系

```sql
CREATE TABLE `menu` (
`id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
`name` varchar(64) NOT NULL COMMENT '菜单名称',
`parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父节点',
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';
```

实体类

```java
public class Menu implements TreeNode<Menu, Integer> {

     private Integer id;
     private String name;
     // 父节点id
     private Integer parentId;
     // 子节点
     private List<Menu> children;

     @Override
     public Integer takeId() {
         return getId();
     }

     @Override
     public Integer takeParentId() {
         return getParentId();
     }

     @Override
     public void setChildren(List<Menu> children) {
         this.children = children;
     }

     getter setter...
}
```

查询

```java
// 查询出来的结果已经具备父子关系
List<Menu> treeData = mapper.listTreeData(query, 0);
System.out.println(JSON.toJSONString(treeData, SerializerFeature.PrettyFormat));
```

打印结果：

```json
[
	{
		"children":[
			{
				"children":[],
				"id":2,
				"name":"苹果",
				"parentId":1
			},
			{
				"children":[],
				"id":3,
				"name":"香蕉",
				"parentId":1
			}
		],
		"id":1,
		"name":"水果",
		"parentId":0
	}
]
```

## 2.3.1

- 优化listBySpecifiedColumns查询
- 新增listMap方法
- 新增saveBatch(Collection<E> records, int partitionSize)方法，分批次批量保存

listBySpecifiedColumns查询指定别名

```java
List<String> columns = Arrays.asList(
  "username as name",
  "add_time as createTime",
  "add_time as addTime",
  "money"
);
// 查询并转换成User类
List<User> userList = mapper.listBySpecifiedColumns(columns, query, User.class);

public static class User {
  private String name;
  private String createTime;
  private Date addTime;
  private Float money;
  // GET SET..
}
```

分批次保存：

```java
List<User> users = ... // 1000个元素
// 分批次保存，每次保存200条        
mapper.saveBatch(users, 200);
```

## 2.3.0

- listByCollection,listByArray,listByIds当value为空直接返回空结果
- 新增子条件查询
- 新增or条件查询
- mapper新增getMap方法，将查询结果转换成Map对象
- saveBatch批量插入返回自增主键值

or条件查询：

```java
// WHERE id = ? OR username = ?
Query query = new Query()
    .eq("id", 6)
    .orEq("username", "jim");
```

子条件查询示例：

```java
// WHERE (id = ? OR id between ? and ?) AND ( money > ? OR state = ? )
Query query = new Query()
    .and(q -> q.eq("id", 3).orBetween("id", 4, 10))
    .and(q -> q.gt("money", 1).orEq("state", 1));

// WHERE ( id = ? AND username = ? ) OR ( money > ? AND state = ? )
Query query = new Query()
    .and(q -> q.eq("id", 3).eq("username", "jim"))
    .or(q -> q.gt("money", 1).eq("state", 1));
```

getMap方法示例：

```java
Query query = new Query()
        .ge("id", 1);

// 查询Map，主键id作为key，行记录作为value，方便通过主键id获取某条记录
// id -> TUser
Map<Integer, TUser> map = mapper.getMap(query, TUser::getId);
System.out.println(map.get(6));
```

批量插入返回自增主键id：

```java
List<TUser> users = ...
mapper.saveBatch(users);

List<Integer> idList = users.stream()
        .map(TUser::getId)
        .collect(Collectors.toList());

System.out.println("主键id：" + idList);
```

## 2.2.8

- updateByQuery增加ignoreProperties参数  [#pr](https://gitee.com/durcframework/fastmybatis/pulls/7/files)

```java
public class User {
  private String username;
  private Byte type = 1; // 这里有个默认值
}

User user = new User();
user.setUsername("Jim");
mapper.updateByQuery(user, new Query().eq("age", 10));
//上面方法生成的SQL为：UPDATE user SET username = 'Jim', type=1 WHERE age = 10
mapper.updateByQuery(user, new Query().eq("age", 10), "type"); // 不更新type
//上面生成的SQL为：UPDATE user SET username = 'Jim' WHERE age = 10
```

## 2.2.6

- 修复自定义主键生成策略时主键值没有回填到对象中 [#I57W5B](https://gitee.com/durcframework/fastmybatis/issues/I57W5B)

## 2.2.5

- 支持Instant时间类型

## 2.2.4

- Query新增between条件，`query.between("age", 1, 30);`
- MapperUtil新增自定义分页查询
- 升级mybatis到3.5.9，升级velocity到2.3，升级junit到4.13.2

## 2.2.3

- 修复spring扫描mapper问题

## 2.2.2

- 增强单值转换能力（参见：`com.myapp.TUserMapperTest#getColumnValue`）
- 完善demo

## 2.2.1

- 去除spring依赖，项目无需依赖spring也能跑，具体查看fastmybatis-demo/fastmybatis-demo-standard
- 新增Vert.x示例，具体查看fastmybatis-demo/fastmybatis-demo-vertx

## 2.1.0

- 支持多租户
- 优化查询参数

## 2.0.0（对1.x不兼容）

- 新增`ActiveRecord`模式
- 移除JPA注解，采用自带注解，精简依赖
- 移除标记为废弃的方法：`listMap`、`pageMap`、`saveMulti`、`updateByQuery(Map<String, ?> setBlock, Query query)`
- 移除`fastmybatis-generator`模块，由 [code-gen](https://gitee.com/durcframework/code-gen) 代替
- 移除`BaseService`模块，由`IService`代替

**1.x升级到2.0**

实体类进行全局替换：

`javax.persistence.Table` 替换成：`com.gitee.fastmybatis.annotation.Table`

`javax.persistence.Column` 替换成：`com.gitee.fastmybatis.annotation.Column`

移除：`javax.persistence.Transient`注解，改成`transient`关键字

移除：`javax.persistence.Id`注解，改成：`@Table(name = "t_user", pk = @Pk(name = "id"))`

移除：`javax.persistence.GeneratedValue`注解，改成：`@Table(name = "t_user", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))`

## 1.11.1

- 新增`int saveUnique(Collection<E> entitys)` 批量保存并去重
- 新增`int saveUnique(Collection<E> entitys, Comparator<E> comparator) ` 批量保存并去重，指定比较器
- 新增`BaseService`通用service
- 新增`IService`通用接口

## 1.11.0

- mapper新增如下方法：
    - `E getBySpecifiedColumns(List<String> columns, Query query)` 查询单条数据并返回指定字段
    - `<T> T getBySpecifiedColumns(List<String> columns, Query query, Class<T> clazz)` 查询单条数据并返回指定字段并转换到指定类中
    - `List<E> listByIds(Collection<I> ids)` 根据多个主键值查询
    - `T getColumnValue(String column, Query query, Class<T> clazz)` 查询某个字段值
    - `List<T> listColumnValues(String column, Query query, Class<T> clazz)` 查询指定列，返指定列集合
    - `int saveOrUpdate(E entity)` 保存或更新（所有字段）
    - `int saveOrUpdateIgnoreNull(E entity)` 保存或更新（不为null的字段）
    - `int deleteByIds(Collection<I> ids)` 根据多个主键id删除，在有逻辑删除字段的情况下，做UPDATE操作
    - `int deleteByColumn(String column, Object value)` 根据指定字段值删除，在有逻辑删除字段的情况下，做UPDATE操作

## 1.10.12

- Query类新增条件表达式`query.eq(StringUtils.hasText(name), "name", name);`
- 修复mysql下tinyint(1)返回boolean值问题
- 检查空字符串默认进行一次trim。`mybatis.empty-string-with-trim=true`
- 查询字段忽略空字符调整为默认开启`mybatis.ignore-empty-string=true`

## 1.10.11

- 修复mapper查询属性拷贝问题

## 1.10.10 （有BUG，勿用）

- dom4j升级到2.1.3
- mapper新增page重载方法，方便类型转换

## 1.10.9

- mybatis升级到3.5.7
- mybatis-spring升级到2.0.6

## 1.10.8

- 修复无主键的表查询多一个字段问题

## 1.10.7

- 支持查询返回指定列并分页 `mapper.pageBySpecifiedColumns`

## 1.10.6

- 支持未设置主键表

## 1.10.5

- mapper新增分页查询方法，支持easyui表格
- 优化条件忽略设置
- starter新增`mybatis.ignore-empty-string=true`配置

## 1.10.4

- Query新增`sql(String, Object...)`方法

## 1.10.3

- 增强`SchMapper.listBySpecifiedColumns(List<java.lang.String>, Query, Class<T>)`方法，Class支持基本类型class。

使用示例：

```java
    // 返回id列
    List<Integer> idList = mapper.listBySpecifiedColumns(Collections.singletonList("id"), query, Integer.class/* 或int.class */);
    for (Integer id : idList) {
        System.out.println(id);
    }

    // 返回id列，并转换成String
    List<String> strIdList = mapper.listBySpecifiedColumns(Collections.singletonList("id"), query, String.class);

    for (String id : strIdList) {
        System.out.println("string:" + id);
    }

    // 返回username列
    List<String> usernameList = mapper.listBySpecifiedColumns(Collections.singletonList("username"), query, String.class);
    for (String username : usernameList) {
        System.out.println(username);
    }

    // 返回时间列
    List<Date> dateList = mapper.listBySpecifiedColumns(Collections.singletonList("add_time"), query, Date.class);
    for (Date date : dateList) {
        System.out.println(date);
    }

    // 返回decimal列
    List<BigDecimal> moneyList = mapper.listBySpecifiedColumns(Collections.singletonList("money"), query, BigDecimal.class);
    for (BigDecimal money : moneyList) {
        System.out.println(money);
    }

    // 返回decimal列并转成float
    List<Float> moneyList2 = mapper.listBySpecifiedColumns(Collections.singletonList("money"), query, float.class);
    for (Float money : moneyList2) {
        System.out.println("float:" + money);
    }

    // 返回tinyint列
    List<Byte> stateList = mapper.listBySpecifiedColumns(Collections.singletonList("state"), query, byte.class);
    for (Byte state : stateList) {
        System.out.println("state:" + state);
    }
```

## 1.10.2

- Mapper新增以下三个方法：
 - `listBySpecifiedColumns(List<String> columns, Query query, Class<T> clazz)`：查询返回指定的列，并将结果转换成Class对应的类
 - `listBySpecifiedColumns(List<String> columns, Query query)`：查询返回指定的列，返回实体类集合
 - `PageInfo<E> page(Query query)` 分页查询，返回分页信息

## 1.10.1

- 修复下划线字段映射问题

## 1.10.0

- Mapper新增forceDelete、forceDeleteById、forceDeleteByQuery、forceById四个方法
- deleteByQuery可检测逻辑删除字段

## 1.9.2

- 级联查询忽略逻辑删除字段

## 1.9.1

- 去除fastjson，改为gson

## 1.9.0（Java8）

- 支持LocalDateTime

## 1.8.6

- 修复saveIgnoreNull自定义填充器失效问题

## 1.8.5

- 修复updateIgnoreNull&saveIgnoreNull问题

## 1.8.4

- 修复热部署空指针问题
- 修复oracle下saveMulti问题

## 1.8.3

- 修复oracle下getByQuery随机取一条问题

## 1.8.2

- 支持oracle sequence

## 1.8.1

- 修复ignoreUpdateColumns字段失效问题

## 1.8.0

- 全局指定忽略更新字段
- 实体类支持泛型主键

## 1.7.4

- 修复oracle下的BUG

## 1.7.3

- 优化自定义主键策略回写id值到实体类

## 1.7.2

- 修复条件删除BUG

## 1.7.1

- 修复Condition注解空指针异常

## 1.7.0

- 支持热部署，修改xml无需重启

## 1.6.0

- @Condition新增handlerClass属性
- @Condition新增ignoreValue属性

## 1.5.1

- @Condition新增index属性优化

## 1.5.0 

- @Condition新增index属性，调整条件顺序
- @新增LazyFetch注解
- 修复oracle数据库插入问题

## 1.4.2

- 修复懒加载BUG

## 1.4.1

- mapper.updateByQuery()方法支持逻辑删除字段

## 1.4.0

- 新增实体类一对一懒加载功能
- 查询BUG修复

## 1.3.0(有BUG不要使用此版本)

- updateByQuery可强制更新null
- 修复distinct使用方式id顺序问题
- @Condition注解可作用在字段上

## 1.2.0

- 修复自定义xml文件合并BUG
- 新增updateByMap方法
- 新增distinct使用方式

## 1.1.1

- 修复实体类静态字段被解析问题

## 1.1.0

- 提供easyui表格分页查询，见MapperUtil.java

## 1.0.17

- 修复SqlServer模板问题（使用SqlServer数据库必须升级到此版本）


## 1.0.16

- @Condition注解新增ignoreEmptyString属性 https://durcframework.gitee.io/fastmybatis/#27040701

## 1.0.15

- 代码优化性能优化（阿里代码规范+sonar）

## 1.0.14

- 代码优化（sonar）

## 1.0.13

- 合并PR2，https://gitee.com/durcframework/fastmybatis/pulls/2

## 1.0.12

- 修复模板问题


## 1.0.11

- 优化属性拷贝

## 1.0.10

- 增强Mapper.xml，不同Mapper文件可指定同一个namespace，最终会合并

## 1.0.9

- 对象转换方法优化

## 1.0.8

- pageSize传0时报错bug

## 1.0.7

- 参数类构建条件可以获取父类属性

## 1.0.6

- EasyuiDtagridParam增加条件忽略

## 1.0.5

- easyui表格参数支持

## 1.0.4

- Mapper对应无实体类BUG

## 1.0.3

- 添加强制查询功能，见TUserMapperTest.testForceQuery()

## 1.0.2

- 完善注释
- 代码优化

## 1.0.1 初版
