# 实体类注解

完整的注解如下

```java
/**
 * 表名：t_user
 * 备注：用户表
 */
@Table(name = "t_user", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
public class TUser {

    /** ID, 数据库字段：id */
    private Integer id;

    /** 用户名, 数据库字段：username */
    private String username;

    /** 状态, 数据库字段：state */
    private Byte state;

    /** 是否删除, 数据库字段：isdel */
    @Column(logicDelete = true)
    private Boolean isdel;
}
```

 **注解说明**

| 注解                     | 说明                                                |
|:-----------------------|---------------------------------------------------|
| @Table.name            | 指定表名，不填则默认类名转下划线当表名                               |
| @Pk.name               | 数据库主键字段名称，不填默认为id                                 |
| @Pk.strategy           | 主键自增策略，默认为INCREMENT                               |
| @Pk.sequenceName       | 主键sequence名称，如果填了该字段则表示使用sequence策略。不能与strategy共存 |
| @Column.name           | 指定数据库字段名，如果表字段命名由下划线组成，可省略                        |
| @Column.logicDelete    | 是否逻辑删除字段, 默认false                                 |
| @Column.notDeleteValue | 当设置logicDelete为true，未删除数据库保存的值,不指定默认为0            |
| @Column.deleteValue    | 当设置logicDelete为true，删除后数据库保存的值,不指定默认为1            |
| @Column.lazyFetch      | 懒加载配置，作用在实体类的字段上， 默认false                         |
| @Column.version        | 是否乐观锁字段，默认false                                   |
| @Column.formatClass    | 指定格式化类                                            |


一般情况下这些注解不会全部用到，常见如下形式：

当数据库设命名范遵循`小写+下划线`方式，主键统一为`id`并自增，一下为最精简配置

```java
/**
 * 表名：t_user
 * 备注：用户表
 */
@Table(name = "t_user")
public class TUser {

    /** ID, 数据库字段：id */
    private Integer id;

    /** 用户名, 数据库字段：username */
    private String username;

    /** 状态, 数据库字段：state */
    private Byte state;
}
```

