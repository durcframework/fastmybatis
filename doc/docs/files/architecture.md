# 实现原理

聊原理之前，先看看不依赖其它框架的情况下如何使用Mybatis。

先是定义一个接口Mapper，然后在Mapper.xml中实现sql语句。

![图片](./images/arc0.png)

程序启动后，mybatis加载本地中的xml文件加载到内存当中，然后解析xml

![图片](./images/arc1.png)

既然每张表都有增删改操作，并且SQL语句都一样，除了表名、字段有区别，那么我们是否可以把这些共性抽取出来，通过程序自动生成呢。

然后修改一下加载xml过程，让mybatis直接加载预先生成好的xml

![图片](./images/arc2.png)


首先要解决的是如何拿到表名和字段信息，这个可以通过实体类拿到，在实体类上方声明一个注解，里面加上表名，类中的字段对应的表字段

```java
@Table("user")
public class User {
    private Integer id;
}
```

启动时拿到UserMapper.class和User.class，通过反射拿到表名，字段信息，动态生成xml内容


这里的xml在内存当中
```xml
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.myapp.dao.UserMapper">
    <select id="list">
        select ...
        from user
    </select>
</mapper>
```

xml中的sql语句需要有对应的接口方法，既然是通用的sql，那么就需要通用的Mapper

```java
public interface BaseMapper<T> {
    List<T> list();
    int save();
    int update();
    int delete();
}
```

然后继承通用Mapper并关联实体类

```java
// 通过扫描package可以获取Mapper和实体类class信息
public interface UserMapper extends BaseMapper<User> {}
```

xml内容直接保存在内存当中，对开发者而言只需维护好实体类和Mapper。

如果要手写自定义SQL怎么办，可以在本地创建一个UserMapper.xml文件，然后把本地xml文件内容合并到内存xml中去

```xml
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.myapp.dao.UserMapper">
    <select id="list">
        select ...
        from user
    </select>
    
    <!-- 本地手写SQL -->
    <select id="xxx"></select>
</mapper>
```

最终mybatis拿到完整的xml内容

以上就是fastmybatis实现原理，即动态生成xml，xml包含基本的CRUD语句
