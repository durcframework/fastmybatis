# 指定外部模板

fastmybatis依赖模板文件来生成mapper，默认的模板存放在 [fastmybatis/tpl/](https://gitee.com/durcframework/fastmybatis/tree/master/fastmybatis-core/src/main/resources/fastmybatis/tpl) 下，模板文件名对应某一种数据库，如mysql.vm对应mysql数据库。

我们可以将模板文件放到自己的项目中，然后根据实际情况进行调整。

- 方式1【推荐】：将模板文件放在classpath根目录下，如：`src/main/resources`下

以MySQL为例：

1. 浏览器访问：[模板列表](https://gitee.com/durcframework/fastmybatis/tree/master/fastmybatis-core/src/main/resources/fastmybatis/tpl)
2. 将`mysql.vm`，复制到自己项目中的`src/main/resources`目录

这样启动程序会优先找classpath下的模板文件。

- 方式2：手动指定路径

我们可以通过更改template-classpath的值来改变模板读取的位置。默认template-classpath的值为/fastmybatis/tpl/。假如你想对mysql.vm做一些修改，那么可以按照如下步骤进行：

1. 使用解压工具解压fastmybatis.jar
2. 在fastmybatis/tpl/下找到mysql.vm，拷贝一份出来，放到你的项目中的classpath下（src/main/resources）
3. 在application.properties中添加一行

```
mybatis.template-classpath=/ 
```

这样在启动时会自动读取classpath根目录下的mysql.vm。控制台也会打印读取模板的信息：

```
2017-12-26 19:32:31.021  INFO 13476 --- [           main] n.o.d.e.ext.MapperLocationsBuilder       : 使用模板:/mysql.vm
```

如果你的项目是springmvc，采用xml配置形式，前两步不变，第三步改为：

```xml
<!-- 替换org.mybatis.spring.SqlSessionFactoryBean -->
	<bean id="sqlSessionFactory"
		class="com.gitee.fastmybatis.core.ext.SqlSessionFactoryBeanExt">
		<property name="dataSource" ref="dataSource" />
		<property name="configLocation">
			<value>classpath:mybatis/mybatisConfig.xml</value>
		</property>
		<property name="mapperLocations">
			<list>
				<value>classpath:mybatis/mapper/*.xml</value>
			</list>
		</property>
		
		<!-- 以下是附加属性 -->
		
		<!-- dao所在的包名,跟MapperScannerConfigurer的basePackage一致 
			多个用;隔开
		-->
		<property name="basePackage" value="com.myapp.dao" />
		<property name="config">
			<bean class="com.gitee.fastmybatis.core.FastmybatisConfig">
				<!-- 指定外部模板 -->
				<property name="templateClasspath" value="/"/>
			</bean>
		</property>
	</bean>
```