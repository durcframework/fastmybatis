# 数据脱敏

数据脱敏，将一些敏感数据隐藏，如：手机号，身份证号等信息

fastmybatis自带了手机号、身份证号、银行卡、邮箱四种脱敏处理。

- 手机号脱敏：MobileSensitiveFormatter
- 身份证号脱敏：IdCardSensitiveFormatter
- 银行卡脱敏：BankCardSensitiveFormatter
- 邮箱脱敏：EmailSensitiveFormatter

以手机号脱敏为例，实现方式如下：

在实体类字段上面加`@Column`注解，并指定formatClass属性

```java
@Table(name = "t_user", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
public class TUser {

    ...
    
    // 手机号脱敏
    @Column(formatClass = MobileSensitiveFormatter.class)
    private String mobile;

    ...
}
```

通过mapper.list, mapper.page, mapper.getById查询出来的的数据都会进行脱敏

## 自定义

自定义格式化数据，新建类并继承`BaseFormatter`，重写`format(Object value)`方法

```java
public class MyFormatter extends BaseFormatter<String> {

    @Override
    public String format(Object value) {
        if (value == null) {
            return null;
        }
        // TODO: 返回格式化后的数据
    }

}
```

然后指定formatClass

```java
@Column(formatClass = MyFormatter.class)
private String username;
```
