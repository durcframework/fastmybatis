# 实体类枚举字段

数据库中一些状态字段通常用0,1,2或者简单的字符串进行维护，然后JavaBean实体类中用枚举类型来保存，这样做便于使用和维护。

fastmybatis上使用枚举属性很简单：枚举类实现`com.gitee.fastmybatis.core.handler.BaseEnum`接口即可。

下面是具体例子：

```java
public enum UserInfoType implements BaseEnum<String> {
    INVALID("0"),VALID("1")
    ;

    private String status;

    UserInfoType(String type) {
        this.status = type;
    }
    
    @Override
    public String getCode() {
        return status;
    }
}
```

首先定义一个枚举类，实现BaseEnum接口，接口类型参数用String，表示保存的值是String类型，如果要保存Int类型的话改用`BaseEnum<Integer>`。
实现`getCode()`方法，返回数据库中存放的值。


在javaBean添加该枚举属性：

```java
public class UserInfo {
    ...
    private UserInfoType status;

    // 省略getter setter
}
```

接下来就可以使用dao来进行数据操作了，下面是完整测试用例：

```java
public class UserInfoDaoTest extends EasymybatisSpringbootApplicationTests {
    
    @Autowired
    UserInfoDao userInfoDao;
    
    @Test
    public void testGet() {
        UserInfo userInfo = userInfoDao.get(3);
        print("枚举字段status：" + userInfo.getStatus().getCode());
        print(userInfo);
    }
    
    @Test
    public void testUpdate() {
        UserInfo userInfo = userInfoDao.get(3);
        // 修改枚举值
        userInfo.setStatus(UserInfoType.INVALID);
        userInfoDao.update(userInfo);
    }
    
    @Test
    public void testSave() {
        UserInfo userInfo = new UserInfo();
        userInfo.setAddress("aa");
        userInfo.setCity("杭州");
        userInfo.setCreateTime(new Date());
        userInfo.setUserId(3);
        // 枚举值
        userInfo.setStatus(UserInfoType.VALID);
        userInfoDao.save(userInfo);
    }
}
```