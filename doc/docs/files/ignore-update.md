# 字段忽略更新

- springboot项目

application.properties中新增：

```properties
# 忽略新增/修改时间字段
mybatis.ignore-update-columns[0]=gmt_create
mybatis.ignore-update-columns[1]=gmt_modified
```

设置完毕后，执行save或update语句时，将自动忽略这两个时间字段。

gmt_create时间字段需要指定`CURRENT_TIMESTAMP`，gmt_modified时间字段需要指定`CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP`

```sql
`gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
```

- 非springboot

在启动前加入如下代码

```java
FastmybatisConfig.defaultIgnoreUpdateColumns = Arrays.asList("gmt_create", "gmt_modified");
```
