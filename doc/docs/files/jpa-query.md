# JPA查询

2.8.1 开始新增JPA Query Method查询，只需要在接口中定义方法就能进行查询，无须编写SQL语句。

原理：根据方法名称中的关键字自动推导出SQL语

目前实现了大部分功能，参考：[JPA Query Method](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods) 除了Distinct不支持，其它已全部支持

在Mapper中定义一个方法，以`findBy`开头

```java
/**
 * … where x.lastname = ?1 and x.firstname = ?2
 * @param lastname
 * @param firstname
 * @return
 */
List<Student> findByLastnameAndFirstname(String lastname, String firstname);
```

在Mapper中定义这个方法，就可以直接使用了，无须编写XML。

使用：

```java
    @Test
    public void findByLastnameAndFirstname() {
        List<Student> users = mapper.findByLastnameAndFirstname("张", "三");
        Assert.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }
```

更多使用示例：

<table class="tableblock frame-all grid-all fit-content">
<tbody>
<tr>
<td class="tableblock halign-left valign-top"><code>And</code></td>
<td class="tableblock halign-left valign-top"><code>findByLastnameAndFirstname</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.lastname = ?1 and x.firstname = ?2</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>Or</code></td>
<td class="tableblock halign-left valign-top"><code>findByLastnameOrFirstname</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.lastname = ?1 or x.firstname = ?2</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>Is</code>, <code>Equals</code></td>
<td class="tableblock halign-left valign-top"><code>findByFirstname</code>,<code>findByFirstnameIs</code>,<code>findByFirstnameEquals</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.firstname = ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>Between</code></td>
<td class="tableblock halign-left valign-top"><code>findByStartDateBetween</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.startDate between ?1 and ?2</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>LessThan</code></td>
<td class="tableblock halign-left valign-top"><code>findByAgeLessThan</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.age &lt; ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>LessThanEqual</code></td>
<td class="tableblock halign-left valign-top"><code>findByAgeLessThanEqual</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.age &lt;= ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>GreaterThan</code></td>
<td class="tableblock halign-left valign-top"><code>findByAgeGreaterThan</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.age &gt; ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>GreaterThanEqual</code></td>
<td class="tableblock halign-left valign-top"><code>findByAgeGreaterThanEqual</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.age &gt;= ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>After</code></td>
<td class="tableblock halign-left valign-top"><code>findByStartDateAfter</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.startDate &gt; ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>Before</code></td>
<td class="tableblock halign-left valign-top"><code>findByStartDateBefore</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.startDate &lt; ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>IsNull</code>, <code>Null</code></td>
<td class="tableblock halign-left valign-top"><code>findByAge(Is)Null</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.age is null</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>IsNotNull</code>, <code>NotNull</code></td>
<td class="tableblock halign-left valign-top"><code>findByAge(Is)NotNull</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.age not null</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>Like</code></td>
<td class="tableblock halign-left valign-top"><code>findByFirstnameLike</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.firstname like '%?1%'</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>NotLike</code></td>
<td class="tableblock halign-left valign-top"><code>findByFirstnameNotLike</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.firstname not like '%?1%'</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>StartingWith</code></td>
<td class="tableblock halign-left valign-top"><code>findByFirstnameStartingWith</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.firstname like '?1%'</code> </td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>EndingWith</code></td>
<td class="tableblock halign-left valign-top"><code>findByFirstnameEndingWith</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.firstname like '%?1'</code> </td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>Containing</code></td>
<td class="tableblock halign-left valign-top"><code>findByFirstnameContaining</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.firstname like '%?1%'</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>OrderBy</code></td>
<td class="tableblock halign-left valign-top"><code>findByAgeOrderByLastnameDesc</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.age = ?1 order by x.lastname desc</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>Not</code></td>
<td class="tableblock halign-left valign-top"><code>findByLastnameNot</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.lastname &lt;&gt; ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>In</code></td>
<td class="tableblock halign-left valign-top"><code>findByAgeIn(Collection&lt;Age&gt; ages)</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.age in ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>NotIn</code></td>
<td class="tableblock halign-left valign-top"><code>findByAgeNotIn(Collection&lt;Age&gt; ages)</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.age not in ?1</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>True</code></td>
<td class="tableblock halign-left valign-top"><code>findByActiveTrue()</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.active = true</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>False</code></td>
<td class="tableblock halign-left valign-top"><code>findByActiveFalse()</code></td>
<td class="tableblock halign-left valign-top"><code>… where x.active = false</code></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><code>IgnoreCase</code></td>
<td class="tableblock halign-left valign-top"><code>findByFirstnameIgnoreCase</code></td>
<td class="tableblock halign-left valign-top"><code>… where UPPER(x.firstname) = UPPER(?1)</code></td>
</tr>
</tbody>
</table>


## 注意事项

- 方法名称必须`findBy`开头
- 数据库字段名必须遵从下划线模式，如：name, add_time，驼峰命名方式不支持JPA查询
- 如果：IDEA下运行 JPA查询 mybatis报错 `Parameter 'arg0' not found`，参考这篇文章：https://www.cnblogs.com/JangoJing/p/10791831.html

maven maven-compiler-plugin插件加上`<parameters>true</parameters>`配置，如下所示：

```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-compiler-plugin</artifactId>
    <configuration>
        <parameters>true</parameters>
        <encoding>UTF-8</encoding>
    </configuration>
</plugin>
```

## 更多例子

```java
/**
 * 演示JPA findBy查询，根据方法名称自动生成查询语句，无需编写SQL
 * @author tanghc
 */
public interface StudentMapper extends CrudMapper<Student, Integer> {

    /**
     * … where x.lastname = ?1 and x.firstname = ?2
     * @param lastname
     * @param firstname
     * @return
     */
    List<Student> findByLastnameAndFirstname(String lastname, String firstname);

    /**
     *
     * … where x.lastname = ?1 or x.firstname = ?2
     * @param lastname
     * @param firstname
     * @return
     */
    List<Student> findByLastnameOrFirstname(String lastname, String firstname);

    /**
     * where x.firstname = ?
     * @param firstname
     * @return
     */
    List<Student> findByFirstname(String firstname);
    List<Student> findByFirstnameIs(String firstname);
    List<Student> findByFirstnameEquals(String firstname);

    /**
     * … where x.startDate between ?1 and ?2
     * @param begin
     * @param end
     * @return
     */
    List<Student> findByStartDateBetween(Date begin, Date end);

    /**
     * … where x.age < ?1
     * @param age
     * @return
     */
    List<Student> findByAgeLessThan(int age);

    /**
     * … where x.age <= ?1
     * @param age
     * @return
     */
    List<Student> findByAgeLessThanEqual(int age);

    /**
     * … where x.age > ?1
     * @param age
     * @return
     */
    List<Student> findByAgeGreaterThan(int age);

    /**
     * … where x.age >= ?1
      * @param age
     * @return
     */
    List<Student> findByAgeGreaterThanEqual(int age);

    /**
     * … where x.startDate > ?1
     * @param date
     * @return
     */
    List<Student> findByStartDateAfter(Date date);

    /**
     * … where x.startDate < ?1
     * @param date
     * @return
     */
    List<Student> findByStartDateBefore(Date date);


    /**
     * … where x.age is null
     * @return
     */
    List<Student> findByAgeNull();
    List<Student> findByAgeIsNull();


    /**
     * … where x.firstname like ?1
     * @param firstname
     * @return
     * @see #findByFirstnameContaining(String)
     */
    List<Student> findByFirstnameLike(String firstname);

    /**
     * … where x.firstname not like ?1
     * @param firstname
     * @return
     */
    List<Student> findByFirstnameNotLike(String firstname);

    /**
     * … where x.firstname like 'xx%'
     * @param firstname
     * @return
     */
    List<Student> findByFirstnameStartingWith(String firstname);

    /**
     * … where x.firstname like '%xx'
     * @param firstname
     * @return
     */
    List<Student> findByFirstnameEndingWith(String firstname);

    /**
     * 等同于like
     * … where x.firstname like '%xx%'
     * @param firstname
     * @return
     */
    List<Student> findByFirstnameContaining(String firstname);

    /**
     * … where x.age = ?1 order by x.lastname desc
     * @param age
     * @return
     */
    List<Student> findByAgeOrderByLastnameDesc(int age);


    /**
     * … where x.lastname <> ?1
     * @param lastname
     * @return
     */
    List<Student> findByLastnameNot(String lastname);

    /**
     * … where x.age in ?1
     *
     * @param ages
     * @return
     */
    List<Student> findByAgeIn(Collection<Integer> ages);

    /**
     * … where x.age not in ?1
     *
     * @param ages
     * @return
     */
    List<Student> findByAgeNotIn(Collection<Integer> ages);


    List<Student> findByAgeNotInAndIdIn(Collection<Integer> ages, List<Integer> ids);

    /**
     * … where x.active = 1
     * @return
     */
    List<Student> findByActiveTrue();

    /**
     * … where x.active = 0
     * @return
     */
    List<Student> findByActiveFalse();

    /**
     * … where UPPER(x.firstname) = UPPER(?1)
     * @param firstname
     * @return
     */
    List<Student> findByFirstnameIgnoreCase(String firstname);


    // 复杂的例子
    List<Student> findByLastnameOrFirstnameAndIdBetweenOrderByAgeDescIdAsc(String lastname, String firstname, int id1, int id2);

}
```
