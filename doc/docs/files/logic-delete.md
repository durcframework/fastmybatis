# 逻辑删除

即更新一个字段标记为已删除。查询的时候会自动过滤掉已删除的数据。

假设数据库表中有一个字段`is_deleted`类型为tinyint，0表示未删除，1表示已删除。

实体类对应代码如下：
```java
public class User {
    @Column(logicDelete = true)
    private Byte isDeleted;
}
```
在执行`mapper.deleteById(1L);`时会触发UPDATE语句，将is_deleted字段更新为1。

如果is_deleted类型为char(1)，f表示未删除，t表示已删除。

```java
@Column(notDeleteValue = "f", deleteValue = "t")
private String isDeleted;
```

`@Column`提供两个属性

- notDeleteValue：指定未删除时的值,不指定默认为0
- deleteValue：指定删除后保存的值,不指定默认为1


假设1表示未删除，2表示已删除，@Column的设置方法如下：@Column(notDeleteValue = "1", deleteValue = "2")。如果每个实体类都要这样设置的话会很麻烦，fastmybatis提供了全局配置

- springboot下，application.properties添加

```
# 未删除数据库保存的值，默认为0
mybatis.logic-not-delete-value=1
# 删除后数据库保存的值，默认为1
mybatis.logic-delete-value=2
```
- springmvc设置方式如下：
```xml
<!-- 替换org.mybatis.spring.SqlSessionFactoryBean -->
	<bean id="sqlSessionFactory"
		class="com.gitee.fastmybatis.core.ext.SqlSessionFactoryBeanExt">
		<property name="dataSource" ref="dataSource" />
		<property name="configLocation">
			<value>classpath:mybatis/mybatisConfig.xml</value>
		</property>
		<property name="mapperLocations">
			<list>
				<value>classpath:mybatis/mapper/*.xml</value>
			</list>
		</property>
		
		<!-- 以下是附加属性 -->
		
		<!-- dao所在的包名,跟MapperScannerConfigurer的basePackage一致 
			多个用;隔开
		-->
		<property name="basePackage" value="com.myapp.dao" />
		<property name="config">
			<bean class="com.gitee.fastmybatis.core.FastmybatisConfig">		
				<property name="logicNotDeleteValue" value="1"/>
                                <property name="logicDeleteValue" value="2"/>
			</bean>
		</property>
	</bean>
```

- 注：如果同时设置了@Column参数和全局配置，会优先读取注解中的配置。


## 物理删除

有些情况需要做物理删除，可用下面的方式做到：

```java
// DELETE FROM t_user WHERE id=?
int i = mapper.forceDeleteById(user.getId());
```

```java
// DELETE FROM t_user WHERE id>6
Query query = new Query();
query.gt("id", 6);
int i = mapper.forceDeleteByQuery(query);
```

## 查询忽略逻辑删除字段

设置了逻辑删除字段后，记录被删除就查不到了，但有些情况下又需要查到记录。可以使用`query.ignoreLogicDeleteColumn()`方法忽略逻辑删除字段。


```java
Query query = new Query()
        .in("id", Arrays.asList(1,2,3))
        // 忽略逻辑删除字段
        .ignoreLogicDeleteColumn();

List<User> list = list.list(query)
```

