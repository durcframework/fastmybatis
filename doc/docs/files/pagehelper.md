# 整合PageHelper

1. 添加pagehelper依赖

```xml
<dependency>
    <groupId>com.github.pagehelper</groupId>
    <artifactId>pagehelper</artifactId>
    <version>5.2.0</version>
</dependency>
```

2. 在mybatisConfig.xml中添加PageHelper插件

```xml
<plugins>
    <!-- com.github.pagehelper为PageHelper类所在包名 -->
    <plugin interceptor="com.github.pagehelper.PageInterceptor" />
</plugins>
```

application.properties指定configLocation

```properties
mybatis.config-location=classpath:mybatis/mybatisConfig.xml
```

如果是xml配置，则为：

```xml
<bean id="sqlSessionFactory" class="com.gitee.fastmybatis.core.ext.SqlSessionFactoryBeanExt">
    ...
    <property name="configLocation">
        <value>classpath:mybatis/mybatisConfig.xml</value>
    </property>
    ...
</bean>
```

测试用例：

下面是一个联表分页查询例子

```java
Query query = new Query();
// 添加查询条件
query.eq("username", "张三")
        // id倒叙
        .orderby("id", Sort.DESC);
Page<UserInfoDO> page = PageHelper.startPage(1, 5).doSelectPage(() -> mapper.findJoin(query));
List<UserInfoDO> result = page.getResult();
for (UserInfoDO userInfoDO : result) {
    System.out.println(userInfoDO);
}
System.out.println("页码 page.getPageNum():" + page.getPageNum());
System.out.println("总页数 page.getPageSize():" + page.getPageSize());
System.out.println("首行 page.getStartRow():" + page.getStartRow());
System.out.println("末行 page.getEndRow():" + page.getEndRow());
System.out.println("总记录数 page.getTotal():" + page.getTotal());
System.out.println("总页数 page.getPages():" + page.getPages());
```

Mapper接口

```java
List<UserInfoDO> findJoin(@Param("query") Query query);
```

SQL

```xml
<select id="findJoin"
        parameterType="com.gitee.fastmybatis.core.query.Query"
        resultType="com.myapp.entity.UserInfoDO">
    SELECT t.id, t.username, t.state, t2.city, t2.address
    FROM t_user t LEFT JOIN user_info t2 ON t.id = t2.user_id
    <include refid="common.where" />
    <include refid="common.orderBy" />
</select>
```

其中 `common.where`和`common.orderBy`是框架中自带的，配合Query使用，有些插件报错不用管它，不影响使用。

以上例子参考：com.myapp.TUserMapperTest.testJoinSqlWithPageHelper