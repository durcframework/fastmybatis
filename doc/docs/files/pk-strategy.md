# 主键策略设置

## 主键自增

适用于：mysql自增主键、sqlserver自增主键、oracle（触发器）

数据库主键设置自增后，这样设置：

```java
// strategy = PkStrategy.INCREMENT 自增策略
@Table(name = "t_user", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
public class TUser {
}
```
这样在做insert后，id会自动填充自增后的值。

## 主键使用sequence（oracle）

适用于oracle表序列

```sql
-- 创建表sequence，名字为t_user_seq
create sequence t_user_seq start with 1 increment by 1;
```

使用如下注解：

```java
@Table(name = "t_user", pk = @Pk(name = "id", sequenceName = "t_user_seq"))
public class TUser {
}
```

## 主键使用uuid

数据库主键是varchar类型，insert后自动填充uuid，并返回。

```java
@Table(name = "log", pk = @Pk(name = "log_id", strategy = PkStrategy.UUID/*配置主键UUID*/))
public class Log {
    private String logId;
}
```

这样在做insert后，id字段会自动填充uuid。

- 注：uuid的生成方式是调用数据库底层实现，如MySql的实现方式为： SELECT UUID()

## 自定义uuid

如果不希望使用底层数据库的uuid，可自定义自己实现，实现方式如下：

- 首先设置`strategy = PkStrategy.NONE`表示无策略

```java
// strategy = PkStrategy.NONE 无策略
@Table(name = "log", pk = @Pk(name = "log_id", strategy = PkStrategy.NONE))
public class Log {
    private String logId;
}
```

- 创建一个类UUIDFill并继承`CustomIdFill`类

```java
public class UUIDFill extends CustomIdFill<String> {

    @Override
    public String getColumnName() {
        return "log_id"; // 作用在id字段上
    }

    @Override
    protected Object getFillValue(String defaultValue) {
        return Optional.ofNullable(defaultValue).orElse(UUID.randomUUID().toString()); // 自定义的uuid生成方式
    }
}
```

-  在application.properties中添加

```
# key:填充器全路径类名,value:构造函数参数值,没有可不填
mybatis.fill.com.xx.aa.UUIDFill=
```
格式为mybatis.fill.类路径=构造参数(没有可不填)

到此已经可以了，当进行insert操作后，id字段会自动插入自定义的uuid。同时id值回写到实体类中，可直接`entity.getLogId()`获取。

