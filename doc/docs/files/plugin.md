# 自带插件

## SQL打印插件

默认mybatis打印SQL参数部分会带上'?'，复制SQL到数据库客户端无法直接执行。

2.4.7自带了一个SQL格式化打印插件，可以打印完整的SQL（参数替换问号）

使用方式：

新增插件

```xml
<plugins>
    <plugin interceptor="com.gitee.fastmybatis.core.support.plugin.SqlFormatterPlugin" />
</plugins>
```

插件属性【可选】

```xml
<plugins>
    <plugin interceptor="com.gitee.fastmybatis.core.support.plugin.SqlFormatterPlugin">
        <!-- 以下配置皆为默认值 -->
        <!-- 打印SQL末尾自动添加结束符（;） -->
        <property name="appendDelimiter" value="true" />
        <!-- 指定结束符 -->
        <property name="delimiter" value=";" />
        <!-- 是否使用Druid -->
        <property name="useDruid" value="false" />
        <!-- format内容，%s表示SQL占位符 -->
        <property name="format" value="====== SQL ======\n%s" />
    </plugin>
</plugins>
```

配置文件指定

```properties
# 禁用mybatis自带sql打印，使用插件打印sql
logging.level.com.xxx.dao=error
logging.level.com.gitee.fastmybatis=warn
# 使用插件打印sql，默认：false
mybatis.print-sql=true
```


打印内容如下所示：

```text
====== SQL ======
SELECT
    t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money` 
FROM
    `t_user` t 
WHERE
    t.`id` = 6 AND t.isdel = 0 LIMIT 1;
```
