# 通用Service

通用Service有两种形式：`继承父类`，`实现接口`

## 继承父类


```java
@Service
public class UserService extends BaseService<TUser/*实体类*/, Integer/*主键类型*/, TUserMapper/*Mapper*/> {

}
```

继承之后，Service具备Mapper接口中方法，可以直接用来操作数据库。

```java
@Autowired
private UserService userService;

TUser user = userSerivce.getById(6);
Assert.assertNotNull(user);

            
```

也可以调用`getMapper()`返回对应的mapper，一般用于执行自定义SQL

```java
TUserMapper userMaper = userService.getMapper();
// 执行自定义SQL
userMaper.xxx();
```

## 实现接口

如果Service已经继承了其它父类，可以通过实现接口来解决

```java
@Service
public class UserService implements CommonService<TUser/*实体类*/, Integer/*主键类型*/, TUserMapper/*Mapper*/> {

}
```

两种实现方式没有什么区别
