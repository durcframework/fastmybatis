# 简介

fastmybatis是一个mybatis开发框架，其宗旨为：简单、快速、有效。

- 零配置快速上手
- 无需编写xml文件即可完成CRUD操作
- 支持mysql，sqlserver，oracle，postgresql,sqlite
- 支持自定义sql，sql语句可写在注解中或xml中
- 支持与spring-boot集成，依赖starter即可
- 轻量级，无侵入性，是官方mybatis的一种扩展


## 快速开始（springboot）

假设有张表如下：

```sql
CREATE TABLE IF NOT EXISTS `student`
(
  `id`        INTEGER PRIMARY KEY auto_increment,
  `firstname` varchar(64) DEFAULT NULL COMMENT '名',
  `lastname` varchar(64) DEFAULT NULL COMMENT '姓',
  `age` int default null COMMENT '年龄',
  `active` tinyint(4) DEFAULT NULL COMMENT '状态',
  `start_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '开始日期'
);

INSERT  INTO `student`
VALUES (1,'Jim', 'Green', 22, 1, '2018-02-21 10:37:00'),
(2,'三', '张', 30, 1, '2019-11-21 11:37:44'),
(3,'四', '张', 40, 1, '2017-12-21 12:55:00'),
(4,'五一', '李', 40, 1, null),
(5,'六', '张', null, 0, '2016-07-21 10:44:00');
```

- 新建一个springboot项目

在resources下面创建一个文件：`schema.sql`，并将上面的SQL语句放进来

- pom.xml添加fastmybatis-spring-boot-starter

```xml
<dependency>
    <groupId>io.gitee.durcframework</groupId>
    <artifactId>fastmybatis-spring-boot-starter</artifactId>
    <version>3.0.17</version>
</dependency>

<!-- h2 数据库 -->
<dependency>
    <groupId>com.h2database</groupId>
    <artifactId>h2</artifactId>
    <version>1.4.199</version>
</dependency>
```

application.properties添加数据库配置

```properties
spring.datasource.driver-class-name=org.h2.Driver
spring.datasource.url=jdbc:h2:mem:test
spring.datasource.schema=classpath:schema.sql
spring.datasource.username=root
spring.datasource.password=root
```

添加实体类：

```java
@Table(name = "student", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
public class Student {
    private Integer id;
    private String firstname;
    private String lastname;
    private Integer age;
    private Byte active;
    private Date startDate;
    
    省略getter setter
}
```

添加Mapper

```java
public interface StudentMapper extends CrudMapper<Student, Integer> {

}
```

编写测试用例

```java
@RunWith(SpringRunner.class)
@SpringBootTest
public class StudentServiceTest {

    @Resource
    private StudentMapper studentMapper;

    @Test
    public void list() {
        LambdaQuery<Student> query = Query.create(Student.class)
                .eq(Student::getLastname, "张");
        List<Student> list = studentMapper.list(query);
        for (Student student : list) {
            System.out.println(student);
        }
    }

}
```

