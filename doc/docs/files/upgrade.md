# 1.x升级到2.0

实体类进行全局替换：

`javax.persistence.Table` 替换成：`com.gitee.fastmybatis.annotation.Table`

`javax.persistence.Column` 替换成：`com.gitee.fastmybatis.annotation.Column`

移除：`javax.persistence.Transient`注解，改成`transient`关键字

移除：`javax.persistence.Id`注解，改成：`@Table(name = "t_user", pk = @Pk(name = "id"))`

移除：`javax.persistence.GeneratedValue`注解，改成：`@Table(name = "t_user", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))`