package com.gitee.fastmybatis.annotation;

import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>
 * 逻辑删除策略
 * </p>
 *
 * @author youbeiwuhuan
 * @version 1.0.0
 * @date 2023/4/23 14:43
 */

public enum LogicDeleteStrategy {

    /**
     * 固定值策略,notDeleteValue和deleteValue生效
     */
    FIXED_VALUE("fixed_value"),


    /**
     * 删除时主键填充逻辑删除字段策略
     */
    ID_FILL("id_fill"),

    ;

    private final String code;


    LogicDeleteStrategy(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
