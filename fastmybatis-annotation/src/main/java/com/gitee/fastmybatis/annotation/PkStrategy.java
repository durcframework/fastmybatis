package com.gitee.fastmybatis.annotation;

/**
 * 主键策略
 *
 * @author thc
 * @since 2.0.0
 */
public enum PkStrategy {
    /**
     * 自增
     */
    INCREMENT,

    /**
     * uuid
     */
    UUID,

    /**
     * 无策略，手动指定主键值
     */
    NONE

}
