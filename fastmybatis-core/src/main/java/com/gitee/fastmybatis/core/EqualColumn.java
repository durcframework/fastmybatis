package com.gitee.fastmybatis.core;

/**
 * @author thc
 */
public class EqualColumn {

    /**
     * 数据库字段名
     */
    private final String columnName;

    /**
     * 更新值
     */
    private final Object value;

    public static EqualColumn of(String columnName, Object value) {
        return new EqualColumn(columnName, value);
    }

    public EqualColumn(String columnName, Object value) {
        this.columnName = columnName;
        this.value = value;
    }

    public String getColumnName() {
        return columnName;
    }

    public Object getValue() {
        return value;
    }
}
