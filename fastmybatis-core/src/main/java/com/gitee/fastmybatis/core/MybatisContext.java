package com.gitee.fastmybatis.core;

import com.gitee.fastmybatis.core.ext.info.TableInfo;
import com.gitee.fastmybatis.core.support.Dialect;
import com.gitee.fastmybatis.core.util.ClassUtil;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tanghc
 */
public class MybatisContext {

    // key: entityClass, value: mapperClass
    private static final Map<Class<?>, Class<?>> entityMapper = new HashMap<>(16);

    // key：mapperClass, value: entityClass
    private static final Map<Class<?>, Class<?>> mapperEntity = new HashMap<>(16);


    private static final Map<Class<?>, SqlSessionFactory> sqlSessionFactoryMapForMapper = new HashMap<>();

    // KEY: mapperClass
    private static final Map<Class<?>, TableInfo> tableInfoMap = new HashMap<>();

    public static void addMapperClass(Collection<Class<?>> mapperClasses) {
        for (Class<?> mapperClass : mapperClasses) {
            Class<?> entityClass = getEntityClass(mapperClass);
            entityMapper.put(entityClass, mapperClass);
            mapperEntity.put(mapperClass, entityClass);
        }
    }

    public static Class<?> getMapperClassByEntityClass(Class<?> entityClass) {
        return entityMapper.get(entityClass);
    }

    public static Class<?> getEntityClassByMapperClass(Class<?> mapperClass) {
        mapperClass = ClassUtil.getRealMapperClass(mapperClass);
        return mapperEntity.get(mapperClass);
    }

    private static Class<?> getEntityClass(Class<?> mapperClass) {
        String name = mapperClass.getName();
        if (mapperClass.isInterface() || name.startsWith(ClassUtil.PROXY_PREFIX)) {
            return ClassUtil.getSuperInterfaceGenericType(mapperClass, 0);
        } else {
            return ClassUtil.getSuperClassGenericType(mapperClass, 0);
        }
    }

    public static void addSqlSessionFactory(Class<?> mapperClass, SqlSessionFactory sqlSessionFactory) {
        sqlSessionFactoryMapForMapper.put(mapperClass, sqlSessionFactory);
    }

    public static SqlSessionFactory getSqlSessionFactoryByMapperClass(Class<?> mapperClass) {
        return sqlSessionFactoryMapForMapper.get(mapperClass);
    }

    public static void addTableInfo(Class<?> mapperClass, TableInfo tableInfo) {
        tableInfoMap.put(mapperClass, tableInfo);
    }

    public static TableInfo getTableInfo(Class<?> mapperClass) {
        return tableInfoMap.get(mapperClass);
    }

    public static void clearTableInfo() {
        tableInfoMap.clear();
    }
}
