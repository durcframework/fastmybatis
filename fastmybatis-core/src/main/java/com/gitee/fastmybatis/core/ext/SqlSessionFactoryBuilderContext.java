package com.gitee.fastmybatis.core.ext;

import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

/**
 * @author thc
 */
public class SqlSessionFactoryBuilderContext {

    private final SqlSessionFactoryBuilder sqlSessionFactoryBuilder;

    private final Configuration configuration;

    private final MapperLocationsBuilder mapperLocationsBuilder;

    private SqlSessionFactory sqlSessionFactory;



    public SqlSessionFactoryBuilderContext(MapperLocationsBuilder mapperLocationsBuilder, SqlSessionFactoryBuilder sqlSessionFactoryBuilder, Configuration configuration) {
        this.sqlSessionFactoryBuilder = sqlSessionFactoryBuilder;
        this.configuration = configuration;
        this.mapperLocationsBuilder = mapperLocationsBuilder;
    }

    public SqlSessionFactory getSqlSessionFactory() {
        if (this.sqlSessionFactory == null) {
            sqlSessionFactory = this.sqlSessionFactoryBuilder.build(configuration);
            mapperLocationsBuilder.afterBuild(sqlSessionFactory);
        }
        return sqlSessionFactory;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

}
