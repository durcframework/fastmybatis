package com.gitee.fastmybatis.core.ext.code.client;

import com.gitee.fastmybatis.core.FastmybatisConfig;
import com.gitee.fastmybatis.core.MybatisContext;

/**
 * 客户端参数
 * @author tanghc
 */
public class ClientParam {
	private Class<?> mapperClass;
	/** 模板资源 */
	private String templateContent;
	private String globalVmLocation;
	private FastmybatisConfig config;

	public Class<?> getEntityClass() {
		return MybatisContext.getEntityClassByMapperClass(mapperClass);
	}

	public String getGlobalVmPlaceholder() {
		return this.config.getGlobalVmPlaceholder();
	}

	public String getGlobalVmLocation() {
		return globalVmLocation;
	}

	public void setGlobalVmLocation(String globalVmLocation) {
		this.globalVmLocation = globalVmLocation;
	}

	public Class<?> getMapperClass() {
		return mapperClass;
	}

	public void setMapperClass(Class<?> mapperClass) {
		this.mapperClass = mapperClass;
	}

	public FastmybatisConfig getConfig() {
		return config;
	}

	public void setConfig(FastmybatisConfig config) {
		this.config = config;
	}

	public String getCountExpression() {
		return this.config.getCountExpression();
	}

	public String getTemplateContent() {
		return templateContent;
	}

	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}
}
