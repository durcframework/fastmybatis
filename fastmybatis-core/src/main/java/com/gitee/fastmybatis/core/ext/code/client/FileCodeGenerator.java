package com.gitee.fastmybatis.core.ext.code.client;

import com.gitee.fastmybatis.core.MybatisContext;
import com.gitee.fastmybatis.core.ext.code.NotEntityException;
import com.gitee.fastmybatis.core.ext.code.generator.ColumnDefinition;
import com.gitee.fastmybatis.core.ext.code.generator.MapperContext;
import com.gitee.fastmybatis.core.ext.code.generator.TableDefinition;
import com.gitee.fastmybatis.core.ext.code.generator.TableSelector;
import com.gitee.fastmybatis.core.ext.code.util.VelocityUtil;
import com.gitee.fastmybatis.core.ext.info.ColumnInfo;
import com.gitee.fastmybatis.core.ext.info.TableInfo;
import com.gitee.fastmybatis.core.ext.spi.SpiContext;
import com.gitee.fastmybatis.core.util.IOUtil;
import com.gitee.fastmybatis.core.util.StringUtil;
import org.apache.ibatis.io.Resources;
import org.apache.velocity.VelocityContext;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * 代码生成器，根据定义好的velocity模板生成代码
 *
 * @author tanghc
 */
public class FileCodeGenerator {

    public String generateCode(ClientParam clientParam) throws NotEntityException, IOException {
        String finalContent = this.buildTemplateInputStream(clientParam);
        MapperContext sqlContext = this.buildClientSQLContextList(clientParam);
        VelocityContext context = new VelocityContext();

        TableDefinition tableDefinition = sqlContext.getTableDefinition();

        initTableInfo(clientParam, tableDefinition);

        // vm模板中的变量
        context.put("context", sqlContext);
        context.put("table", tableDefinition);
        context.put("pk", tableDefinition.getPkColumn());
        context.put("columns", tableDefinition.getTableColumns());
        context.put("allColumns", tableDefinition.getAllColumns());
        context.put("countExpression", clientParam.getCountExpression());
        context.put("associations", tableDefinition.getAssociationDefinitions());

        return VelocityUtil.generate(context, finalContent);
    }

    private void initTableInfo(ClientParam clientParam, TableDefinition tableDefinition) {
        Class<?> mapperClass = clientParam.getMapperClass();
        TableInfo tableInfo = new TableInfo();
        tableInfo.setTableName(tableDefinition.getTableName());
        tableInfo.setDialect(clientParam.getConfig().getDialect());
        boolean hasLogicDeleteColumn = tableDefinition.getHasLogicDeleteColumn();
        tableInfo.setHasLogicDeleteColumn(hasLogicDeleteColumn);
        if (hasLogicDeleteColumn) {
            ColumnDefinition logicDeleteColumn = tableDefinition.getLogicDeleteColumn();
            tableInfo.setLogicDeleteColumnName(logicDeleteColumn.getColumnName());
            tableInfo.setLogicDeleteValue(logicDeleteColumn.getLogicDeleteValue());
            tableInfo.setLogicNotDeleteValue(logicDeleteColumn.getLogicNotDeleteValue());
        }
        List<ColumnDefinition> columnDefinitions = tableDefinition.getColumnDefinitions();
        List<ColumnInfo> columnInfos = SpiContext.getBeanExecutor().copyBean(columnDefinitions, ColumnInfo.class);
        tableInfo.setColumnInfos(columnInfos);
        MybatisContext.addTableInfo(mapperClass, tableInfo);
    }

    /**
     * 返回模板文件内容
     *
     * @throws IOException
     */
    private String buildTemplateInputStream(ClientParam clientParam) throws IOException {
        // 模板文件
        String templateContent = clientParam.getTemplateContent();
        try {
            if (StringUtil.hasText(clientParam.getGlobalVmLocation())) {
                // 全局模板文件
                InputStream inputStream = Resources.getResourceAsStream(clientParam.getGlobalVmLocation());
                return this.mergeGlobalVm(templateContent, IOUtil.toString(inputStream, StandardCharsets.UTF_8), clientParam.getGlobalVmPlaceholder());
            } else {
                return templateContent;
            }
        } catch (IOException e) {
            throw e;
        }
    }

    /**
     * 合并全局模板,globalVmResource的内容合并到vmResource中
     */
    private String mergeGlobalVm(String templateContent, String globalVmContent, String placeholder) throws IOException {
        return templateContent.replace(placeholder, globalVmContent);
    }

    /**
     * 返回SQL上下文列表
     *
     * @param clientParam 参数
     * @return 返回SQL上下文
     * @throws NotEntityException
     */
    private MapperContext buildClientSQLContextList(ClientParam clientParam) throws NotEntityException {
        Class<?> entityClass = clientParam.getEntityClass();
        if (entityClass == Object.class || entityClass == Void.class) {
            throw new NotEntityException();
        }
        TableSelector tableSelector = new TableSelector(entityClass, clientParam.getConfig());

        TableDefinition tableDefinition = tableSelector.getTableDefinition();

        MapperContext context = new MapperContext(tableDefinition);

        String namespace = this.buildNamespace(clientParam.getMapperClass());
        context.setClassName(entityClass.getName());
        context.setClassSimpleName(entityClass.getSimpleName());
        context.setPackageName(entityClass.getPackage().getName());
        context.setNamespace(namespace);

        return context;
    }

    private String buildNamespace(Class<?> mapperClass) {
        return mapperClass.getName();
    }

}
