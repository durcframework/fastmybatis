package com.gitee.fastmybatis.core.ext.code.generator;

import com.gitee.fastmybatis.annotation.Column;
import com.gitee.fastmybatis.core.FastmybatisConfig;
import com.gitee.fastmybatis.core.FastmybatisContext;
import com.gitee.fastmybatis.core.MybatisContext;
import com.gitee.fastmybatis.core.ext.code.util.FieldUtil;
import com.gitee.fastmybatis.core.ext.code.util.JavaTypeUtil;
import com.gitee.fastmybatis.core.ext.code.util.ReflectUtil;
import com.gitee.fastmybatis.core.ext.exception.GenCodeException;
import com.gitee.fastmybatis.core.ext.info.EntityInfo;
import com.gitee.fastmybatis.core.handler.BaseEnum;
import com.gitee.fastmybatis.core.handler.BaseFill;
import com.gitee.fastmybatis.core.handler.BaseFormatter;
import com.gitee.fastmybatis.core.handler.EnumTypeHandler;
import com.gitee.fastmybatis.core.handler.FillType;
import com.gitee.fastmybatis.core.util.ClassUtil;
import com.gitee.fastmybatis.core.util.StringUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 字段查询
 *
 * @author tanghc
 */
public class ColumnSelector {

    private static final String SELECT_GET = "%s.getById";
    private static final String FETCH_GET = "%s.forceById";

    private final Class<?> entityClass;
    private final FastmybatisConfig config;

    public ColumnSelector(Class<?> entityClass, FastmybatisConfig config) {
        super();
        this.entityClass = entityClass;
        this.config = config;
    }

    /**
     * javaBean字段类型，String，Integer等
     *
     * @param field 字段
     * @return 返回简单类型
     */
    private String getColumnType(Field field) {
        String columnType = field.getType().getSimpleName();
        if ("Object".equals(columnType)) {
            columnType = ClassUtil.getSuperClassGenericType(this.entityClass, 0).getSimpleName();
        }
        return columnType;
    }

    /**
     * 返回javaBean字段完整类型，java.lang.String
     *
     * @param field 字段
     * @return 返回完整的类型
     */
    private String getColumnFullType(Field field) {
        String fullType = field.getType().getName();
        if ("java.lang.Object".equals(fullType)) {
            fullType = ClassUtil.getSuperClassGenericType(this.entityClass, 0).getSimpleName();
        }
        return fullType;
    }

    private boolean isEnum(Field field) {
        Class<?> enumType = field.getType();
        return enumType.isEnum();
    }

    private void checkEnum(Class<?> enumType) {
        boolean isBaseEnum = false;
        Class<?> baseEnumClass = BaseEnum.class;
        Type[] arr = enumType.getInterfaces();

        for (Type type : arr) {
            if (type.equals(baseEnumClass)) {
                isBaseEnum = true;
                break;
            }
        }

        if (!isBaseEnum) {
            throw new GenCodeException("枚举类：" + enumType.getName() + "必须实现" + baseEnumClass.getName() + "接口");
        }
    }

    /**
     * 根据java字段获取数据库字段名
     */
    private String getColumnName(Field field) {
        return FieldUtil.getColumnName(field, config);
    }

    /**
     * 构建columnDefinition
     *
     * @return 返回字段定义
     */
    public List<ColumnDefinition> getColumnDefinitions() {
        List<Field> fields = ReflectUtil.getDeclaredFields(entityClass);
        List<ColumnDefinition> columnDefinitionList = new ArrayList<>(fields.size());
        int pkCount = 0;
        for (Field field : fields) {
            ColumnDefinition columnDefinition = buildColumnDefinition(field);
            if (columnDefinition != null) {
                columnDefinitionList.add(columnDefinition);
                if (columnDefinition.getIsPk()) {
                    pkCount++;
                }
            }
        }
        if (pkCount == 0) {
            Optional<ColumnDefinition> id = columnDefinitionList
                    .stream()
                    .filter(columnDefinition -> config.getGlobalIdName().equals(columnDefinition.getColumnName()))
                    .findFirst();
            // 如果找不到主键，字段名为id的作为自增主键
            if (id.isPresent()) {
                ColumnDefinition pk = id.get();
                pk.setIsPk(true);
                pk.setIsIdentity(config.isGlobalIdIncrement());
            } else {
                // 否则取第一个字段当主键
                ColumnDefinition first = columnDefinitionList.get(0);
                first.setIsPk(true);
            }
        }
        return columnDefinitionList;
    }

    /**
     * 返回一对一关联配置
     *
     * @return 返回一对一关联配置
     */
    public List<AssociationDefinition> getAssociationDefinitions() {
        List<Field> fields = ReflectUtil.getDeclaredFields(entityClass);
        List<AssociationDefinition> associations = new ArrayList<>(8);
        for (Field field : fields) {
            AssociationDefinition associationDefinition = buildAssociationDefinition(field);
            if (associationDefinition != null) {
                associations.add(associationDefinition);
            }
        }
        return associations;
    }

    protected AssociationDefinition buildAssociationDefinition(Field field) {
        boolean isTransient = FieldUtil.isTransientField(field);
        if (isTransient) {
            return null;
        }
        if (!FieldUtil.hasTableAnnotation(field)) {
            return null;
        }
        Class<?> clazz = field.getType();
        String column = FieldUtil.getLazyEntityColumnName(field, config);
        if (StringUtil.isEmpty(column)) {
            return null;
        }
        String property = field.getName();
        Class<?> mapperClass = MybatisContext.getMapperClassByEntityClass(clazz);
        String namespace = mapperClass.getName();
        String temp = config.getIgnoreLogicDeleteWithAssociation() ? FETCH_GET : SELECT_GET;
        String select = String.format(temp, namespace);

        AssociationDefinition associationDefinition = new AssociationDefinition();
        associationDefinition.setColumn(column);
        associationDefinition.setProperty(property);
        associationDefinition.setSelect(select);

        return associationDefinition;
    }

    /**
     * 构建列信息
     *
     * @param field 字段信息
     * @return 返回构建列信息
     */
    protected ColumnDefinition buildColumnDefinition(Field field) {
        boolean isTransient = FieldUtil.isTransientField(field);
        if (isTransient) {
            return null;
        }
        ColumnDefinition columnDefinition = new ColumnDefinition();
        columnDefinition.setDialect(this.config.getDialect());
        String columnName = this.getColumnName(field);
        String columnType = this.getColumnType(field);
        String fullType = this.getColumnFullType(field);
        boolean isEnum = this.isEnum(field);
        BaseFill<?> fill = getFill(columnName, field);
        if (isEnum && fill == null) {
            this.checkEnum(field.getType());
        }
        // 不是枚举,也不是java类型
        if (!isEnum && !JavaTypeUtil.isJavaType(columnType) && fill == null) {
            return null;
        }

        columnDefinition.setJavaFieldName(field.getName());
        columnDefinition.setColumnName(columnName);
        columnDefinition.setType(columnType);
        columnDefinition.setFullType(fullType);
        columnDefinition.setEnum(isEnum);
        columnDefinition.setIgnoreUpdate(this.config.getIgnoreUpdateColumns().contains(columnName));

        if (isEnum) {
            columnDefinition.setTypeHandler(EnumTypeHandler.class.getName());
            columnDefinition.setFillType(FillType.UPDATE);
        }

        boolean isPk = FieldUtil.isPk(field, config);
        columnDefinition.setIsPk(isPk);
        if (isPk) {
            String sequenceName = FieldUtil.getSequenceName(field);
            if (StringUtil.hasText(sequenceName)) {
                // 是否使用sequence
                columnDefinition.setSequenceName(sequenceName);
            } else if (FieldUtil.isIncrement(field)) {
                // 是否自增
                columnDefinition.setIsIdentity(true);
            } else if (FieldUtil.isUuid(field)) {
                // 是否使用uuid
                columnDefinition.setIsUuid(true);
                columnDefinition.setIsAuto(false);
            } else {
                // 自定义策略
                columnDefinition.setIsAuto(true);
            }
            this.setEntityInfo(columnDefinition);
        }

        boolean isVersionColumn = FieldUtil.isVersionColumn(field);
        columnDefinition.setIsVersion(isVersionColumn);

        this.bindLogicDeleteColumnInfo(columnDefinition, field);

        this.bindFill(columnDefinition, fill);

        return columnDefinition;
    }


    private void setEntityInfo(ColumnDefinition columnDefinition) {
        EntityInfo entityInfo = new EntityInfo();
        entityInfo.setDialect(columnDefinition.getDialect());
        entityInfo.setPkColumnName(columnDefinition.getColumnName());
        entityInfo.setPkJavaName(columnDefinition.getJavaFieldName());
        FastmybatisContext.setEntityInfo(entityClass, entityInfo);
    }

    /**
     * 绑定逻辑删除字段信息
     */
    private void bindLogicDeleteColumnInfo(ColumnDefinition columnDefinition, Field field) {
        LogicDeleteDefinition logicDeleteDefinition = FieldUtil.getLogicDeleteDefinition(field);
        // 不是逻辑删除字段
        if (logicDeleteDefinition == null) {
            return;
        }
        columnDefinition.setIsLogicDelete(true);
        Object delVal, notDelVal;
        String deleteValue = logicDeleteDefinition.getDeleteValue();
        String notDeleteValue = logicDeleteDefinition.getNotDeleteValue();
        String logicDeleteStrategy = logicDeleteDefinition.getLogicDeleteStrategy();
        // 如果没有指定则使用全局配置的值
        if ("".equals(deleteValue)) {
            deleteValue = this.config.getLogicDeleteValue();
        }

        if ("".equals(notDeleteValue)) {
            notDeleteValue = this.config.getLogicNotDeleteValue();
        }

        if ("".equals(logicDeleteStrategy)) {
            logicDeleteStrategy = this.config.getLogicDeleteStrategy();
        }


        delVal = StringUtil.isInteger(deleteValue) ? Integer.valueOf(deleteValue) : deleteValue;
        columnDefinition.setLogicDeleteValue(delVal);
        notDelVal = StringUtil.isInteger(notDeleteValue) ? Integer.valueOf(notDeleteValue) : notDeleteValue;
        columnDefinition.setLogicNotDeleteValue(notDelVal);
        columnDefinition.setLogicDeleteStrategy(logicDeleteStrategy);
    }

    private BaseFill<?> getFill(String columnName, Field field) {
        Column column = field.getAnnotation(Column.class);
        if (column != null) {
            Class<?> formatClass = column.formatClass();
            if (formatClass != Void.class) {
                if (!BaseFormatter.class.isAssignableFrom(formatClass)) {
                    throw new IllegalArgumentException("类 " + formatClass.getName() + " 必须继承 " + BaseFormatter.class.getName() + " 类");
                }
                return (BaseFill<?>) ClassUtil.newInstance(formatClass);
            }
        }
        return config.getFill(entityClass, field, columnName);
    }

    private void bindFill(ColumnDefinition columnDefinition, BaseFill<?> fill) {
        if (fill != null) {
            FillType fillType = fill.getFillType();
            columnDefinition.setTypeHandler(fill.getClass().getName());
            columnDefinition.setFillType(fillType);
            columnDefinition.setIsCustomFill(fillType == FillType.INSERT || fillType == FillType.UPDATE);
        }
    }


}
