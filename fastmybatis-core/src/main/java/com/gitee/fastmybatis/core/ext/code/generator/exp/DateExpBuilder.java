package com.gitee.fastmybatis.core.ext.code.generator.exp;

import java.time.LocalDate;

class DateExpBuilder implements ExpBuilder {

    @Override
    public String build() {
        return LocalDate.now().toString();
    }
}