package com.gitee.fastmybatis.core.ext.code.generator.exp;

import com.gitee.fastmybatis.core.util.DateUtil;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

class DateTimeExpBuilder implements ExpBuilder {

        @Override
        public String build() {
            return LocalDateTime.now().format(DateTimeFormatter.ofPattern(DateUtil.datetimePattern));
        }
    }