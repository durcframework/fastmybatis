package com.gitee.fastmybatis.core.ext.code.generator.exp;

interface ExpBuilder {
    String build();
}