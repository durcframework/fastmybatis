package com.gitee.fastmybatis.core.ext.code.generator.exp;

public class ExpFactory {

    public static String build(String exp) {
        switch (exp) {
            case "#datetime": return new DateTimeExpBuilder().build();
            case "#date": return new DateExpBuilder().build();
            default: {
                return exp;
            }
        }
    }

}
