package com.gitee.fastmybatis.core.ext.info;

/**
 * @author tanghc
 */
public class ColumnInfo {

    /**
     * java字段名
     */
    private String javaFieldName;
    /**
     * 数据库字段名
     */
    private String columnName;
    /**
     * javaBean字段类型，String，Integer等
     */
    private String type;

    public String getJavaFieldName() {
        return javaFieldName;
    }

    public void setJavaFieldName(String javaFieldName) {
        this.javaFieldName = javaFieldName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
