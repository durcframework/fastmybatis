package com.gitee.fastmybatis.core.ext.info;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author tanghc
 */
public class TableInfo {
    private String tableName;

    private String pkName;

    private boolean hasLogicDeleteColumn;

    private String logicDeleteColumnName;

    /**
     * 删除值
     */
    private Object logicDeleteValue;

    /**
     * 未删除值
     */
    private Object logicNotDeleteValue;

    private String dialect;

    public String getDialect() {
        return dialect;
    }

    public void setDialect(String dialect) {
        this.dialect = dialect;
    }

    private List<ColumnInfo> columnInfos = Collections.emptyList();

    public ColumnInfo getColumnInfoByColumnName(String columnName) {
        for (ColumnInfo columnInfo : columnInfos) {
            if (Objects.equals(columnInfo.getColumnName(), columnName)) {
                return columnInfo;
            }
        }
        return null;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getPkName() {
        return pkName;
    }

    public void setPkName(String pkName) {
        this.pkName = pkName;
    }

    public boolean isHasLogicDeleteColumn() {
        return hasLogicDeleteColumn;
    }

    public void setHasLogicDeleteColumn(boolean hasLogicDeleteColumn) {
        this.hasLogicDeleteColumn = hasLogicDeleteColumn;
    }

    public Object getLogicDeleteValue() {
        return logicDeleteValue;
    }

    public void setLogicDeleteValue(Object logicDeleteValue) {
        this.logicDeleteValue = logicDeleteValue;
    }

    public Object getLogicNotDeleteValue() {
        return logicNotDeleteValue;
    }

    public void setLogicNotDeleteValue(Object logicNotDeleteValue) {
        this.logicNotDeleteValue = logicNotDeleteValue;
    }

    public String getLogicDeleteColumnName() {
        return logicDeleteColumnName;
    }

    public void setLogicDeleteColumnName(String logicDeleteColumnName) {
        this.logicDeleteColumnName = logicDeleteColumnName;
    }

    public List<ColumnInfo> getColumnInfos() {
        return columnInfos;
    }

    public void setColumnInfos(List<ColumnInfo> columnInfos) {
        this.columnInfos = columnInfos;
    }
}
