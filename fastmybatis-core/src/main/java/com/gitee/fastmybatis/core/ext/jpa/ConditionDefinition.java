package com.gitee.fastmybatis.core.ext.jpa;

import java.util.List;

/**
 * @author tanghc
 */
public class ConditionDefinition {

    private List<ConditionWrapper> conditionWrappers;

    private String orderBy;

    public ConditionDefinition(List<ConditionWrapper> conditionWrappers, String orderBy) {
        this.conditionWrappers = conditionWrappers;
        this.orderBy = orderBy;
    }

    public List<ConditionWrapper> getConditionWrappers() {
        return conditionWrappers;
    }

    public void setConditionWrappers(List<ConditionWrapper> conditionWrappers) {
        this.conditionWrappers = conditionWrappers;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }
}
