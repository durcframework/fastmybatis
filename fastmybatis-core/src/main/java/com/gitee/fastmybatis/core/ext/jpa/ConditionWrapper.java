package com.gitee.fastmybatis.core.ext.jpa;

public class ConditionWrapper {

    /**
     * 数据库字段名
     */
    private final String column;

    private final JpaKeyword keyword;

    private final String joint;

    public ConditionWrapper(String column, JpaKeyword keyword, String joint) {
        this.column = column;
        this.keyword = keyword;
        this.joint = joint;
    }

    public String getColumn() {
        return column;
    }

    public JpaKeyword getKeyword() {
        return keyword;
    }

    public String getJoint() {
        return joint;
    }
}