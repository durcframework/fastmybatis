package com.gitee.fastmybatis.core.ext.jpa;

import java.util.Arrays;
import java.util.List;

public enum JpaKeyword {
    equals("=?"),
    is("=?"),
    between("BETWEEN ? AND ?"),
    less_than(cdata("<") + "?"),
    before(cdata("<") + "?"),
    less_than_equal(cdata("<=") + "?"),
    greater_than(cdata(">") + "?"),
    after(cdata(">") + "?"),
    greater_than_equal(cdata(">=") + "?"),
    is_null("IS NULL"),
    Null("null", "IS NULL"),
    is_not_null("IS NOT NULL"),
    not_null("IS NOT NULL"),
    like("LIKE #{pattern_{idx}}"),
    containing("LIKE #{pattern_{idx}}"),
    not_like("NOT LIKE #{pattern_{idx}}"),
    starting_with("LIKE #{pattern_{idx}}"),
    ending_with("LIKE #{pattern_{idx}}"),
    desc("DESC"),
    asc("ASC"),
    not(cdata("<>") + " ?"),
    in("IN ?"),
    not_in("NOT IN ?"),
    True("true", "=?"),
    False("false", "=?"),
    ignore_case("=?"),
    ;

    private final String operate;

    private final String keyword;

    private static final List<JpaKeyword> NO_PARAM = Arrays.asList(True, False, Null, is_null, not_null, is_not_null);

    public static boolean isNoParamKeyword(JpaKeyword jpaKeyword) {
        return NO_PARAM.contains(jpaKeyword);
    }

    JpaKeyword(String keyword, String operate) {
        this.keyword = wrap(keyword);
        this.operate = operate;
    }

    JpaKeyword(String operate) {
        this.keyword = wrap(this.name());
        this.operate = operate;
    }

    private static String cdata(String content) {
        return "<![CDATA[" + content + "]]>";
    }

    private static String wrap(String keyword) {
        return "_" + keyword;
    }

    public String getKeyword() {
        return keyword;
    }

    public String getOperate() {
        return operate;
    }
}