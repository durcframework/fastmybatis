package com.gitee.fastmybatis.core.ext.jpa;

/**
 * @author tanghc
 */
public class JpaQueryMethodException extends RuntimeException {
    public JpaQueryMethodException(String message) {
        super(message);
    }
}
