package com.gitee.fastmybatis.core.ext.spi;

import com.gitee.fastmybatis.core.query.ConditionValueHandler;

/**
 * @author thc
 */
public interface ConditionValueExecutor extends ConditionValueHandler {
}
