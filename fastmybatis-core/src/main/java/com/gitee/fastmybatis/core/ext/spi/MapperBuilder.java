package com.gitee.fastmybatis.core.ext.spi;

import com.gitee.fastmybatis.core.ext.MapperRunner;

/**
 * @author thc
 */
public interface MapperBuilder {

    <T> MapperRunner<T> getMapperRunner(Class<T> mapperClass, Object applicationContext);

}
