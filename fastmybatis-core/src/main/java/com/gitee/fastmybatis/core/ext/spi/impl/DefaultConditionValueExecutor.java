package com.gitee.fastmybatis.core.ext.spi.impl;

import com.gitee.fastmybatis.core.ext.spi.ConditionValueExecutor;

/**
 * 条件参数值处理，可动态返回自定义值
 * @author thc
 */
public class DefaultConditionValueExecutor implements ConditionValueExecutor {
    @Override
    public Object getConditionValue(Object defaultValue, String fieldName, Object target) {
        return defaultValue;
    }
}
