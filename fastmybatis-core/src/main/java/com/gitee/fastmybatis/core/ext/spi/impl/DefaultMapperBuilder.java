package com.gitee.fastmybatis.core.ext.spi.impl;

import com.gitee.fastmybatis.core.MybatisContext;
import com.gitee.fastmybatis.core.ext.MapperRunner;
import com.gitee.fastmybatis.core.ext.spi.MapperBuilder;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

/**
 * @author thc
 */
public class DefaultMapperBuilder implements MapperBuilder {

    @Override
    public <T> MapperRunner<T> getMapperRunner(Class<T> mapperClass, Object applicationContext) {
        SqlSessionFactory sessionFactory = MybatisContext.getSqlSessionFactoryByMapperClass(mapperClass);
        SqlSession session = sessionFactory.openSession();
        T mapper = session.getMapper(mapperClass);
        return new MapperRunner<>(mapper, session);
    }
}
