package com.gitee.fastmybatis.core.handler;

import com.gitee.fastmybatis.annotation.Column;

import java.lang.reflect.Field;

/**
 * @author thc
 */
public abstract class BaseFormatter<T> extends BaseFill<T> implements ValueFormatter<T> {

    @Override
    protected final Object getFillValue(T defaultValue) {
        return writeFormat(defaultValue);
    }

    @Override
    protected final T convertValue(Object columnValue) {
        return this.readFormat(columnValue);
    }



    @Override
    public boolean match(Class<?> entityClass, Field field, String columnName) {
        return true;
    }
}
