package com.gitee.fastmybatis.core.handler;

/**
 * @author thc
 */
public interface ValueFormatter<T> {

    /**
     * 格式化后写入数据库
     * @param value 待写入的值
     * @return 返回格式化后的数据
     */
    Object writeFormat(T value);

    /**
     * 数据库读后格式化
     * @param value 从数据库读出来的值
     * @return 返回格式化后的值
     */
    T readFormat(Object value);



}
