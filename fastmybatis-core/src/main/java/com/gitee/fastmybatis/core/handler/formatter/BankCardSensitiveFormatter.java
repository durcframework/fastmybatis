package com.gitee.fastmybatis.core.handler.formatter;

/**
 * 银行卡号脱敏
 * <pre>
 * 1234561234567895678 -> 123456****5678
 * </pre>
 * @author thc
 */
public class BankCardSensitiveFormatter extends ReadFormatter<String> {

    public static final String REGEX = "(\\d{6})\\d{1,9}(\\d{4})";

    @Override
    public String readFormat(Object value) {
        if (value == null) {
            return null;
        }
        String bankCard = String.valueOf(value);
        return bankCard.replaceAll(REGEX, "$1****$2");
    }

}
