package com.gitee.fastmybatis.core.handler.formatter;

/**
 * 邮箱格脱敏
 * <pre>
 * 13712345678@qq.com -> 1****@qq.com
 * </pre>
 * @author thc
 */
public class EmailSensitiveFormatter extends ReadFormatter<String> {

    public static final String REGEX = "(^\\w)[^@]*(@.*$)";

    @Override
    public String readFormat(Object value) {
        if (value == null) {
            return null;
        }
        String email = String.valueOf(value);
        return email.replaceAll(REGEX, "$1****$2");
    }

}
