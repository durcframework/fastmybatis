package com.gitee.fastmybatis.core.handler.formatter;

/**
 * 身份证号脱敏
 * <pre>
 * 443214198809091234 -> 443****1234
 * </pre>
 * @author thc
 */
public class IdCardSensitiveFormatter extends ReadFormatter<String> {

    public static final String REGEX = "(?<=\\w{3})\\w(?=\\w{4})";

    @Override
    public String readFormat(Object value) {
        if (value == null) {
            return null;
        }
        String idCard = String.valueOf(value);
        return idCard.replaceAll(REGEX, "*");
    }

}
