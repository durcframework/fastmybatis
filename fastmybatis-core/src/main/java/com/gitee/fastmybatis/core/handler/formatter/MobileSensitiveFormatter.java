package com.gitee.fastmybatis.core.handler.formatter;

/**
 * 手机号脱敏
 * <pre>
 * 13712345678 -> 137****5678
 * (+86)13712345678 -> (+86)137****5678
 * </pre>
 * @author thc
 */
public class MobileSensitiveFormatter extends ReadFormatter<String> {

    public static final String REGEX = "(\\d{3})\\d{1,4}(\\d{4})";

    @Override
    public String readFormat(Object value) {
        if (value == null) {
            return null;
        }
        String number = String.valueOf(value);
        if (number.length() <= 7) {
            return number;
        }
        return number.replaceAll(REGEX, "$1****$2");
    }

}
