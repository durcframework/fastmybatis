package com.gitee.fastmybatis.core.handler.formatter;

import com.gitee.fastmybatis.core.handler.BaseFormatter;

/**
 * @author thc
 */
public abstract class ReadFormatter<T> extends BaseFormatter<T> {

    @Override
    public Object writeFormat(T value) {
        return value;
    }
}
