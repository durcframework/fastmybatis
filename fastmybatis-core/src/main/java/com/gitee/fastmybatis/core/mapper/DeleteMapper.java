package com.gitee.fastmybatis.core.mapper;

import com.gitee.fastmybatis.core.EqualColumn;
import com.gitee.fastmybatis.core.FastmybatisContext;
import com.gitee.fastmybatis.core.query.Query;
import com.gitee.fastmybatis.core.support.Getter;
import com.gitee.fastmybatis.core.util.ClassUtil;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * 具备删除功能的Mapper
 *
 * @param <E> 实体类
 * @author tanghc
 */
public interface DeleteMapper<E> extends Mapper<E> {

    /**
     * 删除记录（底层根据id删除），在有逻辑删除字段的情况下，做UPDATE操作。
     *
     * @param entity 实体类
     * @return 受影响行数
     */
    int delete(E entity);

    /**
     * 根据id删除，在有逻辑删除字段的情况下，做UPDATE操作
     *
     * @param id 主键id值
     * @return 受影响行数
     */
    int deleteById(Serializable id);

    /**
     * 根据多个主键id删除，在有逻辑删除字段的情况下，做UPDATE操作
     *
     * @param ids          主键id
     * @param equalColumns 如果是UPDATE逻辑删除，则指定set部分，用来更新其它字段
     * @return 返回影响行数
     */
    default int deleteByIds(Collection<? extends Serializable> ids, EqualColumn... equalColumns) {
        if (ids == null || ids.isEmpty()) {
            throw new IllegalArgumentException("parameter 'ids' can not empty");
        }
        String pkColumnName = FastmybatisContext.getPkColumnName(getEntityClass());
        return this.deleteByColumn(pkColumnName, ids, equalColumns);
    }

    /**
     * 根据指定字段值删除，在有逻辑删除字段的情况下，做UPDATE操作<br>
     * <pre>
     * 根据数组删除
     * {@literal mapper.deleteByColumn("username", Arrays.asList("jim", "tom")); }
     * 对应SQL:DELETE FROM table WHERE username in ('jim', 'tom')
     *
     * 根据某个值删除
     * {@literal mapper.deleteByColumn("username", "jim"); }
     * 对应SQL:DELETE FROM table WHERE username = 'jim'
     * </pre>
     *
     * @param column       数据库字段名
     * @param value        条件值，可以是单值String，int，也可以是集合List，Collection
     * @param equalColumns 如果是UPDATE逻辑删除，则指定set部分，用来更新其它字段
     * @return 返回影响行数
     */
    default int deleteByColumn(String column, Object value, EqualColumn... equalColumns) {
        if (column == null || "".equals(column)) {
            throw new IllegalArgumentException("parameter 'columns' can not blank");
        }
        Objects.requireNonNull(value, "value can not null");
        List<EqualColumn> currentEqualColumns = FastmybatisContext.getDeleteSetBlock();
        if (currentEqualColumns == null) {
            currentEqualColumns = new ArrayList<>(4);
        }
        if (equalColumns != null && equalColumns.length > 0) {
            currentEqualColumns.addAll(Arrays.asList(equalColumns));
        }
        Query query;
        if (value instanceof Collection) {
            query = new Query().in(column, (Collection<?>) value);
        } else {
            query = new Query().eq(column, value);
        }
        return deleteByQuery(query, currentEqualColumns.toArray(new EqualColumn[0]));
    }

    /**
     * 根据指定字段值删除，在有逻辑删除字段的情况下，做UPDATE操作<br>
     * <pre>
     * 根据数组删除
     * {@literal mapper.deleteByColumn(TUser::getUsername, Arrays.asList("jim", "tom")); }
     * 对应SQL:DELETE FROM table WHERE username in ('jim', 'tom')
     *
     * 根据某个值删除
     * {@literal mapper.deleteByColumn(TUser::getUsername, "jim"); }
     * 对应SQL:DELETE FROM table WHERE username = 'jim'
     * </pre>
     *
     * @param column       数据库字段名
     * @param value        条件值，可以是单值String，int，也可以是集合List，Collection
     * @param equalColumns 如果是UPDATE逻辑删除，则指定set部分，用来更新其它字段
     * @return 返回影响行数
     */
    default int deleteByColumn(Getter<E, ?> column, Object value, EqualColumn... equalColumns) {
        return deleteByColumn(ClassUtil.getColumnName(column), value, equalColumns);
    }

    /**
     * 根据条件删除，在有逻辑删除字段的情况下，做UPDATE操作<br>
     * <pre>
     * {@literal
     * Query query = new Query();
     * query.eq("state", 3);
     * int i = mapper.deleteByQuery(query);
     * }
     * 对应SQL:
     * DELETE FROM `t_user` WHERE state = 3
     * </pre>
     *
     * @param query 查询对象
     * @return 受影响行数
     */
    int deleteByQuery(@Param("query") Query query, @Param("equalColumns") EqualColumn... equalColumns);

    /**
     * 强制删除（底层根据id删除），忽略逻辑删除字段，执行DELETE语句
     *
     * @param entity 实体类
     * @return 受影响行数
     */
    int forceDelete(E entity);

    /**
     * 根据id强制删除，忽略逻辑删除字段，执行DELETE语句
     *
     * @param id 主键id值
     * @return 受影响行数
     */
    int forceDeleteById(Serializable id);

    /**
     * 根据条件强制删除，忽略逻辑删除字段，执行DELETE语句
     *
     * @param query 查询对象
     * @return 受影响行数
     */
    int forceDeleteByQuery(@Param("query") Query query);

    /**
     * 根据字段强制删除，忽略逻辑删除字段，执行DELETE语句
     * <pre>
     * {@literal
     * int i = mapper.forceDeleteByColumn(TUser::getUsername, "jim");
     * }
     *
     * 对应SQL: DELETE FROM t_user WHERE username = 'jim'
     * </pre>
     *
     * @param field 字段
     * @param value 删除值
     * @return 受影响行数
     */
    default int forceDeleteByColumn(Getter<E, ?> field, Object value) {
        Query query = new Query()
                .eq(ClassUtil.getColumnName(field), value);
        return forceDeleteByQuery(query);
    }

}
