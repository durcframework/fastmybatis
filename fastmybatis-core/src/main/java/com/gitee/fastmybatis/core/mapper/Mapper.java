package com.gitee.fastmybatis.core.mapper;

import com.gitee.fastmybatis.core.util.ClassUtil;

/**
 * @param <E> 实体类
 * @author tanghc
 */
public interface Mapper<E> {

    /**
     * 获取当前实体类class
     *
     * @return 返回实体类class
     */
    default Class<E> getEntityClass() {
        return (Class<E>) ClassUtil.getSuperInterfaceGenericTypeCache(getClass(), 0);
    }

}
