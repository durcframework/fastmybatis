package com.gitee.fastmybatis.core.mapper;

import org.apache.ibatis.exceptions.TooManyResultsException;

/**
 * @author 六如
 */
public class OneResult<T> implements SearchResult<T> {

    private static final OneResult EMPTY = new OneResult<>(null, null);

    private final T data;

    private final TooManyResultsException exception;

    public static <E> OneResult<E> empty() {
        return EMPTY;
    }

    public OneResult(T data, TooManyResultsException exception) {
        this.data = data;
        this.exception = exception;
    }

    /**
     * 返回结果
     *
     * @return 返回结果
     */
    @Override
    public T get() {
        return data;
    }

    @Override
    public RuntimeException getException() {
        return exception;
    }

    @Override
    public boolean isError() {
        return exception != null;
    }
}
