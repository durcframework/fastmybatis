package com.gitee.fastmybatis.core.mapper;

import com.gitee.fastmybatis.core.FastmybatisContext;
import com.gitee.fastmybatis.core.PageInfo;
import com.gitee.fastmybatis.core.ext.spi.BeanExecutor;
import com.gitee.fastmybatis.core.ext.spi.SpiContext;
import com.gitee.fastmybatis.core.query.Query;
import com.gitee.fastmybatis.core.support.ColumnMeta;
import com.gitee.fastmybatis.core.support.Columns;
import com.gitee.fastmybatis.core.support.Getter;
import com.gitee.fastmybatis.core.support.PageEasyui;
import com.gitee.fastmybatis.core.support.TreeNode;
import com.gitee.fastmybatis.core.util.ClassUtil;
import com.gitee.fastmybatis.core.util.ConvertUtil;
import com.gitee.fastmybatis.core.util.MapperUtil;
import com.gitee.fastmybatis.core.util.TreeUtil;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * 具备查询功能的Mapper
 *
 * @param <E> 实体类，如：Student
 * @param <I> 主键类型，如：Long，Integer
 * @author tanghc
 * @deprecated
 * @see SearchMapper
 */
@Deprecated
public interface SchMapper<E, I> extends Mapper<E> {

    /**
     * 内置resultMap名称
     */
    String BASE_RESULT_MAP = "baseResultMap";

    /**
     * 根据主键查询<br>
     * <pre>
     * {@literal
     * TUser user = mapper.getById(3);
     * }
     * 对应SQL:
     * SELECT col1, col2, ...
     * FROM `t_user` t
     * WHERE id = 3
     * </pre>
     *
     * @param id 主键值
     * @return 返回实体对象，没有返回null
     */
    E getById(I id);

    /**
     * 根据主键查询强制查询，忽略逻辑删除字段<br>
     * <pre>
     * {@literal
     * TUser user = mapper.forceById(3);
     * }
     * 对应SQL:
     * SELECT col1, col2, ...
     * FROM `t_user` t
     * WHERE id = 3
     * </pre>
     *
     * @param id 主键值
     * @return 返回实体对象，没有返回null
     */
    E forceById(I id);

    /**
     * 根据条件查找单条记录<br>
     * <pre>
     * {@literal
     * // 查询id=3,金额大于1的用户
     * Query query = new Query()
     *         .eq("id", 3)
     *         .gt("money", 1);
     * TUser user = mapper.getByQuery(query);
     * }
     * 对应SQL:
     * SELECT col1, col2, ...
     * FROM `t_user` t
     * WHERE id = ? AND money > ? LIMIT 1
     * </pre>
     *
     * @param query 查询条件
     * @return 返回实体对象，没有返回null
     */
    E getByQuery(@Param("query") Query query);

    /**
     * 查询单条数据并返回指定字段<br>
     * <pre>
     * {@literal
     * Query query = new Query().eq("id", 6);
     * TUser tUser = mapper.getBySpecifiedColumns(Arrays.asList("id", "username"), query);
     * }
     * 对应SQL:
     * SELECT id , username FROM `t_user` t WHERE id = 6 AND LIMIT 0,1
     * </pre>
     *
     * @param columns 指定返回的数据字段
     * @param query   查询条件
     * @return 返回某一条数据
     */
    default E getBySpecifiedColumns(List<String> columns, Query query) {
        if (columns == null || columns.isEmpty()) {
            throw new IllegalArgumentException("parameter 'columns' can not empty");
        }
        query.limit(0, 1);
        List<E> list = this.listBySpecifiedColumns(columns, query);
        return list.isEmpty() ? null : list.get(0);
    }

    /**
     * 查询单条数据并返回指定字段<br>
     * <p>使用Function指定返回字段</p>
     * <pre>
     * {@literal
     * Query query = new Query().eq("id", 6);
     * TUser tUser = TUser tUser = mapper.getBySpecifiedColumns(
     *      Columns.of(TUser::getId, TUser::getUsername), query
     * );
     * }
     * 对应SQL:
     * SELECT id , username FROM `t_user` t WHERE id = 6 AND LIMIT 0,1
     * </pre>
     *
     * @param columns 指定返回字段
     * @param query   查询条件
     * @return 返回某一条数据
     */
    default E getBySpecifiedColumns(Columns<E> columns, Query query) {
        Objects.requireNonNull(columns, "parameter 'functionalColumns' can not null");
        List<String> columnNames = columns.getFunctions().stream()
                .map(ClassUtil::getColumnName)
                .collect(Collectors.toList());
        return getBySpecifiedColumns(columnNames, query);
    }

    /**
     * 查询单条数据返回指定字段并转换到指定类中<br>
     * <pre>
     * {@literal
     * Query query = new Query().eq("id", 6);
     * UserVO userVo = mapper.getBySpecifiedColumns(Arrays.asList("id", "username"), query, UserVO.class);
     * }
     * 对应SQL:
     * SELECT id , username FROM `t_user` t WHERE id = 6 AND LIMIT 0,1
     * </pre>
     *
     * @param columns 指定返回的数据字段
     * @param query   查询条件
     * @param clazz   待转换的类，类中的字段类型必须跟实体类的中类型一致
     * @param <T>     转换类类型
     * @return 返回转换类，查不到返回null
     */
    default <T> T getBySpecifiedColumns(List<String> columns, Query query, Class<T> clazz) {
        Objects.requireNonNull(clazz, "parameter 'clazz' can not null");
        E e = getBySpecifiedColumns(columns, query);
        // 如果是单值
        if (ClassUtil.isPrimitive(clazz.getSimpleName())) {
            return SpiContext.getBeanExecutor().pojoToValue(e, clazz, columns.get(0));
        }
        return SpiContext.getBeanExecutor().copyBean(e, clazz);
    }

    /**
     * 查询单条数据返回指定字段并转换到指定类中<br>
     * <p>使用Function指定返回字段</p>
     * <pre>
     * {@literal
     * Query query = new Query().eq("id", 6);
     * UserVO userVo = mapper.getBySpecifiedColumns(
     *      Columns.of(TUser::getId, TUser::getName), query, UserVO.class
     * );
     * }
     * 对应SQL:
     * SELECT id , username FROM `t_user` t WHERE id = 6 AND LIMIT 0,1
     * </pre>
     *
     * @param columns 指定返回的数据字段
     * @param query   查询条件
     * @param clazz   待转换的类，类中的字段类型必须跟实体类的中类型一致
     * @param <T>     转换类类型
     * @return 返回转换类，查不到返回null
     */
    default <T> T getBySpecifiedColumns(Columns<E> columns, Query query, Class<T> clazz) {
        Objects.requireNonNull(columns, "parameter 'columns' can not null");
        List<String> columnNames = columns.getFunctions().stream()
                .map(ClassUtil::getColumnName)
                .collect(Collectors.toList());
        return getBySpecifiedColumns(columnNames, query, clazz);
    }

    /**
     * 查询某一行某个字段值<br>
     * <pre>
     * {@literal
     * Query query = new Query().eq("id", 6);
     * String username = mapper.getColumnValue("username", query, String.class);
     * }
     * 转换成SQL：
     * SELECT username FROM `t_user` t WHERE id = 6 LIMIT 0,1
     * </pre>
     *
     * @param column 数据库字段
     * @param query  查询条件
     * @param clazz  待转换的类，类中的字段类型必须跟实体类的中类型一致
     * @param <T>    转换类类型
     * @return 返回单值，查不到返回null
     * @see #getColumnValue(Getter, Query)
     */
    default <T> T getColumnValue(String column, Query query, Class<T> clazz) {
        if (column == null || "".equals(column)) {
            throw new IllegalArgumentException("parameter 'column' can not blank");
        }
        Objects.requireNonNull(clazz, "parameter 'clazz' can not null");
        if (!ClassUtil.isPrimitive(clazz.getSimpleName())) {
            throw new IllegalArgumentException("parameter `clazz` must be a single value class, such as: Integer.class, String.class, Date.class");
        }
        return getBySpecifiedColumns(Collections.singletonList(column), query, clazz);
    }

    /**
     * 查询某一行某个字段值<br>
     * <p>使用Function指定返回字段</p>
     * <pre>
     * {@literal
     * Query query = new Query().eq("id", 6);
     * String username = mapper.getColumnValue(TUser::getUsername, query);
     * }
     * 转换成SQL：
     * SELECT username FROM `t_user` t WHERE id = 6 LIMIT 0,1
     * </pre>
     *
     * @param column 数据库字段
     * @param query  查询条件
     * @param <T>    转换类类型
     * @return 返回单值，查不到返回null
     */
    default <T> T getColumnValue(Getter<E, ?> column, Query query) {
        ColumnMeta columnMeta = ClassUtil.getColumnMeta(column);
        return getColumnValue(columnMeta.getColumnName(), query, (Class<T>) columnMeta.getClazz());
    }

    /**
     * 根据字段查询一条记录<br>
     * <pre>
     * {@literal
     * TUser user = mapper.getByColumn("username", "王五");
     * }
     * </pre>
     * <code>
     * SELECT col1,col2,... FROM table WHERE {column} = {value} LIMIT 1
     * </code>
     *
     * @param column 数据库字段名
     * @param value  字段值
     * @return 返回实体对象，没有返回null
     */
    E getByColumn(@Param("column") String column, @Param("value") Object value);

    /**
     * 根据字段查询一条记录<br>
     * <pre>
     * {@literal
     * TUser user = mapper.getByColumn(TUser::getUsername, "王五");
     * }
     * </pre>
     * <code>
     * SELECT col1,col2,... FROM table WHERE {column} = {value} LIMIT 1
     * </code>
     *
     * @param column 数据库字段名
     * @param value  字段值
     * @return 返回实体对象，没有返回null
     */
    default E getByColumn(Getter<E, ?> column, Object value) {
        String columnName = ClassUtil.getColumnName(column);
        return getByColumn(columnName, value);
    }

    /**
     * 查询总记录数<br>
     * <pre>
     * {@literal
     * Query query = new Query();
     * // 添加查询条件
     * query.eq("state", 0);
     * // 获取总数
     * long total = mapper.getCount(query);
     *
     * 对应SQL:
     * SELECT COUNT(*) FROM t_user WHERE `state` = 0
     * }
     * </pre>
     *
     * @param query 查询条件
     * @return 返回总记录数
     */
    long getCount(@Param("query") Query query);

    /**
     * 根据字段查询所有记录<br>
     *
     * <pre>
     * {@literal
     * List<TUser> list = mapper.listByColumn("age", 20);
     * }
     * </pre>
     * 对应SQL:
     * <code>
     * SELECT col1, col2, ... FROM t_user WHERE age = 20;
     * </code>
     *
     * @param column 字段
     * @param value  字段值
     * @return 返回实体对象集合，没有返回空集合
     */
    List<E> listByColumn(@Param("column") String column, @Param("value") Object value);

    /**
     * 根据字段查询所有记录<br>
     *
     * <pre>
     * {@literal
     * List<TUser> list = mapper.listByColumn(TUser::getAge, 20);
     * }
     * </pre>
     * 对应SQL:
     * <code>
     * SELECT col1, col2, ... FROM t_user WHERE age = 20;
     * </code>
     *
     * @param column 字段
     * @param value  字段值
     * @return 返回实体对象集合，没有返回空集合
     */
    default List<E> listByColumn(Getter<E, ?> column, Object value) {
        return listByColumn(ClassUtil.getColumnName(column), value);
    }

    /**
     * 根据条件查询所有记录<br>
     * <pre>
     * {@literal
     * Query query = new Query()
     *         .eq("state", 0)
     *         .in("money", Arrays.asList(100, 1.0, 3));
     * List<TUser> list = mapper.list(query);
     * }
     * 对应SQL:
     * SELECT col1, col2, ...
     * FROM `t_user` t
     * WHERE state = ? AND money IN ( ? , ? , ? )
     * </pre>
     *
     * @param query 查询条件
     * @return 返回实体对象集合，没有返回空集合
     */
    List<E> list(@Param("query") Query query);

    /**
     * 根据多个主键查询<br>
     * <pre>
     * {@literal
     * List<User> list = mapper.listByIds(Arrays.asList(1,2,3));
     * }
     * </pre>
     * <code>
     * SELECT col1, col2, ... FROM table WHERE id in (val1, val2, ...)
     * </code>
     *
     * @param ids id集合
     * @return 返回结果集，没有返回空list
     */
    default List<E> listByIds(Collection<I> ids) {
        if (ids == null || ids.isEmpty()) {
            return new ArrayList<>();
        }
        Class<E> entityClass = (Class<E>) ClassUtil.getSuperInterfaceGenericTypeCache(getClass(), 0);
        String pkColumnName = FastmybatisContext.getPkColumnName(entityClass);
        return listByCollection(pkColumnName, ids);
    }

    /**
     * 根据多个字段值查询结果集<br>
     *
     * <code>
     * SELECT col1, col2, ... FROM table WHERE {column} IN (val1, val2, ...)
     * </code>
     *
     * @param column 数据库字段名
     * @param values 多个字段值
     * @return 返回实体对象集合，没有返回空集合
     * @see #listByValues(Getter, Object...)
     * @deprecated 不推荐，使用listByValues(Getter, Object...) 代替
     */
    @Deprecated
    default List<E> listByArray(String column, Object[] values) {
        if (column == null || "".equals(column)) {
            throw new IllegalArgumentException("parameter 'column' can not blank");
        }
        if (values == null || values.length == 0) {
            return new ArrayList<>();
        }
        return listByCollection(column, Arrays.asList(values));
    }

    /**
     * 根据字段多个值查询结果集<br>
     * <pre>
     * {@literal
     * List<TUser> list = mapper.listByValues("age", 20, 30, 40);
     * }
     * <code>
     * SELECT col1, col2, ... FROM table WHERE {column} IN (val1, val2, ...)
     * </code>
     *
     * @param column 数据库字段名
     * @param values 多个字段值
     * @return 返回实体对象集合，没有返回空集合
     */
    default List<E> listByValues(String column, Object... values) {
        return listByArray(column, values);
    }

    /**
     * 根据字段多个值查询结果集<br>
     * <pre>
     * {@literal
     * List<TUser> list = mapper.listByValues(TUser::getAge, 20, 30, 40);
     * }
     * <code>
     * SELECT col1, col2, ... FROM table WHERE age IN (20, 30, 40)
     * </code>
     *
     * @param column 数据库字段名
     * @param values 多个字段值
     * @return 返回实体对象集合，没有返回空集合
     */
    default List<E> listByValues(Getter<E, ?> column, Object... values) {
        return listByArray(ClassUtil.getColumnName(column), values);
    }


    /**
     * 根据字段多个值查询结果集<br>
     * <pre>
     * {@literal
     * List<TUser> list = mapper.listByCollection("age", Arrays.asList(20, 30));
     * }
     * <code>
     * SELECT col1, col2, ... FROM table WHERE {column} in (val1, val2, ...)
     * </code>
     *
     * @param column 数据库字段名
     * @param values 多个字段值
     * @return 返回实体对象集合，没有返回空集合
     */
    default List<E> listByCollection(String column, Collection<?> values) {
        if (column == null || "".equals(column)) {
            throw new IllegalArgumentException("parameter 'column' can not blank");
        }
        if (values == null || values.isEmpty()) {
            return new ArrayList<>();
        }
        Query query = new Query()
                .in(column, values);
        return list(query);
    }

    /**
     * 根据字段多个值查询结果集<br>
     * <pre>
     * {@literal
     * List<TUser> list = mapper.listByCollection(TUser::getAge, Arrays.asList(20, 30));
     * }
     * <code>
     * SELECT col1, col2, ... FROM table WHERE {column} in (val1, val2, ...)
     * </code>
     *
     * @param column 数据库字段名
     * @param values 多个字段值
     * @return 返回实体对象集合，没有返回空集合
     */
    default List<E> listByCollection(Getter<E, ?> column, Collection<?> values) {
        return listByCollection(ClassUtil.getColumnName(column), values);
    }

    /**
     * 查询返回指定的列，返回实体类集合<br>
     * <pre>
     * {@literal
     * Query query = new Query();
     * // 添加查询条件
     * query.eq("username", "张三");
     *
     * // 自定义字段
     * List<String> columns = Arrays.asList("id", "username");
     * // 查询，返回一个集合
     * List<TUser> list = mapper.listBySpecifiedColumns(columns, query);
     * }
     * 对应SQL:SELECT id , username FROM `t_user` t WHERE username = ?
     * </pre>
     *
     * @param columns 指定字段，数据库字段名
     * @param query   查询条件
     * @return 返回实体类集合，没有则返回空list
     */
    List<E> listBySpecifiedColumns(@Param("columns") List<String> columns, @Param("query") Query query);

    /**
     * 查询返回指定的列，返回实体类集合<br>
     * <p>使用Function指定返回字段</p>
     * <pre>
     * {@literal
     * Query query = new Query();
     * // 添加查询条件
     * query.eq("username", "张三");
     *
     * // 自定义字段
     * Columns<TUser> columns = Columns.of(TUser::getId, TUser::getUsername);
     * // 查询，返回一个集合
     * List<TUser> list = mapper.listBySpecifiedColumns(columns, query);
     * }
     * 对应SQL: SELECT id , username FROM `t_user` t WHERE username = ?
     * </pre>
     *
     * @param columns 指定字段，数据库字段名
     * @param query   查询条件
     * @return 返回实体类集合，没有则返回空list
     */
    default List<E> listBySpecifiedColumns(Columns<E> columns, Query query) {
        Objects.requireNonNull(columns, "parameter 'columns' can not null");
        List<String> columnNames = columns.getFunctions().stream()
                .map(ClassUtil::getColumnName)
                .collect(Collectors.toList());
        return listBySpecifiedColumns(columnNames, query);
    }

    /**
     * 查询返回指定的列，返指定类集合<br>
     * <pre>
     * {@literal
     * Query query = new Query();
     * // 添加查询条件
     * query.eq("username", "张三");
     *
     * // 自定义字段
     * List<String> columns = Arrays.asList("t.id", "t.username", "t.add_time");
     * // 查询，自定义集合
     * List<UserVO> list = mapper.listBySpecifiedColumns(columns, query, UserVO.class);
     * }
     * </pre>
     *
     * @param columns 指定字段，数据库字段名
     * @param query   查询条件
     * @param clazz   集合元素类型，可以是对象class，也可以是基本类型class，如：UserVO.class, Integer.class, String.class。<br>
     *                当指定基本类型class时，<code>columns</code>参数只能指定一列。当为对象类型时，类中的字段类型必须跟实体类的中类型一致
     * @return 返回实体类集合，没有则返回空list
     * @see #pageBySpecifiedColumns(List, Query, Class) 分页查询
     */
    default <T> List<T> listBySpecifiedColumns(List<String> columns, Query query, Class<T> clazz) {
        if (columns == null || columns.isEmpty()) {
            throw new IllegalArgumentException("parameter 'columns' can not empty");
        }
        Objects.requireNonNull(query, "parameter 'query' can not null");
        Objects.requireNonNull(clazz, "parameter 'clazz' can not null");
        query.setSelectColumns(columns);
        List<Map<String, Object>> mapList = this.listMap(columns, query);
        if (mapList == null) {
            return new ArrayList<>(0);
        }
        mapList = ConvertUtil.formatMapList(mapList, columns);
        BeanExecutor beanExecutor = SpiContext.getBeanExecutor();
        // 如果是单值
        if (ClassUtil.isPrimitive(clazz.getSimpleName())) {
            return mapList.stream()
                    .map(obj -> beanExecutor.pojoToValue(obj, clazz, columns.get(0)))
                    .collect(Collectors.toList());
        }
        return beanExecutor.copyBean(mapList, clazz);
    }

    /**
     * 查询返回指定的列，并转换<br>
     * <p>使用Function指定返回字段</p>
     * <pre>
     * {@literal
     * Query query = new Query();
     * // 添加查询条件
     * query.eq("username", "张三");
     *
     * // 自定义字段
     * Columns<TUser> columns = Columns.of(TUser::getId, TUser::getUsername, TUser::getAddTime);
     * // 查询，自定义集合
     * List<UserVO> list = mapper.listBySpecifiedColumns(columns, query, UserVO.class);
     * }
     * </pre>
     *
     * @param columns 指定字段，数据库字段名
     * @param query   查询条件
     * @param clazz   集合元素类型，可以是对象class，也可以是基本类型class，如：UserVO.class, Integer.class, String.class。<br>
     *                当指定基本类型class时，<code>columns</code>参数只能指定一列。当为对象类型时，类中的字段类型必须跟实体类的中类型一致
     * @return 返回实体类集合，没有则返回空list
     * @see #pageBySpecifiedColumns(List, Query, Class) 分页查询
     */
    default <T> List<T> listBySpecifiedColumns(Columns<E> columns, Query query, Class<T> clazz) {
        Objects.requireNonNull(columns, "parameter 'columns' can not null");
        List<String> columnNames = columns.getFunctions().stream()
                .map(ClassUtil::getColumnName)
                .collect(Collectors.toList());
        return listBySpecifiedColumns(columnNames, query, clazz);
    }

    /**
     * 查询指定字段，每一行数据存在Map对象中，Map里面key对应数据库字段名/别名，value对应值
     * <pre>
     * {@literal
     * List<String> columns = Arrays.asList(
     *     "username as name",
     *     "add_time as createTime",
     *     "add_time as addTime",
     *     "money"
     * );
     * List<Map<String, Object>> listMap = mapper.listMap(columns, query);
     * }
     * </pre>
     *
     * @param columns 返回的字段
     * @param query   查询条件
     * @return 返回结果集，没有则返回空list
     */
    List<Map<String, Object>> listMap(@Param("columns") List<String> columns, @Param("query") Query query);

    /**
     * 查询指定列，返指定列集合<br>
     * <pre>
     * {@literal
     * // 返回id集合
     * List<Integer> idList = mapper.listColumnValues("id", query, Integer.class);
     * }
     * </pre>
     *
     * @param column 指定列名，数据库字段名
     * @param query  查询条件
     * @param clazz  集合元素类型，基本类型class，如：Integer.class, String.class
     * @return 返回指定列集合，没有则返回空list
     * @see #pageBySpecifiedColumns(List, Query, Class) 分页查询
     */
    default <T> List<T> listColumnValues(String column, Query query, Class<T> clazz) {
        if (column == null || "".equals(column)) {
            throw new IllegalArgumentException("parameter 'column' can not blank");
        }
        Objects.requireNonNull(clazz, "parameter 'clazz' can not null");
        if (!ClassUtil.isPrimitive(clazz.getSimpleName())) {
            throw new IllegalArgumentException("param `clazz` must be a single value class, such as: Integer.class, String.class, Date.class");
        }
        return this.listBySpecifiedColumns(Collections.singletonList(column), query, clazz);
    }

    /**
     * 查询指定列，返指定列集合<br>
     * <p>使用Function指定返回字段</p>
     * <pre>
     * {@literal
     * // 返回id集合
     * List<Integer> idList = mapper.listColumnValues(TUser::getId, query, Integer.class);
     * }
     * </pre>
     *
     * @param column 指定列名，TUser::getId
     * @param query  查询条件
     * @param clazz  集合元素类型，基本类型class，如：Integer.class, String.class
     * @return 返回指定列集合，没有则返回空list
     * @see #pageBySpecifiedColumns(List, Query, Class) 分页查询
     */
    default <T> List<T> listColumnValues(Getter<E, ?> column, Query query, Class<T> clazz) {
        String columnName = ClassUtil.getColumnName(column);
        return listColumnValues(columnName, query, clazz);
    }

    /**
     * 分页查询返回指定的列<br>
     * <pre>
     * {@literal
     * Query query = new Query()
     *         .eq("state", 0)
     *         .page(1, 6);
     * PageInfo<MyUser> pageInfo = mapper.pageBySpecifiedColumns(Arrays.asList("id", "username"), query, MyUser.class);
     * }
     * </pre>
     *
     * @param columns 数据库列名
     * @param query   查询条件
     * @param clazz   元素class，类中的字段类型必须跟实体类的中类型一致
     * @param <T>     元素类
     * @return 返回分页信息
     */
    default <T> PageInfo<T> pageBySpecifiedColumns(List<String> columns, Query query, Class<T> clazz) {
        return MapperUtil.query(columns, this, query, clazz);
    }

    /**
     * 分页查询返回指定的列<br>
     * <pre>
     * {@literal
     * Query query = new Query()
     *         .eq("state", 0)
     *         .page(1, 6);
     * PageInfo<MyUser> pageInfo = mapper.pageBySpecifiedColumns(
     *      Columns.of(TUser::getId, TUser::getUsername), query, MyUser.class
     * );
     * }
     * </pre>
     *
     * @param columns 数据库列名
     * @param query   查询条件
     * @param clazz   元素class，类中的字段类型必须跟实体类的中类型一致
     * @param <T>     元素类
     * @return 返回分页信息
     */
    default <T> PageInfo<T> pageBySpecifiedColumns(Columns<E> columns, Query query, Class<T> clazz) {
        Objects.requireNonNull(columns, "parameter 'columns' can not null");
        List<String> columnNames = columns.getFunctions().stream()
                .map(ClassUtil::getColumnName)
                .collect(Collectors.toList());
        return pageBySpecifiedColumns(columnNames, query, clazz);
    }

    /**
     * 分页查询<br>
     * <pre>
     * {@literal
     * Query query = new Query();
     * // 添加查询条件
     * query.eq("username", "张三")
     *         .page(1, 2) // 分页查询，按页码分，通常使用这种。
     * ;
     *
     * // 分页信息
     * PageInfo<TUser> pageInfo = mapper.page(query);
     *
     * List<TUser> list = pageInfo.getList(); // 结果集
     * long total = pageInfo.getTotal(); // 总记录数
     * int pageCount = pageInfo.getPageCount(); // 共几页
     * }
     * </pre>
     *
     * @param query 查询条件
     * @return 返回分页信息
     */
    default PageInfo<E> page(Query query) {
        return MapperUtil.query(this, query);
    }

    /**
     * 分页查询，并转换结果
     * <pre>
     * {@literal
     * PageInfo<UserVO> users = mapper.page(query, UserVO.class);
     * }
     * </pre>
     * @param query 查询条件
     * @param clazz 结果集转换成指定的class类（通过属性拷贝），类中的字段类型必须跟实体类的中类型一致
     * @return 返回分页信息
     * @see #page(Query, Supplier)
     */
    default <T> PageInfo<T> page(Query query, Class<T> clazz) {
        return MapperUtil.queryAndConvert(this, query, clazz);
    }

    /**
     * 查询结果集，并转换结果集中的记录<br>
     * <code>
     * {@literal PageInfo<UserVO> pageInfo = mapper.page(query, UserVO::new);}
     * </code>
     *
     * @param query  查询条件
     * @param target 转换类
     * @return 返回分页信息
     */
    default <T> PageInfo<T> page(Query query, Supplier<T> target) {
        return MapperUtil.queryAndConvert(this, query, target);
    }

    /**
     * 查询结果集，并转换结果集中的记录，转换处理每一行<br>
     * <pre>
     * {@literal
     *  PageInfo<TUser> pageInfo = mapper.page(query, tUser -> {
     *      // 对每行数据进行转换
     *      String username = tUser.getUsername();
     *      if ("张三".equals(username)) {
     *          tUser.setUsername("法外狂徒");
     *      }
     *      return tUser;
     *   });
     * }
     * 或者：
     * {@literal
     *  // 对结果集进行手动转换，如果仅仅是属性拷贝可以直接：mapper.page(query, UserVO::new);
     *  PageInfo<UserVO> page = mapper.page(query, user -> {
     *      UserVO userVO = new UserVO();
     *      BeanUtils.copyProperties(user, userVO);
     *      return userVO;
     *   });
     * }
     * </pre>
     *
     * @param query     查询条件
     * @param converter 转换类
     * @return 返回分页信息
     */
    default <R> PageInfo<R> page(Query query, Function<E, R> converter) {
        PageInfo pageInfo = this.page(query);
        List<E> list = (List<E>) pageInfo.getList();
        List<R> retList = list.stream()
                .map(converter)
                .collect(Collectors.toList());
        pageInfo.setList(retList);
        return (PageInfo<R>) pageInfo;
    }

    /**
     * 查询结果集，并转换结果集中的记录，转换处理list<br>
     * <pre>
     * {@literal
     * Query query = new Query()
     *         .eq("state", 0);
     * PageInfo<UserVO> pageInfo = mapper.pageAndConvert(query, list -> {
     *     List<UserVO> retList = new ArrayList<>(list.size());
     *     for (TUser tUser : list) {
     *         UserVO userVO = new UserVO();
     *         BeanUtils.copyProperties(tUser, userVO);
     *         retList.add(userVO);
     *     }
     *     return retList;
     * });
     * }
     * </pre>
     *
     * @param query     查询条件
     * @param converter 转换类
     * @return 返回分页信息
     * @since 1.10.11
     */
    default <R> PageInfo<R> pageAndConvert(Query query, Function<List<E>, List<R>> converter) {
        PageInfo pageInfo = this.page(query);
        List<E> list = (List<E>) pageInfo.getList();
        List<R> retList = converter.apply(list);
        pageInfo.setList(retList);
        return (PageInfo<R>) pageInfo;
    }

    /**
     * 查询结果集，并转换结果集中的记录，并对记录进行额外处理<br>
     * <pre>
     * {@literal
     *  PageInfo<UserVO> page = mapper.page(query, UserVO::new, userVO -> {
     *      System.out.println(userVO.getUsername());
     *  });
     * }
     * </pre>
     *
     * @param query  查询条件
     * @param target 转换后的类
     * @param format 对转换后的类格式化，此时的对象已经完成属性拷贝
     * @param <R>    结果集类型
     * @return 返回PageInfo对象
     */
    default <R> PageInfo<R> page(Query query, Supplier<R> target, Consumer<R> format) {
        return this.page(query, t -> {
            R r = target.get();
            SpiContext.getBeanExecutor().copyProperties(t, r);
            format.accept(r);
            return r;
        });
    }

    /**
     * 查询返回easyui结果集<br>
     * 如果前端使用easyui，此返回结果可适用于easyui的datagrid组件
     *
     * @param query 查询条件
     * @return 返回easyui分页信息
     */
    default PageEasyui<E> pageEasyui(Query query) {
        return MapperUtil.queryForEasyuiDatagrid(this, query);
    }

    /**
     * 查询返回easyui结果集，并转换结果集中的记录<br>
     * 如果前端使用easyui，此返回结果可适用于easyui的datagrid组件
     *
     * @param query 查询条件
     * @param clazz 结果集转换成指定的class类（通过属性拷贝）
     * @return 返回easyui分页信息
     */
    default <T> PageEasyui<T> pageEasyui(Query query, Class<T> clazz) {
        return MapperUtil.queryForEasyuiDatagrid(this, query, clazz);
    }

    /**
     * 查询返回easyui结果集，并转换结果集中的记录<br>
     * 如果前端使用easyui，此返回结果可适用于easyui的datagrid组件
     * <pre>
     * {@literal
     *  PageEasyui<UserVO> page = mapper.pageEasyui(query, user -> {
     *      UserVO userVO = new UserVO();
     *      BeanUtils.copyProperties(user, userVO);
     *      return userVO;
     *   });
     * }
     * </pre>
     *
     * @param query     查询条件
     * @param converter 转换类
     * @return 返回easyui分页信息
     */
    default <R> PageEasyui<R> pageEasyui(Query query, Function<E, R> converter) {
        PageEasyui pageInfo = this.pageEasyui(query);
        List<E> list = (List<E>) pageInfo.getRows();
        List<R> retList = list.stream()
                .map(converter)
                .collect(Collectors.toList());
        pageInfo.setList(retList);
        return (PageEasyui<R>) pageInfo;
    }

    /**
     * 查询结果并转换成Map对象<br>
     * 通过list中的某一列（如主键id）当做key返回map对象<br>
     * 如果key重复则抛出异常
     * <pre>
     * {@literal
     * public class User {
     *     private Integer id;
     *     private String name;
     * }
     *
     * Query query = new Query()
     *         .ge("id", 1);
     * // id -> TUser
     * Map<Integer, TUser> map = mapper.getMap(query, TUser::getId);
     * }
     * </pre>
     *
     * @param query     查询条件
     * @param keyGetter 指定map中的key，确保唯一性，一般使用主键id或唯一索引列
     * @param <K>       key类型
     * @return 返回map对象
     */
    default <K> Map<K, E> getMap(Query query, Function<E, K> keyGetter) {
        return getMap(query, keyGetter, Function.identity());
    }

    /**
     * 查询结果并转换成Map对象<br>
     * 通过list中的某一列（如主键id）当做key返回map对象<br>
     * 如果key重复则抛出异常
     * <pre>
     * {@literal
     * public class User {
     *     private Integer id;
     *     private String name;
     * }
     *
     * List<User> -> Map<Integer, User> // 键:id, 值:当前对象
     * List<User> -> Map<Integer, String> // 键:id, 值:name字段
     * }
     * </pre>
     *
     * @param query       查询条件
     * @param keyGetter   指定map中的key，确保唯一性，一般使用主键id或唯一索引列
     * @param valueGetter 指定map中的值
     * @param <K>         key类型
     * @param <V>         value类型
     * @return 返回map对象
     */
    default <K, V> Map<K, V> getMap(Query query, Function<E, K> keyGetter, Function<E, V> valueGetter) {
        return getMap(query, keyGetter, valueGetter, (u, v) -> {
            throw new IllegalStateException(String.format("Duplicate key %s", u));
        });
    }

    /**
     * 查询结果并转换成Map对象<br>
     * 通过list中的某一列（如主键id）当做key返回map对象
     * <pre>
     * {@literal
     * public class User {
     *     private Integer id;
     *     private String name;
     * }
     *
     * List<User> -> Map<Integer, User> // 键:id, 值:当前对象
     * List<User> -> Map<Integer, String> // 键:id, 值:name字段
     * }
     * </pre>
     *
     * @param query         查询条件
     * @param keyGetter     指定map中的key，确保唯一性，一般使用主键id或唯一索引列
     * @param valueGetter   指定map中的值
     * @param mergeFunction key冲突返回哪个值
     * @param <K>           key类型
     * @param <V>           value类型
     * @return 返回map对象
     */
    default <K, V> Map<K, V> getMap(Query query, Function<E, K> keyGetter, Function<E, V> valueGetter, BinaryOperator<V> mergeFunction) {
        return getMap(query, keyGetter, valueGetter, mergeFunction, HashMap::new);
    }

    /**
     * 查询结果并转换成Map对象<br>
     * 通过list中的某一列（如主键id）当做key返回map对象
     * <pre>
     * {@literal
     * public class User {
     *     private Integer id;
     *     private String name;
     * }
     *
     * List<User> -> Map<Integer, User> // 键:id, 值:当前对象
     * List<User> -> Map<Integer, String> // 键:id, 值:name字段
     * }
     * </pre>
     *
     * @param query         查询条件
     * @param keyGetter     指定map中的key，确保唯一性，一般使用主键id或唯一索引列
     * @param valueGetter   指定map中的值
     * @param mergeFunction key冲突返回哪个值
     * @param mapSupplier   构造map
     * @param <K>           key类型
     * @param <V>           value类型
     * @param <M>           Map类型
     * @return 返回map对象
     */
    default <K, V, M extends Map<K, V>> M getMap(Query query,
                                                 Function<E, K> keyGetter,
                                                 Function<E, V> valueGetter,
                                                 BinaryOperator<V> mergeFunction,
                                                 Supplier<M> mapSupplier) {
        return list(query).stream().collect(Collectors.toMap(keyGetter, valueGetter, mergeFunction, mapSupplier));
    }

    /**
     * 查询列表并将结果转换成树结构<br>
     * 实体类必须实现{@link TreeNode}接口
     * <pre>
     * {@literal
     * CREATE TABLE `menu` (
     *   `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
     *   `name` varchar(64) NOT NULL COMMENT '菜单名称',
     *   `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父节点',
     *   PRIMARY KEY (`id`)
     * ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';
     *
     * 实体类
     *
     * public class Menu implements TreeNode<Menu, Integer> {
     *
     *     private Integer id;
     *     private String name;
     *     private Integer parentId;
     *     private List<Menu> children;
     *
     *     @Override
     *     public Integer takeId() {
     *         return getId();
     *     }
     *
     *     @Override
     *     public Integer takeParentId() {
     *         return getParentId();
     *     }
     *
     *     @Override
     *     public void setChildren(List<Menu> children) {
     *         this.children = children;
     *     }
     *
     *     getter setter...
     * }
     *
     * List<Menu> treeData = mapper.listTreeData(query, 0);
     *
     * }
     * </pre>
     *
     * @param query  查询条件
     * @param rootId 根节点id值，一般为0
     * @param <T>    节点类型，必须实现{@link TreeNode}接口
     * @return 返回树列表
     */
    default <T extends TreeNode<T, I>> List<T> listTreeData(Query query, I rootId) {
        return listTreeData(query, rootId, e -> (T) e);
    }

    /**
     * 查询列表并将结果转换成树结构<br>
     * supplier返回的实体类必须实现{@link TreeNode}接口
     *
     * @param query  查询条件
     * @param rootId 根节点id值，一般为0
     * @param supplier 转换
     * @param <T>    节点类型，必须实现{@link TreeNode}接口
     * @return 返回树列表
     */
    default <T extends TreeNode<T, I>> List<T> listTreeData(Query query, I rootId, Supplier<T> supplier) {
        return listTreeData(query, rootId, e -> {
            T t = supplier.get();
            SpiContext.getBeanExecutor().copyProperties(e, t);
            return t;
        });
    }

    /**
     * 查询列表并将结果转换成树结构<br>
     * Function转换的返回类必须实现{@link TreeNode}接口
     *
     * @param query  查询条件
     * @param rootId 根节点id值，一般为0
     * @param <T>    节点类型，必须实现{@link TreeNode}接口
     * @return 返回树列表
     */
    default <T extends TreeNode<T, I>> List<T> listTreeData(Query query, I rootId, Function<E, T> converter) {
        List<T> list = list(query)
                .stream()
                .map(converter)
                .collect(Collectors.toList());
        return TreeUtil.convertTree(list, rootId);
    }

    /**
     * 根据主键id检查记录是否存在<br>
     * <code>boolean exist = mapper.checkExistById(user)</code>
     * @param entity 实体类
     * @return 返回true，记录存在
     */
    default boolean checkExistById(E entity) {
        Object pkValue = FastmybatisContext.getPkValue(entity);
        if (pkValue == null) {
            return false;
        }
        String pkColumnName = FastmybatisContext.getPkColumnName(entity.getClass());
        return checkExist(pkColumnName, pkValue);
    }

    /**
     * 根据某个字段检查记录是否存在
     *
     * @param columnName 数据库字段名
     * @param value      值
     * @return 返回true，记录存在
     */
    default boolean checkExist(String columnName, Object value) {
        return checkExist(columnName, value, null);
    }

    /**
     * 根据某个字段检查记录是否存在
     * <pre>
     * boolean b = mapper.checkExist(TUser::getUsername, "jim");
     * </pre>
     * @param getter 字段
     * @param value      值
     * @return 返回true，记录存在
     */
    default boolean checkExist(Getter<E, ?> getter, Object value) {
        return checkExist(ClassUtil.getColumnName(getter), value);
    }

    /**
     * 根据某个字段检查记录是否存在，且不是指定id的那条记录
     * <pre>
     *     SELECT brand_name FROM brand WHERE brand_name = ? and id != ?
     * </pre>
     *
     * @param columnName 数据库字段名
     * @param value      值
     * @param id         需要排除的id值
     * @return 返回true，记录存在
     */
    default boolean checkExist(String columnName, Object value, I id) {
        Objects.requireNonNull(columnName, "columnName can not null");
        Objects.requireNonNull(value, "value can not null");

        Query query = new Query()
                .eq(columnName, value);
        if (id != null) {
            String pkColumnName = FastmybatisContext.getPkColumnNameFromMapper(this.getClass());
            query.notEq(pkColumnName, id);
        }
        Object columnValue = getColumnValue(columnName, query, Object.class);
        return columnValue != null;
    }

    /**
     * 根据某个字段检查记录是否存在，且不是指定id的那条记录
     * <pre>
     * boolean b = mapper.checkExist(TUser:getUsername, "jim", 1)
     *
     * SELECT username FROM table WHERE username = ? and id != ?
     * </pre>
     *
     * @param getter 数据库字段名
     * @param value      值
     * @param id         需要排除的id值
     * @return 返回true，记录存在
     */
    default boolean checkExist(Getter<E, ?> getter, Object value, I id) {
        return checkExist(ClassUtil.getColumnName(getter), value, id);
    }

}
