package com.gitee.fastmybatis.core.mapper;

import java.util.Optional;

/**
 * @author 六如
 * @param <T> 结果
 */
public interface SearchResult<T> {

    /**
     * 获取数据
     *
     * @return 返回数据，没有返回null
     */
    T get();

    /**
     * 是否有异常
     *
     * @return 返回true：有异常
     */
    boolean isError();

    /**
     * 返回异常对象，没有异常返回null
     *
     * @return 返回异常对象，没有异常返回null
     */
    RuntimeException getException();

    /**
     * 返回Optional结果
     *
     * @return 返回Optional结果
     */
    default Optional<T> getOptional() {
        return Optional.ofNullable(get());
    }

    /**
     * 返回结果，如果有异常则抛出TooManyResultsException异常，没有异常返回正确数据
     *
     * @return 返回结果
     */
    default T getOrThrow() {
        return getIfThrow(getException());
    }

    /**
     * 返回结果，如果有异常则抛出指定异常，没有异常返回正确数据
     *
     * @param exception 指定异常
     * @return 返回结果
     */
    default T getIfThrow(RuntimeException exception) {
        if (isError()) {
            throw exception;
        }
        return get();
    }

    /**
     * 返回Optional结果，如果有异常则抛出指定异常，没有异常返回正确数据
     *
     * @param exception 指定异常
     * @return 返回结果
     */
    default Optional<T> getOptionalIfThrow(RuntimeException exception) {
        return Optional.ofNullable(getIfThrow(exception));
    }

    /**
     * 返回Optional结果，如果有异常则抛出TooManyResultsException异常，没有异常返回正确数据
     *
     * @return 返回Optional结果
     */
    default Optional<T> getOptionalOrThrow() {
        return Optional.ofNullable(getOrThrow());
    }
}
