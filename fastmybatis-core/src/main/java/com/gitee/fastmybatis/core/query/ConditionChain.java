package com.gitee.fastmybatis.core.query;

/**
 * @author thc
 */
@FunctionalInterface
public interface ConditionChain {
    Condition apply(Condition condition);
}
