package com.gitee.fastmybatis.core.query;

import com.gitee.fastmybatis.core.ext.spi.SpiContext;

/**
 * 条件参数值处理，可动态返回自定义值
 * @author tanghc
 */
public interface ConditionValueHandler {

    /**
     * 获取条件的值
     * @param defaultValue 默认值
     * @param fieldName 字段名称
     * @param target 字段所在的类
     * @return 返回实际值，返回null则忽略该字段，即不会拼接条件语句
     */
    Object getConditionValue(Object defaultValue, String fieldName, Object target);

    /** 默认的处理 */
    class DefaultConditionValueHandler implements ConditionValueHandler {
        @Override
        public Object getConditionValue(Object defaultValue, String fieldName, Object target) {
            return SpiContext.getConditionValueExecutor().getConditionValue(defaultValue, fieldName, target);
        }
    }
}
