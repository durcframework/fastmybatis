package com.gitee.fastmybatis.core.query;

/**
 * Lambda条件
 * @author thc
 * @param <E> 实体类
 */
public class LambdaCondition<E> extends Condition implements LambdaConditionAnd<LambdaCondition<E>, E>,
        LambdaConditionOr<LambdaCondition<E>, E>{

    private static final long serialVersionUID = 6022584368154042321L;

    @Override
    public Condition getCondition() {
        return this;
    }
}
