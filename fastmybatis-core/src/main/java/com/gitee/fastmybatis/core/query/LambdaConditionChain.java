package com.gitee.fastmybatis.core.query;

/**
 * Lambda查询链
 * @author thc
 */
@FunctionalInterface
public interface LambdaConditionChain<T> {
    LambdaCondition<T> apply(LambdaCondition<T> condition);
}
