package com.gitee.fastmybatis.core.query;

import com.gitee.fastmybatis.core.query.expression.BetweenValue;
import com.gitee.fastmybatis.core.query.expression.ValueConvert;
import com.gitee.fastmybatis.core.support.Getter;
import com.gitee.fastmybatis.core.util.ClassUtil;

import java.util.Collection;
import java.util.List;

/**
 * or 条件
 * @author thc
 */
public interface LambdaConditionOr<T, R> {

    Condition getCondition();

    /**
     * 添加等于条件<br>
     *
     * <code>query.orEq(User::getUserName, "Jim")</code>
     *
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orEq(Getter<R, ?> getter, Object value) {
        getCondition().orEq(ClassUtil.getColumnName(getter), value);
        return (T) this;
    }

    /**
     * 根据表达式添加等于条件
     * <pre>
     *     query.orEq(StringUtils.hasText(name), User::getUserName, name);
     *     等同于：
     *     if (StringUtils.hasText(name)) {
     *         query.orEq(User::getUserName, name);
     *     }
     * </pre>
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orEq(boolean expression, Getter<R, ?> getter, Object value) {
        if (expression) {
            orEq(getter, value);
        }
        return (T) this;
    }

    /**
     * 添加不等于条件
     *
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orNotEq(Getter<R, ?> getter, Object value) {
        getCondition().orNotEq(ClassUtil.getColumnName(getter), value);
        return (T) this;
    }

    /**
     * 根据表达式添加不等于条件
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orNotEq(boolean expression, Getter<R, ?> getter, Object value) {
        if (expression) {
            orNotEq(getter, value);
        }
        return (T) this;
    }

    /**
     * 添加大于条件,>
     *
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orGt(Getter<R, ?> getter, Object value) {
        getCondition().orGt(ClassUtil.getColumnName(getter), value);
        return (T) this;
    }

    /**
     * 根据表达式添加大于条件,>
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orGt(boolean expression, Getter<R, ?> getter, Object value) {
        if (expression) {
            orGt(getter, value);
        }
        return (T) this;
    }

    /**
     * 添加大于等于条件,>=
     *
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orGe(Getter<R, ?> getter, Object value) {
        getCondition().orGe(ClassUtil.getColumnName(getter), value);
        return (T) this;
    }

    /**
     * 根据表达式添加大于等于条件,>=
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orGe(boolean expression, Getter<R, ?> getter, Object value) {
        if (expression) {
            orGe(getter, value);
        }
        return (T) this;
    }

    /**
     * 添加小于条件,<
     *
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orLt(Getter<R, ?> getter, Object value) {
        getCondition().orLt(ClassUtil.getColumnName(getter), value);
        return (T) this;
    }

    /**
     * 根据表达式添加小于条件,<
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orLt(boolean expression, Getter<R, ?> getter, Object value) {
        if (expression) {
            orLt(getter, value);
        }
        return (T) this;
    }

    /**
     * 小于等于,<=
     *
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orLe(Getter<R, ?> getter, Object value) {
        getCondition().orLe(ClassUtil.getColumnName(getter), value);
        return (T) this;
    }

    /**
     * 根据表达式小于等于条件,<=
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orLe(boolean expression, Getter<R, ?> getter, Object value) {
        if (expression) {
            orLe(getter, value);
        }
        return (T) this;
    }

    /**
     * 添加两边模糊查询条件，两边模糊匹配，即name like '%value%'
     *
     * @param getter 方法函数
     * @param value      值,不需要加%
     * @return 返回Query对象
     * @see #orLikeLeft(Getter, String) 左边模糊匹配
     * @see #orLikeRight(Getter, String) 右边模糊匹配
     */
    default T orLike(Getter<R, ?> getter, String value) {
        getCondition().orLike(ClassUtil.getColumnName(getter), value);
        return (T) this;
    }

    /**
     * 根据表达式添加两边模糊查询条件，两边模糊匹配，即name like '%value%'
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @param value      值,不需要加%
     * @return 返回Query对象
     * @see #orLikeLeft(boolean, Getter, String) 左模糊
     * @see #orLikeRight(boolean, Getter, String) 右模糊
     */
    default T orLike(boolean expression, Getter<R, ?> getter, String value) {
        if (expression) {
            orLike(getter, value);
        }
        return (T) this;
    }

    /**
     * 添加左模糊查询条件，左边模糊匹配，即name like '%value'
     *
     * @param getter 方法函数
     * @param value      值,不需要加%
     * @return 返回Query对象
     */
    default T orLikeLeft(Getter<R, ?> getter, String value) {
        getCondition().orLikeLeft(ClassUtil.getColumnName(getter), value);
        return (T) this;
    }

    /**
     * 根据表达式添加左模糊查询条件，左边模糊匹配，即name like '%value'
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @param value      值,不需要加%
     * @return 返回Query对象
     */
    default T orLikeLeft(boolean expression, Getter<R, ?> getter, String value) {
        if (expression) {
            orLikeLeft(getter, value);
        }
        return (T) this;
    }

    /**
     * 添加右模糊查询条件，右边模糊匹配，即name like 'value%'。mysql推荐用这种
     *
     * @param getter 方法函数
     * @param value      值,不需要加%
     * @return 返回Query对象
     */
    default T orLikeRight(Getter<R, ?> getter, String value) {
        getCondition().orLikeRight(ClassUtil.getColumnName(getter), value);
        return (T) this;
    }

    /**
     * 根据表达式添加右模糊查询条件，右边模糊匹配，即name like 'value%'。mysql推荐用这种
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @param value      值,不需要加%
     * @return 返回Query对象
     */
    default T orLikeRight(boolean expression, Getter<R, ?> getter, String value) {
        if (expression) {
            orLikeRight(getter, value);
        }
        return (T) this;
    }

    /**
     * 添加IN条件
     *
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orIn(Getter<R, ?> getter, Collection<?> value) {
        getCondition().orIn(ClassUtil.getColumnName(getter), value);
        return (T) this;
    }

    /**
     * 根据表达式添加IN条件
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orIn(boolean expression, Getter<R, ?> getter, Collection<?> value) {
        if (expression) {
            orIn(getter, value);
        }
        return (T) this;
    }

    /**
     * 添加IN条件
     *
     * @param getter   数据库字段名
     * @param value        值
     * @param valueConvert 转换
     * @return 返回Query对象
     */
    default <E> T orIn(Getter<R, ?> getter, Collection<E> value, ValueConvert<E> valueConvert) {
        getCondition().orIn(ClassUtil.getColumnName(getter), value, valueConvert);
        return (T) this;
    }

    /**
     * 根据表达式添加IN条件
     *
     * @param expression   表达式，当为true时添加条件
     * @param getter   数据库字段名
     * @param value        值
     * @param valueConvert 转换
     * @return 返回Query对象
     */
    default <E> T orIn(boolean expression, Getter<R, ?> getter, Collection<E> value, ValueConvert<E> valueConvert) {
        if (expression) {
            orIn(getter, value, valueConvert);
        }
        return (T) this;
    }

    /**
     * 添加IN条件
     *
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orIn(Getter<R, ?> getter, Object[] value) {
        getCondition().orIn(ClassUtil.getColumnName(getter), value);
        return (T) this;
    }

    /**
     * 根据表达式添加IN条件
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orIn(boolean expression, Getter<R, ?> getter, Object[] value) {
        if (expression) {
            orIn(getter, value);
        }
        return (T) this;
    }

    /**
     * 添加not in条件
     *
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orNotIn(Getter<R, ?> getter, Collection<?> value) {
        getCondition().orNotIn(ClassUtil.getColumnName(getter), value);
        return (T) this;
    }

    /**
     * 根据表达式添加not in条件
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orNotIn(boolean expression, Getter<R, ?> getter, Collection<?> value) {
        if (expression) {
            orNotIn(getter, value);
        }
        return (T) this;
    }

    /**
     * 添加not in条件
     *
     * @param getter   数据库字段名
     * @param value        值
     * @param valueConvert 转换器
     * @return 返回Query对象
     */
    default <E> T orNotIn(Getter<R, ?> getter, Collection<E> value, ValueConvert<E> valueConvert) {
        getCondition().orNotIn(ClassUtil.getColumnName(getter), value, valueConvert);
        return (T) this;
    }

    /**
     * 根据表达式添加not in条件
     *
     * @param expression   表达式，当为true时添加条件
     * @param getter   数据库字段名
     * @param value        值
     * @param valueConvert 转换器
     * @return 返回Query对象
     */
    default <E> T orNotIn(boolean expression, Getter<R, ?> getter, Collection<E> value, ValueConvert<E> valueConvert) {
        if (expression) {
            orNotIn(getter, value, valueConvert);
        }
        return (T) this;
    }

    /**
     * 添加not in条件
     *
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orNotIn(Getter<R, ?> getter, Object[] value) {
        getCondition().orNotIn(ClassUtil.getColumnName(getter), value);
        return (T) this;
    }

    /**
     * 根据表达式添加not in条件
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @param value      值
     * @return 返回Query对象
     */
    default T orNotIn(boolean expression, Getter<R, ?> getter, Object[] value) {
        if (expression) {
            orNotIn(getter, value);
        }
        return (T) this;
    }

    /**
     * 添加between条件
     *
     * @param getter 方法函数
     * @param startValue 起始值
     * @param endValue   结束值
     * @return 返回Query对象
     */
    default T orBetween(Getter<R, ?> getter, Object startValue, Object endValue) {
        getCondition().orBetween(ClassUtil.getColumnName(getter), startValue, endValue);
        return (T) this;
    }

    /**
     * 添加between条件
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @param startValue 起始值
     * @param endValue   结束值
     * @return 返回Query对象
     */
    default T orBetween(boolean expression, Getter<R, ?> getter, Object startValue, Object endValue) {
        if (expression) {
            orBetween(getter, startValue, endValue);
        }
        return (T) this;
    }

    /**
     * 添加between条件
     * <pre>
     * {@literal
     * Object[] arr = new Object[]{1, 100};
     * query.orBetween(arr);
     * }
     * </pre>
     *
     * @param values 存放起始值、结束值，只能存放2个值，values[0]表示开始值，value[1]表示结束值
     * @return 返回Query对象
     */
    default T orBetween(Getter<R, ?> getter, Object[] values) {
        getCondition().orBetween(ClassUtil.getColumnName(getter), values);
        return (T) this;
    }

    /**
     * 添加between条件
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @param values     存放起始值、结束值，只能存放2个值，values[0]表示开始值，value[1]表示结束值
     * @return 返回Query对象
     */
    default T orBetween(boolean expression, Getter<R, ?> getter, Object[] values) {
        if (expression) {
            orBetween(getter, values);
        }
        return (T) this;
    }

    /**
     * 添加between条件
     * <pre>
     * {@literal
     * query.orBetween(Arrays.asList(1, 100));
     * }
     * </pre>
     *
     * @param values 存放起始值、结束值，只能存放2个值
     * @return 返回Query对象
     */
    default T orBetween(Getter<R, ?> getter, List<?> values) {
        getCondition().orBetween(ClassUtil.getColumnName(getter), values);
        return (T) this;
    }

    /**
     * 添加between条件
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @param values     存放起始值、结束值，只能存放2个值
     * @return 返回Query对象
     */
    default T orBetween(boolean expression, Getter<R, ?> getter, List<?> values) {
        if (expression) {
            orBetween(getter, values);
        }
        return (T) this;
    }

    /**
     * 添加between条件
     * <pre>
     * {@literal
     * query.orBetween(new BetweenValue(1, 100));
     * }
     * </pre>
     *
     * @param betweenValue 起始值、结束值包装对象
     * @return 返回Query对象
     */
    default T orBetween(Getter<R, ?> getter, BetweenValue betweenValue) {
        getCondition().orBetween(ClassUtil.getColumnName(getter), betweenValue);
        return (T) this;
    }

    /**
     * 添加between条件
     *
     * @param expression   表达式，当为true时添加条件
     * @param getter   数据库字段名
     * @param betweenValue 存放起始值、结束值，只能存放2个值
     * @return 返回Query对象
     */
    default T orBetween(boolean expression, Getter<R, ?> getter, BetweenValue betweenValue) {
        if (expression) {
            orBetween(getter, betweenValue);
        }
        return (T) this;
    }


    /**
     * 添加字段不为null的条件
     *
     * @param getter 方法函数
     * @return 返回Query对象
     */
    default T orNotNull(Getter<R, ?> getter) {
        this.getCondition().orSql(ClassUtil.getColumnName(getter) + " IS NOT NULL");
        return (T) this;
    }

    /**
     * 根据表达式添加字段不为null的条件
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @return 返回Query对象
     */
    default T orNotNull(boolean expression, Getter<R, ?> getter) {
        if (expression) {
            orNotNull(getter);
        }
        return (T) this;
    }

    /**
     * 添加字段是null的条件
     *
     * @param getter 方法函数
     * @return 返回Query对象
     */
    default T orIsNull(Getter<R, ?> getter) {
        this.getCondition().orSql(ClassUtil.getColumnName(getter) + " IS NULL");
        return (T) this;
    }

    /**
     * 根据表达式添加字段是null的条件
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @return 返回Query对象
     */
    default T orIsNull(boolean expression, Getter<R, ?> getter) {
        if (expression) {
            orIsNull(getter);
        }
        return (T) this;
    }

    /**
     * 添加不为空字符串条件
     *
     * @param getter 方法函数
     * @return 返回Query对象
     */
    default T orNotEmpty(Getter<R, ?> getter) {
        String column = ClassUtil.getColumnName(getter);
        this.getCondition().orSql(column + " IS NOT NULL AND " + column + " <> '' ");
        return (T) this;
    }

    /**
     * 根据表达式添加不为空字符串条件
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @return 返回Query对象
     */
    default T orNotEmpty(boolean expression, Getter<R, ?> getter) {
        if (expression) {
            orNotEmpty(getter);
        }
        return (T) this;
    }

    /**
     * 添加空字段条件，null或者空字符串
     *
     * @param getter 方法函数
     * @return 返回Query对象
     */
    default T orIsEmpty(Getter<R, ?> getter) {
        String column = ClassUtil.getColumnName(getter);
        this.getCondition().orSql(column + " IS NULL OR " + column + " = '' ");
        return (T) this;
    }

    /**
     * 根据表达式添加空字段条件，null或者空字符串
     *
     * @param expression 表达式，当为true时添加条件
     * @param getter 方法函数
     * @return 返回Query对象
     */
    default T orIsEmpty(boolean expression, Getter<R, ?> getter) {
        if (expression) {
            orIsEmpty(getter);
        }
        return (T) this;
    }

}
