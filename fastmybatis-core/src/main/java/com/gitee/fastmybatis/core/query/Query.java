package com.gitee.fastmybatis.core.query;

import com.gitee.fastmybatis.core.FastmybatisConfig;
import com.gitee.fastmybatis.core.SqlConsts;
import com.gitee.fastmybatis.core.annotation.LogicDelete;
import com.gitee.fastmybatis.core.query.expression.Expression;
import com.gitee.fastmybatis.core.query.expression.ExpressionJoinable;
import com.gitee.fastmybatis.core.query.expression.Expressional;
import com.gitee.fastmybatis.core.query.expression.Expressions;
import com.gitee.fastmybatis.core.query.expression.builder.ConditionBuilder;
import com.gitee.fastmybatis.core.query.param.IParam;
import com.gitee.fastmybatis.core.query.param.SchSortableParam;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

/**
 * 查询类
 *
 * <pre>
 * 查询姓名为张三，并且年龄为22岁的用户：
 * Query query = new Query().eq("username","张三").eq("age",22);
 * List<User> users = mapper.list(query);
 *
 * 查询年龄为10,20,30的用户：
 * Query query = new Query().in("age",Arrays.asList(10,20,30));
 * List<User> users = mapper.list(query);
 *
 * 查询注册日期大于2017-11-11的用户：
 * Date regDate = ...
 * Query query = new Query().gt("reg_date",regDate);
 * List<User> users = mapper.list(query);
 *
 * 查询性别为男的，年龄大于等于20岁的用户，按年龄降序：
 * Query query = new Query().eq("gender",1).ge("age",20).orderby("age",Sort.DESC);
 * List<User> users = mapper.list(query);
 *
 * 分页查询：
 * Query query = new Query().eq("age",10).page(1,10); // 第一页，每页10条数据
 * List<User> users = mapper.list(query);
 *
 * 查询总记录数：
 * Query query = new Query().eq("age",10).page(1,10); // 第一页，每页10条数据
 * long total = mapper.getCount(query); // 该条件下总记录数
 * </pre>
 *
 * @author tanghc
 */
public class Query implements Queryable, ConditionAnd<Query>, ConditionOr<Query>, Serializable {

    private static final long serialVersionUID = 3258849904672856978L;

    private static final String COUNT_EXPRESSION = "count(*) AS cnt";

    private String countExpression = COUNT_EXPRESSION;

    /**
     * 分页起始位置
     */
    private int start;
    /**
     * 分页大小
     */
    private int limit;
    /**
     * 总记录数，默认为-1，表示没有设置总记录数
     */
    private int total = -1;

    /**
     * 排序信息
     */
    private LinkedHashSet<String> orderInfo;

    /**
     * 额外参数，供xml使用
     */
    private Map<String, Object> paramMap;

    /**
     * 强力查询，设置为true，将无视删除字段
     */
    private boolean forceQuery;

    private boolean distinct;

    private boolean forceUpdate;

    /**
     * 连接表达式
     */
    private List<ExpressionJoinable> joinExpressions;

    private Condition condition = new Condition();

    private List<String> selectColumns = new ArrayList<>();

    /**
     * 构建更新查询
     * <pre>
     * {@literal
     * LambdaQuery<DocDiffRecord> updateQuery = Query.query(DocDiffRecord.class)
     *                         .set(DocDiffRecord::getDocKey, docKey)
     *                         .eq(DocDiffRecord::getId, docDiffRecord.getId());
     * mapper.update(updateQuery);
     * }
     * </pre>
     * @param clazz
     * @return
     * @param <T>
     */
    public static <T> LambdaQuery<T> query(Class<T> clazz) {
        return new LambdaQuery<>(clazz);
    }

    /**
     * 创建Query
     *
     * @return 返回Query实例
     */
    public static Query create() {
        return new Query();
    }

    /**
     * 创建用于更新的Query
     *
     * @return 返回Query实例
     */
    public static UpdateQuery updateQuery() {
        return new UpdateQuery();
    }

    /**
     * 创建多租户Query
     *
     * @return 返回Query实例
     */
    public static TenantQuery tenantQuery() {
        return new TenantQuery();
    }

    /**
     * 指定返回列
     *
     * @param column 表字段名
     * @return 返回Query实例
     */
    public Query select(String... column) {
        this.selectColumns.addAll(Arrays.asList(column));
        return this;
    }

    /**
     * Query转成LambdaQuery
     *
     * @param clazz 实体类class
     * @param <T>   实体类类型
     * @return 返回LambdaQuery
     */
    public <T> LambdaQuery<T> toLambdaQuery(Class<T> clazz) {
        return (LambdaQuery<T>) copyQuery(this, new LambdaQuery<>(clazz));
    }

    public Query copy() {
        return copyQuery(this, new Query());
    }

    private static Query copyQuery(Query from, Query target) {
        target.start = from.start;
        target.limit = from.limit;
        target.total = from.total;
        target.forceQuery = from.forceQuery;
        target.distinct = from.distinct;
        target.forceUpdate = from.forceUpdate;
        if (from.orderInfo != null) {
            target.orderInfo = new LinkedHashSet<>(from.orderInfo);
        }
        if (from.paramMap != null) {
            target.paramMap = new LinkedHashMap<>(from.paramMap);
        }
        if (from.joinExpressions != null) {
            target.joinExpressions = new ArrayList<>(from.joinExpressions);
        }

        target.condition = new Condition(from.condition);
        target.selectColumns = new ArrayList<>(from.selectColumns);
        return target;
    }

    /**
     * 添加关联条件
     *
     * @param joinSql 连接sql语句，如：“left join user_info t2 on t.id=t2.user_id”
     * @return 返回Query对象
     */
    public Query join(String joinSql) {
        if (FastmybatisConfig.DISABLE_JOIN_SQL_IN_QUERY) {
            throw new UnsupportedOperationException("Query.join() disabled with config:mybatis.disable-join-sql-in-query=true");
        }
        if (joinExpressions == null) {
            joinExpressions = new ArrayList<>(2);
        }
        joinExpressions.add((ExpressionJoinable) Expressions.join(joinSql));
        return this;
    }


    public Query and(ConditionChain conditionChain) {
        this.condition.subCondition(Joint.AND, conditionChain.apply(new Condition()));
        return this;
    }

    public Query or(ConditionChain conditionChain) {
        this.condition.subCondition(Joint.OR, conditionChain.apply(new Condition()));
        return this;
    }

    public List<Expression> getExpressions() {
        return condition.getExpressions();
    }


    // ------------ 设置分页信息 ------------

    /**
     * 设置分页信息
     *
     * @param pageIndex 当前第几页,从1开始
     * @param pageSize  每页结果集大小
     * @return 返回Query对象
     */
    public Query page(int pageIndex, int pageSize) {
        if (pageIndex < 1) {
            throw new IllegalArgumentException("pageIndex必须大于等于1");
        }
        if (pageSize < 1) {
            throw new IllegalArgumentException("pageSize必须大于等于1");
        }
        int start = (int) ((pageIndex - 1) * pageSize);
        return this.limit(start, pageSize);
    }

    /**
     * 设置分页信息,针对不规则分页。对应mysql分页语句：limit {start},{offset}
     *
     * @param start  记录起始位置
     * @param offset 偏移量
     * @return 返回Query对象
     */
    public Query limit(int start, int offset) {
        if (offset == 0) {
            this.setQueryAll(true);
            return this;
        }
        if (start < 0) {
            throw new IllegalArgumentException("public Query limit(int start, int offset)方法start必须大于等于0");
        }
        if (offset < 1) {
            throw new IllegalArgumentException("public Query limit(int start, int offset)方法offset必须大于等于1");
        }
        this.start = start;
        this.limit = offset;
        return this;
    }

    @Override
    public int getStart() {
        return this.start;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public boolean getIsSetTotal() {
        //不为-1，设置了总记录数
        return total != -1;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public int getLimit() {
        return this.limit;
    }

    // ------------ 设置分页信息 end ------------

    // ------------ 设置排序 ------------

    /**
     * 字段排序
     *
     * @param sortname 数据库字段名
     * @param sort     排序类型
     * @return 返回Query对象
     */
    public Query orderby(String sortname, Sort sort) {
        return this.addSort(sortname, sort);
    }

    /**
     * 设置字段倒序
     *
     * @param sortname 数据库字段名
     * @return 返回Query对象
     */
    public Query orderByDesc(String sortname) {
        return orderby(sortname, Sort.DESC);
    }

    /**
     * 设置字段正序
     *
     * @param sortname 数据库字段名
     * @return 返回Query对象
     */
    public Query orderByAsc(String sortname) {
        return orderby(sortname, Sort.ASC);
    }

    /**
     * 添加ASC排序字段,
     *
     * @param sortname 数据库字段名
     * @return 返回Query对象
     */
    public Query addSort(String sortname) {
        return this.addSort(sortname, SqlConsts.ASC);
    }

    /**
     * 添加字段排序
     *
     * @param sortname 数据库字段名
     * @param sort     排序类型
     * @return 返回Query对象
     */
    public Query addSort(String sortname, Sort sort) {
        return this.addSort(sortname, sort.name());
    }

    /**
     * 添加排序字段。 已废弃，推荐用：public Query addSort(String sortname, Sort sort)
     *
     * @param sortname  数据库字段名
     * @param sortorder 排序方式,ASC,DESC
     * @return 返回Query对象
     */
    public Query addSort(String sortname, String sortorder) {
        if (!SqlConsts.DESC.equalsIgnoreCase(sortorder) && !SqlConsts.ASC.equalsIgnoreCase(sortorder)) {
            throw new IllegalArgumentException("error order:" + sortorder);
        }
        if (sortname != null && sortname.length() > 0) {
            if (this.orderInfo == null) {
                orderInfo = new LinkedHashSet<String>();
            }
            // 简单防止SQL注入
            sortname = sortname.replaceAll(condition.getSqlInjectRegex(), SqlConsts.EMPTY);

            if (!SqlConsts.DESC.equalsIgnoreCase(sortorder)) {
                sortorder = SqlConsts.ASC;
            }

            orderInfo.add(sortname + SqlConsts.BLANK + sortorder);
        }

        return this;
    }

    @Override
    public boolean getOrderable() {
        return orderInfo != null && !orderInfo.isEmpty();
    }

    @Override
    public String getOrder() {
        if (orderInfo == null) {
            throw new NullPointerException("orderInfo为空,必须设置排序字段.");
        }
        StringBuilder sb = new StringBuilder();
        for (String order : this.orderInfo) {
            sb.append(",").append(order);
        }
        if (sb.length() > 0) {
            return sb.substring(1);
        } else {
            return "";
        }
    }
    // ------------ 设置排序 end ------------


    /**
     * 添加注解查询条件
     *
     * @param searchEntity 查询实体
     * @return 返回Query对象
     */
    public Query addAnnotionExpression(Object searchEntity) {
        bindExpressionsFromBean(searchEntity, this);
        return this;
    }


    /**
     * 添加排序信息
     *
     * @param searchEntity 查询实体
     * @return 返回Query对象
     */
    public Query addSortInfo(SchSortableParam searchEntity) {
        this.addSort(searchEntity.fetchDBSortname(), searchEntity.fetchSortorder());
        return this;
    }

    /**
     * 构建查询条件.
     *
     * @param searchEntity 查询实体
     * @return 返回Query对象
     */
    public static Query build(Object searchEntity) {
        if (searchEntity instanceof IParam) {
            return ((IParam) searchEntity).toQuery();
        } else {
            return buildFromBean(searchEntity);
        }
    }

    /**
     * 将bean中的字段转换成条件,字段名会统一转换成下划线形式.已废弃，改用Query.build(bean)
     *
     * <pre>
     * <code>
     * User user = new User();
     * user.setUserName("jim");
     * Query query = Query.buildFromBean(user);
     * </code>
     * 这样会组装成一个条件:where user_name='jim'
     * 更多功能可查看开发文档.
     * </pre>
     *
     * @param bean 查询实体
     * @return 返回Query对象
     */
    private static Query buildFromBean(Object bean) {
        Query query = new Query();
        bindExpressionsFromBean(bean, query);
        return query;
    }

    protected static void bindExpressionsFromBean(Object bean, Query query) {
        List<Expression> expressions = ConditionBuilder.getUnderlineFieldBuilder().buildExpressions(bean);
        for (Expression expression : expressions) {
            query.addExpression(expression);
        }
    }

    /**
     * 将bean中的字段转换成条件,不会将字段名转换成下划线形式.
     *
     * <pre>
     * <code>
     * User user = new User();
     * user.setUserName("jim");
     * Query query = Query.buildFromBeanByProperty(user);
     * </code>
     * 这样会组装成一个条件:where userName='jim'
     * 更多功能可查看开发文档.
     * </pre>
     *
     * @param bean 查询实体
     * @return 返回Query对象
     */
    public static Query buildFromBeanByProperty(Object bean) {
        Query query = new Query();
        List<Expression> expressions = ConditionBuilder.getCamelFieldBuilder().buildExpressions(bean);
        for (Expression expression : expressions) {
            query.addExpression(expression);
        }
        return query;

    }

    @Override
    public Expressional addExpression(Expression expression) {
        if (expression instanceof ExpressionJoinable) {
            if (joinExpressions == null) {
                joinExpressions = new ArrayList<>(2);
            }
            joinExpressions.add((ExpressionJoinable) expression);
        } else {
            condition.addExpression(expression);
        }
        return this;
    }

    public void addAll(List<Expression> expressions) {
        if (expressions != null) {
            for (Expression expression : expressions) {
                this.addExpression(expression);
            }
        }
    }

    /**
     * 添加额外参数
     *
     * @param name  参数名
     * @param value 值
     * @return 返回Query对象
     */
    public Query addParam(String name, Object value) {
        if (this.paramMap == null) {
            this.paramMap = new HashMap<>(16);
        }
        this.paramMap.put(name, value);
        return this;
    }

    @Override
    public Map<String, Object> getParam() {
        return this.paramMap;
    }

    @Override
    public boolean getIsQueryAll() {
        return this.limit == 0;
    }

    /**
     * 查询全部
     *
     * @param queryAll true，则查询全部
     * @return 返回Query对象
     */
    public Query setQueryAll(boolean queryAll) {
        if (queryAll) {
            this.limit = 0;
        }
        return this;
    }

    @Override
    public List<ExpressionJoinable> getJoinExpressions() {
        return ExpressionSortUtil.sort(this.joinExpressions);
    }

    public boolean getForceQuery() {
        return forceQuery;
    }

    /**
     * 开启强力查询，将无视逻辑删除字段
     *
     * @return 返回Query对象
     * @see LogicDelete 逻辑删除注解
     */
    public Query enableForceQuery() {
        this.forceQuery = true;
        return this;
    }

    /**
     * 无视逻辑删除字段
     *
     * @return 返回Query对象
     * @see LogicDelete 逻辑删除注解
     */
    public Query ignoreLogicDeleteColumn() {
        this.forceQuery = true;
        return this;
    }

    /**
     * 关闭强力查询，逻辑删除字段生效
     *
     * @return 返回Query对象
     * @see LogicDelete 逻辑删除注解
     */
    public Query disableForceQuery() {
        this.forceQuery = false;
        return this;
    }

    /**
     * 使用distinct，开启后，会在主键上加distinct。distinct(t.id)<br>
     * 仅限于mysql
     */
    public Query enableDistinct() {
        this.distinct = true;
        return this;
    }

    public Query disableDistinct() {
        this.distinct = false;
        return this;
    }

    public boolean getDistinct() {
        return this.distinct;
    }

    /**
     * 开启强制更新，那么null字段也会更新进去
     *
     * @return 返回query对象
     */
    public Query enableForceUpdate() {
        this.forceUpdate = true;
        return this;
    }

    public Query disableForceUpdate() {
        this.forceUpdate = false;
        return this;
    }

    public boolean getForceUpdate() {
        return this.forceUpdate;
    }

    /**
     * 设置参数占位符，默认'?'
     *
     * @param argPlaceholder 占位符
     * @return 返回Query对象
     */
    public Query argPlaceholder(String argPlaceholder) {
        condition.setArgPlaceholder(argPlaceholder);
        return this;
    }

    /**
     * 设置SQL注入正则表达式
     *
     * @param sqlInjectRegex 正则
     * @return 返回Query对象
     */
    public Query sqlInjectRegex(String sqlInjectRegex) {
        condition.setSqlInjectRegex(sqlInjectRegex);
        return this;
    }

    @Override
    public Condition getCondition() {
        if (FastmybatisConfig.FORCE_TENANT_QUERY && this.getClass() == Query.class) {
            throw new UnsupportedOperationException("new Query() is forbidden, please use new TenantQuery() or Query.createTenant()");
        }
        return condition;
    }

    public String getCountExpression() {
        return countExpression;
    }

    public void setCountExpression(String countExpression) {
        this.countExpression = countExpression;
    }

    public List<String> getSelectColumns() {
        return selectColumns;
    }

    public Query setSelectColumns(List<String> selectColumns) {
        this.selectColumns = selectColumns;
        return this;
    }

    public Query setOrderInfo(LinkedHashSet<String> orderInfo) {
        this.orderInfo = orderInfo;
        return this;
    }

    /**
     * 添加条件
     * @param column 字段
     * @param value 条件值，可以是单值/数组/集合
     * @return 返回自身
     */
    public Query addCondition(String column, Object value) {
        if (value != null) {
            if (value instanceof Collection<?>) {
                this.in(column, (Collection<?>) value);
            } else if (value.getClass().isArray()) {
                Object[] array = (Object[]) value;
                this.in(column, array);
            } else {
                this.eq(column, value);
            }
        }
        return this;
    }
}
