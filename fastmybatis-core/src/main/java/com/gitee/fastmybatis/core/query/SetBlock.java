package com.gitee.fastmybatis.core.query;

import java.util.LinkedHashMap;

/**
 * @author 六如
 */
public class SetBlock extends LinkedHashMap {

    private static final long serialVersionUID = -6991030351240083407L;
    private static final String EXP_PLACEHOLDER = "__EXP_PLACEHOLDER__";

    /**
     * 添加表达式，如：update_time=now()
     *
     * @param expression 表达式
     */
    public void addExpression(String expression) {
        put(expression, EXP_PLACEHOLDER);
    }


}
