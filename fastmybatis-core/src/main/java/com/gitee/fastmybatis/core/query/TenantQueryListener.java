package com.gitee.fastmybatis.core.query;

/**
 * @author thc
 */
@FunctionalInterface
public interface TenantQueryListener {

    /**
     * 每次初始化Query时触发
     * @param query TenantQuery
     */
    void onInitQuery(TenantQuery query);

}
