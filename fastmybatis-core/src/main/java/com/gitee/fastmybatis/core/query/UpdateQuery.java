package com.gitee.fastmybatis.core.query;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 条件更新
 * <pre>
 * {@literal
 *
 *
 * }
 * </pre>
 *
 * @author 六如
 */
public class UpdateQuery extends Query {

    private static final long serialVersionUID = 6395125477190867752L;

    private final Map<String, Object> set = new LinkedHashMap<>(8);

    /**
     * 设置字段值
     *
     * @param column 数据库字段名
     * @param value  值
     * @return 返回自身
     */
    public UpdateQuery set(String column, Object value) {
        set.put(column, value);
        return this;
    }

    public Map<String, Object> getSet() {
        return set;
    }
}
