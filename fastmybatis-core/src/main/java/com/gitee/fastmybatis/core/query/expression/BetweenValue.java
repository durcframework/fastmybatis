package com.gitee.fastmybatis.core.query.expression;

import java.io.Serializable;

/**
 * @author thc
 */
public class BetweenValue implements Serializable {

    private static final long serialVersionUID = -3695443921037949374L;

    /**
     * 开始值
     */
    private final Object startValue;

    /**
     * 结束值
     */
    private final Object endValue;

    public BetweenValue(Object startValue, Object endValue) {
        this.startValue = startValue;
        this.endValue = endValue;
    }


    public Object getStartValue() {
        return startValue;
    }

    public Object getEndValue() {
        return endValue;
    }
}
