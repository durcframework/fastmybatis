package com.gitee.fastmybatis.core.query.expression;

/**
 * @author tanghc
 */
public interface ExpressionBetweenable extends Expression {

    /**
     * 返回数据库字段名
     *
     * @return 返回数据库字段名
     */
    String getColumn();

    /**
     * 开始值
     * 
     * @return 返回开始值
     */
    Object getStartValue();

    /**
     * 结束值
     *
     * @return 返回结束值
     */
    Object getEndValue();
}
