package com.gitee.fastmybatis.core.query.expression;

/**
 * @author thc
 */
public enum ExpressionType {
    JOIN(-1),
    VALUE(0),
    IN(1),
    BETWEEN(2),
    SQL(3),
    /** 子表达式 */
    SUB(4),
    ;

    private final int type;

    ExpressionType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
