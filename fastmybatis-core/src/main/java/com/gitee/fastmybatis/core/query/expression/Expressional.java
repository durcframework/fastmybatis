package com.gitee.fastmybatis.core.query.expression;

import java.util.List;

/**
 * 查询条件支持
 * 
 * @author tanghc
 */
public interface Expressional {

    /**
     * 添加表达式
     * 
     * @param expression
     *            表达式对象
     * @return 返回Expressional对象
     */
    Expressional addExpression(Expression expression);

    /**
     * 返回连接表达式列表
     *
     * @return 返回连接表达式列表
     */
    List<ExpressionJoinable> getJoinExpressions();

}
