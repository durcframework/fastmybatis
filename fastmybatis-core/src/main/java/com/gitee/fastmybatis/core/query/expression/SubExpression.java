package com.gitee.fastmybatis.core.query.expression;

import com.gitee.fastmybatis.core.SqlConsts;
import com.gitee.fastmybatis.core.query.Condition;

/**
 * @author thc
 */
public class SubExpression implements Expression {

    private static final long serialVersionUID = 7017552650161378894L;

    private String joint = SqlConsts.AND;

    private final Condition condition;

    private int index = DEFAULT_INDEX;

    public SubExpression(Condition condition) {
        this.condition = condition;
    }

    public SubExpression(String joint, Condition condition) {
        this.joint = joint;
        this.condition = condition;
    }

    @Override
    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public int index() {
        return index;
    }

    @Override
    public ExpressionType expressionType() {
        return ExpressionType.SUB;
    }

    public Condition getCondition() {
        return condition;
    }

    public String getJoint() {
        return joint;
    }
}
