package com.gitee.fastmybatis.core.query.expression.builder.factory;

import com.gitee.fastmybatis.core.query.Joint;
import com.gitee.fastmybatis.core.query.Operator;
import com.gitee.fastmybatis.core.query.expression.BetweenExpression;
import com.gitee.fastmybatis.core.query.expression.Expression;

/**
 * @author thc
 */
public class BetweenExpressionFactory implements ExpressionFactory {
    @Override
    public Expression buildExpression(Joint joint, String columnName, Operator operator, Object value) {
        return new BetweenExpression(joint, columnName, value);
    }
}
