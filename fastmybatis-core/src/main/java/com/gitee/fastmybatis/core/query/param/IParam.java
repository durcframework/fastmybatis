package com.gitee.fastmybatis.core.query.param;

import com.gitee.fastmybatis.core.query.LambdaQuery;
import com.gitee.fastmybatis.core.query.Query;
import com.gitee.fastmybatis.core.query.TenantQuery;

import java.io.Serializable;

/**
 * @author thc
 */
public interface IParam extends Serializable {

    /**
     * 生成Query查询对象
     * @return 返回查询对象
     */
    default Query toQuery() {
        return new Query().addAnnotionExpression(this);
    }

    /**
     * 生成LambdaQuery查询对象
     * @param clazz 实体类class
     * @return 返回LambdaQuery
     * @param <T> 实体类
     */
    default <T> LambdaQuery<T> toLambdaQuery(Class<T> clazz) {
        return toQuery().toLambdaQuery(clazz);
    }

    /**
     * 生成TenantQuery查询对象
     * @return 返回TenantQuery
     */
    default TenantQuery toTenantQuery() {
        TenantQuery tenantQuery = new TenantQuery();
        tenantQuery.addAnnotionExpression(this);
        return tenantQuery;
    }
}
