package com.gitee.fastmybatis.core.query.param;

import com.gitee.fastmybatis.core.ext.code.util.FieldUtil;
import com.gitee.fastmybatis.core.query.Query;
import com.gitee.fastmybatis.core.query.TenantQuery;
import com.gitee.fastmybatis.core.query.annotation.Condition;

import java.util.List;

/**
 * 分页排序查询参数，支持多参数排序
 *
 * @author tanghc
 */
public class PageMultiSortParam extends PageParam implements SchPageableParam {

    private static final long serialVersionUID = -5188396714188791626L;

    /**
     * 排序字段
     */
    @Condition(ignore = true)
    private List<SortInfo> sorts;

    /**
     * 排字段，格式：<code>sortname1=sortorder1,sortname2=sortorder2</code><br>
     * 前端需要urlencode处理encodeURIComponent("id=desc,addTime=asc")，拼接请求链接：xx/order/find?sortInfo=id%3Ddesc%2CaddTime%3Dasc
     */
    @Condition(ignore = true)
    private String sortInfo;

    @Override
    public Query toQuery() {
        Query query = super.toQuery();
        addSort(query);
        return query;
    }

    @Override
    public TenantQuery toTenantQuery() {
        TenantQuery tenantQuery = super.toTenantQuery();
        addSort(tenantQuery);
        return tenantQuery;
    }

    private void addSort(Query query) {
        if (sortInfo != null && sortInfo.length() > 0) {
            String[] split = sortInfo.split(",");
            for (String item : split) {
                String[] pair = item.split("=");
                query.addSort(formatSortname(pair[0]), pair[1]);
            }
        } else if (sorts != null) {
            for (SortInfo sort : sorts) {
                query.addSort(formatSortname(sort.getSortname()), sort.getSortorder());
            }
        }
    }

    protected String formatSortname(String sortname) {
        return FieldUtil.camelToUnderline(sortname);
    }

    public List<SortInfo> getSorts() {
        return sorts;
    }

    public void setSorts(List<SortInfo> sorts) {
        this.sorts = sorts;
    }

    public String getSortInfo() {
        return sortInfo;
    }

    public void setSortInfo(String sortInfo) {
        this.sortInfo = sortInfo;
    }
}
