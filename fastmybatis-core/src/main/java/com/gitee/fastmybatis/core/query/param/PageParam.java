package com.gitee.fastmybatis.core.query.param;

import com.gitee.fastmybatis.core.query.annotation.Condition;

/**
 * 分页查询参数
 *
 * @author tanghc
 */
public class PageParam implements SchPageableParam {

    private static final long serialVersionUID = -1439545730744052118L;

    /**
     * 当前第几页
     *
     * @mock 1
     */
    @Condition(ignore = true)
    private int pageIndex = 1;

    /**
     * 每页记录数
     *
     * @mock 20
     */
    @Condition(ignore = true)
    private int pageSize = 20;

    /**
     * @return
     * @ignore
     */
    @Override
    public int fetchStart() {
        return (this.getPageIndex() - 1) * this.getPageSize();
    }

    /**
     * @return
     * @ignore
     */
    @Override
    public int fetchLimit() {
        return this.getPageSize();
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

}
