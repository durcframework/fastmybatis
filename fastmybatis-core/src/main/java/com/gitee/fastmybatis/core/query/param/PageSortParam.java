package com.gitee.fastmybatis.core.query.param;

import com.gitee.fastmybatis.core.query.Query;
import com.gitee.fastmybatis.core.query.TenantQuery;
import com.gitee.fastmybatis.core.query.annotation.Condition;
import com.gitee.fastmybatis.core.util.StringUtil;

/**
 * 分页排序查询参数，单字段排序，多字段排序参考：{@link PageMultiSortParam}
 *
 * @author tanghc
 * @see PageMultiSortParam 多字段排序
 */
public class PageSortParam extends PageParam implements SchPageableParam, SchSortableParam {

    private static final long serialVersionUID = 8763198019993334577L;

    /**
     * 排序字段，数据库字段
     *
     * @mock name
     */
    @Condition(ignore = true)
    private String sort;

    /**
     * 排序排序方式，asc或desc
     *
     * @mock desc
     */
    @Condition(ignore = true)
    private String order;

    @Override
    public Query toQuery() {
        Query query = super.toQuery();
        if (StringUtil.hasText(fetchDBSortname()) && StringUtil.hasText(fetchSortorder())) {
            return query.addSort(fetchDBSortname(), fetchSortorder());
        }
        return query;
    }

    @Override
    public TenantQuery toTenantQuery() {
        TenantQuery tenantQuery = super.toTenantQuery();
        if (StringUtil.hasText(fetchDBSortname()) && StringUtil.hasText(fetchSortorder())) {
            tenantQuery.addSort(fetchDBSortname(), fetchSortorder());
        }
        return tenantQuery;
    }

    @Override
    public String fetchSortname() {
        return getSort();
    }

    @Override
    public String fetchSortorder() {
        return getOrder();
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }
}
