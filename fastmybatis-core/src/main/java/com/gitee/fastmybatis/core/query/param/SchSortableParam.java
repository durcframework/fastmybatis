package com.gitee.fastmybatis.core.query.param;

import com.gitee.fastmybatis.core.ext.code.util.FieldUtil;
import com.gitee.fastmybatis.core.query.Query;
import com.gitee.fastmybatis.core.query.TenantQuery;

/**
 * @author tanghc
 */
public interface SchSortableParam extends IParam {
	/**返回排序字段
	 * @return 返回排序字段
	 */
	String fetchSortname();
	/**返回排序字段
	 * @return 返回排序字段
	 */
	String fetchSortorder();
	/**
	 * 数据库排序字段
	 * @return 返回数据库排序字段
	 */
	default String fetchDBSortname() {
		return FieldUtil.camelToUnderline(fetchSortname());
	}


	@Override
	default Query toQuery() {
		return IParam.super.toQuery().addSort(fetchDBSortname(), fetchSortorder());
	}

	@Override
	default TenantQuery toTenantQuery() {
		TenantQuery tenantQuery = IParam.super.toTenantQuery();
		tenantQuery.addSort(fetchDBSortname(), fetchSortorder());
		return tenantQuery;
	}

}
