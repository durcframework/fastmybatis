package com.gitee.fastmybatis.core.query.param;

import java.io.Serializable;

/**
 * @author thc
 */
public class SortInfo implements Serializable {
    private static final long serialVersionUID = -3418776158007181597L;

    /**
     * 排序字段
     * @mock addTime
     */
    private String sortname;

    /**
     * 排序方式，ASC,DESC
     * @mock DESC
     */
    private String sortorder;

    public SortInfo() {
    }

    public SortInfo(String sortname, String sortorder) {
        this.sortname = sortname;
        this.sortorder = sortorder;
    }

    public String getSortname() {
        return sortname;
    }

    public void setSortname(String sortname) {
        this.sortname = sortname;
    }

    public String getSortorder() {
        return sortorder;
    }

    public void setSortorder(String sortorder) {
        this.sortorder = sortorder;
    }
}
