package com.gitee.fastmybatis.core.query.param;

import com.gitee.fastmybatis.core.query.annotation.Condition;

/**
 * 排序查询参数
 * @author tanghc
 */
public class SortParam implements SchSortableParam {
	private static final long serialVersionUID = -7722723814617910040L;

	/**
	 * 排序字段，数据库字段
	 * @mock name
	 */
	@Condition(ignore = true)
	private String sort;

	/**
	 * 排序排序方式，asc或desc
	 * @mock desc
	 */
	@Condition(ignore = true)
	private String order;

	@Override
	public String fetchSortname() {
		return getSort();
	}

	@Override
	public String fetchSortorder() {
		return getOrder();
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}
}
