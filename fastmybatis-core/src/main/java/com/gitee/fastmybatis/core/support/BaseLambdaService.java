package com.gitee.fastmybatis.core.support;

import com.gitee.fastmybatis.core.FastmybatisContext;
import com.gitee.fastmybatis.core.ext.MapperRunner;
import com.gitee.fastmybatis.core.mapper.BaseMapper;
import com.gitee.fastmybatis.core.util.ClassUtil;


/**
 * 通用service<br>
 * 使用方式：
 * <pre>
 * <code>
 * {@literal
 * @Service
 * public class UserService extends BaseLambdaService<TUser, Integer, TUserMapper> {
 *
 * }
 * }
 * </code>
 * </pre>
 *
 * @param <E>      实体类，如：Student
 * @param <Mapper> Mapper接口
 * @author thc
 * @since 1.11.1
 */
public abstract class BaseLambdaService<E, Mapper extends BaseMapper<E>> implements LambdaService<E, Mapper> {

    public MapperRunner<Mapper> getMapperRunner() {
        return FastmybatisContext.getCrudMapperRunner(getEntityClass());
    }

    @Override
    public Class<E> getEntityClass() {
        return (Class<E>) ClassUtil.getSuperClassGenericTypeCache(getClass(), 0);
    }

}
