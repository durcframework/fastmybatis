package com.gitee.fastmybatis.core.support;

import com.gitee.fastmybatis.core.FastmybatisContext;
import com.gitee.fastmybatis.core.ext.MapperRunner;
import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.gitee.fastmybatis.core.util.ClassUtil;

import java.io.Serializable;


/**
 * 通用service<br>
 * 使用方式：
 * <pre>
 * <code>
 * {@literal
 * @Service
 * public class UserService extends BaseService<TUser, Integer, TUserMapper> {
 *
 * }
 * }
 * </code>
 * </pre>
 *
 * @param <E>      实体类，如：Student
 * @param <I>      主键类型，如：Long，Integer
 * @param <Mapper> Mapper接口
 * @author thc
 * @since 1.11.1
 */
public abstract class BaseService<E, I extends Serializable, Mapper extends CrudMapper<E, I>> implements CommonService<E, I, Mapper> {

    public MapperRunner<Mapper> getMapperRunner() {
        Class<E> entityClass = (Class<E>) ClassUtil.getSuperClassGenericTypeCache(getClass(), 0);
        return FastmybatisContext.getCrudMapperRunner(entityClass);
    }
}
