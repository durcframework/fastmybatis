package com.gitee.fastmybatis.core.support;

import com.gitee.fastmybatis.core.util.ClassUtil;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 字段设置辅助类
 * <pre>
 * {@literal
 * ColumnValue<TUser> columnValue = ColumnValue.create(TUser.class)
 *    .set(TUser::getUsername, "李四2")
 *    .set(TUser::getRemark, "123");
 * int i = mapper.updateByMap(columnValue, new Query().eq("id", 1));
 * }
 * </pre>
 *
 * @author thc
 */
public class ColumnValue<T> {

    // key: columnName
    private final Map<String, Object> valueMap = new LinkedHashMap<>(8);

    public static <E> ColumnValue<E> create(Class<E> clazz) {
        return new ColumnValue<>();
    }

    public ColumnValue<T> set(Getter<T, ?> getter, Object value) {
        String columnName = ClassUtil.getColumnName(getter);
        valueMap.put(columnName, value);
        return this;
    }

    public Map<String, Object> getValueMap() {
        return valueMap;
    }
}
