package com.gitee.fastmybatis.core.support;

import java.util.Arrays;
import java.util.List;

/**
 * 字段辅助类
 * <pre>
 * {@literal
 * TUser tUser = mapper.getBySpecifiedColumns(Columns.of(TUser::getId, TUser::getUsername), query);
 * }
 * </pre>
 * @author thc
 */
public class Columns<T> {

    private final List<Getter<T, ?>> functions;

    public Columns(List<Getter<T, ?>> functions) {
        this.functions = functions;
    }

    @SafeVarargs
    public static <E> Columns<E> of(Getter<E, ?> ...columns) {
        return new Columns<>(Arrays.asList(columns));
    }

    public List<Getter<T, ?>> getFunctions() {
        return functions;
    }
}
