package com.gitee.fastmybatis.core.support;

import java.io.Serializable;

/**
 * Get方法对应的函数式接口
 * <pre>
 * {@literal
 * TUser::getUsername
 * }
 * </pre>
 * @param <T>
 */
@FunctionalInterface
public interface Getter<T, R> extends Serializable {

    R get(T source);
}
