package com.gitee.fastmybatis.core.support;

import com.gitee.fastmybatis.core.mapper.CrudMapper;

import java.io.Serializable;

/**
 * 通用service接口<br>
 * 使用方式：
 * <pre>
 * <code>
 * {@literal
 * @Service
 * public class UserService implements IService<TUser, Integer> {
 *
 * }
 * }
 * </code>
 * </pre>
 *
 * @param <E> 实体类，如：Student
 * @param <I> 主键类型，如：Long，Integer
 * @author thc
 * @since 1.11.1
 * @see CommonService
 */
public interface IService<E, I extends Serializable> extends CommonService<E, I, CrudMapper<E, I>> {

}
