package com.gitee.fastmybatis.core.support;

/**
 * insert时的字段填充，LocalDateTime<br>
 * 在做insert操作时,如果表里面有gmt_create字段,则自动填充时间
 * @author tanghc
 *
 */
public class LocalDateTimeFillInsert extends LocalDateTimeFillGmtCreate {

    public LocalDateTimeFillInsert() {
    }

    public LocalDateTimeFillInsert(String columnName) {
        super(columnName);
    }
}
