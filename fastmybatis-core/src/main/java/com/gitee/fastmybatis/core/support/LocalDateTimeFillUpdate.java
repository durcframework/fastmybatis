package com.gitee.fastmybatis.core.support;

/**
 * update时的字段填充，LocalDateTime<br>
 * 在做insert或update操作时,如果表里面有gmt_update字段,则自动填充时间
 * @author tanghc
 *
 */
public class LocalDateTimeFillUpdate extends LocalDateTimeFillGmtModified {

    public LocalDateTimeFillUpdate() {
    }

    public LocalDateTimeFillUpdate(String columnName) {
        super(columnName);
    }
}
