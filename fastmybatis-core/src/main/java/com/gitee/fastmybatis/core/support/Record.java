package com.gitee.fastmybatis.core.support;

import com.gitee.fastmybatis.core.FastmybatisContext;
import com.gitee.fastmybatis.core.mapper.BaseMapper;
import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.gitee.fastmybatis.core.mapper.EditMapper;

/**
 * 支持Active Record模式<br>
 * <pre>
 * {@literal
 * // 保存全部字段
 * @Test
 * public void save() {
 *     UserInfoRecord userInfoRecord = new UserInfoRecord();
 *     userInfoRecord.setUserId(11);
 *     userInfoRecord.setCity("杭州");
 *     userInfoRecord.setAddress("西湖");
 *     boolean success = userInfoRecord.save();
 *     Assert.assertTrue(success);
 * }
 *
 * // 保存不为null的字段
 * @Test
 * public void saveIgnoreNull() {
 *     UserInfoRecord userInfoRecord = new UserInfoRecord();
 *     userInfoRecord.setUserId(11);
 *     userInfoRecord.setCity("杭州");
 *     userInfoRecord.setAddress("西湖");
 *     boolean success = userInfoRecord.saveIgnoreNull();
 *     Assert.assertTrue(success);
 * }
 *
 * // 修改全部字段
 * @Test
 * public void update() {
 *     UserInfoRecord userInfoRecord = new UserInfoRecord();
 *     userInfoRecord.setId(4);
 *     userInfoRecord.setUserId(11);
 *     userInfoRecord.setCity("杭州");
 *     userInfoRecord.setAddress("西湖");
 *     boolean success = userInfoRecord.update();
 *     Assert.assertTrue(success);
 * }
 *
 * // 修改不为null的字段
 * @Test
 * public void updateIgnoreNull() {
 *     UserInfoRecord userInfoRecord = new UserInfoRecord();
 *     userInfoRecord.setId(5);
 *     userInfoRecord.setUserId(11);
 *     userInfoRecord.setCity("杭州");
 *     userInfoRecord.setAddress("西湖");
 *     boolean success = userInfoRecord.updateIgnoreNull();
 *     Assert.assertTrue(success);
 * }
 *
 * // 保存或修改不为null的字段
 * @Test
 * public void saveOrUpdateIgnoreNull() {
 *     UserInfoRecord userInfoRecord = new UserInfoRecord();
 *     userInfoRecord.setUserId(11);
 *     userInfoRecord.setCity("杭州");
 *     userInfoRecord.setAddress("西湖");
 *     boolean success = userInfoRecord.saveOrUpdateIgnoreNull();
 *     Assert.assertTrue(success);
 *     System.out.println("id:" + userInfoRecord.getId());
 * }
 *
 * // 删除记录
 * @Test
 * public void delete() {
 *     UserInfoRecord userInfoRecord = new UserInfoRecord();
 *     userInfoRecord.setId(8);
 *     boolean success = userInfoRecord.delete();
 *     Assert.assertTrue(success);
 * }
 * }
 * </pre>
 *
 * @author thc
 * @since 2.0.0
 */
public interface Record {

    /**
     * 保存全部字段
     *
     * @return 是否保存成功
     */
    default boolean save() {
        return FastmybatisContext.getCrudMapperRunner(this.getClass()).run(mapper -> ((EditMapper)mapper).save(this)) > 0;
    }

    /**
     * 保存不为null的字段
     *
     * @return 是否保存成功
     */
    default boolean saveIgnoreNull() {
        return FastmybatisContext.getCrudMapperRunner(this.getClass()).run(mapper -> ((EditMapper)mapper).saveIgnoreNull(this)) > 0;
    }

    /**
     * 修改全部字段
     *
     * @return 是否修改成功
     */
    default boolean update() {
        return FastmybatisContext.getCrudMapperRunner(this.getClass()).run(mapper -> ((EditMapper)mapper).update(this)) > 0;
    }

    /**
     * 修改不为null的字段
     *
     * @return 是否修改成功
     */
    default boolean updateIgnoreNull() {
        return FastmybatisContext.getCrudMapperRunner(this.getClass()).run(mapper -> ((EditMapper)mapper).updateIgnoreNull(this)) > 0;
    }

    /**
     * 保存或修改全部字段
     *
     * @return 是否成功
     */
    default boolean saveOrUpdate() {
        return FastmybatisContext.getCrudMapperRunner(this.getClass()).run(mapper -> {
            if (mapper instanceof BaseMapper) {
                return ((BaseMapper) mapper).saveOrUpdate(this);
            } else {
                return ((CrudMapper) mapper).saveOrUpdate(this);
            }
        }) > 0;
    }

    /**
     * 保存或修改不为null的字段
     *
     * @return 是否成功
     */
    default boolean saveOrUpdateIgnoreNull() {
        return FastmybatisContext.getCrudMapperRunner(this.getClass()).run(mapper -> {
            if (mapper instanceof BaseMapper) {
                return ((BaseMapper) mapper).saveOrUpdateIgnoreNull(this);
            } else {
                return ((CrudMapper) mapper).saveOrUpdateIgnoreNull(this);
            }
        }) > 0;
    }

    /**
     * 删除记录（底层根据id删除），在有逻辑删除字段的情况下，做UPDATE操作。
     *
     * @return 是否成功
     */
    default boolean delete() {
        return FastmybatisContext.getCrudMapperRunner(this.getClass()).run(mapper -> ((EditMapper)mapper).delete(this)) > 0;
    }

}
