package com.gitee.fastmybatis.core.support;

public interface SqlWrapper {
    String wrap(String sql);
}

