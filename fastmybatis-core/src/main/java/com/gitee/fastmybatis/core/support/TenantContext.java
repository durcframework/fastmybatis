package com.gitee.fastmybatis.core.support;

import java.util.function.Supplier;

public class TenantContext {

    private static Supplier<Object> defaultBuilder = () -> null;
    private static ThreadLocal<Object> tenantLocal = new ThreadLocal<>();

    public static void setTenantId(Object tenantId) {
        tenantLocal.set(tenantId);
    }

    public static Object getTenantId() {
        Object tenantId = tenantLocal.get();
        if (tenantId == null) {
            tenantId = defaultBuilder.get();
        }
        return tenantId;
    }

    public static void remove() {
        tenantLocal.remove();
    }

    public static void setTenantLocal(ThreadLocal<Object> tenantLocal) {
        TenantContext.tenantLocal = tenantLocal;
    }

    public static void setDefaultBuilder(Supplier<Object> defaultBuilder) {
        TenantContext.defaultBuilder = defaultBuilder;
    }
}
