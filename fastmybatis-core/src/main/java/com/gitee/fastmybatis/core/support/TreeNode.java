package com.gitee.fastmybatis.core.support;

import java.util.List;

/**
 * @param <T> 节点类型
 * @param <I> 节点id类型
 * @author tanghc
 */
public interface TreeNode<T, I> {

    /**
     * 获取节点id值
     *
     * @return 返回id值
     */
    I takeId();

    /**
     * 获取父节点id
     *
     * @return 返回父节点id值
     */
    I takeParentId();

    /**
     * 设置子节点
     *
     * @param children 子节点
     */
    void setChildren(List<T> children);
}
