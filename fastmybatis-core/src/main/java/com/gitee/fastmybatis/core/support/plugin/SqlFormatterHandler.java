package com.gitee.fastmybatis.core.support.plugin;

import org.apache.ibatis.plugin.Invocation;

import java.util.Properties;

/**
 * @author thc
 */
public interface SqlFormatterHandler {

    void setProperties(Properties properties);

    default void doPrintSql(Invocation invocation) throws Exception {
        if (canPrint(invocation)) {
            String sql = getSql(invocation);
            sql = formatSql(sql);
            printSql(sql);
        }
    }

    /**
     * 能否打印sql
     *
     * @return
     */
    boolean canPrint(Invocation invocation);

    /**
     * 返回sql
     *
     * @param invocation invocation
     * @return 返回sql语句
     * @throws Exception
     */
    String getSql(Invocation invocation) throws Exception;

    /**
     * 格式化SQL
     *
     * @param sql 待格式化sql
     * @return 返回格式化后的SQL
     */
    String formatSql(String sql);

    /**
     * 打印SQL
     *
     * @param sql 完整SQL语句
     */
    void printSql(String sql);
}
