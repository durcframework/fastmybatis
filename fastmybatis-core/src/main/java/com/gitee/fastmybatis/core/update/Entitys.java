package com.gitee.fastmybatis.core.update;

import com.gitee.fastmybatis.core.FastmybatisContext;
import com.gitee.fastmybatis.core.update.ModifyAttrsRecordProxyFactory;
import com.gitee.fastmybatis.core.util.MyBeanUtil;

public class Entitys {

    private Entitys() {
    }

    public static <T> T of(T entity) {
        if (entity == null) {
            return null;
        }
        Object wrapperEntity = ModifyAttrsRecordProxyFactory.getInstance().get(entity.getClass());
        MyBeanUtil.copyPropertiesIgnoreNull(entity, wrapperEntity);
        return (T) wrapperEntity;
    }

    public static <T> T of(Class<T> clazz) {
        return ModifyAttrsRecordProxyFactory.getInstance().get(clazz);
    }

    public static <T> T of(Class<T> clazz, Object id) {
        T newEntity = ModifyAttrsRecordProxyFactory.getInstance().get(clazz);
        FastmybatisContext.setPkValue(newEntity, clazz, id);
        return newEntity;
    }


}
