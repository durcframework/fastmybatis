package com.gitee.fastmybatis.core.util;

import com.gitee.fastmybatis.core.FastmybatisConstants;
import com.gitee.fastmybatis.core.ext.exception.MapperFileException;
import org.apache.ibatis.logging.Log;
import org.apache.ibatis.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author thc
 */
public class MybatisFileUtil {

    private static final Log LOG = LogFactory.getLog(MybatisFileUtil.class);

    /**
     * 获取扩展文件内容
     * @param in 扩展文件
     * @return
     * @throws IOException
     * @throws DocumentException
     */
    public static String getExtFileContent(InputStream in) throws DocumentException {
        Document document = buildSAXReader().read(in);
        Element mapperNode = document.getRootElement();
        return trimMapperNode(mapperNode);
    }

    /**
     * 去除{@literal <mapper></mapper>}只保留中间内容部分
     * @param mapperNode
     * @return
     */
    public static String trimMapperNode(Element mapperNode) {
        String rootNodeName = mapperNode.getName();

        if (!FastmybatisConstants.NODE_MAPPER.equals(rootNodeName)) {
            throw new MapperFileException("mapper文件必须含有<mapper>节点,是否缺少<mapper></mapper>?");
        }
        // 去除namespace属性
        mapperNode.remove(FastmybatisConstants.NAMESPACE);
        String xml = mapperNode.asXML();
        xml = xml
                // 去除<mapper>
                .replace(FastmybatisConstants.MAPPER_START, FastmybatisConstants.EMPTY)
                // 去除</mapper>
                .replace(FastmybatisConstants.MAPPER_END, FastmybatisConstants.EMPTY)
                // 去除<mapper/>
                .replace(FastmybatisConstants.MAPPER_EMPTY, FastmybatisConstants.EMPTY);
        return xml;
    }

    private static SAXReader buildSAXReader() {
        SAXReader reader = new SAXReader();
        reader.setEncoding(FastmybatisConstants.ENCODE);
        try {
            reader.setFeature(FastmybatisConstants.SAXREADER_FEATURE, false);
        } catch (SAXException e) {
            LOG.error("reader.setFeature fail by ", e);
        }
        return reader;
    }
}
