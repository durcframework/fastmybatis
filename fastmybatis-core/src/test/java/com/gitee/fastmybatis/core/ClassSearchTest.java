package com.gitee.fastmybatis.core;

import com.gitee.fastmybatis.core.ext.spi.ClassSearch;
import com.gitee.fastmybatis.core.ext.spi.impl.MapperClassSearch;
import org.junit.Test;

import java.util.Set;

/**
 * @author thc
 */
public class ClassSearchTest {

    @Test
    public void test() {
        ClassSearch classSearch = new MapperClassSearch();
        try {
            Set<Class<?>> set = classSearch.search(EntityProcessor.class, "com.gitee.fastmybatis");
            for (Class<?> aClass : set) {
                System.out.println(aClass.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
