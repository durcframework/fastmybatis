package com.gitee.fastmybatis.core;

import com.gitee.fastmybatis.core.ext.code.util.FieldUtil;
import com.gitee.fastmybatis.core.ext.code.util.ReflectUtil;
import org.junit.Test;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.util.List;

import static java.lang.annotation.ElementType.FIELD;

/**
 * @author thc
 */
public class ClassUtilTest {

    @Test
    public void test() {
        List<Field> declaredFields = ReflectUtil.getDeclaredFields(User.class);
        for (Field declaredField : declaredFields) {
            boolean b = FieldUtil.hasTransientAnno(declaredField);
            System.out.println( declaredField + ":" + b);
        }
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(FIELD)
    @interface Transient {
    }

    interface S<T> {}
    class A {}
    class B implements S<A> {}


    static class User {
        private int id;

        @Transient
        private String userName;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }
    }
}
