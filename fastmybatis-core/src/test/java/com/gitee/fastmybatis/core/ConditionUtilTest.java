package com.gitee.fastmybatis.core;

import com.gitee.fastmybatis.core.ext.jpa.ConditionUtil;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author tanghc
 */
public class ConditionUtilTest {

    @Test
    public void test() {
        Assert.assertEquals("username DESC", ConditionUtil.buildOrderBy("name_order_by_username_desc"));
        Assert.assertEquals("username ASC", ConditionUtil.buildOrderBy("name_order_by_username"));
        Assert.assertEquals("username DESC,age ASC", ConditionUtil.buildOrderBy("name_order_by_username_desc_age"));
        Assert.assertEquals("username ASC,age DESC", ConditionUtil.buildOrderBy("name_order_by_username_asc_age_desc"));
        Assert.assertEquals("username DESC,age DESC,add_time ASC", ConditionUtil.buildOrderBy("name_order_by_username_desc_age_desc_add_time"));
    }

}
