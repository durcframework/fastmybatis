package com.gitee.fastmybatis.core;

import com.gitee.fastmybatis.core.query.Query;
import com.gitee.fastmybatis.core.query.TenantQuery;
import org.junit.Test;

public class ForceTenantTest {

    @Test
    public void tes() {
        FastmybatisConfig.FORCE_TENANT_QUERY = true;
        new Query().eq("a", 1);
    }

    @Test
    public void tes2() {
        FastmybatisConfig.FORCE_TENANT_QUERY = true;
        new TenantQuery().eq("a", 1);
    }

}
