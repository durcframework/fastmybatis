package com.gitee.fastmybatis.core;

import com.gitee.fastmybatis.core.handler.formatter.BankCardSensitiveFormatter;
import com.gitee.fastmybatis.core.handler.formatter.EmailSensitiveFormatter;
import com.gitee.fastmybatis.core.handler.formatter.IdCardSensitiveFormatter;
import com.gitee.fastmybatis.core.handler.formatter.MobileSensitiveFormatter;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author thc
 */
public class FormatterTest {

    @Test
    public void testFormatter() {
        Assert.assertEquals("137****5678", new MobileSensitiveFormatter().readFormat("13712345678"));
        Assert.assertEquals("123***********5678", new IdCardSensitiveFormatter().readFormat("123456789012345678"));
        Assert.assertEquals("1****@qq.com", new EmailSensitiveFormatter().readFormat("12345678@qq.com"));
        Assert.assertEquals("123456****5678", new BankCardSensitiveFormatter().readFormat("123456789012345678"));
    }

}
