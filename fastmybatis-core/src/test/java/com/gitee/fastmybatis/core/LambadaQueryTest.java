package com.gitee.fastmybatis.core;

import com.gitee.fastmybatis.annotation.Column;
import com.gitee.fastmybatis.core.query.LambdaQuery;
import org.junit.Test;

public class LambadaQueryTest {

    @Test
    public void test() {
        LambdaQuery<User> query = new LambdaQuery<>(User.class);
        query.eq(User::getUserName, "111")
                .between(User::getId, 1, 20)
                .eq(User::getUserAddress, "address");

        query.getExpressions().forEach(expression -> System.out.println(expression.toString()));


        LambdaQuery<User> query1 = LambdaQuery.create(User.class)
                .eq(User::getUserName, "222");
        query1.getExpressions().forEach(expression -> System.out.println(expression.toString()));
    }


   public static class User {
        private int id;

        @Column(name = "username")
        private String userName;

        private String userAddress;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

       public String getUserAddress() {
           return userAddress;
       }
   }

}
