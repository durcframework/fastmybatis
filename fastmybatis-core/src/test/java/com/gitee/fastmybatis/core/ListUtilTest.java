package com.gitee.fastmybatis.core;

import com.gitee.fastmybatis.core.util.ListUtil;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author thc
 */
public class ListUtilTest extends TestCase {

    @Test
    public void test() {
        List<Integer> list = Stream.of(1, 2, 3, 4, 5, 6).collect(Collectors.toList());
        List<List<Integer>> partition = ListUtil.partition(list, 2);
        System.out.println(partition.size());
        for (List<Integer> subList : partition) {
            System.out.println(subList);
        }
        System.out.println("partition.get:" + partition.get(partition.size() - 1));
    }
}
