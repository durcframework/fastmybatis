package com.gitee.fastmybatis.core;

import com.gitee.fastmybatis.core.ext.spi.BeanExecutor;
import com.gitee.fastmybatis.core.ext.spi.SpiContext;
import com.gitee.fastmybatis.core.util.MyBeanUtil;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author thc
 */
public class MyBeanUtilTest {

    @Test
    public void copy() {
        Person person = new Person();
        person.setId(1);
        person.setName("jim");
        person.setAge(22);
        person.setDate(new Date());
        person.setMoney(new BigDecimal("100"));

        Student student = new Student();
        MyBeanUtil.copyPropertiesIgnoreNull(person, student);
        Assert.assertEquals("Student{id=1, name='jim', age=22}", student.toString());
    }

    @Test
    public void copy2() {
        Person person = new Person();
        person.setId(1);
        person.setName("jim");
        person.setAge(22);
        person.setDate(new Date());
        person.setMoney(new BigDecimal("100"));

        Teacher target = new Teacher();
        MyBeanUtil.copyPropertiesIgnoreNull(person, target);
        Assert.assertEquals("Teacher{id=0, name='jim', age=22}", target.toString());
    }

    @Test
    public void copy3() {
        Person person = new Person();
        person.setId(1);
        person.setName("jim");
        person.setAge(22);
        person.setAddTime(LocalDateTime.of(2022,1,1,1,1,1,1));
        person.setMoney(new BigDecimal("100.0001"));
        person.setRegister(true);

        Man man = new Man();
        MyBeanUtil.copyProperties(person, man);
        System.out.println(man);

        Assert.assertEquals("Person{id=1, name='jim', age=22, date=null, addTime=2022-01-01T01:01:01.000000001, money=100.0001, isDel=0, isRegister=true}", man.toString());
    }

    @Test
    public void copyBoolean() {
        PojoBool pojoBool = new PojoBool();
        pojoBool.setActive(true);
        pojoBool.setIsOld(true);

        PojoBool pojoBool1 = new PojoBool();
        MyBeanUtil.copyProperties(pojoBool, pojoBool1);
        Assert.assertTrue(pojoBool1.getIsOld());
        Assert.assertTrue(pojoBool1.isActive());
    }

    @Test
    public void pojoToValue() {
        Person person = new Person();
        person.setId(1);
        person.setName("jim");
        person.setAge(22);
        person.setDate(new Date());
        person.setAddTime(LocalDateTime.now());
        person.setMoney(new BigDecimal("100.0001"));
        person.setIsDel((byte) 0);

        BeanExecutor beanExecutor = SpiContext.getBeanExecutor();

        assert beanExecutor.pojoToValue(person, int.class, "id").equals(person.getId());
        assert beanExecutor.pojoToValue(person, String.class, "name").equals(person.getName());
        assert beanExecutor.pojoToValue(person, Integer.class, "age").equals(person.getAge());
        assert beanExecutor.pojoToValue(person, Date.class, "date").equals(person.getDate());
        assert beanExecutor.pojoToValue(person, LocalDateTime.class, "addTime").equals(person.getAddTime());
        assert beanExecutor.pojoToValue(person, BigDecimal.class, "money").equals(person.getMoney());
        assert beanExecutor.pojoToValue(person, boolean.class, "isDel").equals(false);
    }

    public static class PojoBool {
        private boolean isOld;
        private boolean active;

        public boolean getIsOld() {
            return isOld;
        }

        public void setIsOld(boolean old) {
            isOld = old;
        }

        public boolean isActive() {
            return active;
        }

        public void setActive(boolean active) {
            this.active = active;
        }
    }


    public static class Teacher {
        private long id;
        private String name;
        private Integer age;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "Teacher{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }

    public static class Student {
        private int id;
        private String name;
        private Integer age;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "Student{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }

    public static class Person {
        private int id;
        private String name;
        private Integer age;
        private Date date;
        private LocalDateTime addTime;
        private BigDecimal money;
        private byte isDel;
        private boolean isRegister;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public BigDecimal getMoney() {
            return money;
        }

        public void setMoney(BigDecimal money) {
            this.money = money;
        }

        public LocalDateTime getAddTime() {
            return addTime;
        }

        public void setAddTime(LocalDateTime addTime) {
            this.addTime = addTime;
        }

        public byte getIsDel() {
            return isDel;
        }

        public void setIsDel(byte isDel) {
            this.isDel = isDel;
        }

        public boolean isRegister() {
            return isRegister;
        }

        public void setRegister(boolean register) {
            isRegister = register;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", age=" + age +
                    ", date=" + date +
                    ", addTime=" + addTime +
                    ", money=" + money +
                    ", isDel=" + isDel +
                    ", isRegister=" + isRegister +
                    '}';
        }
    }

    public static class Man extends Person {
        @Override
        public String toString() {
            return super.toString();
        }
    }

}
