package com.gitee.fastmybatis.core;

import com.gitee.fastmybatis.core.support.plugin.SqlFormatter;
import junit.framework.TestCase;
import org.junit.Test;

/**
 * @author thc
 */
public class SqlFormatterTest extends TestCase {

    private SqlFormatter sqlFormatter = new SqlFormatter();

    @Test
    public void test() {
        String sql = "select `is_rent`, `stop_status`, count(*) as COUNT \n" +
                "from `dhf_equipment_rent_record` \n" +
                "where `store_id` not in (\"54\", \"69\") and `status` not in (\"6\", \"7\") and `date` >= \"2022-01-01\" group by `is_rent`, `stop_status`;\n";
        String format = sqlFormatter.format(sql);
        System.out.println(format);
        System.out.println("-------------");
    }

    @Test
    public void test2() {
        String sql = "SELECT\n" +
                " t1.type AS `type`, COUNT(1) AS `count`\n" +
                "FROM\n" +
                " `task_instance_info` t1\n" +
                "LEFT JOIN task_transfer_log t2 ON t1.id = t2.task_id\n" +
                "WHERE\n" +
                " t1.tenant_id = 0 AND t1.deleted = 0 AND t2.transferee_id IN (1,10,3,4, 888)\n" +
                "GROUP BY t1.type;";
        String format = sqlFormatter.format(sql);
        System.out.println(format);
        System.out.println("-------------");
    }

    @Test
    public void test3() {
        String sql = "SELECT carrier_id id, AVG(work_on_time) work_on_time_avg_day, AVG(lift_loops_on_times) lift_on_times_avg_day\n" +
                "     FROM xxx\n" +
                "     WHERE collect_date BETWEEN TO_DATE(DATE_ADD(CURDATE(), INTERVAL -30 DAY)) AND CURDATE() AND id=1\n" +
                "     GROUP BY id\n" +
                "     ORDER BY id ASC, name desc;";
        String format = sqlFormatter.format(sql);
        System.out.println(format);
        System.out.println("-------------");
    }

    @Test
    public void test4() {
        String sql = "INSERT INTO `t_user` (`id`, `username`, `state`, `isdel`, `remark`, `add_time`, `money`, `left_money`) VALUES (192, 'username2', 0, b'0', 'remark1', '2022-09-05 09:02:28', 1.00, 200);";
        String format = sqlFormatter.format(sql);
        System.out.println(format);
        System.out.println("-------------");
    }

    @Test
    public void test5() {
        String sql = "UPDATE `t_user` SET `id`=186, `username`='username1', `state`=0, `isdel`=b'0', `remark`=NULL, `add_time`='2022-09-05 09:02:28', `money`=1.00, `left_money`=NULL WHERE `id`=186;\n";
        String format = sqlFormatter.format(sql);
        System.out.println(format);
        System.out.println("-------------");
    }

    @Test
    public void test6() {
        String sql = "DELETE FROM `t_user` WHERE `id`=185;";
        String format = sqlFormatter.format(sql);
        System.out.println(format);
        System.out.println("-------------");
    }

}
