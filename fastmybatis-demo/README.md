## fastmybatis-demo

fastmybatis的demo

- fastmybatis-demo-standard：标准用法，没有依赖Spring，只依赖了mybatis.jar
- fastmybatis-demo-standard-mutil-datasource：标准用法多数据源示例，没有依赖Spring，只依赖了mybatis.jar
- fastmybatis-demo-vert.x：整合[Vert.x](https://vertx.io/)
- fastmybatis-demo-springboot：整合springboot，无须配置数据库，可直接执行单元测试进行体验
- fastmybatis-demo-testcase：各种测试用例
- fastmybatis-demo-springmvc：整合springmvc
- springboot-multi-datasource：整合springboot多数据源，分package
- springboot-multi-datasource-routing：整合springboot多数据源，不分package，动态切换数据源
- mutil-module：演示多模块
- tenant-demo：演示多租户，同一数据库，同一张表，通过字段区分
- tenant-demo-table：演示多租户，同一数据库，不同表
- fastmybatis-demo-plugin：演示插件（分表）
