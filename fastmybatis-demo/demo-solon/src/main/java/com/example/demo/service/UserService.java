package com.example.demo.service;

import com.example.demo.dao.TUserMapper;
import com.example.demo.model.TUser;
import com.gitee.fastmybatis.core.support.BaseLambdaService;
import org.noear.solon.annotation.Component;

/**
 * @author 六如
 */
@Component
public class UserService extends BaseLambdaService<TUser, TUserMapper> {
}
