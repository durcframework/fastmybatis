package com.myapp;

import com.gitee.fastmybatis.core.ext.code.util.JavaTypeUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FastmybatisSpringbootOldApplication {

    public static void main(String[] args) {
        JavaTypeUtil.addJavaType("List", "List", "VARCHAR");
        SpringApplication.run(FastmybatisSpringbootOldApplication.class, args);
    }
}
