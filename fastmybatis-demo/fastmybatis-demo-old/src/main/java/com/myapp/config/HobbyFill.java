package com.myapp.config;

import com.alibaba.fastjson.JSON;
import com.gitee.fastmybatis.core.handler.BaseFill;
import com.gitee.fastmybatis.core.handler.FillType;
import com.myapp.entity.UserHobby;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;

/**
 * application.properties新增配置：
 * <code>
 * mybatis.fill.com.myapp.config.HobbyFill=
 * </code>
 * <p>
 * 格式：mybatis.fill.包名.类名=
 * </p>
 * 自定义字段保存类型
 */
public class HobbyFill extends BaseFill<List<String>> {

    @Override
    public boolean match(Class<?> entityClass, Field field, String columnName) {
        return entityClass == UserHobby.class && Objects.equals(columnName, "hobby");
    }

    @Override
    public String getColumnName() {
        return "hobby";
    }

    @Override
    public FillType getFillType() {
        return FillType.UPDATE;
    }

    // 数据库值转换到实体类
    @Override
    protected List<String> convertValue(Object columnValue) {
        return JSON.parseArray(String.valueOf(columnValue), String.class);
    }

    // 保存到数据库
    @Override
    protected Object getFillValue(List<String> defaultValue) {
        return JSON.toJSONString(defaultValue);
    }
}