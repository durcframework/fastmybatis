package com.myapp;

import com.gitee.fastmybatis.core.ext.code.util.JavaTypeUtil;
import com.myapp.dao.UserHobbyMapper;
import com.myapp.entity.UserHobby;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * 演示字段转换
 * @author thc
 */
public class UserHobbyTest extends FastmybatisSpringbootOldApplicationTests {

    @Resource
    private UserHobbyMapper userHobbyMapper;

    static {
        JavaTypeUtil.addJavaType("List", "List", "VARCHAR");
    }

    @Test
    public void save() {
        UserHobby userHobby = new UserHobby();
        // 自定义类型
        userHobby.setHobby(Arrays.asList("旅游", "摄影"));
        userHobbyMapper.save(userHobby);
    }

    @Test
    public void get() {
        UserHobby userHobby = userHobbyMapper.getById(17);
        List<String> hobby = userHobby.getHobby();
        System.out.println(hobby);
    }

}
