package com.myapp;

import com.gitee.fastmybatis.core.query.Query;
import com.myapp.dao.UserLogMapper;
import com.myapp.entity.UserLog;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author tanghc
 */
public class PluginTest extends BaseTests {

    @Autowired
    private UserLogMapper userLogMapper;

    private static final int TABLE_COUNT = 4;

    @Test
    public void testInsert() {
        int userId = 111;
        int tableIndex = userId % TABLE_COUNT;
        RequestContext.getCurrentContext().setIndex(tableIndex);
        UserLog userLog = new UserLog();
        userLog.setUserId(111);
        userLog.setLog("insert 111");
        userLogMapper.saveIgnoreNull(userLog);
    }

    @Test
    public void testUpdate() {
        int userId = 111;
        int tableIndex = userId % TABLE_COUNT;
        RequestContext.getCurrentContext().setIndex(tableIndex);
        UserLog userLog = userLogMapper.getByColumn("user_id", userId);
        userLog.setLog("update 111");
        userLog.setUserId(userId);
        userLogMapper.updateIgnoreNull(userLog);
    }

    @Test
    public void testGet() {
        int userId = 111;
        int tableIndex = userId % TABLE_COUNT;
        RequestContext.getCurrentContext().setIndex(tableIndex);
        UserLog userLog = userLogMapper.getById(1L);
        userLog.setUserId(userId);
        System.out.println(userLog);
    }

    @Test
    public void testQuery() {
        int userId = 111;
        int tableIndex = userId % TABLE_COUNT;
        RequestContext.getCurrentContext().setIndex(tableIndex);
        Query query = new Query();
        query.eq("user_id", userId);
        List<UserLog> list = userLogMapper.list(query);
        System.out.println(list);
    }

}
