package com.myapp;

import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BaseTest {

    public void print(Object o) {
        System.out.println("=================");
        System.out.println(o);
        System.out.println("=================");
    }

}
