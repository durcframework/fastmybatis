package com.myapp;

import com.myapp.dao.TUserMapper;
import com.myapp.entity.TUser;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

/**
 * @author tanghc
 */
public class JpaFindTest extends BaseTest {

    @Resource
    TUserMapper mapper;

    @Test
    public void findById() {
        TUser user = mapper.findById(6);
        System.out.println(user);
        Assertions.assertNotNull(user);
    }

    @Test
    public void findByIdIs() {
        TUser user = mapper.findByIdIs(6);
        System.out.println(user);
        Assertions.assertNotNull(user);

        user = mapper.findByIdEquals(6);
        System.out.println(user);
        Assertions.assertNotNull(user);
    }

    @Test
    public void findByIdLessThan() {
        List<TUser> users = mapper.findByIdLessThan(6);
        Assertions.assertEquals(5, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByIdLessThanEqual() {
        List<TUser> users = mapper.findByIdLessThanEqual(3);
        Assertions.assertEquals(3, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByIdGreaterThan() {
        List<TUser> users = mapper.findByIdGreaterThan(3);
        Assertions.assertEquals(3, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByIdGreaterThanEqual() {
        List<TUser> users = mapper.findByIdGreaterThanEqual(3);
        Assertions.assertEquals(4, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByIdAfter() {
        List<TUser> users = mapper.findByIdAfter(3);
        Assertions.assertEquals(3, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByIdBefore() {
        List<TUser> users = mapper.findByIdBefore(3);
        Assertions.assertEquals(2, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByUsernameAndAddTimeNull() {
        List<TUser> users = mapper.findByUsernameAndAddTimeNull("张三");
        Assertions.assertEquals(0, users.size());
        users.forEach(System.out::println);

        users = mapper.findByUsernameAndAddTimeIsNull("张三");
        Assertions.assertEquals(0, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByUsernameAndAddTimeNotNull() {
        List<TUser> users = mapper.findByUsernameAndAddTimeNotNull("张三");
        Assertions.assertEquals(5, users.size());
        users.forEach(System.out::println);

        users = mapper.findByUsernameAndAddTimeIsNotNull("张三");
        Assertions.assertEquals(5, users.size());
        users.forEach(System.out::println);
    }


    @Test
    public void findByUsernameLike() {
        List<TUser> users = mapper.findByUsernameLike("张");
        Assertions.assertEquals(5, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByUsernameStartingWith() {
        List<TUser> users = mapper.findByUsernameStartingWith("张");
        Assertions.assertEquals(5, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByUsernameEndingWith() {
        // firstname like '%三'
        List<TUser> users = mapper.findByUsernameEndingWith("三");
        Assertions.assertEquals(5, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByUsernameContaining() {
        List<TUser> users = mapper.findByUsernameContaining("张");
        Assertions.assertEquals(5, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByUsernameOrderByIdDescAddTimeAsc() {
        List<TUser> users = mapper.findByUsernameOrderByIdDescAddTimeAsc("张三");
        Assertions.assertEquals(5, users.size());
        users.forEach(System.out::println);
    }


    @Test
    public void findByIdOrUsernameEndingWithOrRemarkStartingWith() {
        // firstname like '%三'
        List<TUser> users = mapper.findByIdOrUsernameEndingWithOrRemarkStartingWith(6,"三", "11");
        Assertions.assertEquals(5, users.size());
        users.forEach(System.out::println);
    }



    @Test
    public void findByIdBetween() {
        List<TUser> users = mapper.findByIdBetween(1, 6);
        Assertions.assertEquals(6, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByNameLikeOrIdBetween() {
        List<TUser> users = mapper.findByUsernameLikeOrIdBetween("王", 2, 3);
        users.forEach(System.out::println);
        Assertions.assertEquals(3, users.size());
    }


    @Test
    public void findByStateTrue() {
        List<TUser> users = mapper.findByStateTrue();
        users.forEach(System.out::println);
        Assertions.assertEquals(1, users.size());
    }

    @Test
    public void findByStateFalse() {
        List<TUser> users = mapper.findByStateFalse();
        users.forEach(System.out::println);
        Assertions.assertEquals(5, users.size());
    }

    @Test
    public void findByIdIn() {
        List<TUser> users = mapper.findByIdIn(Arrays.asList(1,2,3));
        Assertions.assertEquals(3, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByIdNotIn() {
        List<TUser> users = mapper.findByIdNotIn(Arrays.asList(1,2,3));
        Assertions.assertEquals(3, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByUsernameNot() {
        List<TUser> users = mapper.findByUsernameNot("张三");
        Assertions.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByUsernameAndState() {
        List<TUser> users = mapper.findByUsernameAndState("张三", (byte) 0);
        users.forEach(System.out::println);
        Assertions.assertEquals(4, users.size());
    }

    @Test
    public void findByUsernameIgnoreCase() {
        List<TUser> users = mapper.findByUsernameIgnoreCase("张三");
        users.forEach(System.out::println);
        Assertions.assertEquals(5, users.size());
    }



    @Test
    public void findByUsernameLikeAndLeftMoneyLessThanOrState() {
        List<TUser> users = mapper.findByUsernameLikeAndLeftMoneyLessThanOrState("张三", 30, (byte) 0);
        users.forEach(System.out::println);
        Assertions.assertEquals(6, users.size());
    }

    // TODO:  After, Before


}
