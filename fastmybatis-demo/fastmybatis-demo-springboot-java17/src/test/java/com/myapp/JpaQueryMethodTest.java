package com.myapp;

import com.gitee.fastmybatis.core.query.Query;
import com.myapp.dao.StudentMapper;
import com.myapp.entity.Student;
import jakarta.annotation.Resource;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * JPA findBy查询
 * @author tanghc
 */
public class JpaQueryMethodTest extends BaseTest {

    @Resource
    StudentMapper mapper;


    @Test
    public void findByLastnameAndFirstname() {
        List<Student> users = mapper.findByLastnameAndFirstname("张", "三");
        Assertions.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByLastnameOrFirstname() {
        List<Student> users = mapper.findByLastnameOrFirstname("王", "三");
        Assertions.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByFirstname() {
        List<Student> users = mapper.findByFirstname("三");
        Assertions.assertEquals(1, users.size());
        users.forEach(System.out::println);

        users = mapper.findByFirstnameIs("三");
        Assertions.assertEquals(1, users.size());
        users.forEach(System.out::println);

        users = mapper.findByFirstnameEquals("三");
        Assertions.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }


    @Test
    public void findByStartDateBetween() throws ParseException {
        Date start = DateUtils.parseDate("2018-02-21", "yyyy-MM-dd");
        Date end = DateUtils.parseDate("2019-12-21", "yyyy-MM-dd");
        List<Student> users = mapper.findByStartDateBetween(start, end);
        Assertions.assertEquals(2, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByAgeLessThan() {
        List<Student> users = mapper.findByAgeLessThan(30);
        Assertions.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByAgeLessThanEqual() {
        List<Student> users = mapper.findByAgeLessThanEqual(30);
        Assertions.assertEquals(2, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByAgeGreaterThan() {
        List<Student> users = mapper.findByAgeGreaterThan(30);
        Assertions.assertEquals(2, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByAgeGreaterThanEqual() {
        List<Student> users = mapper.findByAgeGreaterThanEqual(30);
        Assertions.assertEquals(3, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByStartDateAfter() throws ParseException {
        Date date = DateUtils.parseDate("2018-02-21", "yyyy-MM-dd");
        List<Student> users = mapper.findByStartDateAfter(date);
        Assertions.assertEquals(2, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByStartDateBefore() throws ParseException {
        Date date = DateUtils.parseDate("2018-02-21", "yyyy-MM-dd");
        List<Student> users = mapper.findByStartDateBefore(date);
        Assertions.assertEquals(2, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByAgeNull() {
        List<Student> users = mapper.findByAgeNull();
        Assertions.assertEquals(1, users.size());
        users.forEach(System.out::println);

        users = mapper.findByAgeIsNull();
        Assertions.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByFirstnameLike() {
        List<Student> users = mapper.findByFirstnameLike("三");
        Assertions.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByFirstnameNotLike() {
        List<Student> users = mapper.findByFirstnameNotLike("三");
        Assertions.assertEquals(4, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByFirstnameStartingWith() {
        List<Student> users = mapper.findByFirstnameStartingWith("Ji");
        Assertions.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByFirstnameEndingWith() {
        List<Student> users = mapper.findByFirstnameEndingWith("一");
        Assertions.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByFirstnameContaining() {
        List<Student> users = mapper.findByFirstnameContaining("三");
        Assertions.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByAgeOrderByLastnameDesc() {
        List<Student> users = mapper.findByAgeOrderByLastnameDesc(40);
        Assertions.assertEquals(2, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByLastnameNot() {
        List<Student> users = mapper.findByLastnameNot("张");
        Assertions.assertEquals(2, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByAgeIn() {
        List<Student> users = mapper.findByAgeIn(Arrays.asList(30, 40));
        Assertions.assertEquals(3, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByAgeNotIn() {
        List<Student> users = mapper.findByAgeNotIn(Arrays.asList(30, 40));
        Assertions.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByAgeNotInAndIdIn() {
        List<Student> users = mapper.findByAgeNotInAndIdIn(Arrays.asList(30, 40), Arrays.asList(1,2,3,4));
        Assertions.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByActiveTrue() {
        List<Student> users = mapper.findByActiveTrue();
        Assertions.assertEquals(4, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByActiveFalse() {
        List<Student> users = mapper.findByActiveFalse();
        Assertions.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByFirstnameIgnoreCase() {
        List<Student> users = mapper.findByFirstnameIgnoreCase("jim");
        Assertions.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByLastnameOrFirstnameOrderByAgeDescIdAsc() {
        List<Student> users = mapper.findByLastnameOrFirstnameAndIdBetweenOrderByAgeDescIdAsc("张", "Jim", 1, 4);
        Assertions.assertEquals(4, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void list() {
        List<Student> list = mapper.list(new Query());
        System.out.println(list);
    }

    @Test
    public void delete() {
        Student student = mapper.getById(4);
        mapper.delete(student);
    }




}
