package com.myapp;

import com.gitee.fastmybatis.core.PageInfo;
import com.gitee.fastmybatis.core.query.LambdaQuery;
import com.myapp.dao.TUserMapper;
import com.myapp.entity.TUser;
import com.myapp.service.UserService;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * @author thc
 */
public class UserMapperLamdaTest extends BaseTest {

    @Autowired
    private UserService userService;

    @Resource
    private TUserMapper tUserMapper;


    @Test
    public void lambda() {
        List<TUser> list = userService.query()
                .gt(TUser::getId, 4)
                .list();
        System.out.println(list);
    }

    @Test
    public void lambda1() {
        List<TUser> users = tUserMapper.query()
                .eq(TUser::getUsername, "张三")
                .list();
        Assertions.assertTrue(users.size() > 0);

        TUser tUser = userService.getById(6);
        System.out.println(tUser);
        Assertions.assertNotNull(tUser);

        LambdaQuery<TUser> query3 = userService.query()
                .eq(TUser::getUsername, 1);

        LambdaQuery<TUser> query = userService.query()
                .page(1, 10)
                .eq(TUser::getId, 6);

        PageInfo<TUser> page = userService.page(query);
        Assertions.assertTrue(page.getTotal() == 1);
        Assertions.assertTrue(page.getList().size() > 0);

        TUser user = userService.get(query);
        Assertions.assertNotNull(user);

        TUser user1 = userService.get(TUser::getUsername, "张三");
        Assertions.assertNotNull(user1);

        String columnValue = userService.getValue(userService.query().eq(TUser::getId, 6), TUser::getUsername);
        System.out.println("columnValue:" + columnValue);
        Assertions.assertNotNull(columnValue);

        List<Integer> list1 = Arrays.asList(1, 2, 3, 6);
        List<TUser> tUsers = userService.list(list1);
        System.out.println(tUsers);
        Assertions.assertTrue(tUsers.size() > 0);

        LambdaQuery<TUser> query1 = userService.query()
                .in(TUser::getId, Arrays.asList(1, 2, 6));
        List<String> username = userService.listValue(query1, TUser::getUsername);
        System.out.println(username);
        Assertions.assertTrue(username.size() > 0);

        LambdaQuery<TUser> lambdaQuery = userService.query()
                .set(TUser::getUsername, "aa")
                .set(TUser::getMoney, 33)
                .setExpression("add_time=now()")
                .eq(TUser::getId, 6);
        int i = userService.update(lambdaQuery);
        Assertions.assertEquals(1, i);

        LambdaQuery<TUser> updateQuery2 = userService.query()
                .set(TUser::getUsername, "王五")
                .set(TUser::getRemark, null) // 设置null
                .setExpression("add_time=now()")
                .eq(TUser::getId, 6);

        int j = userService.update(updateQuery2);
        Assertions.assertEquals(1, j);

        boolean exist = userService.checkExist(TUser::getId, 6);
        Assertions.assertTrue(exist);

        LambdaQuery<TUser> lambdaQuery1 = userService.query()
                // 只查询id, username两列
                .select(TUser::getId, TUser::getUsername)
                .eq(TUser::getId, 6);
        List<TUser> tUsers1 = userService.list(lambdaQuery1);
        System.out.println(tUsers1);
        Assertions.assertTrue(tUsers1.get(0).getId() != null);
        Assertions.assertTrue(tUsers1.get(0).getUsername() != null);
        Assertions.assertTrue(tUsers1.get(0).getState() == null);

        // QUERY

        // 根据条件查询
        List<TUser> list = userService.query()
                .eq(TUser::getUsername, "张三")
                .list();
        Assertions.assertTrue(list.size() > 0);

        PageInfo<TUser> pageInfo = userService.query()
                .eq(TUser::getUsername, "张三")
                .orderByDesc(TUser::getId)
                .paginate(1, 10);
        Assertions.assertTrue(pageInfo.getTotal() > 0);
        Assertions.assertTrue(pageInfo.getList().size() > 0);


        int cnt = userService.query()
                .eq(TUser::getUsername, "张三")
                .ignoreLogicDeleteColumn()
                .delete();
        Assertions.assertTrue(cnt > 0);

        int cnt2 = userService.query()
                .eq(TUser::getUsername, "张三")
                .deleteForce();
        Assertions.assertTrue(cnt2 > 0);

        int update = userService.query()
                .set(TUser::getUsername, "李四")
                .setExpression("add_time=now()")
                .eq(TUser::getId, 6)
                .update();
        Assertions.assertTrue(update > 0);


        int i1 = userService.query()
                .set(TUser::getRemark, "被xx删除")
                .eq(TUser::getId, 6)
                .delete();
        Assertions.assertEquals(1, i1);


        // 实体类创建query
        int i2 = TUser.query()
                .eq(TUser::getId, 6)
                .deleteForce();
        Assertions.assertTrue(i2 > 0);
    }

    @Test
    public void updateNull() {
        LambdaQuery<TUser> updateQuery2 = userService.query()
                .set(TUser::getUsername, "王五")
                .set(TUser::getRemark, null) // 设置null
                .setExpression("add_time=now()")
                .eq(TUser::getId, 6);

        int j = userService.update(updateQuery2);
        Assertions.assertEquals(1, j);
    }

    @Test
    public void lambada2() {
        LambdaQuery<TUser> query = userService.query()
                .eq(TUser::getId, 1);
        String username = userService.getValueOptional(query, TUser::getUsername).orElse("");
        Assertions.assertNotNull(username);
        System.out.println(username);

        String username2 = userService.query()
                .eq(TUser::getId, 1)
                .getValueOptional(TUser::getUsername)
                .orElse("");
        System.out.println(username2);
    }


}
