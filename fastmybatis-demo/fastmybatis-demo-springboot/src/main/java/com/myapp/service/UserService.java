package com.myapp.service;

import com.gitee.fastmybatis.core.support.BaseLambdaService;
import com.gitee.fastmybatis.core.support.LambdaService;
import com.myapp.dao.TUserMapper;
import com.myapp.entity.TUser;
import org.springframework.stereotype.Service;

/**
 * 用户service
 * 方式1：继承
 * 方式2：实现接口
 * @author thc
 */
@Service
//public class UserService extends BaseLambdaService<TUser/*实体类*/, TUserMapper> {
public class UserService implements LambdaService<TUser, TUserMapper> {


}
