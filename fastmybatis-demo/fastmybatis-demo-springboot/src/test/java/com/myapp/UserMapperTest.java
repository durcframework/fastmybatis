package com.myapp;

import com.gitee.fastmybatis.core.mapper.OneResult;
import com.gitee.fastmybatis.core.query.LambdaQuery;
import com.gitee.fastmybatis.core.query.Query;
import com.gitee.fastmybatis.core.update.Entitys;
import com.myapp.dao.TUserMapper;
import com.myapp.entity.TUser;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author thc
 */
public class UserMapperTest extends BaseTest {

    @Autowired
    private TUserMapper userMapper;

    @Test
    public void test() {
        TUser user = userMapper.getById(6);
        System.out.println(user);
    }

    @Test
    public void test2() {
        List<TUser> tUsers = userMapper.list(Arrays.asList(6));
        System.out.println(tUsers);
    }

    @Test
    public void save() {
        TUser user = new TUser();
        user.setLeftMoney(22.1F);
        user.setMoney(new BigDecimal("100.5"));
        user.setRemark("备注");
        user.setState((byte) 0);
        user.setUsername("张三2");
        userMapper.save(user);
        System.out.println(user);
    }

    @Test
    public void deleteById() {
        int effectRows = userMapper.deleteById(1);
        System.out.println(effectRows);
    }

    @Test
    public void delete() {
        TUser user = new TUser();
        user.setId(2);
        int effectRows = userMapper.delete(user);
        System.out.println(effectRows);
    }

    @Test
    public void deleteByQuery() {
        Query query = new Query();
        query.eq("id", 4);
        int effectRows = userMapper.deleteByQuery(query);
        System.out.println(effectRows);
    }

    @Test
    public void deleteByQuery2() {
        Query query = new Query();
        query.eq("id", 4);
        // 忽略逻辑删除字段，执行DELETE 语句
        query.ignoreLogicDeleteColumn();
        int effectRows = userMapper.deleteByQuery(query);
        System.out.println(effectRows);
    }

    @Test
    public void testForceUpdateById() {
        TUser user = Entitys.of(TUser.class, 6);
        user.setUsername("李四");
        user.setRemark(null);
        int i = userMapper.forceUpdateById(user);
        print("updateByQuery --> " + i);
        Assert.assertEquals(1, i);

        TUser user2 = Entitys.of(TUser.class, 6);
//        user.setUsername("李四");
        user2.setRemark(null);
        int i2 = userMapper.forceUpdateById(user2);
        print("updateByQuery2 --> " + i2);
        Assert.assertEquals(1, i2);

    }

    /**
     * UPDATE
     * `t_user`
     * SET
     * `username`='李四', `remark`=null
     * WHERE
     * username = '张三' AND isdel = 0;
     */
    @Test
    public void testForceUpdate() {
        // 根据条件批量更新
        TUser user = Entitys.of(TUser.class);
        user.setUsername("李四");
        user.setRemark(null);
        Query query = Query.create().eq("username", "张三");
        int i = userMapper.updateByQuery(user, query);
        Assert.assertEquals(4, i);
    }

    @Test
    public void ignoreLogicDelete() {
        // 根据条件批量更新
        Query query = Query.create()
                .select("id", "username")
                .eq("username", "张三")
                .ignoreLogicDeleteColumn()
                ;
        List<TUser> tUsers = userMapper.list(query);
        System.out.println(tUsers);

        List<Map<String, Object>> maps = userMapper.listMap(query);
        System.out.println(maps);
    }

    /**
     * 数据脱敏
     */
    @Test
    public void formatter() {
        TUser user = userMapper.getById(7);
        System.out.println(user);
        Assert.assertEquals("137****5678", user.getUsername());

        List<TUser> list = userMapper.list(new Query().gt("id", 6));
        for (TUser tUser : list) {
            System.out.println(tUser);
        }
    }


    @Test
    public void getOne() {
        TUser tUser = userMapper.query()
                .gt(TUser::getId, 1)
                .getOne()
                // 如果有多条记录，抛出指定异常
                .getIfThrow(new RuntimeException("查询失败"));
        System.out.println(tUser);
    }
    @Test
    public void getOne2() {
        TUser tUser = userMapper.query()
                .gt(TUser::getId, 1)
                // 追加limit 1
                .getOne(true)
                .get();
        System.out.println(tUser);
    }

    @Test
    public void getOne3() {
        TUser tUser = userMapper.query()
                .gt(TUser::getId, 1)
                .getOne()
                // 如果有多条数据，抛出TooManyResultsException异常
                .getOrThrow();
        System.out.println(tUser);
    }

    @Test
    public void getOne4() {
        OneResult<TUser> result = userMapper.query()
                .eq(TUser::getId, 3)
                .getOne();
        if (result.isError()) {
            throw new RuntimeException("");
        }
        Optional<TUser> optional = result.getOptional();
        if (optional.isPresent()) {
            System.out.println(optional.get());
        }
    }
}
