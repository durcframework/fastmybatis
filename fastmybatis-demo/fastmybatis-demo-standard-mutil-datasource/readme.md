# 标准用法，多数据源

## 打包

根目录执行：`mvn clean package -pl fastmybatis-demo/fastmybatis-demo-standard-mutil-datasource -am`

```java
    public static void main(String[] args) {
        // 加载数据源1
        Fastmybatis
                .create()
                .configLocation("mybatis/db1/mybatis-config.xml")
                .basePackage("com.myapp.db1")
                .load();
        // 加载数据源2
        Fastmybatis
                .create()
                .configLocation("mybatis/db2/mybatis-config.xml")
                .basePackage("com.myapp.db2")
                .load();

        TUser user = Mappers.run(TUserMapper.class, mapper -> {
            return mapper.getById(6);
        });
        System.out.println(user);

        Goods goods = Mappers.run(GoodsMapper.class, mapper -> {
            return mapper.getById(1);
        });
        System.out.println(goods);
    }
```