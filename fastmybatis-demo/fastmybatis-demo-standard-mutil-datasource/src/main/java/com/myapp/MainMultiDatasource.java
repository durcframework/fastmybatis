package com.myapp;

import com.gitee.fastmybatis.core.Fastmybatis;
import com.gitee.fastmybatis.core.ext.Mappers;
import com.myapp.db1.dao.TUserMapper;
import com.myapp.db1.entity.TUser;
import com.myapp.db2.dao.GoodsMapper;
import com.myapp.db2.entity.Goods;

/**
 * 多数据源示例
 * @author thc
 */
public class MainMultiDatasource {

    public static void main(String[] args) {
        // 加载数据源1
        Fastmybatis
                .create()
                .configLocation("mybatis/db1/mybatis-config.xml")
                .basePackage("com.myapp.db1")
                .load();
        // 加载数据源2
        Fastmybatis
                .create()
                .configLocation("mybatis/db2/mybatis-config.xml")
                .basePackage("com.myapp.db2")
                .load();

        TUser user = Mappers.run(TUserMapper.class, mapper -> {
            return mapper.getById(6);
        });
        System.out.println(user);

        Goods goods = Mappers.run(GoodsMapper.class, mapper -> {
            return mapper.getById(1);
        });
        System.out.println(goods);
    }

}
