package com.myapp.db2.dao;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.myapp.db2.entity.Goods;
import org.apache.ibatis.annotations.Param;


/**
 * @author tanghc
 */
public interface GoodsMapper extends CrudMapper<Goods, Integer> {
    Goods selectByName(@Param("goodsName") String goodsName);
}
