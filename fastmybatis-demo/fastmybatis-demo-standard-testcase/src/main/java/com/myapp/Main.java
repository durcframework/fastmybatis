package com.myapp;

import com.gitee.fastmybatis.core.Fastmybatis;
import com.gitee.fastmybatis.core.FastmybatisConfig;
import com.gitee.fastmybatis.core.ext.Mappers;
import com.myapp.config.ListFill;
import com.myapp.dao.TUserMapper;
import com.myapp.entity.TUser;

import java.util.Arrays;

/**
 * @author thc
 */
public class Main {
    public static void main(String[] args) {
        // 启动加载
        FastmybatisConfig config = new FastmybatisConfig();
        // 设置填充器
        config.setFills(Arrays.asList(new ListFill()));
        Fastmybatis.create()
                // 指定mybatis-config.xml文件classpath路径
                .configLocation("mybatis/mybatis-config.xml")
                // 指定mybatis sql文件classpath目录
                .mapperLocations("mybatis/mapper")
                // 指定配置文件
                .config(config)
                // 指定Mapper接口package
                .basePackage("com.myapp.dao")
                .load();

        // 使用mapper，自动commit，报错回滚事务
        TUser user = Mappers.run(TUserMapper.class, mapper -> {
            return mapper.getById(6);
        });
        System.out.println(user);
        // 更多测试用例参考：com.myapp.TUserMapperTest
    }

}
