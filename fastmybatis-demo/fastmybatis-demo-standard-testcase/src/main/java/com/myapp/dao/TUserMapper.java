package com.myapp.dao;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.myapp.entity.TUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

public interface TUserMapper extends CrudMapper<TUser, Integer> {

    // 自定义sql
    @Update("update t_user set username = #{username} where id = #{id}")
    int updateById(@Param("id") int id, @Param("username") String username);

}