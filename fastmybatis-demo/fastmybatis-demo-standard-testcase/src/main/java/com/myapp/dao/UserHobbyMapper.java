package com.myapp.dao;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.myapp.entity.UserHobby;

/**
 * @author thc
 */
public interface UserHobbyMapper extends CrudMapper<UserHobby, Integer> {
}
