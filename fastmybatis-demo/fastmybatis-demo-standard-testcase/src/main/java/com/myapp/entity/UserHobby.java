package com.myapp.entity;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.PkStrategy;
import com.gitee.fastmybatis.annotation.Table;

import java.util.List;
import java.util.Set;

/**
 * @author thc
 */
@Table(name = "user_hobby", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
public class UserHobby {
    private Integer id;

    // 数据库是varchar类型
    private List<String> hobby;

    private transient Set<Integer> items;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<String> getHobby() {
        return hobby;
    }

    public void setHobby(List<String> hobby) {
        this.hobby = hobby;
    }

    public Set<Integer> getItems() {
        return items;
    }

    public void setItems(Set<Integer> items) {
        this.items = items;
    }
}
