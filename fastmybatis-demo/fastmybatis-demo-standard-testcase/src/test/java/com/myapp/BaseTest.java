package com.myapp;

import com.gitee.fastmybatis.core.Fastmybatis;

/**
 * @author thc
 */
public class BaseTest {

    protected Fastmybatis fastmybatis;

    public BaseTest() {
        fastmybatis = Fastmybatis.create()
                        // 指定mybatis-config.xml文件classpath路径
                        .configLocation("mybatis/mybatis-config.xml")
                        // 指定mybatis sql文件classpath目录
                        .mapperLocations("mybatis/mapper")
                        // 指定Mapper接口package
                        .basePackage("com.myapp.dao")
                        .load();
    }

    public void print(Object o) {
        System.out.println("=================");
        System.out.println(o);
        System.out.println("=================");
    }

}
