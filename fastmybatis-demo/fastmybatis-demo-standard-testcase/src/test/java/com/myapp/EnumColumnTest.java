package com.myapp;

import com.gitee.fastmybatis.core.PageInfo;
import com.gitee.fastmybatis.core.ext.Mappers;
import com.gitee.fastmybatis.core.query.Query;
import com.myapp.dao.UserInfoWithEnumMapper;
import com.myapp.entity.UserInfoType;
import com.myapp.entity.UserInfoWithEnum;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * 枚举字段测试
 * @author thc
 */
public class EnumColumnTest extends BaseTest {

    @Test
    public void save() {
        UserInfoWithEnum userInfoWithEnum = new UserInfoWithEnum();
        userInfoWithEnum.setUserId(1);
        userInfoWithEnum.setStatus(UserInfoType.INVALID);
        userInfoWithEnum.setCity("北京");
        userInfoWithEnum.setAddress("王府井大街");
        Mappers.run(UserInfoWithEnumMapper.class, mapper -> mapper.saveIgnoreNull(userInfoWithEnum));
    }

    @Test
    public void page() {
        Query query = new Query()
                .page(1, 50);
        PageInfo<UserInfoWithEnum> page = Mappers.run(UserInfoWithEnumMapper.class, mapper-> mapper.page(query));
        List<UserInfoWithEnum> list = page.getList();
        for (UserInfoWithEnum userInfoWithEnum : list) {
            System.out.println(userInfoWithEnum);
        }
    }

    @Test
    public void pageBySpecifiedColumns() {
        Query query = new Query();
        PageInfo<UserVO> page = Mappers.run(UserInfoWithEnumMapper.class, mapper-> mapper.pageBySpecifiedColumns(Arrays.asList("user_id", "status"), query, UserVO.class));
        List<UserVO> list = page.getList();
        for (UserVO userVO : list) {
            System.out.println(userVO);
        }
    }

    public static class UserVO {
        /** t_user外键, 数据库字段：user_id */
        private Integer userId;

        /** 类型, 数据库字段：status */
        private UserInfoType status;

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public UserInfoType getStatus() {
            return status;
        }

        public void setStatus(UserInfoType status) {
            this.status = status;
        }

        @Override
        public String toString() {
            return "UserVO{" +
                    "userId=" + userId +
                    ", status=" + status +
                    '}';
        }
    }
}
