package com.myapp;

import com.gitee.fastmybatis.core.Fastmybatis;
import com.gitee.fastmybatis.core.FastmybatisConfig;
import com.gitee.fastmybatis.core.ext.Mappers;
import com.myapp.config.ListFill;
import com.myapp.dao.UserHobbyMapper;
import com.myapp.entity.UserHobby;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * 演示字段转换
 * @author thc
 */
public class FillTest {

    private final Fastmybatis fastmybatis;

    public FillTest() {
        FastmybatisConfig config = new FastmybatisConfig();
        // 设置填充器
        config.setFills(Arrays.asList(new ListFill()));
        fastmybatis = Fastmybatis.create()
                // 指定mybatis-config.xml文件classpath路径
                .configLocation("mybatis/mybatis-config.xml")
                // 指定mybatis sql文件classpath目录
                .mapperLocations("mybatis/mapper")
                // 指定配置文件
                .config(config)
                // 指定Mapper接口package
                .basePackage("com.myapp.dao")
                .load();
    }

    @Test
    public void save() {
        UserHobby userHobby = new UserHobby();
        // List类型
        userHobby.setHobby(Arrays.asList("乒乓球", "篮球"));
        Mappers.run(UserHobbyMapper.class, mapper -> mapper.save(userHobby));
        System.out.println("主键id：" + userHobby.getId());
    }

    @Test
    public void get() {
        UserHobby userHobby = Mappers.run(UserHobbyMapper.class, mapper -> mapper.getById(4));
        List<String> hobby = userHobby.getHobby();
        System.out.println(hobby);
    }

}
