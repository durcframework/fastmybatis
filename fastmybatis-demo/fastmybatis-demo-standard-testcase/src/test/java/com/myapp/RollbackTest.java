package com.myapp;

import com.gitee.fastmybatis.core.ext.Mappers;
import com.myapp.dao.TUserMapper;
import com.myapp.entity.TUser;
import org.junit.Test;

/**
 * 事务回滚
 * @author thc
 */
public class RollbackTest extends BaseTest {

    @Test
    public void rollback() {
        Integer i = Mappers.run(TUserMapper.class, mapper -> {
            int k = mapper.updateById(4, "tom");
            // 模拟报错回滚事务
            int j = 1 / 0;
            return k;
        });
        System.out.println(i);
    }

    @Test
    public void rollback2() {
        Integer i = Mappers.run(TUserMapper.class, mapper -> {
            TUser user = mapper.getById(4);
            user.setUsername("tom");
            int k = mapper.update(user);
            // 模拟报错回滚事务
            int j = 1 / 0;
            return k;
        });
        System.out.println(i);
    }
}
