package com.myapp;

import com.gitee.fastmybatis.core.PageInfo;
import com.gitee.fastmybatis.core.ext.MapperRunner;
import com.gitee.fastmybatis.core.ext.Mappers;
import com.gitee.fastmybatis.core.query.Query;
import com.gitee.fastmybatis.core.util.IOUtil;
import com.gitee.fastmybatis.core.util.MyBeanUtil;
import com.myapp.dao.AddressMapper;
import com.myapp.dao.TUserMapper;
import com.myapp.entity.Address;
import com.myapp.entity.TUser;
import com.myapp.entity.UserInfoRecord;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author thc
 */
public class TUserMapperTest extends BaseTest {

    /**
     * 根据主键查询
     *
     * <pre>
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money`
     * FROM `t_user` t
     * WHERE `id` = ? LIMIT 1
     * </pre>
     */
    @Test
    public void testGetById() {
        TUser user = Mappers.run(TUserMapper.class, mapper -> mapper.getById(6));
        print(user);
    }

    /**
     * 根据条件查询一条记录
     *
     * <pre>
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money`
     * FROM `t_user` t
     * WHERE id = ? AND money > ? LIMIT 1
     * </pre>
     */
    @Test
    public void testGetByQuery() {
        // 查询ID=3,金额大于1的用户
        Query query = new Query()
                .eq("id", 6)
                .gt("money", 1);

        TUser user = Mappers.run(TUserMapper.class, mapper -> mapper.getByQuery(query));
        print(user);
    }

    /**
     * 手动处理session
     */
    @Test
    public void sessionFactory() {
        try (SqlSession sqlSession = fastmybatis.getSqlSessionFactory().openSession(true)) {
            TUserMapper mapper = sqlSession.getMapper(TUserMapper.class);
            TUser tUser = mapper.getById(6);
            System.out.println(tUser);
        }
    }

    /**
     * 自动提交事务且出错回滚
     */
    @Test
    public void sessionFactoryAutoCommitAndRollback() {
        SqlSession sqlSession = fastmybatis.getSqlSessionFactory().openSession();
        TUserMapper mapper = sqlSession.getMapper(TUserMapper.class);
        TUser tUser = new MapperRunner<>(mapper, sqlSession)
                .run(m -> m.getById(6));
        System.out.println(tUser);
    }

    @Test
    public void runAnnotation() {
        Integer i = Mappers.run(TUserMapper.class, mapper -> {
            int k = mapper.updateById(4, "jim121111");
            // 模拟报错回滚事务
            //int j = 1 / 0;
            return k;
        });
        System.out.println(i);
    }


    @Test
    public void saveByRecord() {
        UserInfoRecord userInfoRecord = new UserInfoRecord();
        userInfoRecord.setUserId(11);
        userInfoRecord.setCity("杭州");
        userInfoRecord.setAddress("西湖");
        boolean success = userInfoRecord.save();
        assert success;
    }

    @Test
    public void saveOrUpdateByRecord() {
        UserInfoRecord userInfoRecord = new UserInfoRecord();
        userInfoRecord.setId(18);
        userInfoRecord.setUserId(11);
        userInfoRecord.setCity("杭州1133");
        userInfoRecord.setAddress("西湖1133");
        boolean success = userInfoRecord.saveOrUpdate();
        assert success;
    }

    @Test
    public void deleteByRecord() {
        UserInfoRecord userInfoRecord = new UserInfoRecord();
        userInfoRecord.setId(6);
        boolean success = userInfoRecord.delete();
        System.out.println(success);
    }

    /**
     * 查询某个字段值
     */
    @Test
    public void getColumnValue() {
        Query query = new Query().eq("id", 6);
        String username = Mappers.run(TUserMapper.class, mapper -> mapper.getColumnValue("username", query, String.class));
        System.out.println(username);

        String idString = Mappers.run(TUserMapper.class, mapper -> mapper.getColumnValue("id", query, String.class));
        System.out.println("idString:" + idString);

        int idInt = Mappers.run(TUserMapper.class, mapper -> mapper.getColumnValue("id", query, int.class));
        System.out.println("idInt:" + idInt);

        int idInteger = Mappers.run(TUserMapper.class, mapper -> mapper.getColumnValue("id", query, Integer.class));
        System.out.println("idInteger:" + idInteger);

        BigInteger idBigInteger = Mappers.run(TUserMapper.class, mapper -> mapper.getColumnValue("id", query, BigInteger.class));
        System.out.println("idBigInteger:" + idBigInteger);

        BigDecimal moneyBigDecimal = Mappers.run(TUserMapper.class, mapper -> mapper.getColumnValue("money", query, BigDecimal.class));
        System.out.println("moneyBigDecimal:" + moneyBigDecimal);

        Double moneyDouble = Mappers.run(TUserMapper.class, mapper -> mapper.getColumnValue("money", query, Double.class));
        System.out.println("moneyDouble:" + moneyDouble);
    }

    /**
     * 根据字段查询一条记录
     *
     * <pre>
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money`
     * FROM `t_user` t
     * WHERE t.`username` = ? LIMIT 1
     * </pre>
     */
    @Test
    public void testGetByColumn() {
        TUser user = Mappers.run(TUserMapper.class, mapper -> mapper.getByColumn("username", "张三"));
        print(user);
    }

    /**
     * 返回自定义字段，并转换成自定义类集合
     *
     * <pre>
     * SELECT t.id , t.username as username FROM `t_user` t WHERE username = ?
     * </pre>
     */
    @Test
    public void testGivenColumns2() {
        Query query = new Query();
        // 添加查询条件
        query.eq("username", "张三");

        // 自定义字段
        List<String> columns = Arrays.asList("t.id", "t.username", "t.add_time");
        // 查询，自定义集合
        List<UserVO> list = Mappers.run(TUserMapper.class, mapper -> mapper.listBySpecifiedColumns(columns, query, UserVO.class));

        for (UserVO obj : list) {
            System.out.println(obj);
        }
    }

    /**
     * 返回单值集合
     *
     * <pre>
     * SELECT t.id FROM `t_user` t WHERE username = ?
     * </pre>
     */
    @Test
    public void testGivenColumns3() {
        Query query = new Query();
        // 添加查询条件
        query.eq("username", "张三");

        MapperRunner<TUserMapper> mapperRunner = Mappers.getMapperRunner(TUserMapper.class);
        TUserMapper mapper = mapperRunner.getMapper();

        // 返回id列
        List<Integer> idList = mapper.listBySpecifiedColumns(Collections.singletonList("id"), query, Integer.class/* 或int.class */);
        for (Integer id : idList) {
            System.out.println(id);
        }

        // 返回id列，并转换成String
        List<String> strIdList = mapper.listBySpecifiedColumns(Collections.singletonList("id"), query, String.class);

        for (String id : strIdList) {
            System.out.println("string:" + id);
        }

        // 返回username列
        List<String> usernameList = mapper.listBySpecifiedColumns(Collections.singletonList("username"), query, String.class);
        for (String username : usernameList) {
            System.out.println(username);
        }

        // 返回时间列
        List<Date> dateList = mapper.listBySpecifiedColumns(Collections.singletonList("add_time"), query, Date.class);
        for (Date date : dateList) {
            System.out.println(date);
        }

        // 返回decimal列
        List<BigDecimal> moneyList = mapper.listBySpecifiedColumns(Collections.singletonList("money"), query, BigDecimal.class);
        for (BigDecimal money : moneyList) {
            System.out.println("BigDecimal:" + money);
        }

        // 返回decimal列并转成float
        List<Float> moneyList2 = mapper.listBySpecifiedColumns(Collections.singletonList("money"), query, float.class);
        for (Float money : moneyList2) {
            System.out.println("float:" + money);
        }

        // 返回decimal
        List<BigDecimal> moneyList3 = mapper.listBySpecifiedColumns(Collections.singletonList("money"), query, BigDecimal.class);
        for (BigDecimal money : moneyList3) {
            System.out.println("BigDecimal:" + money);
        }

        // 返回decimal
        List<BigInteger> moneyList4 = mapper.listBySpecifiedColumns(Collections.singletonList("id"), query, BigInteger.class);
        for (BigInteger money : moneyList4) {
            System.out.println("BigInteger:" + money);
        }

        // 返回tinyint列
        List<Byte> stateList = mapper.listBySpecifiedColumns(Collections.singletonList("state"), query, byte.class);
        for (Byte state : stateList) {
            System.out.println("state:" + state);
        }

        mapperRunner.getSessionOptional().ifPresent(IOUtil::closeQuietly);
    }


    /**
     * 查询指定列，返指定列集合
     */
    @Test
    public void listColumnValues() {
        // id > 10
        Query query = new Query().gt("id", 10);

        List<String> list = Mappers.run(TUserMapper.class, mapper -> mapper.listColumnValues("username", query, String.class));
        System.out.println(list);
    }

    @Test
    public void pageInfoFun2() {
        Query query = new Query();
        // 添加查询条件
        query.eq("username", "张三")
                .page(1, 2) // 分页查询，按页码分，通常使用这种。
        ;

        PageInfo<UserVO> pageInfo = Mappers.run(TUserMapper.class, mapper -> {
            PageInfo<UserVO> page = mapper.page(query, tUser -> {
                // 对每行数据进行转换
                UserVO userVO = new UserVO();
                MyBeanUtil.copyProperties(tUser, userVO);
                Byte state = tUser.getState();
                switch (state) {
                    case 0:
                        userVO.setStateName("未启用");
                        break;
                    case 1:
                        userVO.setStateName("已启用");
                        break;
                    case 2:
                        userVO.setStateName("已禁用");
                        break;
                    default: {
                        userVO.setStateName("未知状态");
                    }
                }
                return userVO;
            });
            return page;
        });

        List<UserVO> list = pageInfo.getList(); // 结果集
        long total = pageInfo.getTotal(); // 总记录数
        int pageCount = pageInfo.getPageCount(); // 共几页

        System.out.println("total:" + total);
        System.out.println("pageCount:" + pageCount);
        list.forEach(System.out::println);
    }

    @Test
    public void testInsertBatch() {
        List<TUser> users = new ArrayList<>();

        for (int i = 0; i < 3; i++) { // 创建3个对象
            TUser user = new TUser();
            user.setUsername("username" + i);
            user.setMoney(new BigDecimal(i));
            user.setRemark("remark" + i);
            user.setState((byte) 0);
            user.setIsdel(0);
            user.setAddTime(new Date());
            user.setLeftMoney(200F);
            users.add(user);
        }

        int i = Mappers.run(TUserMapper.class, mapper -> mapper.saveBatch(users)); // 返回成功数

        System.out.println("saveBatch --> " + i);
    }

    @Test
    public void testInsertMultiSet() {
        List<TUser> users = new ArrayList<>();

        for (int i = 0; i < 3; i++) { // 创建3个重复对象
            TUser user = new TUser();
            user.setUsername("username" + 1);
            user.setMoney(new BigDecimal(1));
            user.setRemark("remark" + 1);
            user.setState((byte) 0);
            user.setIsdel(0);
            user.setAddTime(new Date());
            user.setLeftMoney(200F);
            users.add(user);
        }

        int i = Mappers.run(TUserMapper.class, mapper -> mapper.saveMultiSet(users)); // 返回成功数

        System.out.println("saveMulti --> " + i);
    }

    @Test
    public void noCrud() {
        List<Address> list = Mappers.run(AddressMapper.class, AddressMapper::listAddress);
        System.out.println(list);
    }


    public static class UserVO {
        private Integer id;

        /**
         * 用户名, 数据库字段：username
         */
        private String username;

        private String stateName;

        private LocalDateTime addTime;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public LocalDateTime getAddTime() {
            return addTime;
        }

        public void setAddTime(LocalDateTime addTime) {
            this.addTime = addTime;
        }

        public String getStateName() {
            return stateName;
        }

        public void setStateName(String stateName) {
            this.stateName = stateName;
        }

        @Override
        public String toString() {
            return "UserVO{" +
                    "id=" + id +
                    ", username='" + username + '\'' +
                    ", stateName='" + stateName + '\'' +
                    ", addTime=" + addTime +
                    '}';
        }
    }
}
