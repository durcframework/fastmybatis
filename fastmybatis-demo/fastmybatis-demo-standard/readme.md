# 标准用法，不依赖spring

标准用法，没有依赖Spring，只依赖了mybatis.jar

## 打包

根目录执行：`mvn clean package -pl fastmybatis-demo/fastmybatis-demo-standard -am`

## 例子

```java
    /**
     * 根据主键查询
     *
     * <pre>
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money`
     * FROM `t_user` t
     * WHERE `id` = ? LIMIT 1
     * </pre>
     */
    @Test
    public void testGetById() {
        TUser user = Mappers.run(TUserMapper.class, mapper -> mapper.getById(6));
        print(user);
    }

    /**
     * 根据条件查询一条记录
     *
     * <pre>
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money`
     * FROM `t_user` t
     * WHERE id = ? AND money > ? LIMIT 1
     * </pre>
     */
    @Test
    public void testGetByQuery() {
        // 查询ID=3,金额大于1的用户
        Query query = new Query()
                .eq("id", 6)
                .gt("money", 1);

        TUser user = Mappers.run(TUserMapper.class, mapper -> mapper.getByQuery(query));
        print(user);
    }

    /**
     * 手动处理session
     */
    @Test
    public void sessionFactory() {
        try (SqlSession sqlSession = fastmybatis.getSqlSessionFactory().openSession(true)) {
            TUserMapper mapper = sqlSession.getMapper(TUserMapper.class);
            TUser tUser = mapper.getById(6);
            System.out.println(tUser);
        }
    }
```

