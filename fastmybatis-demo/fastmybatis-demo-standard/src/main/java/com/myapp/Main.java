package com.myapp;

import com.gitee.fastmybatis.core.Fastmybatis;
import com.gitee.fastmybatis.core.ext.Mappers;
import com.gitee.fastmybatis.core.support.plugin.SqlFormatterPlugin;
import com.myapp.common.DataSourceFactory;
import com.myapp.dao.TUserMapper;
import com.myapp.entity.TUser;

/**
 * @author thc
 */
public class Main {
    public static void main(String[] args) {
        // 启动加载
        Fastmybatis.create(Main.class)
                // 指定数据源
                .dataSource(DataSourceFactory.getDataSource())
                // 指定mybatis sql文件classpath目录
                .mapperLocations("mybatis/mapper")
                // 加载插件
                .plugins(SqlFormatterPlugin.create())
                .load();

        // 使用mapper，自动commit，报错回滚事务
        TUser user = Mappers.run(TUserMapper.class, mapper -> {
            return mapper.getById(6);
        });
        System.out.println(user);
        // 更多测试用例参考：com.myapp.TUserMapperTest
    }

}
