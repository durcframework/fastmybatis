package com.myapp.common;

import com.gitee.fastmybatis.core.support.datasource.H2MemDataSourceBuilder;

import javax.sql.DataSource;

/**
 * @author thc
 */
public class DataSourceFactory {

    public static DataSource getDataSource() {
        // Hikari数据源配置
        /*
           pom依赖
           <dependency>
                <groupId>com.zaxxer</groupId>
                <artifactId>HikariCP</artifactId>
                <version>4.0.3</version>
            </dependency>
         */
        /*Properties properties = new Properties();
        properties.put("driverClassName", "com.mysql.cj.jdbc.Driver");
        properties.put("jdbcUrl", "jdbc:mysql://localhost:3306/stu?useUnicode=true&characterEncoding=utf-8&zeroDateTimeBehavior=convertToNull&serverTimezone=Asia/Shanghai");
        properties.put("username", "root");
        properties.put("password", "root");
        properties.put("minimumIdle", 5);
        properties.put("maximumPoolSize", 10);
        properties.put("connectionTestQuery", "SELECT 1");
        HikariConfig hikariConfig = new HikariConfig(properties);
        return new HikariDataSource(hikariConfig);*/

        // H2内存数据库
        DataSource dataSource = new H2MemDataSourceBuilder()
                .addScript("schema.sql")
                .build();


        return dataSource;
    }


}
