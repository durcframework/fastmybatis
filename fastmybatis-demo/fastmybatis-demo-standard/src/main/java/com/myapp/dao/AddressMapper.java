package com.myapp.dao;

import com.myapp.entity.Address;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 不继承其它接口，只使用注解写sql。
 *
 * @author thc
 */
// 最好加上这个注解，有利于框架扫描判断
// 如果不加这个注解，系统会根据文件名判断，如果以Mapper/Dao/DAO/Repository结尾的接口，则被当做mybatis mapper
@Mapper
public interface AddressMapper {

    @Select("select id, address from address")
    List<Address> listAddress();

}
