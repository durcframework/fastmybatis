package com.myapp.dao;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.myapp.entity.Student;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * 演示JPA findBy查询，根据方法名称自动生成查询语句，无需编写SQL
 * @author tanghc
 */
public interface StudentMapper extends CrudMapper<Student, Integer> {

    /**
     * … where x.lastname = ?1 and x.firstname = ?2
     * @param lastname
     * @param firstname
     * @return
     */
    List<Student> findByLastnameAndFirstname(String lastname, String firstname);

    /**
     *
     * … where x.lastname = ?1 or x.firstname = ?2
     * @param lastname
     * @param firstname
     * @return
     */
    List<Student> findByLastnameOrFirstname(String lastname, String firstname);

    /**
     * where x.firstname = ?
     * @param firstname
     * @return
     */
    List<Student> findByFirstname(String firstname);
    List<Student> findByFirstnameIs(String firstname);
    List<Student> findByFirstnameEquals(String firstname);

    /**
     * … where x.startDate between ?1 and ?2
     * @param begin
     * @param end
     * @return
     */
    List<Student> findByStartDateBetween(Date begin, Date end);

    /**
     * … where x.age < ?1
     * @param age
     * @return
     */
    List<Student> findByAgeLessThan(int age);

    /**
     * … where x.age <= ?1
     * @param age
     * @return
     */
    List<Student> findByAgeLessThanEqual(int age);

    /**
     * … where x.age > ?1
     * @param age
     * @return
     */
    List<Student> findByAgeGreaterThan(int age);

    /**
     * … where x.age >= ?1
      * @param age
     * @return
     */
    List<Student> findByAgeGreaterThanEqual(int age);

    /**
     * … where x.startDate > ?1
     * @param date
     * @return
     */
    List<Student> findByStartDateAfter(Date date);

    /**
     * … where x.startDate < ?1
     * @param date
     * @return
     */
    List<Student> findByStartDateBefore(Date date);


    /**
     * … where x.age is null
     * @return
     */
    List<Student> findByAgeNull();
    List<Student> findByAgeIsNull();


    /**
     * … where x.firstname like ?1
     * @param firstname
     * @return
     * @see #findByFirstnameContaining(String)
     */
    List<Student> findByFirstnameLike(String firstname);

    /**
     * … where x.firstname not like ?1
     * @param firstname
     * @return
     */
    List<Student> findByFirstnameNotLike(String firstname);

    /**
     * … where x.firstname like 'xx%'
     * @param firstname
     * @return
     */
    List<Student> findByFirstnameStartingWith(String firstname);

    /**
     * … where x.firstname like '%xx'
     * @param firstname
     * @return
     */
    List<Student> findByFirstnameEndingWith(String firstname);

    /**
     * 等同于like
     * … where x.firstname like '%xx%'
     * @param firstname
     * @return
     */
    List<Student> findByFirstnameContaining(String firstname);

    /**
     * … where x.age = ?1 order by x.lastname desc
     * @param age
     * @return
     */
    List<Student> findByAgeOrderByLastnameDesc(int age);


    /**
     * … where x.lastname <> ?1
     * @param lastname
     * @return
     */
    List<Student> findByLastnameNot(String lastname);

    /**
     * … where x.age in ?1
     *
     * @param ages
     * @return
     */
    List<Student> findByAgeIn(Collection<Integer> ages);

    /**
     * … where x.age not in ?1
     *
     * @param ages
     * @return
     */
    List<Student> findByAgeNotIn(Collection<Integer> ages);


    List<Student> findByAgeNotInAndIdIn(Collection<Integer> ages, List<Integer> ids);

    /**
     * … where x.active = 1
     * @return
     */
    List<Student> findByActiveTrue();

    /**
     * … where x.active = 0
     * @return
     */
    List<Student> findByActiveFalse();

    /**
     * … where UPPER(x.firstname) = UPPER(?1)
     * @param firstname
     * @return
     */
    List<Student> findByFirstnameIgnoreCase(String firstname);


    // 复杂的例子
    List<Student> findByLastnameOrFirstnameAndIdBetweenOrderByAgeDescIdAsc(String lastname, String firstname, int id1, int id2);

}
