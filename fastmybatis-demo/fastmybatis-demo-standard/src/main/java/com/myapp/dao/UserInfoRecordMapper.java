package com.myapp.dao;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.myapp.entity.UserInfoRecord;


public interface UserInfoRecordMapper extends CrudMapper<UserInfoRecord, Integer> {
}
