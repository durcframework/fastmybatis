package com.myapp.entity;

import com.gitee.fastmybatis.annotation.Table;
import com.gitee.fastmybatis.core.support.Record;

import java.util.Date;


/**
 * Active Record
 * 表名：user_info
 * 备注：用户信息表
 */
@Table(name = "user_info")
public class UserInfoRecord implements Record {

    private Integer id;

    /** t_user外键, 数据库字段：user_id */
    private Integer userId;

    /** 城市, 数据库字段：city */
    private String city;

    /** 街道, 数据库字段：address */
    private String address;

    /** 类型, 数据库字段：status */
    private String status;

    /** 添加时间, 数据库字段：create_time */
    private Date createTime;

    /** 修改时间, 数据库字段：update_time */
    private Date updateTime;

    /** 设置t_user外键, 数据库字段：user_info.user_id */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /** 获取t_user外键, 数据库字段：user_info.user_id */
    public Integer getUserId() {
        return this.userId;
    }

    /** 设置城市, 数据库字段：user_info.city */
    public void setCity(String city) {
        this.city = city;
    }

    /** 获取城市, 数据库字段：user_info.city */
    public String getCity() {
        return this.city;
    }

    /** 设置街道, 数据库字段：user_info.address */
    public void setAddress(String address) {
        this.address = address;
    }

    /** 获取街道, 数据库字段：user_info.address */
    public String getAddress() {
        return this.address;
    }

    /** 设置类型, 数据库字段：user_info.status */
    public void setStatus(String status) {
        this.status = status;
    }

    /** 获取类型, 数据库字段：user_info.status */
    public String getStatus() {
        return this.status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "UserInfoRecord{" +
                "id=" + id +
                ", userId=" + userId +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", status='" + status + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
