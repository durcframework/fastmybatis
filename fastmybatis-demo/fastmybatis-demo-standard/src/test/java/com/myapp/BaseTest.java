package com.myapp;

import com.gitee.fastmybatis.core.Fastmybatis;
import com.gitee.fastmybatis.core.support.plugin.SqlFormatterPlugin;
import com.myapp.common.DataSourceFactory;

import javax.sql.DataSource;

/**
 * @author thc
 */
public class BaseTest {

    protected static Fastmybatis fastmybatis;

    public BaseTest() {
        if (fastmybatis == null) {
            fastmybatis = Fastmybatis.create(Main.class)
                    // 指定数据源
                    .dataSource(DataSourceFactory.getDataSource())
                    // 指定mybatis sql文件classpath目录
                    .mapperLocations("mybatis/mapper")
                    // 加载插件
                    .plugins(SqlFormatterPlugin.create())
                    .load();
        }

    }

    public void print(Object o) {
        System.out.println("=================");
        System.out.println(o);
        System.out.println("=================");
    }

}
