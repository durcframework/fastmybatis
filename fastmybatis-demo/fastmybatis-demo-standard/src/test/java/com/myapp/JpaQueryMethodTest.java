package com.myapp;

import com.gitee.fastmybatis.core.Fastmybatis;
import com.gitee.fastmybatis.core.ext.Mappers;
import com.gitee.fastmybatis.core.support.plugin.SqlFormatterPlugin;
import com.myapp.common.DataSourceFactory;
import com.myapp.dao.StudentMapper;
import com.myapp.entity.Student;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * JPA findBy查询
 * @author tanghc
 */
public class JpaQueryMethodTest {

    protected static Fastmybatis fastmybatis;

    static StudentMapper mapper;

    public JpaQueryMethodTest() {
        if (fastmybatis == null) {
            fastmybatis = Fastmybatis.create(Main.class)
                    // 指定数据源
                    .dataSource(DataSourceFactory.getDataSource())
                    // 指定mybatis sql文件classpath目录
                    .mapperLocations("mybatis/mapper")
                    // 加载插件
                    .plugins(SqlFormatterPlugin.create())
                    .load();

            mapper = Mappers.getMapperRunner(StudentMapper.class).getMapper();
        }

    }

    @Test
    public void findByLastnameAndFirstname() {
        List<Student> users = mapper.findByLastnameAndFirstname("张", "三");
        Assert.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByLastnameOrFirstname() {
        List<Student> users = mapper.findByLastnameOrFirstname("王", "三");
        Assert.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByFirstname() {
        List<Student> users = mapper.findByFirstname("三");
        Assert.assertEquals(1, users.size());
        users.forEach(System.out::println);

        users = mapper.findByFirstnameIs("三");
        Assert.assertEquals(1, users.size());
        users.forEach(System.out::println);

        users = mapper.findByFirstnameEquals("三");
        Assert.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }


    @Test
    public void findByStartDateBetween() throws ParseException {
        Date start = DateUtils.parseDate("2018-02-21", "yyyy-MM-dd");
        Date end = DateUtils.parseDate("2019-12-21", "yyyy-MM-dd");
        List<Student> users = mapper.findByStartDateBetween(start, end);
        Assert.assertEquals(2, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByAgeLessThan() {
        List<Student> users = mapper.findByAgeLessThan(30);
        Assert.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByAgeLessThanEqual() {
        List<Student> users = mapper.findByAgeLessThanEqual(30);
        Assert.assertEquals(2, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByAgeGreaterThan() {
        List<Student> users = mapper.findByAgeGreaterThan(30);
        Assert.assertEquals(2, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByAgeGreaterThanEqual() {
        List<Student> users = mapper.findByAgeGreaterThanEqual(30);
        Assert.assertEquals(3, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByStartDateAfter() throws ParseException {
        Date date = DateUtils.parseDate("2018-02-21", "yyyy-MM-dd");
        List<Student> users = mapper.findByStartDateAfter(date);
        Assert.assertEquals(2, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByStartDateBefore() throws ParseException {
        Date date = DateUtils.parseDate("2018-02-21", "yyyy-MM-dd");
        List<Student> users = mapper.findByStartDateBefore(date);
        Assert.assertEquals(2, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByAgeNull() {
        List<Student> users = mapper.findByAgeNull();
        Assert.assertEquals(1, users.size());
        users.forEach(System.out::println);

        users = mapper.findByAgeIsNull();
        Assert.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByFirstnameLike() {
        List<Student> users = mapper.findByFirstnameLike("三");
        Assert.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByFirstnameNotLike() {
        List<Student> users = mapper.findByFirstnameNotLike("三");
        Assert.assertEquals(4, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByFirstnameStartingWith() {
        List<Student> users = mapper.findByFirstnameStartingWith("Ji");
        Assert.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByFirstnameEndingWith() {
        List<Student> users = mapper.findByFirstnameEndingWith("一");
        Assert.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByFirstnameContaining() {
        List<Student> users = mapper.findByFirstnameContaining("三");
        Assert.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByAgeOrderByLastnameDesc() {
        List<Student> users = mapper.findByAgeOrderByLastnameDesc(40);
        Assert.assertEquals(2, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByLastnameNot() {
        List<Student> users = mapper.findByLastnameNot("张");
        Assert.assertEquals(2, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByAgeIn() {
        List<Student> users = mapper.findByAgeIn(Arrays.asList(30, 40));
        Assert.assertEquals(3, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByAgeNotIn() {
        List<Student> users = mapper.findByAgeNotIn(Arrays.asList(30, 40));
        Assert.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByAgeNotInAndIdIn() {
        List<Student> users = mapper.findByAgeNotInAndIdIn(Arrays.asList(30, 40), Arrays.asList(1,2,3,4));
        Assert.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByActiveTrue() {
        List<Student> users = mapper.findByActiveTrue();
        Assert.assertEquals(4, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByActiveFalse() {
        List<Student> users = mapper.findByActiveFalse();
        Assert.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByFirstnameIgnoreCase() {
        List<Student> users = mapper.findByFirstnameIgnoreCase("jim");
        Assert.assertEquals(1, users.size());
        users.forEach(System.out::println);
    }

    @Test
    public void findByLastnameOrFirstnameOrderByAgeDescIdAsc() {
        List<Student> users = mapper.findByLastnameOrFirstnameAndIdBetweenOrderByAgeDescIdAsc("张", "Jim", 1, 4);
        Assert.assertEquals(4, users.size());
        users.forEach(System.out::println);
    }




}
