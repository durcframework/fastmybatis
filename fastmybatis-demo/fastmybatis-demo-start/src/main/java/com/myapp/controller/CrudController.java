package com.myapp.controller;

import com.gitee.fastmybatis.core.PageInfo;
import com.gitee.fastmybatis.core.query.Query;
import com.myapp.controller.param.UserParam;
import com.myapp.entity.Student;
import com.myapp.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 增删改查例子
 *
 * @author thc
 */
@RestController
public class CrudController {

    @Autowired
    private StudentService studentService;


    /**
     * 分页查询
     * http://localhost:8081/user/page?id=1
     * http://localhost:8081/user/page?pageIndex=1&pageSize=5
     *
     * @param param
     * @return
     */
    @GetMapping("/user/page")
    public Result<PageInfo<Student>> page(UserParam param) {
        Query query = param.toQuery();
        PageInfo<Student> pageInfo = studentService.page(query);
        return Result.ok(pageInfo);
    }

    /**
     * 新增记录，这里为了方便演示用了GET方法，实际上应该使用POST
     * http://localhost:8081/user/save?firstname=Jim
     *
     * @param user
     * @return
     */
    @GetMapping("/user/save")
    public Result<Integer> save(Student user) {
        studentService.saveIgnoreNull(user);
        // 返回添加后的主键值
        return Result.ok(user.getId());
    }

    /**
     * 修改记录，这里为了方便演示用了GET方法，实际上应该使用POST
     * http://localhost:8081/user/update?id=1&firstname=tom
     *
     * @param user 表单数据
     * @return
     */
    @GetMapping("/user/update")
    public Result<?> update(Student user) {
        studentService.updateIgnoreNull(user);
        return Result.ok();
    }

    /**
     * 删除记录，这里为了方便演示用了GET方法，实际上应该使用DELETE
     * http://localhost:8081/user/delete?id=1
     *
     * @param id 主键id
     * @return
     */
    @GetMapping("/user/delete")
    public Result<?> delete(Integer id) {
        studentService.deleteById(id);
        return Result.ok();
    }

    /**
     * 分页+多字段排序查询
     * http://localhost:8081/user/pagesort?pageIndex=1&pageSize=5&id=10&sortInfo=id%3Ddesc%2CstartDate%3Dasc
     *
     * @param param
     * @return
     */
    @GetMapping("/user/pagesort")
    public Result<PageInfo<Student>> pagesort(UserParam param) {
        Query query = param.toQuery();
        PageInfo<Student> pageInfo = studentService.page(query);
        return Result.ok(pageInfo);
    }
}
