package com.myapp.controller.param;

import com.gitee.fastmybatis.core.query.annotation.Condition;
import com.gitee.fastmybatis.core.query.param.PageMultiSortParam;

/**
 * @author thc
 */
public class UserParam extends PageMultiSortParam {

    @Condition
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
