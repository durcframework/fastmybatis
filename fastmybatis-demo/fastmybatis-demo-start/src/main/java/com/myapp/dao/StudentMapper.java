package com.myapp.dao;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.myapp.entity.Student;

/**
 * @author tanghc
 */
public interface StudentMapper extends CrudMapper<Student, Integer> {

}
