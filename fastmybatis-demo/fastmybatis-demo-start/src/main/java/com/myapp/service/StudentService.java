package com.myapp.service;

import com.gitee.fastmybatis.core.support.CommonService;
import com.myapp.dao.StudentMapper;
import com.myapp.entity.Student;
import org.springframework.stereotype.Service;

/**
 * 用户service
 * @author thc
 */
@Service
public class StudentService implements CommonService<Student/*实体类*/, Integer/*主键类型*/, StudentMapper> {


}
