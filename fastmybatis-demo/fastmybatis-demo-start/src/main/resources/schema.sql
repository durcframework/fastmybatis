CREATE TABLE IF NOT EXISTS `student`
(
  `id`        INTEGER PRIMARY KEY auto_increment,
  `firstname` varchar(64) DEFAULT NULL COMMENT '名',
  `lastname` varchar(64) DEFAULT NULL COMMENT '姓',
  `age` int default null COMMENT '年龄',
  `active` tinyint(4) DEFAULT NULL COMMENT '状态',
  `start_date` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '开始日期'
);

INSERT  INTO `student`
VALUES (1,'Jim', 'Green', 22, 1, '2018-02-21 10:37:00'),
(2,'三', '张', 30, 1, '2019-11-21 11:37:44'),
(3,'四', '张', 40, 1, '2017-12-21 12:55:00'),
(4,'五一', '李', 40, 1, null),
(5,'六', '张', null, 0, '2016-07-21 10:44:00');
