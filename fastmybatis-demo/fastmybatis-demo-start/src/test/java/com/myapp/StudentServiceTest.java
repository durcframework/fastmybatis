package com.myapp;

import com.gitee.fastmybatis.core.query.LambdaQuery;
import com.gitee.fastmybatis.core.query.LambdaQuery;
import com.gitee.fastmybatis.core.query.Query;
import com.myapp.dao.StudentMapper;
import com.myapp.entity.Student;
import com.myapp.service.StudentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author thc
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class StudentServiceTest {

    @Resource
    private StudentService studentService;

    @Resource
    private StudentMapper studentMapper;

    @Test
    public void list() {
        LambdaQuery<Student> query = Query.query(Student.class)
                .eq(Student::getLastname, "张");
        List<Student> list = studentService.list(query);
        for (Student student : list) {
            System.out.println(student);
        }
    }

    @Test
    public void list2() {
        LambdaQuery<Student> query = Query.query(Student.class)
                .eq(Student::getLastname, "张");
        List<Student> list = studentMapper.list(query);
        for (Student student : list) {
            System.out.println(student);
        }

    }

    @Test
    public void list3() {
        Query query = new Query()
                .eq("lastname", "张");
        LambdaQuery<Student> lambdaQuery = query.toLambdaQuery(Student.class)
                .orderByDesc(Student::getId);
        List<Student> list = studentMapper.list(lambdaQuery);
        for (Student student : list) {
            System.out.println(student);
        }

    }

}
