package com.myapp.config;

/**
 * @author thc
 */
public enum GenderEnum {
    MALE("男"),
    FEMALE("女"),
    OTHER("其它"),
    ;

    GenderEnum(String description) {
        this.description = description;
    }

    private final String description;

    public String getDescription() {
        return description;
    }
}
