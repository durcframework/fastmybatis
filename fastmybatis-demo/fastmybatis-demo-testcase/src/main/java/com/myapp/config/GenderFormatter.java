package com.myapp.config;

import com.gitee.fastmybatis.core.handler.BaseFormatter;

import java.util.Objects;

/**
 * @author thc
 */
public class GenderFormatter extends BaseFormatter<GenderEnum> {

    @Override
    public Object writeFormat(GenderEnum value) {
        return value.name();
    }

    @Override
    public GenderEnum readFormat(Object value) {
        for (GenderEnum genderEnum : GenderEnum.values()) {
            if (Objects.equals(genderEnum.name(), value)) {
                return genderEnum;
            }
        }
        return GenderEnum.OTHER;
    }
}
