package com.myapp.config;

import com.alibaba.fastjson.JSON;
import com.gitee.fastmybatis.core.handler.BaseFill;
import com.myapp.entity.Hobby;

import java.lang.reflect.Field;

/**
 * application.properties新增配置：
 * <code>
 *     mybatis.fill.com.myapp.config.HobbyFill=
 * </code>
 * <p>
 * 格式：mybatis.fill.包名.类名=
 * </p>
 * 自定义字段保存类型
 */
public class HobbyFill extends BaseFill<Hobby> {

    @Override
    public boolean match(Class<?> entityClass, Field field, String columnName) {
        return field.getType() == Hobby.class;
    }

    // 数据库值转换到实体类
    @Override
    protected Hobby convertValue(Object columnValue) {
        return JSON.parseObject(String.valueOf(columnValue), Hobby.class);
    }

    // 保存到数据库
    @Override
    protected Object getFillValue(Hobby defaultValue) {
        return JSON.toJSONString(defaultValue);
    }
}