package com.myapp.config;

import com.alibaba.fastjson.JSON;
import com.gitee.fastmybatis.core.handler.BaseFormatter;
import com.myapp.entity.Hobby;

/**
 * @author thc
 */
public class HobbyFormatter extends BaseFormatter<Hobby> {

    @Override
    public Object writeFormat(Hobby value) {
        return JSON.toJSONString(value);
    }

    @Override
    public Hobby readFormat(Object value) {
        return JSON.parseObject(String.valueOf(value), Hobby.class);
    }
}
