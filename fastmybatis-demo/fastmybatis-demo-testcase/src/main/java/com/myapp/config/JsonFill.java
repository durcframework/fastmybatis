package com.myapp.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gitee.fastmybatis.core.handler.BaseFill;

import java.lang.reflect.Field;
import java.util.Optional;

/**
 * 演示自定义uuid，查看Address.java
 */
public class JsonFill extends BaseFill<JSONObject> {

    // 保存
    @Override
    protected Object getFillValue(JSONObject defaultValue) {
        return Optional.ofNullable(defaultValue).map(JSONObject::toString).orElse("{}");
    }

    // 读取
    @Override
    protected JSONObject convertValue(Object columnValue) {
        return JSON.parseObject(String.valueOf(columnValue));
    }

    // 命中字段
    @Override
    public boolean match(Class<?> entityClass, Field field, String columnName) {
        return field.getType() == JSONObject.class;
    }
}