package com.myapp.config;

import com.gitee.fastmybatis.core.handler.CustomIdFill;
import com.myapp.entity.Address;

import java.util.Optional;
import java.util.UUID;

/**
 * 演示自定义uuid，查看Address.java
 */
public class UUIDFill extends CustomIdFill<String> {

    private static final Class<?>[] TARGET_CLASS = {
        Address.class
    };

    @Override
    public String getColumnName() {
        return "id"; // 作用在id字段上
    }

    @Override
    protected Object getFillValue(String defaultValue) {
        return Optional.ofNullable(defaultValue).orElse(UUID.randomUUID().toString()); // 自定义的uuid生成方式
    }

    @Override
    public Class<?>[] getTargetEntityClasses() {
        return TARGET_CLASS;
    }
}