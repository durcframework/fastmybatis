package com.myapp.controller;

import com.gitee.fastmybatis.core.PageInfo;
import com.gitee.fastmybatis.core.query.Query;
import com.gitee.fastmybatis.core.query.param.PageParam;
import com.myapp.entity.Address;
import com.myapp.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xxx
 */
@RestController
@RequestMapping("address")
public class AddressController {

    @Autowired
    private AddressService addressService;

    /**
     * 分页查询
     *
     * @param param
     * @return
     */
    @GetMapping("/page")
    public Result<PageInfo<Address>> page(PageParam param) {
        Query query = param.toQuery();
        PageInfo<Address> pageInfo = addressService.page(query);
        return Result.ok(pageInfo);
    }
    
    /**
     * 新增记录
     *
     * @param user
     * @return
     */
    @PostMapping("/save")
    public Result<String> save(Address user) {
        addressService.saveIgnoreNull(user);
        // 返回添加后的主键值
        return Result.ok(user.getId());
    }
    
    /**
     * 修改记录
     *
     * @param user 表单数据
     * @return
     */
    @PutMapping("/update")
    public Result<?> update(Address user) {
        addressService.updateIgnoreNull(user);
        return Result.ok();
    }
     
    /**
     * 删除记录
     *
     * @param id 主键id
     * @return
     */
    @DeleteMapping("/delete")
    public Result<?> delete(String id) {
        addressService.deleteById(id);
        return Result.ok();
    }
    
}