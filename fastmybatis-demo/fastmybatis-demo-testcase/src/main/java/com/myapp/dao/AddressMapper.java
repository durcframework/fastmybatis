package com.myapp.dao;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.myapp.entity.Address;

/**
 * @author xxx
 */
public interface AddressMapper extends CrudMapper<Address, String> {
	
}