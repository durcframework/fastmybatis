package com.myapp.dao;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.myapp.entity.LogUuid;

public interface LogUuidMapper extends CrudMapper<LogUuid, String> {


}