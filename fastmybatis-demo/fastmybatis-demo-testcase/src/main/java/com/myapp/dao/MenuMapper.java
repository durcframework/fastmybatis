package com.myapp.dao;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.myapp.entity.Menu;

/**
 * @author thc
 */
public interface MenuMapper extends CrudMapper<Menu, Integer> {
}
