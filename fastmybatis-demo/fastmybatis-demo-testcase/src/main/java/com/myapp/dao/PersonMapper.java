package com.myapp.dao;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.myapp.entity.Person;

/**
 * @author thc
 */
public interface PersonMapper extends CrudMapper<Person, Integer> {
}
