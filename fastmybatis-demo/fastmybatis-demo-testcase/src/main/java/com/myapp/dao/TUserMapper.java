package com.myapp.dao;

import java.util.List;
import java.util.Map;

import com.myapp.entity.UserInfoDO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.gitee.fastmybatis.core.query.Query;
import com.myapp.entity.TUser;

public interface TUserMapper extends CrudMapper<TUser, Integer> {

    // 自定义sql

    TUser selectByName(@Param("username") String username);

    List<TUser> findByMap(@Param("map") Map<String, Object> map);

    TUser getByMap(@Param("map") Map<String, Object> map);

    List<UserInfoDO> findJoin(@Param("query") Query query);

    List<UserInfoDO> listUserInfo(@Param("query") Query query);

    Long getUserInfoCount(@Param("query") Query query);

    UserInfoDO selectById(int id);

}
