package com.myapp.dao;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.myapp.entity.TestJson;

/**
 * @author thc
 */
public interface TestJsonMapper extends CrudMapper<TestJson, Integer> {
}
