package com.myapp.dao;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.myapp.entity.UserInfo;


public interface UserInfoMapper extends CrudMapper<UserInfo, Integer> {
}
