package com.myapp.dao;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.myapp.entity.UserInfoWithEnum;


public interface UserInfoWithEnumMapper extends CrudMapper<UserInfoWithEnum, Integer> {
}
