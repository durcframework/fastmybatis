package com.myapp.entity;

import com.gitee.fastmybatis.core.support.TreeNode;

import java.util.List;

/**
 * @author thc
 */
public class Menu implements TreeNode<Menu, Integer> {

    private Integer id;
    private String name;
    private Integer parentId;
    private transient List<Menu> children;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public List<Menu> getChildren() {
        return children;
    }

    @Override
    public Integer takeId() {
        return getId();
    }

    @Override
    public Integer takeParentId() {
        return getParentId();
    }

    @Override
    public void setChildren(List<Menu> children) {
        this.children = children;
    }
}
