package com.myapp.entity;

import com.gitee.fastmybatis.annotation.Column;
import com.gitee.fastmybatis.annotation.Table;
import com.myapp.config.GenderEnum;
import com.myapp.config.GenderFormatter;

/**
 * @author thc
 */
@Table(name = "person")
public class Person {

    private Integer id;

    @Column(formatClass = GenderFormatter.class)
    private GenderEnum gender;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public GenderEnum getGender() {
        return gender;
    }

    public void setGender(GenderEnum gender) {
        this.gender = gender;
    }
}
