package com.myapp.entity;

/**
 * t_user表字段映射关系<br>
 * 使用方式：
 * <pre>
 * {@literal
 * Query query = new Query().eq(TUserColumns.id, 6);
 * TUser tUser = mapper.getBySpecifiedColumns(Arrays.asList(TUserColumns.id, TUserColumns.username), query);
 * }
 * </pre>
 * @author xxx
 */
public interface TUserColumns {
	/** ID */
	String id = "id";
    
	/** 用户名 */
	String username = "username";
    
	/** 状态 */
	String state = "state";
    
	/** 是否删除 */
	String isdel = "isdel";
    
	/** 备注 */
	String remark = "remark";
    
	/** 添加时间 */
	String add_time = "add_time";
    
	/** 金额 */
	String money = "money";
    
	/** 剩下的钱 */
	String left_money = "left_money";
    
}