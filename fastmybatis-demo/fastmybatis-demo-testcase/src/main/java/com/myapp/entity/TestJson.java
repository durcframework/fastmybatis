package com.myapp.entity;

import com.alibaba.fastjson.JSONObject;
import com.gitee.fastmybatis.annotation.Table;

/**
 * @author thc
 */
@Table(name = "test_json")
public class TestJson {

    private Integer id;

    private JSONObject content;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public JSONObject getContent() {
        return content;
    }

    public void setContent(JSONObject content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "TestJson{" +
                "id=" + id +
                ", content=" + content +
                '}';
    }
}
