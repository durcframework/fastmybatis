package com.myapp.entity;

import com.gitee.fastmybatis.annotation.Column;
import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.PkStrategy;
import com.gitee.fastmybatis.annotation.Table;
import com.myapp.config.HobbyFormatter;

import java.util.Set;

/**
 * @author thc
 */
@Table(name = "user_hobby", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
public class UserHobby {
    private Integer id;

    @Column(formatClass = HobbyFormatter.class)
    // 数据库是varchar类型
    private Hobby hobby;

    private transient Set<Integer> items;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Hobby getHobby() {
        return hobby;
    }

    public void setHobby(Hobby hobby) {
        this.hobby = hobby;
    }

    public Set<Integer> getItems() {
        return items;
    }

    public void setItems(Set<Integer> items) {
        this.items = items;
    }
}
