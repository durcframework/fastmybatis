package com.myapp.entity;

import com.gitee.fastmybatis.annotation.Table;


/**
 * 表名：user_info
 * 备注：用户信息表; InnoDB free: 11264 kB
 */
@Table(name = "user_info")
public class UserInfoWithEnum extends BaseEntity<Integer> {

    /** t_user外键, 数据库字段：user_id */
    private Integer userId;

    /** 城市, 数据库字段：city */
    private String city;

    /** 街道, 数据库字段：address */
    private String address;

    /** 类型, 数据库字段：status */
    private UserInfoType status;

    /** 设置t_user外键, 数据库字段：user_info.user_id */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /** 获取t_user外键, 数据库字段：user_info.user_id */
    public Integer getUserId() {
        return this.userId;
    }

    /** 设置城市, 数据库字段：user_info.city */
    public void setCity(String city) {
        this.city = city;
    }

    /** 获取城市, 数据库字段：user_info.city */
    public String getCity() {
        return this.city;
    }

    /** 设置街道, 数据库字段：user_info.address */
    public void setAddress(String address) {
        this.address = address;
    }

    /** 获取街道, 数据库字段：user_info.address */
    public String getAddress() {
        return this.address;
    }

    public UserInfoType getStatus() {
        return status;
    }

    public void setStatus(UserInfoType status) {
        this.status = status;
    }


    @Override
    public String toString() {
        return "UserInfo{" +
                "userId=" + userId +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
