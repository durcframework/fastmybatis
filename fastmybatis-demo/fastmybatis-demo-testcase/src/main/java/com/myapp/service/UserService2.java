package com.myapp.service;

import com.gitee.fastmybatis.core.support.CommonService;
import com.myapp.dao.TUserMapper;
import com.myapp.entity.TUser;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 用户service
 * @author thc
 */
@Service
public class UserService2 implements CommonService<TUser/*实体类*/, Integer/*主键类型*/, TUserMapper> {

    @Async
    public void doXX() {
        try {
            Thread.sleep(1400);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("UserService2 done");
    }

}
