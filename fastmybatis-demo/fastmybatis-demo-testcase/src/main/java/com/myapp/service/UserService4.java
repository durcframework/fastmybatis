package com.myapp.service;

import com.gitee.fastmybatis.core.support.IService;
import com.myapp.entity.TUser;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 用户service
 * @author thc
 */
@Service
public class UserService4 implements MyServiceI, IService<TUser/*实体类*/, Integer/*主键类型*/> {

    @Async
    public void doXX() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("UserService done");
    }

}
