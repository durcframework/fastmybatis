package com.myapp;

import com.alibaba.fastjson.JSON;
import com.myapp.dao.UserInfoMapper;
import com.myapp.entity.TUser;
import com.myapp.entity.UserInfo;
import org.junit.Test;

import javax.annotation.Resource;

/**
 * 演示级联查询
 * @author tanghc
 */
public class AssociationTest extends FastmybatisSpringbootApplicationTests {

	@Resource
	UserInfoMapper mapper;


	@Test
	public void testGet() {
		UserInfo userInfo = mapper.getById(3);
		System.out.println(JSON.toJSONString(userInfo));
		// 这里触发懒加载，将会执行
		// SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money` FROM `t_user` t WHERE `id` = ? AND t.isdel = 0 LIMIT 1 
		// 可将下面两句注释查看sql执行情况
		TUser user = userInfo.getUser();

		System.out.println("延迟数据：" + user);
	}

	@Test
	public void testUpdate() {
		UserInfo userInfo = mapper.getById(2);
		userInfo.setAddress("北京" + System.currentTimeMillis());
		int i = mapper.update(userInfo);
		System.out.println("update i:" + i);
	}
}

