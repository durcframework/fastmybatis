package com.myapp;

import com.gitee.fastmybatis.core.Fastmybatis;
import com.gitee.fastmybatis.core.ext.Mappers;
import com.myapp.dao.TUserMapper;
import com.myapp.entity.TUser;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;

import javax.sql.DataSource;

/**
 * @author thc
 */
public class DataSourceTest {

    Fastmybatis fastmybatis;

    public DataSourceTest() {
        DataSource dataSource = DataSourceFactory.getDataSource();
        fastmybatis = Fastmybatis.create()
                .dataSource(dataSource)
                .addMapper(TUserMapper.class)
                .load();
    }

    /**
     * 根据主键查询
     *
     * <pre>
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money`
     * FROM `t_user` t
     * WHERE `id` = ? LIMIT 1
     * </pre>
     */
    @Test
    public void testGetById() {
        TUser user = Mappers.run(TUserMapper.class, mapper -> mapper.getById(1));
        System.out.println(user);
    }

    @Test
    public void testGetById2() {
        SqlSessionFactory sqlSessionFactory = fastmybatis.getSqlSessionFactory();
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            TUserMapper mapper = sqlSession.getMapper(TUserMapper.class);
            TUser user = mapper.getById(1);
            System.out.println(user);
        }
    }

}
