package com.myapp;

import com.alibaba.fastjson.JSON;
import com.gitee.fastmybatis.core.PageInfo;
import com.gitee.fastmybatis.core.query.Query;
import com.myapp.dao.UserInfoWithEnumMapper;
import com.myapp.entity.UserInfoType;
import com.myapp.entity.UserInfoWithEnum;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * 演示枚举字段
 * @author tanghc
 */
public class EnumClassTest extends FastmybatisSpringbootApplicationTests {

	@Resource
	UserInfoWithEnumMapper mapper;

	@Test
	public void save() {
		UserInfoWithEnum userInfoWithEnum = new UserInfoWithEnum();
		userInfoWithEnum.setUserId(1);
		userInfoWithEnum.setStatus(UserInfoType.INVALID);
		userInfoWithEnum.setCity("北京");
		userInfoWithEnum.setAddress("王府井大街");
		mapper.saveIgnoreNull(userInfoWithEnum);
	}

	@Test
	public void page() {
		Query query = new Query();
		PageInfo<UserInfoWithEnum> page = mapper.page(query);
		System.out.println(JSON.toJSONString(page));
	}

	@Test
	public void pageBySpecifiedColumns() {
		Query query = new Query();
		PageInfo<UserVO> page = mapper.pageBySpecifiedColumns(Arrays.asList("user_id", "status"), query, UserVO.class);
		System.out.println(JSON.toJSONString(page));
	}

	public static class UserVO {
		/** t_user外键, 数据库字段：user_id */
		private Integer userId;

		/** 类型, 数据库字段：status */
		private UserInfoType status;

		public Integer getUserId() {
			return userId;
		}

		public void setUserId(Integer userId) {
			this.userId = userId;
		}

		public UserInfoType getStatus() {
			return status;
		}

		public void setStatus(UserInfoType status) {
			this.status = status;
		}
	}

}

