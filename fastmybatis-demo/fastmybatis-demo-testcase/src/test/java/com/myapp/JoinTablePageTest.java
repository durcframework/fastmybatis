package com.myapp;

import com.gitee.fastmybatis.core.PageInfo;
import com.gitee.fastmybatis.core.query.Query;
import com.gitee.fastmybatis.core.util.MapperUtil;
import com.myapp.dao.TUserMapper;
import com.myapp.entity.UserInfoDO;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author thc
 */
public class JoinTablePageTest extends FastmybatisSpringbootApplicationTests {

    @Autowired
    TUserMapper tUserMapper;

    /**
     * 演示联表查询且分页
     */
    @Test
    public void joinPage() {
        Query query = new Query()
                // 联表查询需要带上表别名t.
                .gt("t.id", 1)
                .eq("t2.city", "杭州")
                .page(1, 10);

        // 分页查询只需要返回总数，以及当前分页内容
        PageInfo<UserInfoDO> pageInfo = MapperUtil.query(query, tUserMapper::getUserInfoCount, tUserMapper::listUserInfo);

        List<UserInfoDO> list = pageInfo.getList(); // 结果集
        long total = pageInfo.getTotal(); // 总记录数
        int pageCount = pageInfo.getPageCount(); // 共几页

        System.out.println("total:" + total);
        System.out.println("pageCount:" + pageCount);
        list.forEach(System.out::println);
    }

}
