package com.myapp;

import com.alibaba.fastjson.JSONObject;
import com.myapp.dao.TestJsonMapper;
import com.myapp.entity.TestJson;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * @author thc
 */
public class JsonTest extends FastmybatisSpringbootApplicationTests{

    @Resource
    TestJsonMapper testJsonMapper;

    @Test
    public void add() {
        JSONObject json = new JSONObject();
        json.put("name", "Jim");
        json.put("hobby", Arrays.asList("sing", "football"));

        TestJson testJson = new TestJson();
        testJson.setContent(json);

        testJsonMapper.save(testJson);
    }

    @Test
    public void get() {
        TestJson testJson = testJsonMapper.getById(1);
        System.out.println(testJson);
    }

    @Test
    public void update() {
        TestJson testJson = testJsonMapper.getById(1);
        JSONObject content = testJson.getContent();
        content.put("age", 22);
        testJsonMapper.update(testJson);
    }

}
