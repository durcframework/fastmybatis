package com.myapp;

import com.myapp.dao.LogMapper;
import com.myapp.entity.Log;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class NoPkTest extends FastmybatisSpringbootApplicationTests {

    @Autowired
    private LogMapper logMapper;

    @Test
    public void testGet() {
        Log log = logMapper.getByColumn("log_id", "1");
        System.out.println(log);
    }
}
