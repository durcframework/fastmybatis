package com.myapp;

import com.myapp.config.GenderEnum;
import com.myapp.dao.PersonMapper;
import com.myapp.entity.Person;
import org.junit.Test;

import javax.annotation.Resource;

/**
 * @author thc
 */
public class PersonGenderTest extends FastmybatisSpringbootApplicationTests{

    @Resource
    PersonMapper personMapper;

    @Test
    public void testGender() {
        Person person = new Person();
        person.setGender(GenderEnum.FEMALE);
        personMapper.save(person);

        Person person1 = personMapper.getById(person.getId());
        System.out.println(person1.getGender().getDescription());
    }

}
