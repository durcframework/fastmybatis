package com.myapp;

import com.myapp.dao.AddressMapper;
import com.myapp.dao.LogCustomMapper;
import com.myapp.entity.Address;
import com.myapp.entity.LogCustom;
import com.myapp.entity.LogUuid;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class PkCustomTest extends FastmybatisSpringbootApplicationTests {

    @Autowired
    private LogCustomMapper logCustomMapper;

    @Autowired
    private AddressMapper addressMapper;

    @Test
    public void testSave() {
        LogCustom logUuid = new LogCustom();
        logUuid.setLogId(String.valueOf(System.currentTimeMillis()));
        logUuid.setContent("aaa");
        logCustomMapper.saveIgnoreNull(logUuid);
    }

    @Test
    public void testSave2() {
        // 不需要设置主键，查看：com.myapp.config.UUIDFill
        Address address = new Address();
        address.setAddress("aaaa");
        addressMapper.saveIgnoreNull(address);
        System.out.println(address.getId());
    }
}
