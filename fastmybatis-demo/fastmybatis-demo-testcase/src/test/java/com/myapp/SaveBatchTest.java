package com.myapp;

import com.myapp.dao.TUserMapper;
import com.myapp.entity.TUser;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author thc
 */
public class SaveBatchTest extends FastmybatisSpringbootApplicationTests{

    @Resource
    TUserMapper mapper;

    @Test
    public void saveBatchIgnoreNull() {
        List<TUser> users = new ArrayList<>();

        for (int i = 0; i < 3; i++) { // 创建3个对象
            TUser user = new TUser();
            user.setUsername("username" + i);
            user.setMoney(new BigDecimal(i));
            user.setState((byte)0);
            user.setIsdel(0);
            user.setAddTime(new Date());
            users.add(user);
        }

        int i = mapper.saveBatchIgnoreNull(users);// 返回成功数
        Assert.assertTrue(i > 0);

        List<Integer> idList = users.stream()
                .map(TUser::getId)
                .collect(Collectors.toList());
        Assert.assertTrue(idList.size() > 0);
        System.out.println("主键id：" + idList);
    }

    @Test
    public void saveBatchBySpecifiedColumns() {
        List<TUser> users = new ArrayList<>();

        for (int i = 0; i < 3; i++) { // 创建3个对象
            TUser user = new TUser();
            user.setUsername("username" + i);
            user.setMoney(new BigDecimal(i));
            user.setState((byte)0);
            user.setIsdel(0);
            user.setAddTime(new Date());
            users.add(user);
        }

        int i = mapper.saveBatchBySpecifiedColumns(users, Arrays.asList("username", "addTime"));
        Assert.assertTrue(i > 0);

        List<Integer> idList = users.stream()
                .map(TUser::getId)
                .collect(Collectors.toList());
        Assert.assertTrue(idList.size() > 0);
        System.out.println("主键id：" + idList);
    }

    @Test
    public void saveBatchIgnoreNullPartition() {
        List<TUser> users = new ArrayList<>();

        for (int i = 0; i < 5; i++) { // 创建3个对象
            TUser user = new TUser();
            user.setUsername("username" + i);
            user.setMoney(new BigDecimal(i));
            user.setState((byte)0);
            user.setIsdel(0);
            user.setAddTime(new Date());
            users.add(user);
        }

        int i = mapper.saveBatchIgnoreNull(users, 2); // 返回成功数
        System.out.println(i);
        Assert.assertTrue(i > 0);

        List<Integer> idList = users.stream()
                .map(TUser::getId)
                .collect(Collectors.toList());
        Assert.assertTrue(idList.size() > 0);
        System.out.println("主键id：" + idList);
    }
}
