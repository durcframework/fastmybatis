package com.myapp;

import com.gitee.fastmybatis.core.query.LambdaQuery;
import com.gitee.fastmybatis.core.query.Operator;
import com.gitee.fastmybatis.core.query.Query;
import com.gitee.fastmybatis.core.query.annotation.Condition;
import com.gitee.fastmybatis.core.query.expression.BetweenValue;
import com.gitee.fastmybatis.core.query.param.PageParam;
import com.myapp.dao.TUserMapper;
import com.myapp.entity.TUser;
import com.myapp.entity.UserInfoDO;
import org.junit.Test;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * mapper测试
 */
public class TUserMapperLambdaTest extends FastmybatisSpringbootApplicationTests {

    @Resource
    TUserMapper mapper;


    /**
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money` FROM `t_user` t
     * WHERE
     * id = ?
     * AND id >= ?
     * AND id IN ( ? , ? )
     * AND id BETWEEN ? AND ?
     * AND (id=1)
     * AND t.isdel = 0
     */
    @Test
    public void query() {
        Query query = Query.query(TUser.class)
                .eq(TUser::getId, 1)
                .ge(TUser::getId, 1)
                .in(TUser::getId, Arrays.asList(1,2))
                .between(TUser::getId, 1, 2)
                .sql("id=1");
        List<TUser> list = mapper.list(query);
        System.out.println(list);
    }


    /**
     * 子表达式
     *
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money`
     * FROM `t_user` t
     * WHERE id = ? AND ( username = ? OR state = ? ) OR username = ? AND t.isdel = 0
     */
    @Test
    public void and() {
        Query query = Query.query(TUser.class)
                .eq(TUser::getId, 6)
                // 子表达式
                .andLambda(q -> q.eq(TUser::getUsername, "jim")
                        .orEq(TUser::getState, 1)
                )
                .orLambda(q -> q.eq(TUser::getId, 1).orBetween(TUser::getId, 1, 90))
                .orEq(TUser::getUsername, "tom");
        List<TUser> list = mapper.list(query);
        System.out.println(list);
    }

    /**
     * or查询
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money`
     * FROM `t_user` t
     * WHERE id = ? OR ( username = ? OR state = ? ) OR username = ? AND t.isdel = 0
     */
    @Test
    public void orCondition() {
        Query query = Query.query(TUser.class)
                .eq(TUser::getId, 6)
                // 子表达式
                .orLambda(q -> q.eq(TUser::getUsername, "jim")
                        .orEq(TUser::getState, 1)
                )
                .orEq(TUser::getUsername, "tom");
        List<TUser> list = mapper.list(query);
        System.out.println(list);
    }

    /**
     * OR 查询
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money`
     * FROM `t_user` t
     * WHERE id = ? OR username = ? AND t.isdel = 0
     */
    @Test
    public void or() {
        LambdaQuery<TUser> query = new LambdaQuery<>(TUser.class);

                // WHERE id = ? OR username = ?
                query.eq(TUser::getId, 6)
                .orEq(TUser::getUsername, "jim");
        List<TUser> list = mapper.list(query);
        System.out.println(list);
    }

    /**
     * 全是or
     * SELECT
     * 	t.`id`,
     * 	t.`username`,
     * 	t.`state`,
     * 	t.`isdel`,
     * 	t.`remark`,
     * 	t.`add_time`,
     * 	t.`money`,
     * 	t.`left_money`
     * FROM
     * 	`t_user` t
     * WHERE
     * 	id = ?
     * 	OR id <> ?
     * 	OR id IN ( ?, ? )
     * 	OR id NOT IN ( ?, ? )
     * 	OR id > ?
     * 	OR id >= ?
     * 	OR id < ?
     * 	OR id <= ?
     * 	OR username LIKE ?
     * 	OR username LIKE ?
     * 	OR username LIKE ?
     * 	OR id BETWEEN ? 	AND ?
     * 	OR id BETWEEN ? 	AND ?
     * 	OR id BETWEEN ? 	AND ?
     * 	OR id BETWEEN ? 	AND ?
     * 	OR ( id IS NULL OR id = '' )
     * 	OR ( id IS NULL )
     * 	OR ( 1 = 1 )
     * 	AND t.isdel = 0
     *
     * 	SELECT
     *     t. `id`,
     *     t. `username`,
     *     t. `state`,
     *     t. `isdel`,
     *     t. `remark`,
     *     t. `add_time`,
     *     t. `money`,
     *     t. `left_money`
     * FROM
     *     `t_user` t
     * WHERE
     *     id = 6
     *     OR id <> 1
     *     OR id IN(1, 2)
     *     OR id NOT IN(2, 3)
     *     OR id > 1
     *     OR id >= 1
     *     OR id < 2
     *     OR id <= 2
     *     OR username LIKE '%jim%'
     *     OR username LIKE '%a'
     *     OR username LIKE 'b%'
     *     OR id BETWEEN 1 AND 3
     *     OR id BETWEEN 1 AND 3
     *     OR id BETWEEN 2 AND 6
     *     OR id BETWEEN 1 AND 6
     *     OR(id IS NULL
     *         OR id = '')
     *     OR(id IS NULL)
     *     OR(username IS NOT NULL
     *         AND username <> '')
     *     OR(1 = 1)
     *     AND t.isdel = 0;
     */
    @Test
    public void or2() {
        Query query = LambdaQuery.create(TUser.class)
                // WHERE id = ? OR username = ?
                .orEq(TUser::getId, 6)
                .orNotEq(TUser::getId, 1)
                .orIn(TUser::getId, Arrays.asList(1,2))
                .orNotIn(TUser::getId, Arrays.asList(2,3))
                .orGt(TUser::getId, 1)
                .orGe(TUser::getId, 1)
                .orLt(TUser::getId, 2)
                .orLe(TUser::getId, 2)
                .orLike(TUser::getUsername, "jim")
                .orLikeLeft(TUser::getUsername, "a")
                .orLikeRight(TUser::getUsername, "b")
                .orBetween(TUser::getId, 1, 3)
                .orBetween(TUser::getId, Arrays.asList(1, 3))
                .orBetween(TUser::getId, new BetweenValue(2,6))
                .orBetween(TUser::getId, new Object[]{1,6})
                .orIsEmpty(TUser::getId)
                .orIsNull(TUser::getId)
                .orNotEmpty(TUser::getUsername)
                .orSql("1=1")
                ;
        List<TUser> list = mapper.list(query);
        System.out.println(list);
    }

    /**
     * 子条件
     */
    @Test
    public void subExpression() {
        Query query = new Query()
                // WHERE id = ? AND ( money > ? OR state = ? )
                .and(q -> q.eq("id", 3))
                .and(q -> q.gt("money", 1).orEq("state", 1));

        TUser user = mapper.getByQuery(query);
        print(user);
    }

    /**
     * 子条件
     */
    @Test
    public void subExpression2() {
        Query query = new Query()
                // WHERE ( id = ? AND username = ? ) OR ( money > ? AND state = ? )
                .and(q -> q.eq("id", 3).eq("username", "jim"))
                .or(q -> q.gt("money", 1).eq("state", 1));

        TUser user = mapper.getByQuery(query);
        print(user);
    }

    @Test
    public void listTreeData() {

    }

    @Test
    public void selectById() {
        UserInfoDO userInfoDO = mapper.selectById(6);
        System.out.println(userInfoDO);
    }

    public static class IdParam {

        @Condition(column = "id", index = 1)
        private Integer eq = 1;

        @Condition(column = "id", operator = Operator.ge, index = 2)
        private Integer ge = 1;

        @Condition(column = "id", index = 3)
        private List<Integer> in = Arrays.asList(1, 2);

        @Condition(column = "id", index = 0)
        private BetweenValue between = new BetweenValue(1, 2);

        public Integer getEq() {
            return eq;
        }

        public void setEq(Integer eq) {
            this.eq = eq;
        }

        public Integer getGe() {
            return ge;
        }

        public void setGe(Integer ge) {
            this.ge = ge;
        }

        public List<Integer> getIn() {
            return in;
        }

        public void setIn(List<Integer> in) {
            this.in = in;
        }

        public BetweenValue getBetween() {
            return between;
        }

        public void setBetween(BetweenValue between) {
            this.between = between;
        }
    }

    public static class IdParam2 {

        @Condition(column = "id")
        private Integer eq = 1;

        @Condition(column = "id", operator = Operator.ge)
        private Integer ge = 1;

        @Condition(column = "id")
        private List<Integer> in = Arrays.asList(1, 2);

        @Condition(column = "id")
        private BetweenValue between = new BetweenValue(1, 2);

        @Condition(column = "id", operator = Operator.between)
        private List<Object> between2 = Arrays.asList(1, 2);


        public Integer getEq() {
            return eq;
        }

        public void setEq(Integer eq) {
            this.eq = eq;
        }

        public Integer getGe() {
            return ge;
        }

        public void setGe(Integer ge) {
            this.ge = ge;
        }

        public List<Integer> getIn() {
            return in;
        }

        public void setIn(List<Integer> in) {
            this.in = in;
        }

        public BetweenValue getBetween() {
            return between;
        }

        public void setBetween(BetweenValue between) {
            this.between = between;
        }

        public List<Object> getBetween2() {
            return between2;
        }

        public void setBetween2(List<Object> between2) {
            this.between2 = between2;
        }
    }

    public static class MyUser {
        private Integer id;
        private String username;
        private String addTime;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getAddTime() {
            return addTime;
        }

        public void setAddTime(String addTime) {
            this.addTime = addTime;
        }

        @Override
        public String toString() {
            return "MyUser{" +
                    "id=" + id +
                    ", username='" + username + '\'' +
                    ", addTime='" + addTime + '\'' +
                    '}';
        }
    }

    public static class MyUser2 {
        private Integer id;
        private String username;

    }

    public static enum StatusEnum {

    }

    public static class UserMoneyParam {
        @Condition(column = "money",operator = Operator.between)
        private Object[] moneyArr;

        public Object[] getMoneyArr() {
            return moneyArr;
        }

        public void setMoneyArr(Object[] moneyArr) {
            this.moneyArr = moneyArr;
        }
    }


    public static class UserParam extends PageParam {

        private Integer id;

        private String username;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }

    public static class User2 {
        private String name;

        private String createTime;

        private LocalDateTime addTime;

        private Float money;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public LocalDateTime getAddTime() {
            return addTime;
        }

        public void setAddTime(LocalDateTime addTime) {
            this.addTime = addTime;
        }

        public Float getMoney() {
            return money;
        }

        public void setMoney(Float money) {
            this.money = money;
        }

        @Override
        public String toString() {
            return "User2{" +
                    "name='" + name + '\'' +
                    ", createTime='" + createTime + '\'' +
                    ", addTime=" + addTime +
                    ", money=" + money +
                    '}';
        }
    }


}
