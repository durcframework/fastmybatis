package com.myapp;

import com.alibaba.fastjson.JSON;
import com.gitee.fastmybatis.core.PageInfo;
import com.gitee.fastmybatis.core.query.LambdaQuery;
import com.gitee.fastmybatis.core.query.Operator;
import com.gitee.fastmybatis.core.query.Query;
import com.gitee.fastmybatis.core.query.Sort;
import com.gitee.fastmybatis.core.query.UpdateQuery;
import com.gitee.fastmybatis.core.query.annotation.Condition;
import com.gitee.fastmybatis.core.query.expression.BetweenValue;
import com.gitee.fastmybatis.core.query.expression.ValueExpression;
import com.gitee.fastmybatis.core.query.param.OffsetParam;
import com.gitee.fastmybatis.core.query.param.PageMultiSortParam;
import com.gitee.fastmybatis.core.query.param.PageParam;
import com.gitee.fastmybatis.core.query.param.PageSortParam;
import com.gitee.fastmybatis.core.query.param.SortInfo;
import com.gitee.fastmybatis.core.support.ColumnValue;
import com.gitee.fastmybatis.core.support.Columns;
import com.gitee.fastmybatis.core.support.PageEasyui;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.myapp.dao.TUserMapper;
import com.myapp.entity.TUser;
import com.myapp.entity.UserInfoDO;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * mapper测试
 */
public class TUserMapperTest extends FastmybatisSpringbootApplicationTests {

    @Resource
    TUserMapper mapper;

    @Resource
    TransactionTemplate transactionTemplate;

    /**
     * 根据主键查询
     *
     * <pre>
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money`
     * FROM `t_user` t
     * WHERE `id` = ? LIMIT 1
     * </pre>
     */
    @Test
    public void testGetById() {
        TUser user = mapper.getById(6);
        print(user);
    }

    /**
     * 根据条件查询一条记录
     *
     * <pre>
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money`
     * FROM `t_user` t
     * WHERE id = ? AND money > ? LIMIT 1
     * </pre>
     */
    @Test
    public void testGetByQuery() {
        // 查询ID=3,金额大于1的用户
        Query query = new Query()
                .eq("id", 3)
                .gt("money", 1);

        TUser user = mapper.getByQuery(query);
        print(user);
    }

    /**
     * 根据字段查询一条记录
     *
     * <pre>
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money`
     * FROM `t_user` t
     * WHERE t.`username` = ? LIMIT 1
     * </pre>
     */
    @Test
    public void testGetByColumn() {
        TUser user = mapper.getByColumn("username", "王五");
        print(user);
    }

    /**
     * 更新所有字段
     */
    @Test
    public void testUpdate() {
        TUser user = mapper.getById(6);
        user.setUsername("李四");
        user.setMoney(Optional.ofNullable(user.getMoney()).orElse(BigDecimal.ZERO).add(new BigDecimal("0.1")));
        user.setState((byte) 1);
        user.setIsdel(1);

        int i = mapper.update(user);
        print("testUpdate --> " + i);
    }

    /**
     * 根据条件查询多条记录
     *
     * <pre>
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money`
     * FROM `t_user` t
     * WHERE state = ? AND money IN ( ? , ? , ? )
     * </pre>
     */
    @Test
    public void testList() {
        Query query = new Query()
                .eq("state", 0)
                .in("money", Arrays.asList(100, 1.0, 3));
        List<TUser> list = mapper.list(query);
        for (TUser tUser : list) {
            print(tUser);
        }
    }

    @Test
    public void testBetween() {
        Query query = new Query()
                .eq("state", 0)
                .between("money", 1, 100);
        List<TUser> list = mapper.list(query);
        for (TUser tUser : list) {
            print(tUser);
        }
    }

    @Test
    public void testBetween2() {
        UserMoneyParam userMoneyParam = new UserMoneyParam();
        userMoneyParam.setMoneyArr(new Object[]{1, 100});
        Query query = Query.build(userMoneyParam);
        List<TUser> list = mapper.list(query);
        for (TUser tUser : list) {
            print(tUser);
        }
    }

    // UserMoneyParam

    /**
     * 对象字段转换成条件
     */
    @Test
    public void testParam() {
        UserParam userParam = new UserParam();
        userParam.setId(6);
        userParam.setPageIndex(1);
        userParam.setPageSize(2);
        Query query = Query.build(userParam);
        PageInfo<TUser> page = mapper.page(query);
        System.out.println(JSON.toJSONString(page));
    }

    /**
     * 不规则翻页
     */
    @Test
    public void testOffsetParam() {
        OffsetParam param = new OffsetParam();
        param.setStart(1);
        param.setLimit(2);
        Query query = param.toQuery();
        PageInfo<TUser> page = mapper.page(query);
        System.out.println(JSON.toJSONString(page));
    }

    /**
     * 分页+排序
     */
    @Test
    public void testPageSortParam() {
        PageSortParam param = new PageSortParam();
        param.setPageIndex(1);
        param.setPageSize(5);
        param.setSort("id");
        param.setOrder("desc");
        Query query = param.toQuery();
        PageInfo<TUser> page = mapper.page(query);
        System.out.println(JSON.toJSONString(page));
    }

    /**
     * 分页+多字段排序
     */
    @Test
    public void testPageMultiSortParam() {
        PageMultiSortParam param = new PageMultiSortParam();
        param.setPageIndex(1);
        param.setPageSize(5);
        param.setSorts(Arrays.asList(
                new SortInfo("id", "desc"),
                new SortInfo("add_time", "asc")
        ));
        Query query = param.toQuery();
        PageInfo<TUser> page = mapper.page(query);
        System.out.println(JSON.toJSONString(page));
    }

    /**
     * 分页+多字段排序2
     */
    @Test
    public void testPageMultiSortParam2() {
        PageMultiSortParam param = new PageMultiSortParam();
        param.setPageIndex(1);
        param.setPageSize(5);
        param.setSortInfo("id=desc,addTime=asc");
        Query query = param.toQuery();
        PageInfo<TUser> page = mapper.page(query);
        System.out.println(JSON.toJSONString(page));
    }

    /**
     * 返回自定义字段
     */
    @Test
    public void testGivenColumns() {
        Query query = new Query();
        // 添加查询条件
        query.eq("username", "张三");

        // 自定义字段
        List<String> columns = Arrays.asList("id", "username");
        // 查询，返回一个Map集合
        List<TUser> list = mapper.listBySpecifiedColumns(columns, query);

        for (TUser obj : list) {
            System.out.println(obj);
        }
    }

    /**
     * 返回自定义字段，并转换成自定义类集合
     *
     * <pre>
     * SELECT t.id , t.username as username FROM `t_user` t WHERE username = ?
     * </pre>
     */
    @Test
    public void testGivenColumns2() {
        Query query = new Query();
        // 添加查询条件
        query.eq("username", "张三")
                .orderby("id", Sort.DESC);

        // 自定义字段
        List<String> columns = Arrays.asList("t.id", "t.username", "t.add_time as addTime");
        // 查询，自定义集合
        List<UserVO> list = mapper.listBySpecifiedColumns(columns, query, UserVO.class);

        for (UserVO obj : list) {
            Assert.assertNotNull(obj.getId());
            Assert.assertNotNull(obj.getUsername());
            Assert.assertNull(obj.getStateName());
            System.out.println(obj);
        }

        List<User2> user2List = mapper.listBySpecifiedColumns(
                Arrays.asList(
                        "username as name",
                        "add_time as createTime",
                        "add_time as addTime",
                        "money"
                ),
                query,
                User2.class);
        for (User2 user2 : user2List) {
            Assert.assertNotNull(user2.getName());
            Assert.assertNotNull(user2.getMoney());
            System.out.println(user2);
        }
    }

    /**
     * 返回单值集合
     *
     * <pre>
     * SELECT t.id FROM `t_user` t WHERE username = ?
     * </pre>
     */
    @Test
    public void testGivenColumns3() {
        Query query = new Query();
        // 添加查询条件
        query.eq("username", "张三");

        // 返回id列
        List<Integer> idList = mapper.listBySpecifiedColumns(Collections.singletonList("id"), query, Integer.class/* 或int.class */);
        for (Integer id : idList) {
            System.out.println(id);
        }

        // 返回id列，并转换成String
        List<String> strIdList = mapper.listBySpecifiedColumns(Collections.singletonList("id"), query, String.class);

        for (String id : strIdList) {
            System.out.println("string:" + id);
        }

        // 返回username列
        List<String> usernameList = mapper.listBySpecifiedColumns(Collections.singletonList("username"), query, String.class);
        for (String username : usernameList) {
            System.out.println(username);
        }

        // 返回时间列
        List<Date> dateList = mapper.listBySpecifiedColumns(Collections.singletonList("add_time"), query, Date.class);
        for (Date date : dateList) {
            System.out.println("Date:" + date);
        }

        List<LocalDateTime> date2List = mapper.listBySpecifiedColumns(Collections.singletonList("add_time"), query, LocalDateTime.class);
        for (LocalDateTime date : date2List) {
            System.out.println("LocalDateTime:" + date);
        }

        // 返回decimal列
        List<BigDecimal> moneyList = mapper.listBySpecifiedColumns(Collections.singletonList("money"), query, BigDecimal.class);
        for (BigDecimal money : moneyList) {
            System.out.println("BigDecimal:" + money);
        }

        // 返回decimal列并转成float
        List<Float> moneyList2 = mapper.listBySpecifiedColumns(Collections.singletonList("money"), query, float.class);
        for (Float money : moneyList2) {
            System.out.println("float:" + money);
        }

        // 返回tinyint列
        List<Byte> stateList = mapper.listBySpecifiedColumns(Collections.singletonList("state"), query, byte.class);
        for (Byte state : stateList) {
            System.out.println("state:" + state);
        }

        // 返回tinyint列
        List<BigDecimal> state2List = mapper.listBySpecifiedColumns(Collections.singletonList("id"), query, BigDecimal.class);
        for (BigDecimal state : state2List) {
            System.out.println("state BigDecimal:" + state);
        }

    }

    /**
     * 获取记录数
     *
     * <pre>
     * SELECT count(*) FROM `t_user` t WHERE username = ?
     * </pre>
     */
    @Test
    public void testGetCount() {
        Query query = new Query();
        // 添加查询条件
        query.eq("username", "张三");

        long total = mapper.getCount(query); // 获取总数

        print("total:" + total);
    }

    /**
     * 根据多个主键值查询
     */
    @Test
    public void testListByIds() {
        List<TUser> tUsers = mapper.listByIds(Arrays.asList(6, 7));
        for (TUser tUser : tUsers) {
            System.out.println(tUser);
        }
    }

    /**
     * 查询单条数据并返回指定字段
     */
    @Test
    public void getBySpecifiedColumns() {
        Query query = new Query().eq("id", 6);
        TUser tUser = mapper.getBySpecifiedColumns(Arrays.asList("id", "username"), query);
        System.out.println(tUser);
    }

    /**
     * 查询单条数据并返回指定字段并转换到指定类中
     */
    @Test
    public void getBySpecifiedColumnsAndQuery2() {
        Query query = new Query().eq("id", 6);
        UserVO userVo = mapper.getBySpecifiedColumns(Arrays.asList("id", "username"), query, UserVO.class);
        System.out.println(userVo);
    }

    @Test
    public void testCount() {
        Query query = new Query();
        // 添加查询条件
        query.gt("id", 1)
                .ge("id", 2)
                .le("id", 32)
                .notEq("id", 12);

        long total = mapper.getCount(query); // 获取总数

        print("total:" + total);
    }

    /**
     * 分页查询
     *
     * <pre>
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money`
     * FROM `t_user` t
     * WHERE username = ? LIMIT ?,?
     * </pre>
     */
    @Test
    public void testPageInfo() {
        Query query = new Query();
        // 添加查询条件
        query.eq("username", "张三")
                .page(1, 2) // 分页查询，按页码分，通常使用这种。
        // .limit(start, offset) // 分页查询，这种是偏移量分页
        // .setTotal(4);//手动设置总记录数，可选，设置后可减少一次sql请求
        ;

        // 分页信息
        PageInfo<TUser> pageInfo = mapper.page(query);

        List<TUser> list = pageInfo.getList(); // 结果集
        long total = pageInfo.getTotal(); // 总记录数
        int pageCount = pageInfo.getPageCount(); // 共几页

        System.out.println("total:" + total);
        System.out.println("pageCount:" + pageCount);
        list.forEach(System.out::println);
    }

    /**
     * 分页查询并且类型转换
     */
    @Test
    public void pageInfoFun() {
        Query query = new Query();
        // 添加查询条件
        query.eq("username", "张三")
                .page(1, 2) // 分页查询，按页码分，通常使用这种。
        ;

        // 分页信息
        PageInfo<TUser> pageInfo = mapper.page(query, tUser -> {
            // 对每行数据进行转换
            String username = tUser.getUsername();
            if ("张三".equals(username)) {
                tUser.setUsername("法外狂徒");
            }
            return tUser;
        });
        List<TUser> list = pageInfo.getList(); // 结果集
        long total = pageInfo.getTotal(); // 总记录数
        int pageCount = pageInfo.getPageCount(); // 共几页

        System.out.println("total:" + total);
        System.out.println("pageCount:" + pageCount);
        list.forEach(System.out::println);
    }

    /**
     * 分页查询并且类型转换
     */
    @Test
    public void pageInfoFun2() {
        Query query = new Query();
        // 添加查询条件
        query.eq("username", "张三")
                .page(1, 2) // 分页查询，按页码分，通常使用这种。
        ;

        // 分页信息
        PageInfo<UserVO> pageInfo = mapper.page(query, tUser -> {
            // 对每行数据进行转换
            UserVO userVO = new UserVO();
            BeanUtils.copyProperties(tUser, userVO);
            Byte state = tUser.getState();
            switch (state) {
                case 0:
                    userVO.setStateName("未启用");
                    break;
                case 1:
                    userVO.setStateName("已启用");
                    break;
                case 2:
                    userVO.setStateName("已禁用");
                    break;
                default: {
                    userVO.setStateName("未知状态");
                }
            }
            return userVO;
        });
        List<UserVO> list = pageInfo.getList(); // 结果集
        long total = pageInfo.getTotal(); // 总记录数
        int pageCount = pageInfo.getPageCount(); // 共几页

        System.out.println("total:" + total);
        System.out.println("pageCount:" + pageCount);
        list.forEach(System.out::println);
    }

    /**
     * 排序
     *
     * <pre>
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money`
     * FROM `t_user` t
     * ORDER BY id ASC,state DESC
     * </pre>
     */
    @Test
    public void testOrder() {
        Query query = new Query()
                .orderby("id", Sort.ASC)
                .orderby("state", Sort.DESC);

        List<TUser> list = mapper.list(query);
        print(list);
    }

    // 配合PageHelper
    @Test
    public void testWithPageHelper() {
        Query query = new Query();
        // 添加查询条件
        query.eq("username", "张三")
                // id倒叙
                .orderby("id", Sort.DESC);
        Page<TUser> page = PageHelper.startPage(1, 3).doSelectPage(() -> mapper.list(query));
        List<TUser> result = page.getResult();
        for (TUser userInfoDO : result) {
            System.out.println(userInfoDO);
        }
        System.out.println("页码 page.getPageNum():" + page.getPageNum());
        System.out.println("总页数 page.getPageSize():" + page.getPageSize());
        System.out.println("首行 page.getStartRow():" + page.getStartRow());
        System.out.println("末行 page.getEndRow():" + page.getEndRow());
        System.out.println("总记录数 page.getTotal():" + page.getTotal());
        System.out.println("总页数 page.getPages():" + page.getPages());
    }

    // 自定义SQL，联表分页
    // 配合PageHelper
    @Test
    public void testJoinSqlWithPageHelper() {
        Query query = new Query();
        // 添加查询条件
        query.eq("username", "张三")
                // id倒叙
                .orderby("id", Sort.DESC);
        Page<UserInfoDO> page = PageHelper.startPage(1, 5).doSelectPage(() -> mapper.findJoin(query));
        List<UserInfoDO> result = page.getResult();
        for (UserInfoDO userInfoDO : result) {
            System.out.println(userInfoDO);
        }
        System.out.println("页码 page.getPageNum():" + page.getPageNum());
        System.out.println("总页数 page.getPageSize():" + page.getPageSize());
        System.out.println("首行 page.getStartRow():" + page.getStartRow());
        System.out.println("末行 page.getEndRow():" + page.getEndRow());
        System.out.println("总记录数 page.getTotal():" + page.getTotal());
        System.out.println("总页数 page.getPages():" + page.getPages());
    }

    /**
     * 联表分页
     *
     * <pre>
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money`
     * FROM `t_user` t LEFT JOIN user_info t2 ON t.id = t2.user_id
     * WHERE t.isdel = 0 LIMIT ?,?
     * </pre>
     */
    @Test
    public void testJoinPage() {
        Query query = new Query()
                // 左连接查询,主表的alias默认为t
                .join("LEFT JOIN user_info t2 ON t.id = t2.user_id").page(1, 5);

        List<TUser> list = mapper.list(query);

        System.out.println("==============");
        for (TUser user : list) {
            System.out.println(user.getId() + " " + user.getUsername());
        }
        System.out.println("==============");
    }

    /**
     * OR
     */
    @Test
    public void testOr2() {
        Query query = new Query();
        // WHERE username = ? or state = ?
        query.addExpression(new ValueExpression("username", "Jim"))
                .addExpression(new ValueExpression("or", "state", "=", 1));
        List<TUser> list = mapper.list(query);
        System.out.println("==============");
        for (TUser user : list) {
            System.out.println(user.getId() + " " + user.getUsername());
        }
        System.out.println("==============");
    }

    /**
     * sql
     */
    @Test
    public void testSql() {
        Query query = new Query();
        query.eq("state", 0)
                .sql("username like '%?%' or isdel=?", "'--;\\'三", 1);
        List<TUser> list = mapper.list(query);
        System.out.println("==============");
        for (TUser user : list) {
            System.out.println(user.getId() + " " + user.getUsername());
        }
        System.out.println("==============");
    }

    /**
     * sql2
     */
    @Test
    public void testSql2() {
        Query query = new Query();
        query.eq("state", 0)
                .argPlaceholder("{}") // 设置参数占位符，默认'?'
                .sql("username like '%{}%' or isdel={}", "'--;\\三", 1);
        List<TUser> list = mapper.list(query);
        System.out.println("==============");
        for (TUser user : list) {
            System.out.println(user.getId() + " " + user.getUsername());
        }
        System.out.println("==============");
    }

    /**
     * 联表查询，并返回指定字段
     * <pre>
     * SELECT t2.user_id userId , t.username , t2.city
     * FROM `t_user` t
     * LEFT JOIN user_info t2 ON t.id = t2.user_id WHERE t.isdel = 0
     * </pre>
     */
    @Test
    public void testJoinColumn() {
        Query query = new Query();
        // 左连接查询,主表的alias默认为t
        query.join("LEFT JOIN user_info t2 ON t.id = t2.user_id");
        // 指定返回字段
        List<String> column = Arrays.asList("t2.user_id userId", "t.username", "t2.city");
        // 再将map转换成实体bean
        List<UserInfoVo> list = mapper.listBySpecifiedColumns(column, query, UserInfoVo.class);

        this.print(list);
    }

    /**
     * 自定义sql方式2，见TUserMapper.xml
     */
    @Test
    public void testSelfSql2() {
        TUser user = mapper.selectByName("张三");
        print(user);
    }

    /**
     * 添加-保存所有字段
     */
    @Test
    public void testSave() {
        TUser user = save();

        print("添加后的主键:" + user.getId());

        print(user);
    }

    /**
     * 添加-保存所有字段
     */
    @Test
    public void testSaveOrUpdate() {
        TUser user = new TUser();
        user.setId(20);
        user.setIsdel(0);
        user.setLeftMoney(22.1F);
        user.setMoney(new BigDecimal("100.5"));
        user.setRemark("备注");
        user.setState((byte) 0);
        user.setUsername("张三2");
        mapper.saveOrUpdate(user);
        System.out.println(user);
    }

    private TUser save() {
        TUser user = new TUser();
        user.setId(30);
        user.setIsdel(0);
        user.setLeftMoney(22.1F);
        user.setMoney(new BigDecimal("100.5"));
        user.setRemark("备注");
        user.setState((byte) 0);
        user.setUsername("张三");

        mapper.save(user);
        return user;
    }

    /**
     * 添加-保存非空字段
     */
    @Test
    public void testSaveIgnoreNull() {
        TUser user = new TUser();
//        user.setAddTime(new Date());
        user.setIsdel(1);
        user.setMoney(new BigDecimal(100.5));
        user.setState((byte) 0);
        user.setUsername("张三notnull");
        user.setLeftMoney(null);
        user.setRemark(null);

        mapper.saveIgnoreNull(user);

        print("添加后的主键:" + user.getId());
        print(user);
    }

    /**
     * 批量添加.支持mysql,sqlserver2008。如需支持其它数据库使用saveMulti方法
     *
     * <pre>
     * INSERT INTO person (id, name, age) VALUES (1, 'Kelvin', 22), (2, 'ini_always', 23);
     * </pre>
     */
    @Test
    public void testInsertBatch() {
        List<TUser> users = new ArrayList<>();

        for (int i = 0; i < 3; i++) { // 创建3个对象
            TUser user = new TUser();
            user.setUsername("username" + i);
            user.setMoney(new BigDecimal(i));
            user.setRemark("remark" + i);
            user.setState((byte) 0);
            user.setIsdel(0);
            user.setAddTime(new Date());
            user.setLeftMoney(200F);
            users.add(user);
        }

        int i = mapper.saveBatch(users); // 返回成功数
        Assert.assertTrue(i > 0);
        List<Integer> idList = users.stream()
                .map(TUser::getId)
                .collect(Collectors.toList());
        Assert.assertTrue(idList.size() > 0);
        System.out.println("主键id：" + idList);

        System.out.println("saveBatch --> " + i);
        for (TUser user : users) {
            System.out.println(user);
        }
    }

    @Test
    public void saveBatchIgnoreNull() {
        List<TUser> users = new ArrayList<>();

        for (int i = 0; i < 3; i++) { // 创建3个对象
            TUser user = new TUser();
            user.setUsername("username" + i);
            user.setMoney(new BigDecimal(i));
            user.setState((byte) 0);
            user.setIsdel(0);
            user.setAddTime(new Date());
            users.add(user);
        }

        int i = mapper.saveBatchIgnoreNull(users);// 返回成功数
        Assert.assertTrue(i > 0);

        List<Integer> idList = users.stream()
                .map(TUser::getId)
                .collect(Collectors.toList());
        Assert.assertTrue(idList.size() > 0);
        System.out.println("主键id：" + idList);
    }

    @Test
    public void saveBatchIgnoreNull2() {
        List<TUser> users = new ArrayList<>();

        for (int i = 0; i < 5; i++) { // 创建3个对象
            TUser user = new TUser();
            user.setUsername("username" + i);
            user.setMoney(new BigDecimal(i));
            user.setState((byte) 0);
            user.setIsdel(0);
            user.setAddTime(new Date());
            users.add(user);
        }

        int i = mapper.saveBatchIgnoreNull(users, 2); // 返回成功数
        System.out.println(i);
        Assert.assertTrue(i > 0);

        List<Integer> idList = users.stream()
                .map(TUser::getId)
                .collect(Collectors.toList());
        Assert.assertTrue(idList.size() > 0);
        System.out.println("主键id：" + idList);
    }

    @Test
    public void testInsertBatchForPartition() {
        List<TUser> users = new ArrayList<>();

        for (int i = 0; i < 5; i++) { // 创建3个对象
            TUser user = new TUser();
            user.setUsername("username" + i);
            user.setMoney(new BigDecimal(i));
            user.setRemark("remark" + i);
            user.setState((byte) 0);
            user.setIsdel(0);
            user.setAddTime(new Date());
            user.setLeftMoney(200F);
            users.add(user);
        }

        int i = mapper.saveBatch(users, 2);
        System.out.println(i);
    }


    /**
     * 事务回滚
     */
    @Test
    public void testUpdateTran() {
        try {
            TUser user = transactionTemplate.execute(new TransactionCallback<TUser>() {
                @Override
                public TUser doInTransaction(TransactionStatus arg0) {
                    TUser user = mapper.getById(3);
                    user.setUsername("王五1");
                    user.setMoney(user.getMoney().add(new BigDecimal("0.1")));
                    user.setIsdel(1);

                    int i = mapper.update(user);
                    print("testUpdate --> " + i);
                    int j = 1 / 0; // 模拟错误
                    return user;
                }
            });
            print(user);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /**
     * 更新不为null的字段
     * UPDATE `t_user` SET `username`=?, `state`=?, `isdel`=? WHERE `id` = ?
     */
    @Test
    public void testUpdateIgnoreNull() {
        TUser user = new TUser();
        user.setId(3);
        user.setUsername("王五");
        user.setState((byte) 2);
        user.setIsdel(0);
        user.setAddTime(new Date());
        int i = mapper.updateIgnoreNull(user);
        print("updateNotNull --> " + i);
    }

    /**
     * 根据条件更新。将状态为2的数据姓名更新为李四
     * UPDATE `t_user` SET `username`=?, `add_time`=? WHERE state = ?
     */
    @Test
    public void testUpdateByQuery() {
        Query query = new Query().eq("state", 2);
        // 方式1
        TUser user = new TUser();
        user.setUsername("李四");
        int i = mapper.updateByQuery(user, query);
        print("updateByQuery --> " + i);

       /* // 方式2
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("username", "李四2");
        i = mapper.updateByQuery(map, query);
        print("updateByQuery --> " + i);*/
    }

    /**
     * 根据条件更新。忽略某个属性
     * UPDATE `t_user` SET `username`=?, `add_time`=? WHERE state = ?
     */
    @Test
    public void testUpdateByQuery2() {
        Query query = new Query().eq("id", 6);
        // 方式1
        TUser user = new TUser();
        user.setUsername("李四");
        user.setState((byte) 1);
        user.setAddTime(new Date());
        int i = mapper.updateByQuery(user, query, "state", "addTime");
        print("updateByQuery --> " + i);

       /* // 方式2
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("username", "李四2");
        i = mapper.updateByQuery(map, query);
        print("updateByQuery --> " + i);*/
    }

    @Test
    public void testUpdateByMap() {
        Query query = new Query().eq("id", 1);
        // 方式1
        // key为数据库字段名
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("username", "李四2");
        map.put("remark", "123");
        // UPDATE `t_user` SET username = ? , remark = ? WHERE id = ?
        int i = mapper.updateByMap(map, query);
        print("updateByMap --> " + i);


        // 方式2：Lambda
        ColumnValue<TUser> columnValue = ColumnValue.create(TUser.class)
                .set(TUser::getUsername, "李四2")
                .set(TUser::getRemark, "123");
        int j = mapper.updateByQuery(columnValue, query);
        System.out.println(j);

        UpdateQuery updateQuery = new UpdateQuery();
        updateQuery.set("username", "王五")
                .eq("id", 6);
        mapper.update(updateQuery);


        LambdaQuery<TUser> updateQuery2 = Query.query(TUser.class);
        updateQuery2.set(TUser::getUsername, "王五")
                .set(TUser::getRemark, "aaa")
                .eq(TUser::getId, 6);
        mapper.update(updateQuery2);
    }

    @Test
    public void testUpdateByMap2() {
        ColumnValue<TUser> columnValue = ColumnValue.create(TUser.class)
                .set(TUser::getRemark, "123");
        int i = mapper.updateByColumn(columnValue, TUser::getUsername, "李四2");
        print("updateByMap --> " + i);
    }

    @Test
    public void testUpdateByMap21() {
        ColumnValue<TUser> columnValue = ColumnValue.create(TUser.class)
                .set(TUser::getUsername, "李四2")
                .set(TUser::getRemark, "123");
        int i = mapper.updateById(columnValue, 1);
        print("updateByMap --> " + i);
    }

    @Test
    public void testUpdateByMap3() {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("username", null);
        map.put("remark", "123");
        int i = mapper.updateByMap(map, new Query().eq("id", 1));
        print("updateByMap --> " + i);
    }

    /**
     * 根据主键删除
     */
    @Test
    public void testDeleteById() {
        int i = mapper.deleteById(3);
        print("del --> " + i);
    }

    @Test
    public void testDeleteByIds() {
        int i = mapper.deleteByIds(Arrays.asList(1, 2, 3));
        print("del --> " + i);
    }

    @Test
    public void testDeleteByColumn() {
        int i = mapper.deleteByColumn("id", Arrays.asList(1, 2, 3));
        print("del --> " + i);
    }

    /**
     * 根据对象删除
     */
    @Test
    public void testDelete() {
        TUser user = new TUser();
        user.setId(15);
        int i = mapper.delete(user);
        print("del --> " + i);
    }

    /**
     * 根据条件删除 DELETE FROM `t_user` WHERE state = ?
     */
    @Test
    public void testDeleteByQuery() {
        Query query = new Query();
        query.eq("state", 3);
        int i = mapper.deleteByQuery(query);
        print("deleteByQuery --> " + i);
    }

    /**
     * 强力查询，将无视逻辑删除字段
     */
    @Test
    public void testForceQuery() {
        Query query = new Query().eq("id", 3)
                .enableForceQuery();
        TUser user = mapper.getByQuery(query);
        this.print(user);
    }

    /**
     * 强力查询，无视逻辑删除字段
     */
    @Test
    public void testForceGetById() {
        TUser user = mapper.forceById(2);
        print(user);
    }

    /**
     * 强力删除，无视逻辑删除字段，执行DELETE FROM ...
     */
    @Test
    public void testForceDelete() {
        TUser user = this.save();
        mapper.forceDelete(user);
        print(user);
    }

    /**
     * 强力删除，无视逻辑删除字段，执行DELETE FROM ...
     * DELETE FROM t_user WHERE id=?
     */
    @Test
    public void testForceDeleteById() {
        TUser user = this.save();
        int i = mapper.forceDeleteById(user.getId());
        Assert.assertEquals(i, 1);
    }

    /**
     * 强力删除，无视逻辑删除字段，自定义条件
     * DELETE FROM t_user WHERE id=?
     */
    @Test
    public void testForceByQuery() {
        TUser user = this.save();
        Query query = new Query();
        query.eq("id", user.getId());
        int i = mapper.forceDeleteByQuery(query);
        Assert.assertEquals(i, 1);
    }

    @Test
    public void testListByArray() {
        List<TUser> users = mapper.listByArray("id", new Integer[]{1, 2, 3});
        System.out.println(JSON.toJSONString(users));
    }

    @Test
    public void testListByCollection() {
        List<TUser> users = mapper.listByCollection("id", Arrays.asList(1, 2, 3));
        System.out.println(JSON.toJSONString(users));
    }

    /**
     * 转换成特定对象
     */
    @Test
    public void testPageConvert() {
        Query query = new Query()
                .eq("id", 3);
        PageInfo<UserVO> page = mapper.page(query, UserVO::new);
        System.out.println(JSON.toJSONString(page));
    }

    @Test
    public void pageAndConvert() {
        Query query = new Query()
                .eq("state", 0);
        PageInfo<UserVO> pageInfo = mapper.pageAndConvert(query, list -> {
            List<UserVO> retList = new ArrayList<>(list.size());
            for (TUser tUser : list) {
                UserVO userVO = new UserVO();
                BeanUtils.copyProperties(tUser, userVO);
                retList.add(userVO);
            }
            return retList;
        });
    }

    /**
     * 转换成特定对象,并做额外处理
     */
    @Test
    public void testPageConvert2() {
        Query query = new Query()
                .eq("id", 3);
        PageInfo<UserVO> page = mapper.page(query, UserVO::new, userVO -> {
            System.out.println(userVO.getUsername());
        });
        System.out.println(JSON.toJSONString(page));
    }

    /**
     * 手动转换
     */
    @Test
    public void testPageConvert3() {
        Query query = new Query()
                .eq("id", 3);
        // 对结果集进行手动转换，如果仅仅是属性拷贝可以直接：mapper.page(query, UserVO::new);
        PageInfo<UserVO> page = mapper.page(query, user -> {
            UserVO userVO = new UserVO();
            BeanUtils.copyProperties(user, userVO);
            return userVO;
        });
        System.out.println(JSON.toJSONString(page));
    }

    @Test
    public void testPageEasyui() {
        Query query = new Query()
                .eq("id", 3);
        PageEasyui<TUser> page = mapper.pageEasyui(query);
        System.out.println(JSON.toJSONString(page));
    }

    @Test
    public void testPageEasyui2() {
        Query query = new Query()
                .eq("id", 3);
        PageEasyui<UserVO> page = mapper.pageEasyui(query, UserVO.class);
        System.out.println(JSON.toJSONString(page));
    }


    @Test
    public void pageByColumn() {
        Query query = new Query()
                .eq("state", 0)
                .page(1, 6);
        PageInfo<MyUser> pageInfo = mapper.pageBySpecifiedColumns(Arrays.asList("id", "username", "add_time"), query, MyUser.class);
        System.out.println(pageInfo);
    }

    @Test
    public void pageByColumn2() {
        Query query = new Query()
                .eq("state", 0)
                .page(1, 6);
        PageInfo<MyUser> pageInfo = mapper.pageBySpecifiedColumns(Arrays.asList("id", "username", "add_time"), query, MyUser.class);
        System.out.println(pageInfo);
    }

    @Test
    public void someColumns() {
        Query query = new Query().ge("id", 3);
        List<TUser> stateList = mapper.listBySpecifiedColumns(Arrays.asList("id", "state", "add_time"), query);
        for (TUser tUser : stateList) {
            System.out.println(tUser);
        }
    }

    @Test
    public void oneColumn() {
        Query query = new Query().eq("id", 6);
        List<Integer> stateList = mapper.listBySpecifiedColumns(Collections.singletonList("state"), query, Integer.class);
        for (Integer state : stateList) {
            System.out.println(state);
        }
    }

    /**
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money` FROM `t_user` t
     * WHERE
     * id = ?
     * AND id >= ?
     * AND id IN ( ? , ? )
     * AND id BETWEEN ? AND ?
     * AND (id=1)
     * AND t.isdel = 0
     */
    @Test
    public void query() {
        Query query = new Query()
                .eq("id", 1)
                .ge("id", 1)
                .in("id", Arrays.asList(1, 2))
                .between("id", 1, 2)
                .sql("id=1");
        List<TUser> list = mapper.list(query);
        System.out.println(list);
    }

    @Test
    public void query2() {
        IdParam idParam = new IdParam();
        Query query = Query.build(idParam);
        List<TUser> list = mapper.list(query);
        System.out.println(list);
    }

    @Test
    public void query3() {
        IdParam2 idParam = new IdParam2();
        Query query = Query.build(idParam);
        List<TUser> list = mapper.list(query);
        System.out.println(list);
    }

    /**
     * List转换成Map
     */
    @Test
    public void getMap() {
        Query query = new Query()
                .ge("id", 1);
        // id -> TUser
        Map<Integer, TUser> map = mapper.getMap(query, TUser::getId);
        System.out.println(map.get(6));
    }

    @Test
    public void getMap2() {
        Query query = new Query()
                .ge("id", 1);
        // id -> username
        Map<Integer, String> map = mapper.getMap(query, TUser::getId, TUser::getUsername);
        System.out.println(map.get(6));
    }

    /**
     * 子表达式
     * <p>
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money`
     * FROM `t_user` t
     * WHERE id = ? AND ( username = ? OR state = ? ) OR username = ? AND t.isdel = 0
     */
    @Test
    public void and() {
        Query query = new Query()
                .eq("`id`", 6)
                // 子表达式
                .and(q -> q.eq("username", "jim")
                        .orEq("state", 1)
                )
                .orEq("username", "tom");
        List<TUser> list = mapper.list(query);
        System.out.println(list);
    }

    /**
     * or查询
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money`
     * FROM `t_user` t
     * WHERE id = ? OR ( username = ? OR state = ? ) OR username = ? AND t.isdel = 0
     */
    @Test
    public void orCondition() {
        Query query = new Query()
                .eq("id", 6)
                // 子表达式
                .or(q -> q.eq("username", "jim")
                        .orEq("state", 1)
                )
                .orEq("username", "tom");
        List<TUser> list = mapper.list(query);
        System.out.println(list);
    }

    /**
     * OR 查询
     * SELECT t.`id` , t.`username` , t.`state` , t.`isdel` , t.`remark` , t.`add_time` , t.`money` , t.`left_money`
     * FROM `t_user` t
     * WHERE id = ? OR username = ? AND t.isdel = 0
     */
    @Test
    public void or() {
        Query query = new Query()
                // WHERE id = ? OR username = ?
                .eq("id", 6)
                .orEq("username", "jim");
        List<TUser> list = mapper.list(query);
        System.out.println(list);
    }

    /**
     * 全是or
     * SELECT
     * t.`id`,
     * t.`username`,
     * t.`state`,
     * t.`isdel`,
     * t.`remark`,
     * t.`add_time`,
     * t.`money`,
     * t.`left_money`
     * FROM
     * `t_user` t
     * WHERE
     * id = ?
     * OR id <> ?
     * OR id IN ( ?, ? )
     * OR id NOT IN ( ?, ? )
     * OR id > ?
     * OR id >= ?
     * OR id < ?
     * OR id <= ?
     * OR username LIKE ?
     * OR username LIKE ?
     * OR username LIKE ?
     * OR id BETWEEN ? 	AND ?
     * OR id BETWEEN ? 	AND ?
     * OR id BETWEEN ? 	AND ?
     * OR id BETWEEN ? 	AND ?
     * OR ( id IS NULL OR id = '' )
     * OR ( id IS NULL )
     * OR ( 1 = 1 )
     * AND t.isdel = 0
     */
    @Test
    public void or2() {
        Query query = new Query()
                // WHERE id = ? OR username = ?
                .orEq("id", 6)
                .orNotEq("id", 1)
                .orIn("id", Arrays.asList(1, 2))
                .orNotIn("id", Arrays.asList(2, 3))
                .orGt("id", 1)
                .orGe("id", 1)
                .orLt("id", 2)
                .orLe("id", 2)
                .orLike("username", "jim")
                .orLikeLeft("username", "a")
                .orLikeRight("username", "b")
                .orBetween("id", 1, 3)
                .orBetween("id", Arrays.asList(1, 3))
                .orBetween("id", new BetweenValue(2, 6))
                .orBetween("id", new Object[]{1, 6})
                .orIsNull("id")
                .orNotEmpty("username")
                .orSql("1=1");
        List<TUser> list = mapper.list(query);
        System.out.println(list);
        Assert.assertFalse(list.isEmpty());
    }

    /**
     * 子条件
     */
    @Test
    public void subExpression() {
        Query query = new Query()
                // WHERE id = ? AND ( money > ? OR state = ? )
                .and(q -> q.eq("id", 3))
                .and(q -> q.gt("money", 1).orEq("state", 1));

        TUser user = mapper.getByQuery(query);
        print(user);
    }

    /**
     * 子条件
     */
    @Test
    public void subExpression2() {
        Query query = new Query()
                // WHERE ( id = ? AND username = ? ) OR ( money > ? AND state = ? )
                .and(q -> q.eq("id", 3).eq("username", "jim"))
                .or(q -> q.gt("money", 1).eq("state", 1));

        TUser user = mapper.getByQuery(query);
        print(user);
    }

    @Test
    public void listTreeData() {

    }

    @Test
    public void selectById() {
        UserInfoDO userInfoDO = mapper.selectById(6);
        System.out.println(userInfoDO);
    }

    @Test
    public void ignoreArray() {
        IdsParam idsParam = new IdsParam();
        idsParam.setIds(new ArrayList<>());

        Query query = Query.build(idsParam);
        List<TUser> list = mapper.list(query);
        System.out.println(list);
    }

    @Test
    public void mobileFormatter() {
        TUser user = mapper.getById(1);
        System.out.println(user);
    }

    @Test
    public void testV2_11_0() {
        // getBySpecifiedColumns
        Query query = new Query().eq("id", 6);
        TUser tUser = mapper.getBySpecifiedColumns(Columns.of(TUser::getId, TUser::getUsername), query);
        System.out.println(tUser);
        Assert.assertNotNull(tUser.getId());
        Assert.assertNotNull(tUser.getUsername());
        Assert.assertNull(tUser.getAddTime());

        // getBySpecifiedColumns
        UserVO userVo = mapper.getBySpecifiedColumns(
                Columns.of(TUser::getId, TUser::getUsername), query, UserVO.class
        );
        System.out.println(userVo);
        Assert.assertNotNull(tUser.getId());
        Assert.assertNotNull(tUser.getUsername());
        Assert.assertNull(tUser.getAddTime());

        Integer id = mapper.getColumnValue(TUser::getId, query);
        Assert.assertNotNull(id);

        // 查询，返回一个Map集合
        List<TUser> list = mapper.listBySpecifiedColumns(Columns.of(TUser::getId, TUser::getUsername), query);
        for (TUser user : list) {
            Assert.assertNotNull(user.getId());
            Assert.assertNotNull(user.getUsername());
            Assert.assertNull(user.getAddTime());
        }

        // 查询，返回一个集合
        List<UserVO> list2 = mapper.listBySpecifiedColumns(Columns.of(TUser::getId, TUser::getUsername), query, UserVO.class);
        for (UserVO user : list2) {
            Assert.assertNotNull(user.getId());
            Assert.assertNotNull(user.getUsername());
            Assert.assertNull(user.getAddTime());
        }

        // 查询一个字段集合
        List<Integer> list1 = mapper.listColumnValues(TUser::getId, query, int.class);
        for (Integer i : list1) {
            Assert.assertNotNull(i);
        }

        PageInfo<UserVO> page = mapper.pageBySpecifiedColumns(Columns.of(TUser::getId, TUser::getUsername), query, UserVO.class);
        for (UserVO user : page.getList()) {
            Assert.assertNotNull(user.getId());
            Assert.assertNotNull(user.getUsername());
            Assert.assertNull(user.getAddTime());
        }

        TUser obj = mapper.getByColumn(TUser::getId, 6);
        Assert.assertNotNull(obj);

        List<TUser> tUsers = mapper.listByColumn(TUser::getId, 6);
        Assert.assertFalse(tUsers.isEmpty());

        List<TUser> tUsers1 = mapper.listByValues(TUser::getId, 4, 5, 6);
        Assert.assertFalse(tUsers1.isEmpty());

        List<TUser> tUsers2 = mapper.listByValues("id", 4, 5, 6);
        Assert.assertFalse(tUsers2.isEmpty());

        List<TUser> tUsers3 = mapper.listByCollection(TUser::getId, Arrays.asList(4, 5, 6));
        Assert.assertFalse(tUsers3.isEmpty());

        ColumnValue<TUser> columnValue = ColumnValue.create(TUser.class)
                .set(TUser::getRemark, "123");
        int i = mapper.updateByColumn(columnValue, TUser::getId, 1);
        Assert.assertEquals(1, i);

        int i2 = mapper.updateById(columnValue, 1);
        Assert.assertEquals(1, i2);
    }

    public static class IdsParam {

        @Condition(column = "id")
        private List<Integer> ids;

        public List<Integer> getIds() {
            return ids;
        }

        public void setIds(List<Integer> ids) {
            this.ids = ids;
        }
    }

    public static class IdParam {

        @Condition(column = "id", index = 1)
        private Integer eq = 1;

        @Condition(column = "id", operator = Operator.ge, index = 2)
        private Integer ge = 1;

        @Condition(column = "id", index = 3)
        private List<Integer> in = Arrays.asList(1, 2);

        @Condition(column = "id", index = 0)
        private BetweenValue between = new BetweenValue(1, 2);

        public Integer getEq() {
            return eq;
        }

        public void setEq(Integer eq) {
            this.eq = eq;
        }

        public Integer getGe() {
            return ge;
        }

        public void setGe(Integer ge) {
            this.ge = ge;
        }

        public List<Integer> getIn() {
            return in;
        }

        public void setIn(List<Integer> in) {
            this.in = in;
        }

        public BetweenValue getBetween() {
            return between;
        }

        public void setBetween(BetweenValue between) {
            this.between = between;
        }
    }

    public static class IdParam2 {

        @Condition(column = "id")
        private Integer eq = 1;

        @Condition(column = "id", operator = Operator.ge)
        private Integer ge = 1;

        @Condition(column = "id")
        private List<Integer> in = Arrays.asList(1, 2);

        @Condition(column = "id")
        private BetweenValue between = new BetweenValue(1, 2);

        @Condition(column = "id", operator = Operator.between)
        private List<Object> between2 = Arrays.asList(1, 2);


        public Integer getEq() {
            return eq;
        }

        public void setEq(Integer eq) {
            this.eq = eq;
        }

        public Integer getGe() {
            return ge;
        }

        public void setGe(Integer ge) {
            this.ge = ge;
        }

        public List<Integer> getIn() {
            return in;
        }

        public void setIn(List<Integer> in) {
            this.in = in;
        }

        public BetweenValue getBetween() {
            return between;
        }

        public void setBetween(BetweenValue between) {
            this.between = between;
        }

        public List<Object> getBetween2() {
            return between2;
        }

        public void setBetween2(List<Object> between2) {
            this.between2 = between2;
        }
    }

    public static class MyUser {
        private Integer id;
        private String username;
        private String addTime;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getAddTime() {
            return addTime;
        }

        public void setAddTime(String addTime) {
            this.addTime = addTime;
        }

        @Override
        public String toString() {
            return "MyUser{" +
                    "id=" + id +
                    ", username='" + username + '\'' +
                    ", addTime='" + addTime + '\'' +
                    '}';
        }
    }

    public static class MyUser2 {
        private Integer id;
        private String username;

    }

    public static enum StatusEnum {

    }

    public static class UserMoneyParam {
        @Condition(column = "money", operator = Operator.between)
        private Object[] moneyArr;

        public Object[] getMoneyArr() {
            return moneyArr;
        }

        public void setMoneyArr(Object[] moneyArr) {
            this.moneyArr = moneyArr;
        }
    }


    public static class UserParam extends PageParam {

        private Integer id;

        private String username;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }

    public static class User2 {
        private String name;

        private String createTime;

        private LocalDateTime addTime;

        private Float money;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public LocalDateTime getAddTime() {
            return addTime;
        }

        public void setAddTime(LocalDateTime addTime) {
            this.addTime = addTime;
        }

        public Float getMoney() {
            return money;
        }

        public void setMoney(Float money) {
            this.money = money;
        }

        @Override
        public String toString() {
            return "User2{" +
                    "name='" + name + '\'' +
                    ", createTime='" + createTime + '\'' +
                    ", addTime=" + addTime +
                    ", money=" + money +
                    '}';
        }
    }


}
