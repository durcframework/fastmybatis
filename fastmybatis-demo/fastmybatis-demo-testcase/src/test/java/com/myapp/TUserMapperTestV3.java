package com.myapp;

import com.alibaba.fastjson.JSON;
import com.gitee.fastmybatis.core.query.Query;
import com.gitee.fastmybatis.core.query.Sort;
import com.myapp.dao.TUserMapper;
import com.myapp.entity.TUser;
import com.myapp.entity.TUserColumns;
import org.junit.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Columns测试
 *
 * @author thc
 */
public class TUserMapperTestV3 extends FastmybatisSpringbootApplicationTests {

    @Resource
    TUserMapper mapper;

    /**
     * 查询单条数据并返回指定字段
     */
    @Test
    public void getBySpecifiedColumns() {
        Query query = new Query().eq(TUserColumns.id, 6);
        TUser tUser = mapper.getBySpecifiedColumns(Arrays.asList(TUserColumns.id, TUserColumns.username), query);
        System.out.println(tUser);
    }


    /**
     * 查询单条数据并返回指定字段并转换到指定类中
     */
    @Test
    public void getBySpecifiedColumns2() {
        Query query = new Query().eq(TUserColumns.id, 6);
        UserVO userVo = mapper.getBySpecifiedColumns(Arrays.asList(TUserColumns.id, TUserColumns.username), query, UserVO.class);
        System.out.println(userVo);
    }

    /**
     * 查询某个字段值
     */
    @Test
    public void getColumnValue() {
        Query query = new Query().eq("id", 6);
        String username = mapper.getColumnValue("username", query, String.class);
        System.out.println(username);

        String idString = mapper.getColumnValue("id", query, String.class);
        System.out.println("idString:" + idString);

        int idInt = mapper.getColumnValue("id", query, int.class);
        System.out.println("idInt:" + idInt);

        int idInteger = mapper.getColumnValue("id", query, Integer.class);
        System.out.println("idInteger:" + idInteger);

        BigInteger idBigInteger = mapper.getColumnValue("id", query, BigInteger.class);
        System.out.println("idBigInteger:" + idBigInteger);

        BigDecimal moneyBigDecimal = mapper.getColumnValue("money", query, BigDecimal.class);
        System.out.println("moneyBigDecimal:" + moneyBigDecimal);

        Double moneyDouble = mapper.getColumnValue("money", query, Double.class);
        System.out.println("moneyDouble:" + moneyDouble);
    }

    /**
     * 查询指定列，返指定列集合
     */
    @Test
    public void listColumnValues() {
        // id > 10
        Query query = new Query().gt(TUserColumns.id, 10);
        List<String> list = mapper.listColumnValues(TUserColumns.username, query, String.class);
        System.out.println(list);
    }

    /**
     * 根据多个主键值查询
     */
    @Test
    public void listByIds() {
        List<TUser> tUsers = mapper.listByIds(Arrays.asList(6, 7));
        for (TUser tUser : tUsers) {
            System.out.println(tUser);
        }
    }

    /**
     * 保存或更新（所有字段）
     */
    @Test
    public void saveOrUpdate() {
        TUser user = new TUser();
        user.setId(201);
        user.setIsdel(0);
        user.setLeftMoney(22.1F);
        user.setMoney(new BigDecimal("100.5"));
        user.setRemark("备注");
        user.setState((byte)0);
        user.setUsername("张三2");
        mapper.saveOrUpdate(user);
        System.out.println(user);
    }

    /**
     * 保存或更新（不为null的字段）
     */
    @Test
    public void saveOrUpdateIgnoreNull() {
        TUser user = new TUser();
        user.setId(20);
        user.setIsdel(0);
        user.setState((byte)0);
        user.setUsername("张三2");
        mapper.saveOrUpdateIgnoreNull(user);
        System.out.println(user);
    }

    /**
     * 保存或更新
     */
    @Test
    public void saveOrUpdateCustom() {
        TUser user = new TUser();
        user.setId(20);
        user.setIsdel(0);
        user.setState((byte)0);
        user.setUsername("张三2");
        user.setRemark("ok");
        mapper.saveOrUpdate(user, e -> mapper.checkExist("username", "张三2"));
        System.out.println(user);
    }


    /**
     * 保存或更新（不为null的字段）
     */
    @Test
    public void saveOrUpdateIgnoreNullCustom() {
        TUser user = new TUser();
        user.setId(20);
        user.setIsdel(0);
        user.setState((byte)0);
        user.setUsername("张三2");
        mapper.saveOrUpdateIgnoreNull(user, e -> mapper.checkExist("id", user.getId()));
        System.out.println(user);
    }

    @Test
    public void checkExist() {
        boolean checkExist = mapper.checkExist("username", "王五", 1);
        System.out.println(checkExist);
    }

    /**
     * 根据多个主键id删除，在有逻辑删除字段的情况下，做UPDATE操作
     */
    @Test
    public void deleteByIds() {
        int i = mapper.deleteByIds(Arrays.asList(1,2,3));
        print("del --> " + i);
    }

    /**
     * 根据指定字段值删除，在有逻辑删除字段的情况下，做UPDATE操作
     */
    @Test
    public void deleteByColumn() {
        int i = mapper.deleteByColumn(TUserColumns.username, Arrays.asList("username0", "tom"));
        print("del --> " + i);
        i = mapper.deleteByColumn(TUserColumns.username, "jim");
        print("del --> " + i);
    }

    @Test
    public void saveUnique() {
        List<TUser> users = new ArrayList<>();
        Date date = new Date();
        for (int i = 0; i < 3; i++) { // 创建3个重复对象
            TUser user = new TUser();
            user.setUsername(TUserColumns.username + 1);
            user.setMoney(new BigDecimal(1));
            user.setRemark("remark" + 1);
            user.setState((byte)0);
            user.setIsdel(0);
            user.setAddTime(date);
            user.setLeftMoney(200F);
            users.add(user);
        }
        mapper.saveUnique(users);
    }

    @Test
    public void saveUnique2() {
        List<TUser> users = new ArrayList<>();
        Date date = new Date();
        for (int i = 0; i < 3; i++) { // 创建3个重复对象
            TUser user = new TUser();
            user.setUsername(TUserColumns.username + (i % 2));
            user.setMoney(new BigDecimal(1));
            user.setRemark("remark" + 1);
            user.setState((byte)0);
            user.setIsdel(0);
            user.setAddTime(date);
            user.setLeftMoney(200F);
            users.add(user);
        }
        mapper.saveUnique(users, Comparator.comparing(TUser::getUsername));
    }

    @Test
    public void listMap() {
        Query query = new Query();
        // 添加查询条件
        query.eq("username", "张三")
                .orderby("add_time", Sort.DESC);
        List<String> columns = Arrays.asList(
                "username as name",
                "add_time as createTime",
                "add_time as addTime",
                "money"
        );
        List<Map<String, Object>> listMap = mapper.listMap(columns, query);
        for (Map<String, Object> map : listMap) {
            System.out.println(JSON.toJSONString(map));
        }
    }

}
