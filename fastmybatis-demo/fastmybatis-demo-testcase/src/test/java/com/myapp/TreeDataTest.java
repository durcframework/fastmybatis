package com.myapp;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.gitee.fastmybatis.core.query.Query;
import com.gitee.fastmybatis.core.support.TreeNode;
import com.myapp.dao.MenuMapper;
import com.myapp.entity.Menu;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

/**
 * 演示查询表数据并返回树状节点
 * @author thc
 */
public class TreeDataTest extends FastmybatisSpringbootApplicationTests {

    @Resource
    private MenuMapper mapper;

    /**
     * 查询转成tree菜单
     */
    @Test
    public void listTreeData() {
        List<Menu> treeData = mapper.listTreeData(new Query().eq("id", 1).orEq("parent_id", 1), 0);
        System.out.println(JSON.toJSONString(treeData, SerializerFeature.PrettyFormat));
    }

    /**
     * 查询转成tree菜单，自定义返回字段
     */
    @Test
    public void listTreeData2() {
        List<MenuVO> treeData = mapper.listTreeData(new Query(), 0, menu -> {
            MenuVO menuVO = new MenuVO();
            menuVO.setValue(menu.getId());
            menuVO.setLabel(menu.getName());
            menuVO.setPid(menu.getParentId());
            return menuVO;
        });
        System.out.println(JSON.toJSONString(treeData, SerializerFeature.PrettyFormat));
    }

    /**
     * 查询转成tree菜单，自定义返回字段, 通过序列化方式
     */
    @Test
    public void listTreeData3() {
        List<MenuDTO> treeData = mapper.listTreeData(new Query(), 0, MenuDTO::new);
        System.out.println(JSON.toJSONString(treeData, SerializerFeature.PrettyFormat));
    }


    public static class MenuVO implements TreeNode<MenuVO, Integer> {
        private Integer value;
        private String label;
        private Integer pid;
        private List<MenuVO> items;

        @Override
        public Integer takeId() {
            return value;
        }

        @Override
        public Integer takeParentId() {
            return pid;
        }

        @Override
        public void setChildren(List<MenuVO> children) {
            this.items = children;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Integer getPid() {
            return pid;
        }

        public void setPid(Integer pid) {
            this.pid = pid;
        }

        public List<MenuVO> getItems() {
            return items;
        }

        public void setItems(List<MenuVO> items) {
            this.items = items;
        }
    }

    public static class MenuDTO implements TreeNode<MenuDTO, Integer> {

        @JSONField(name = "value")
        private Integer id;
        @JSONField(name = "label")
        private String name;
        @JSONField(name = "pid")
        private Integer parentId;
        private List<MenuDTO> children;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getParentId() {
            return parentId;
        }

        public void setParentId(Integer parentId) {
            this.parentId = parentId;
        }

        public List<MenuDTO> getChildren() {
            return children;
        }

        @Override
        public Integer takeId() {
            return getId();
        }

        @Override
        public Integer takeParentId() {
            return getParentId();
        }

        @Override
        public void setChildren(List<MenuDTO> children) {
            this.children = children;
        }
    }


}
