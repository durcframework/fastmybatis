package com.myapp;

import com.myapp.dao.UserHobbyMapper;
import com.myapp.entity.Hobby;
import com.myapp.entity.UserHobby;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * 演示字段转换
 * @author thc
 */
public class UserHobbyTest extends FastmybatisSpringbootApplicationTests {

    @Resource
    private UserHobbyMapper userHobbyMapper;

    @Test
    public void save() {
        UserHobby userHobby = new UserHobby();
        // 自定义类型
        userHobby.setHobby(new Hobby(1, "旅游"));
        userHobbyMapper.save(userHobby);


        userHobby = userHobbyMapper.getById(userHobby.getId());
        Hobby hobby = userHobby.getHobby();
        System.out.println(hobby);
    }


}
