package com.myapp;

import com.gitee.fastmybatis.core.support.CommonService;
import com.myapp.entity.TUser;
import com.myapp.service.UserService;
import com.myapp.service.UserService2;
import com.myapp.service.UserService3;
import com.myapp.service.UserService4;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * @author thc
 */
public class UserServiceTest extends FastmybatisSpringbootApplicationTests{

    @Autowired
    private UserService userService;

    @Autowired
    private UserService2 userService2;

    @Autowired
    private UserService3 userService3;

    @Autowired
    private UserService4 userService4;

    @Test
    public void test() {

        List<CommonService<TUser, Integer, ?>> commonServices = Arrays.asList(userService, userService2, userService3, userService4);
        for (CommonService<TUser, Integer, ?> commonService : commonServices) {
            System.out.println(commonService);
            TUser user = commonService.getById(6);
            Assert.assertNotNull(user);

            TUser user1 = commonService.getById(6);
            Assert.assertNotNull(user1);

            int i = saveOrUpdate(commonService);
            Assert.assertEquals(1, i);
        }

    }

    @Test
    public void async() throws InterruptedException {
        userService.doXX();
        userService2.doXX();
        System.out.println("test done");
        Thread.sleep(3000);
    }

    public int saveOrUpdate(CommonService<TUser, Integer, ?> commonService) {
        TUser user = new TUser();
        user.setId(201);
        user.setIsdel(0);
        user.setLeftMoney(22.1F);
        user.setMoney(new BigDecimal("100.5"));
        user.setRemark("备注");
        user.setState((byte)0);
        user.setUsername("张三2");
        return commonService.saveOrUpdate(user);
    }
}
