package com.myapp;

import java.time.LocalDateTime;

public class UserVO {
    private Integer id;

    /**
     * 用户名, 数据库字段：username
     */
    private String username;

    private String stateName;

    private LocalDateTime addTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocalDateTime getAddTime() {
        return addTime;
    }

    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    @Override
    public String toString() {
        return "UserVO{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", stateName='" + stateName + '\'' +
                ", addTime=" + addTime +
                '}';
    }
}