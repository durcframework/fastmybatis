package com.myapp.dao;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.myapp.entity.TUser;

public interface TUserMapper extends CrudMapper<TUser, Integer> {


}
