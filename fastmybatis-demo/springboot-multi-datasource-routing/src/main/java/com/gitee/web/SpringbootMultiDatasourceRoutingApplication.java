package com.gitee.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMultiDatasourceRoutingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMultiDatasourceRoutingApplication.class, args);
    }

}
