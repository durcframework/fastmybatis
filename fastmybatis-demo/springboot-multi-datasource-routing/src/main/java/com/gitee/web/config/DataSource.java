package com.gitee.web.config;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface DataSource {

    /**
     * 数据库名称
     * @return 数据库名称
     */
    String value() default DbName.DATASOURCE_STU;
 
}