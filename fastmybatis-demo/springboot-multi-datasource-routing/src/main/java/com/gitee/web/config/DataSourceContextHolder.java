package com.gitee.web.config;

public class DataSourceContextHolder {

	private static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();


	/**
	 * 获取数据源名称
	 * @param dataSourceName  数据库类型
	 */
	public static void setDataSourceName(String dataSourceName) {
		contextHolder.set(dataSourceName);
	}
	
	/**
	 * 获取数据源名称
	 * @return String
	 */
	public static String getDataSourceName() {
		return contextHolder.get();
	}
	
	/**
	 * 获取数据源名称
	 */
	public static void clearDataSourceName() {
		contextHolder.remove();
	}
}