package com.gitee.web.config;

import com.gitee.fastmybatis.core.FastmybatisConfig;
import com.gitee.fastmybatis.core.ext.SqlSessionFactoryBeanExt;
import com.gitee.fastmybatis.core.support.DateFillInsert;
import com.gitee.fastmybatis.core.support.DateFillUpdate;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.Assert;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author thc
 */
@Configuration
@MapperScan(basePackages = { DbConfig.basePackage }, sqlSessionFactoryRef = DbConfig.sqlSessionFactoryName)
public class DbConfig {

    /* ********************只需要改这里的配置******************** */
    final String DATASOURCE_DYN = "datasource_dyn";
    /** 数据库名称 */
    static final String dbName = "dyn";
    /** 存放mapper包路径 */
    public static final String basePackage = "com.gitee.web";
    /** mybatis的config文件路径 */
    public static final String mybatisConfigLocation = "classpath:mybatis/mybatisConfig.xml";
    /** mybatis的mapper文件路径 */
    public static final String mybatisMapperLocations = "classpath:mybatis/mapper/*.xml";
    /** 表新增时间字段名 */
    public static final String dbInsertDateColumnName = "gmt_create";
    /** 表更新时间字段名 */
    public static final String dbUpdateDateColumnName = "gmt_modified";
    /* **************************************************** */

    /** sqlSessionTemplate名称 */
    public static final String sqlSessionTemplateName = "sqlSessionTemplate" + dbName;
    /** sqlSessionFactory名称 */
    public static final String sqlSessionFactoryName = "sqlSessionFactory" + dbName;
    /** transactionManager名称 */
    public static final String transactionManagerName = "transactionManager" + dbName;
    /** transactionTemplate名称 */
    public static final String transactionTemplateName = "transactionTemplate" + dbName;

    // 主数据源配置
    @Value("${spring.datasource.stu.driver-class-name}")
    private String stuDriver;
    @Value("${spring.datasource.stu.url}")
    private String stuUrl;
    @Value("${spring.datasource.stu.username}")
    private String stuUsername;
    @Value("${spring.datasource.stu.password}")
    private String stuPassword;

    // 第二数据源配置
    @Value("${spring.datasource.stu2.driver-class-name}")
    private String stu2Driver;
    @Value("${spring.datasource.stu2.url}")
    private String stu2Url;
    @Value("${spring.datasource.stu2.username}")
    private String stu2Username;
    @Value("${spring.datasource.stu2.password}")
    private String stu2Password;


    // 动态数据源
    @Bean(name = DATASOURCE_DYN)
    public DataSource dynamicDataSource() {
        DataSource dataSource = dataSource();
        DataSource dataSource2 = dataSource2();
        DynamicDataSource dynamicDataSource = new DynamicDataSource();
        dynamicDataSource.setDefaultTargetDataSource(dataSource);
        Map<Object, Object> targetDataSources = new HashMap<>(8);
        targetDataSources.put(DbName.DATASOURCE_STU, dataSource);
        targetDataSources.put(DbName.DATASOURCE_STU2, dataSource2);
        dynamicDataSource.setTargetDataSources(targetDataSources);
        return dynamicDataSource;
    }


    public DataSource dataSource() {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName(stuDriver);
        driverManagerDataSource.setUrl(stuUrl);
        driverManagerDataSource.setUsername(stuUsername);
        driverManagerDataSource.setPassword(stuPassword);
        return driverManagerDataSource;
    }

    public DataSource dataSource2() {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName(stu2Driver);
        driverManagerDataSource.setUrl(stu2Url);
        driverManagerDataSource.setUsername(stu2Username);
        driverManagerDataSource.setPassword(stu2Password);
        return driverManagerDataSource;
    }

    public FastmybatisConfig fastmybatisConfig() {
        FastmybatisConfig config = new FastmybatisConfig();
        /*
         * 驼峰转下划线形式，默认是true 开启后java字段映射成数据库字段将自动转成下划线形式 如：userAge -> user_age
         * 如果数据库设计完全遵循下划线形式，可以启用 这样可以省略Entity中的注解，@Table，@Column都可以不用，只留
         *
         * @Id
         *
         * @GeneratedValue 参见：UserInfo.java
         */
        config.setCamel2underline(true);
        config.setFills(Arrays.asList(new DateFillInsert(dbInsertDateColumnName),
                new DateFillUpdate(dbUpdateDateColumnName)));

        return config;
    }

    @Bean(name = sqlSessionFactoryName)
    public SqlSessionFactory sqlSessionFactory(@Autowired @Qualifier(DATASOURCE_DYN) DataSource dataSource, ApplicationContext applicationContext) throws Exception {
        Assert.notNull(dataSource, "dataSource can not be null.");
        SqlSessionFactoryBeanExt bean = new SqlSessionFactoryBeanExt();

        bean.setDataSource(dataSource);
        bean.setConfigLocation(this.getResource(mybatisConfigLocation));
        bean.setMapperLocations(this.getResources(mybatisMapperLocations));

        // ====以下是附加属性====

        // dao所在的包名,跟MapperScannerConfigurer的basePackage一致,多个用;隔开
        bean.setBasePackage(basePackage);
        bean.setConfig(fastmybatisConfig());
        bean.setApplicationContext(applicationContext);
        return bean.getObject();

    }

    @Bean(name = sqlSessionTemplateName)
    public SqlSessionTemplate sqlSessionTemplate(
            @Autowired @Qualifier(sqlSessionFactoryName) SqlSessionFactory sessionFactory) throws Exception {
        return new SqlSessionTemplate(sessionFactory);
    }

    @Bean(name = transactionManagerName)
    public PlatformTransactionManager annotationDrivenTransactionManager(
            @Autowired @Qualifier(DATASOURCE_DYN) DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean(name = transactionTemplateName)
    public TransactionTemplate transactionTemplate(@Autowired @Qualifier(transactionManagerName)PlatformTransactionManager transactionManager) {
        return new TransactionTemplate(transactionManager);
    }


    private Resource[] getResources(String path) throws IOException {
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        return resolver.getResources(path);
    }

    private Resource getResource(String path) {
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        return resolver.getResource(path);
    }



}
