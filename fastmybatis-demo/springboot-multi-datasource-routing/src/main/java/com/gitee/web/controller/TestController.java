package com.gitee.web.controller;

import com.gitee.web.entity.Goods;
import com.gitee.web.entity.TUser;
import com.gitee.web.service.GoodsService;
import com.gitee.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author thc
 */
@RestController
public class TestController {
    @Autowired
    UserService userService;

    @Autowired
    GoodsService goodsService;

    @GetMapping("test")
    public String test() {
        TUser tUser = userService.getById(6);
        Assert.notNull(tUser, "tUser can not null");

        tUser = userService.getMapper().selectByName("张三");
        Assert.notNull(tUser, "select tUser can not null");

        Goods goods = goodsService.getById(1);
        Assert.notNull(goods, "goods can not null");

        goods = goodsService.getMapper().selectByName("iPhoneX");
        Assert.notNull(goods, "select goods can not null");

        return "success";
    }

}
