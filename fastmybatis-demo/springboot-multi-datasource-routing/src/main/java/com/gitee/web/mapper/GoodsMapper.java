package com.gitee.web.mapper;

import com.gitee.fastmybatis.core.mapper.CrudMapper;

import com.gitee.web.entity.Goods;
import org.apache.ibatis.annotations.Param;


/**
 * @author tanghc
 */
public interface GoodsMapper extends CrudMapper<Goods, Integer> {
    Goods selectByName(@Param("goodsName") String goodsName);
}
