package com.gitee.web.service;

import com.gitee.fastmybatis.core.support.BaseService;
import com.gitee.web.config.DataSource;
import com.gitee.web.config.DbName;
import com.gitee.web.entity.TUser;
import com.gitee.web.mapper.TUserMapper;
import org.springframework.stereotype.Service;

/**
 * @author thc
 */
@Service
// 指定数据源
@DataSource(DbName.DATASOURCE_STU)
public class UserService extends BaseService<TUser, Integer, TUserMapper> {
}
