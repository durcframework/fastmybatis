package com.gitee.web.service;

import com.gitee.web.SpringbootMultiDatasourceRoutingApplicationTests;
import com.gitee.web.config.DataSourceContextHolder;
import com.gitee.web.config.DbName;
import com.gitee.web.entity.Goods;
import com.gitee.web.entity.TUser;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

/**
 * @author tanghc
 */
public class ServiceTest extends SpringbootMultiDatasourceRoutingApplicationTests {

    @Autowired
    UserService userService;

    @Autowired
    GoodsService goodsService;

    @Test
    public void get() {
        DataSourceContextHolder.setDataSourceName(DbName.DATASOURCE_STU);

        TUser tUser = userService.getById(6);
        Assert.notNull(tUser, "tUser can not null");

        tUser = userService.getMapper().selectByName("张三");
        Assert.notNull(tUser, "select tUser can not null");

        // 切换到数据2
        DataSourceContextHolder.setDataSourceName(DbName.DATASOURCE_STU2);

        Goods goods = goodsService.getById(1);
        Assert.notNull(goods, "goods can not null");

        goods = goodsService.getMapper().selectByName("iPhoneX");
        Assert.notNull(goods, "select goods can not null");
    }

}
