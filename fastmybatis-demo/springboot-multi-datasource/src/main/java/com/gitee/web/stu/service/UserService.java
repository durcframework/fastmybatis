package com.gitee.web.stu.service;

import com.gitee.fastmybatis.core.support.BaseService;
import com.gitee.web.stu.entity.TUser;
import com.gitee.web.stu.mapper.TUserMapper;
import org.springframework.stereotype.Service;

/**
 * @author thc
 */
@Service
public class UserService extends BaseService<TUser, Integer, TUserMapper> {
}
