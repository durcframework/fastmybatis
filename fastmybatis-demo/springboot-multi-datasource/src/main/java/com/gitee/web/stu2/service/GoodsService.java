package com.gitee.web.stu2.service;

import com.gitee.fastmybatis.core.support.BaseService;
import com.gitee.web.stu2.entity.Goods;
import com.gitee.web.stu2.mapper.GoodsMapper;
import org.springframework.stereotype.Service;

/**
 * @author thc
 */
@Service
public class GoodsService extends BaseService<Goods, Integer, GoodsMapper> {
}
