package com.gitee.web;

import com.gitee.web.stu.entity.TUser;
import com.gitee.web.stu.service.UserService;
import com.gitee.web.stu2.entity.Goods;
import com.gitee.web.stu2.service.GoodsService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

/**
 * @author tanghc
 */
public class DbTest extends SpringbootMultiDatasourceApplicationTests {

    @Autowired
    UserService userService;

    @Autowired
    GoodsService goodsService;

    @Test
    public void get() {
        TUser tUser = userService.getById(6);
        Assert.notNull(tUser, "tUser can not null");

        Goods goods = goodsService.getById(1);
        Assert.notNull(goods, "goods can not null");

        tUser = userService.getMapper().selectByName("张三");
        Assert.notNull(tUser, "select tUser can not null");

        goods = goodsService.getMapper().selectByName("iPhoneX");
        Assert.notNull(goods, "select goods can not null");
    }

}
