package com.myapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TenantDemoTableApplication {

    public static void main(String[] args) {
        SpringApplication.run(TenantDemoTableApplication.class, args);
    }
}
