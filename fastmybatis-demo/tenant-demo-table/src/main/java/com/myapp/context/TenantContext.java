package com.myapp.context;

public class TenantContext {

    private static ThreadLocal<String> tenantLocal = new ThreadLocal<>();

    public static void setTenantId(String tenantId) {
        tenantLocal.set(tenantId);
    }

    public static String getTenantId() {
        return tenantLocal.get();
    }

    public static void remove() {
        tenantLocal.remove();
    }
}
