package com.myapp.entity;

import com.gitee.fastmybatis.annotation.Table;

import java.util.Date;


/**
 * 表名：user_log_xxx
 * $tenantId$ 占位符
 * @author tanghc
 */
@Table(name = "user_log_$tenantId$")
public class UserLog {
    /** 注释, 数据库字段：id */
    private Long id;

    /** 注释, 数据库字段：user_id */
    private Integer userId;

    /** 注释, 数据库字段：log */
    private String log;

    /** 注释, 数据库字段：log_date */
    private Date logDate;

    /** 注释, 数据库字段：is_deleted */
    @com.gitee.fastmybatis.core.annotation.LogicDelete
    private Byte isDeleted;

    /** 设置注释, 数据库字段：user_log0.id */
    public void setId(Long id) {
        this.id = id;
    }

    /** 获取注释, 数据库字段：user_log0.id */
    public Long getId() {
        return this.id;
    }

    /** 设置注释, 数据库字段：user_log0.user_id */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /** 获取注释, 数据库字段：user_log0.user_id */
    public Integer getUserId() {
        return this.userId;
    }


    /** 设置注释, 数据库字段：user_log0.log */
    public void setLog(String log) {
        this.log = log;
    }

    /** 获取注释, 数据库字段：user_log0.log */
    public String getLog() {
        return this.log;
    }

    /** 设置注释, 数据库字段：user_log0.log_date */
    public void setLogDate(Date logDate) {
        this.logDate = logDate;
    }

    /** 获取注释, 数据库字段：user_log0.log_date */
    public Date getLogDate() {
        return this.logDate;
    }

    /** 设置注释, 数据库字段：user_log0.is_deleted */
    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    /** 获取注释, 数据库字段：user_log0.is_deleted */
    public Byte getIsDeleted() {
        return this.isDeleted;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((id == null) ? 0 : id.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        UserLog other = (UserLog) obj;

        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("UserLog0 [");
        sb.append("id=").append(id);
        sb.append(", ");
        sb.append("userId=").append(userId);
        sb.append(", ");
        sb.append("log=").append(log);
        sb.append(", ");
        sb.append("logDate=").append(logDate);
        sb.append(", ");
        sb.append("isDeleted=").append(isDeleted);
        sb.append("]");

        return sb.toString();
    }
}
