package com.myapp.service;

import com.gitee.fastmybatis.core.support.IService;
import com.myapp.entity.UserLog;
import org.springframework.stereotype.Service;

/**
 * @author thc
 */
@Service
public class UserLogService implements IService<UserLog, Long> {
}
