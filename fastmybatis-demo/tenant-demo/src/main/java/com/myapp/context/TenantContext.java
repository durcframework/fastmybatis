package com.myapp.context;

import java.util.function.Supplier;

public class TenantContext {

    private static Supplier<Object> defaultBuilder = () -> null;

    private static ThreadLocal<Object> tenantLocal = new ThreadLocal<>();

    public static void setTenantId(String tenantId) {
        tenantLocal.set(tenantId);
    }

    public static Object getTenantId() {
        Object tenantId = tenantLocal.get();
        if (tenantId == null) {
            tenantId = defaultBuilder.get();
        }
        return tenantId;
    }

    public static void remove() {
        tenantLocal.remove();
    }

    public static void setDefaultBuilder(Supplier<Object> defaultBuilder) {
        TenantContext.defaultBuilder = defaultBuilder;
    }
}
