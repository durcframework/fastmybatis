package com.myapp.dao;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.myapp.entity.TenantDemo;

/**
 * @author xxx
 */
public interface TenantDemoMapper extends CrudMapper<TenantDemo, Integer> {
	
}