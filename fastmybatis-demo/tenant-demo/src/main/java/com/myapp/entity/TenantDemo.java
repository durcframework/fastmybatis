package com.myapp.entity;


import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.PkStrategy;
import com.gitee.fastmybatis.annotation.Table;


/**
 * 表名：tenant_demo
 * 备注：多租户表演示
 *
 * @author tanghc
 */
@Table(name = "tenant_demo", pk = @Pk(name = "id", strategy = PkStrategy.INCREMENT))
public class TenantDemo {

    /**  数据库字段：id */
    private Integer id;

    /** 租户id, 数据库字段：tenant_id */
    private String tenantId;

    /** 名称, 数据库字段：name */
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}