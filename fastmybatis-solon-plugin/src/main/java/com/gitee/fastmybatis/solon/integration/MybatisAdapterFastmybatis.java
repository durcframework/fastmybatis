package com.gitee.fastmybatis.solon.integration;

import com.gitee.fastmybatis.core.FastmybatisConfig;
import com.gitee.fastmybatis.core.FastmybatisContext;
import com.gitee.fastmybatis.core.ext.MapperLocationsBuilder;
import com.gitee.fastmybatis.core.ext.MyBatisResource;
import com.gitee.fastmybatis.core.mapper.Mapper;
import com.gitee.fastmybatis.core.util.DbUtil;
import org.apache.ibatis.builder.xml.XMLMapperBuilder;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.solon.integration.MybatisAdapterDefault;
import org.noear.solon.Utils;
import org.noear.solon.core.BeanWrap;
import org.noear.solon.core.Props;
import org.noear.solon.core.VarHolder;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

/**
 * 适配器 for fastmybatis
 *
 * @author thc
 */
public class MybatisAdapterFastmybatis extends MybatisAdapterDefault {
    protected FastmybatisConfig globalConfig;
    protected MapperLocationsBuilder mapperLocationsBuilder;
    protected List<String> mapperResources;

    /**
     * 构建Sql工厂适配器，使用默认的 typeAliases 和 mappers 配置
     */
    protected MybatisAdapterFastmybatis(BeanWrap dsWrap) {
        super(dsWrap);
    }

    /**
     * 构建Sql工厂适配器，使用属性配置
     */
    protected MybatisAdapterFastmybatis(BeanWrap dsWrap, Props dsProps) {
        super(dsWrap, dsProps);
    }

    @Override
    protected void initConfiguration(Environment environment) {
        FastmybatisContext.setApplicationContext(dsWrap.context());
        //for configuration section
        config = new Configuration(environment);
        Props cfgProps = dsProps.getProp("configuration");
        if (cfgProps.size() > 0) {
            Utils.injectProperties(config, cfgProps);
        }

        //for globalConfig section
        this.globalConfig = new FastmybatisConfig();
        Props globalProps = dsProps.getProp("globalConfig");
        if (globalProps.size() > 0) {
            //尝试配置注入
            Utils.injectProperties(globalConfig, globalProps);
        }

        this.mapperLocationsBuilder = new MapperLocationsBuilder(globalConfig);
        this.mapperResources = new ArrayList<>(8);
    }

    @Override
    protected boolean isMapper(Class<?> clz) {
        return Mapper.class.isAssignableFrom(clz) || super.isMapper(clz);
    }

    @Override
    protected void addMapperByXml(String uri) {
        this.mapperResources.add(uri);
    }

    public FastmybatisConfig getGlobalConfig() {
        return globalConfig;
    }

    /**
     * 获取会话工厂
     */
    @Override
    public SqlSessionFactory getFactory() {
        if (factory == null) {
            try {
                this.initXml();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            factory = factoryBuilder.build(getConfiguration());
        }
        return factory;
    }

    protected void initXml() throws IOException {
        List<MyBatisResource> myBatisResources = new ArrayList<>(16);
        if (mapperResources != null) {
            for (String mapperResource : mapperResources) {
                MyBatisResource myBatisResource = MyBatisResource.buildFromClasspath(mapperResource);
                myBatisResources.add(myBatisResource);
            }
        }
        Objects.requireNonNull(globalConfig);

        DataSource dataSource = this.getDataSource();
        String dialect = DbUtil.getDialect(dataSource);
        Collection<Class<?>> mapperClasses = config.getMapperRegistry().getMappers();
        MyBatisResource[] allMybatisMapperResources = mapperLocationsBuilder.build(new HashSet<>(mapperClasses), myBatisResources, dialect);
        for (MyBatisResource myBatisResource : allMybatisMapperResources) {
            try (InputStream inputStream = myBatisResource.getInputStream()) {
                String resource = myBatisResource.getFilepath();
                if (resource == null) {
                    resource = myBatisResource.getFilename();
                }
                XMLMapperBuilder mapperParser = new XMLMapperBuilder(inputStream, config, resource, config.getSqlFragments());
                mapperParser.parse();
            }
        }
    }

    @Override
    public void injectTo(VarHolder varH) {
        super.injectTo(varH);

        //@Db("db1") SqlSessionFactory factory;
        if (FastmybatisConfig.class.isAssignableFrom(varH.getType())) {
            varH.setValue(this.getGlobalConfig());
            return;
        }
    }
}
