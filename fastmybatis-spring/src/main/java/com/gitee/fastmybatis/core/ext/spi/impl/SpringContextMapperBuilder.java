package com.gitee.fastmybatis.core.ext.spi.impl;

import com.gitee.fastmybatis.core.ext.MapperRunner;
import com.gitee.fastmybatis.core.ext.code.util.FieldUtil;
import com.gitee.fastmybatis.core.ext.spi.MapperBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author thc
 */
public class SpringContextMapperBuilder implements MapperBuilder {

    private static final Logger log = LoggerFactory.getLogger(SpringContextMapperBuilder.class);

    private static final Map<Class<?>, MapperRunner<?>> CACHE = new ConcurrentHashMap<>(64);


    @Override
    public <T> MapperRunner<T> getMapperRunner(Class<T> mapperClass, Object applicationContext) {
        return (MapperRunner<T>) CACHE.computeIfAbsent(mapperClass, k -> {
            ApplicationContext ctx = (ApplicationContext) applicationContext;
            T mapper;
            try {
                mapper = ctx.getBean(mapperClass);
            } catch (Exception e) {
                // 修复使用spring-boot-devtools导致获取不到实例
                String simpleName = mapperClass.getSimpleName();
                String beanName = FieldUtil.lowerFirstLetter(simpleName);
                log.debug("ApplicationContext.getBean(" + simpleName + ".class) can not found instance, try to find use ctx.getBean(\"" + beanName + "\")");
                mapper = (T) ctx.getBean(beanName);
            }
            return new MapperRunner<>(mapper, null);
        });


    }
}
